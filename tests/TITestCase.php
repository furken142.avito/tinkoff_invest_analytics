<?php

declare(strict_types=1);

namespace Tests;

use App\Broker\Models\BrokerAccountModel;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Models\BrokerShareModel;
use App\Broker\Types\BrokerAccountStatusType;
use App\Broker\Types\BrokerAccountTypeType;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Entities\MarketSectorEntity;
use App\Intl\Entities\CountryEntity;
use App\Intl\Entities\CurrencyEntity;
use App\Models\OperationModel;
use jamesRUS52\TinkoffInvest\TIInstrument;
use PHPUnit\Framework\TestCase;

class TITestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function createBrokerAccount(array $data): BrokerAccountModel
    {
        $defaultData = [
            'id' => 'account1',
            'type' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
            'status' => BrokerAccountStatusType::ACCOUNT_STATUS_OPEN,
            'name' => 'Primary Account',
            'openedDate' => new \DateTime('2022-01-02 00:15:17'),
            'closedDate' => null
        ];
        $data = array_merge($defaultData, $data);

        return new BrokerAccountModel($data);
    }

    protected function createBrokerPortfolioPosition(array $data): BrokerPortfolioPositionModel
    {
        $averagePositionPrice = new BrokerMoneyModel([
            'amount' => $data['averagePositionPrice']['amount'] ?? 70,
            'currency' => $data['averagePositionPrice']['currency'] ?? 'RUB'
        ]);

        $expectedYield = new BrokerMoneyModel([
            'amount' => $data['expectedYield']['amount'] ?? 10,
            'currency' => $data['expectedYield']['currency'] ?? 'RUB'
        ]);

        $currentNkd = new BrokerMoneyModel([
            'amount' => $data['currentNkd']['amount'] ?? 75,
            'currency' => $data['currentNkd']['currency'] ?? 'RUB'
        ]);

        $currentPrice = new BrokerMoneyModel([
            'amount' => $data['currentPrice']['amount'] ?? 60,
            'currency' => $data['currentPrice']['currency'] ?? 'RUB'
        ]);

        $portfolioPosition = new BrokerPortfolioPositionModel();
        $portfolioPosition->setFigi($data['figi'] ?? 'figi');
        $portfolioPosition->setInstrumentType($data['instrumentType'] ?? 'stock');
        $portfolioPosition->setQuantity($data['quantity'] ?? 13);
        $portfolioPosition->setAveragePositionPrice($averagePositionPrice);
        $portfolioPosition->setExpectedYield($expectedYield);
        $portfolioPosition->setCurrentNkd($currentNkd);
        $portfolioPosition->setCurrentPrice($currentPrice);

        return $portfolioPosition;
    }

    protected function createOperation(array $data): OperationModel
    {
        $defaultData = [
            'id' => '1',
            'parentOperationId' => '2',
            'status' => 'Done',
            'currency' => 'RUB',
            'payment' => new BrokerMoneyModel(['amount' => 100, 'currency' => 'RUB']),
            'price' => new BrokerMoneyModel(['amount' => 5.15, 'currency' => 'RUB']),
            'quantity' => 20,
            'figi' => 'LKOH',
            'type' => 'Stock',
            'isMarginCall' => false,
            'date' => new \DateTime('2021-04-01 18:00:13'),
            'operationType' => 'Buy',
            'instrument' => null
        ];

        $data = array_merge($defaultData, $data);

        return new OperationModel($data);
    }

    public function createBrokerOperation(array $data): BrokerOperationModel
    {
        $defaultData = [
            'id' => '1',
            'parentOperationId' => '2',
            'status' => 'Done',
            'currency' => 'RUB',
            'payment' => new BrokerMoneyModel(['amount' => 100, 'currency' => 'RUB']),
            'price' => new BrokerMoneyModel(['amount' => 5.15, 'currency' => 'RUB']),
            'quantity' => 20,
            'figi' => 'LKOH',
            'type' => 'Stock',
            'isMarginCall' => false,
            'date' => new \DateTime('2021-04-01 18:00:13'),
            'operationType' => 'Buy'
        ];

        $data = array_merge($defaultData, $data);

        return new BrokerOperationModel($data);
    }

    public function createMarketInstrument(array $data): TIInstrument
    {
        $defaultData = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond'
        ];

        $data = array_merge($defaultData, $data);

        return new TIInstrument(
            $data['figi'],
            $data['ticker'],
            $data['isin'],
            $data['minPriceIncrement'],
            $data['lot'],
            $data['currency'],
            $data['name'],
            $data['type']
        );
    }

    public function createBrokerShare(array $data): BrokerShareModel
    {
        $defaultData = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond',
            'country' => 'RU',
            'sector' => 'Consumer Cyclical',
            'industry' => 'Footwear & Accessories'
        ];

        $data = array_merge($defaultData, $data);

        $instrument = new BrokerInstrumentModel([
            'figi' => $data['figi'],
            'ticker' => $data['ticker'],
            'isin' => $data['isin'],
            'minPriceIncrement' => $data['minPriceIncrement'],
            'lot' => $data['lot'],
            'currency' => $data['currency'],
            'name' => $data['name'],
            'type' => $data['type']
        ]);

        return new BrokerShareModel([
            'instrument' => $instrument,
            'country' => $data['country'],
            'sector' => $data['sector'],
            'industry' => $data['industry'],
        ]);
    }

    public function createBrokerInstrument(array $data): BrokerInstrumentModel
    {
        $defaultData = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond'
        ];

        $data = array_merge($defaultData, $data);

        return new BrokerInstrumentModel($data);
    }

    public function createBrokerCandle(array $data): BrokerCandleModel
    {
        $defaultData = [
            'open' => 10,
            'close' => 15,
            'high' => 18,
            'low' => 8,
            'volume' => 1000,
            'time' => new \DateTime('2022-01-01 00:00:00'),
            'interval' => BrokerCandleIntervalType::INTERVAL_DAY,
            'isComplete' => true
        ];

        $data = array_merge($defaultData, $data);

        return new BrokerCandleModel($data);
    }

    public function createCountryEntity(array $data = []): CountryEntity
    {
        $country = new CountryEntity();
        $country->setIso($data['iso'] ?? 'RU');
        $country->setName($data['name'] ?? 'Russian Federation');
        $country->setContinent($data['continent'] ?? 'Europe');

        return $country;
    }

    public function createCurrencyEntity(array $data = []): CurrencyEntity
    {
        $currency = new CurrencyEntity();
        $currency->setIso($data['iso'] ?? 'RUB');
        $currency->setName($data['name'] ?? 'Российский рубль');
        $currency->setSymbol($data['symbol'] ?? '₽');
        $currency->setId($data['id'] ?? 1);

        return $currency;
    }

    public function createMarketSectorEntity(array $data = []): MarketSectorEntity
    {
        $sector = new MarketSectorEntity();
        $sector->setId($data['id'] ?? 1);
        $sector->setName($data['name'] ?? 'Utilities');

        return $sector;
    }
}
