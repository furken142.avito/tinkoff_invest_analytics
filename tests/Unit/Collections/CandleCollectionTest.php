<?php

declare(strict_types=1);

namespace Tests\Unit\Collections;

use App\Broker\Models\BrokerCandleModel;
use App\Collections\CandleCollection;
use jamesRUS52\TinkoffInvest\TICandle;
use Tests\TITestCase;

class CandleCollectionTest extends TITestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @dataProvider dataGetLastCandle
     * @param BrokerCandleModel|null $expected
     * @param TICandle[] $candles
     * @return void
     */
    public function testGetLastCandle(?BrokerCandleModel $expected, array $candles)
    {
        $candleCollection = new CandleCollection($candles);
        $this->assertSame($expected, $candleCollection->getLastCandle());
    }

    public function dataGetLastCandle()
    {
        $candle01 = $this->createBrokerCandle([
            'time' => new \DateTime('2022-01-01 04:00:00'),
            'close' => 70
        ]);
        $candle02 = $this->createBrokerCandle([
            'time' => new \DateTime('2022-01-03 04:00:00'),
            'close' => 65
        ]);
        $candle03 = $this->createBrokerCandle([
            'time' => new \DateTime('2022-01-02 04:00:00'),
            'close' => 68
        ]);
        $candle04 = $this->createBrokerCandle([
            'time' => new \DateTime('2022-01-03 04:00:00'),
            'close' => 69
        ]);
        $candles01 = [$candle01, $candle02, $candle03, $candle04];
        $candles02 = [];


        return [
            'Common Case' => [
                $candle02,
                $candles01
            ],
            'Empty Collection Case' => [
                null,
                $candles02
            ]
        ];
    }
}
