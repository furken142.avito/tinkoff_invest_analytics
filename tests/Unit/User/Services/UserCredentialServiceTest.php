<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserCredentialEntity;
use App\User\Entities\UserEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Models\UserCredentialModel;
use App\User\Services\EncryptService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use App\User\Storages\UserCredentialStorage;
use Monolog\Test\TestCase;

class UserCredentialServiceTest extends TestCase
{
    private UserCredentialService $userCredentialService;
    private UserService $userServiceMock;
    private UserCredentialStorage $userCredentialStorageMock;
    private EncryptService $encryptService;

    public function setUp(): void
    {
        parent::setUp();

        $this->userServiceMock = $this->createMock(UserService::class);
        $this->userCredentialStorageMock = $this->createMock(UserCredentialStorage::class);
        $this->encryptService = $this->createMock(EncryptService::class);
        $this->userCredentialService = new UserCredentialService(
            $this->encryptService,
            $this->userCredentialStorageMock,
            $this->userServiceMock
        );
    }

    private function getDefaultUserId(): int
    {
        return 1;
    }

    /**
     * @return UserCredentialEntity[]
     */
    private function getDefaultCredentials(): array
    {
        $userId01 = 1;
        $userEntity01 = new UserEntity();
        $userEntity01->setId(1);

        $userId02 = 2;
        $userEntity02 = new UserEntity();
        $userEntity02->setId(2);

        $credential01 = new UserCredentialEntity();
        $credential01->setId(1);
        $credential01->setApiKey('AAAA-BBBB-CCCC');
        $credential01->setBrokerId('tinkoff');
        $credential01->setUser($userEntity01);
        $credential01->setActive(1);
        $credential01->setUserId($userId01);

        $credential02 = new UserCredentialEntity();
        $credential02->setId(2);
        $credential02->setApiKey('DDDD-EEEE-FFFF');
        $credential02->setBrokerId('bcs');
        $credential02->setUser($userEntity02);
        $credential02->setActive(0);
        $credential02->setUserId($userId02);

        return [$credential01, $credential02];
    }

    public function testGetCredentialList()
    {
        $userId01 = 1;
        $userEntity01 = new UserEntity();
        $userEntity01->setId(1);

        $userId02 = 2;
        $userEntity02 = new UserEntity();
        $userEntity02->setId(2);

        $credential01 = new UserCredentialEntity();
        $credential01->setId(1);
        $credential01->setApiKey('ENCRYPT1');
        $credential01->setBrokerId('tinkoff');
        $credential01->setUser($userEntity01);
        $credential01->setActive(1);
        $credential01->setUserId($userId01);

        $credential02 = new UserCredentialEntity();
        $credential02->setId(2);
        $credential02->setApiKey('ENCRYPT2');
        $credential02->setBrokerId('bcs');
        $credential02->setUser($userEntity02);
        $credential02->setActive(0);
        $credential02->setUserId($userId02);

        $credentialModel01 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'AAAA-BBBB',
            'broker' => 'tinkoff',
            'isActive' => 1,
            'userId' => $userId01
        ]);
        $credentialModel02 = new UserCredentialModel([
            'userCredentialId' => 2,
            'apiKey' => 'CCCC-DDDD',
            'broker' => 'bcs',
            'isActive' => 0,
            'userId' => $userId02
        ]);

        $userId01 = $this->getDefaultUserId();

        $expectedCollection = new UserCredentialCollection([$credentialModel01, $credentialModel02]);

        $this->userCredentialStorageMock->expects($this->once())->method('findByUserId')
            ->with($userId01)->willReturn([$credential01, $credential02]);
        $this->encryptService->expects($this->exactly(2))->method('decrypt')
            ->withConsecutive(['ENCRYPT1'], ['ENCRYPT2'])
            ->willReturnOnConsecutiveCalls('AAAA-BBBB', 'CCCC-DDDD');

        $this->assertEquals($expectedCollection, $this->userCredentialService->getCredentialList($userId01));
    }

    public function testGetUserActiveCredential()
    {
        $credentials = $this->getDefaultCredentials();
        $userId01 = $this->getDefaultUserId();
        $broker01 = 'tinkoff';

        $expected = $credentials[0];

        $this->userCredentialStorageMock->expects($this->once())->method('findActiveByUserIdAndBroker')
            ->with($userId01)->willReturn($credentials[0]);

        $this->assertEquals($expected, $this->userCredentialService->getUserActiveCredential($userId01, $broker01));
    }

    public function testGetActiveCredentialList()
    {
        $credentials01 = $this->getDefaultCredentials();
        $userId01 = $this->getDefaultUserId();
        $collection01 = new UserCredentialCollection($credentials01);

        $this->userCredentialStorageMock->expects($this->once())->method('findActiveByUserId')
            ->with($userId01)->willReturn($credentials01);

        $this->assertEquals($collection01, $this->userCredentialService->getActiveCredentialList($userId01));
    }

    public function testGetAllActiveCredentialList()
    {
        $credentials01 = $this->getDefaultCredentials();
        $collection01 = new UserCredentialCollection($credentials01);

        $this->userCredentialStorageMock->expects($this->once())->method('findActive')
            ->willReturn($credentials01);

        $this->assertEquals($collection01, $this->userCredentialService->getAllActiveCredentialList());
    }

    /** @dataProvider dataGetUserCredentialById */
    public function testGetUserCredentialById(
        ?UserCredentialEntity $credentialEntity,
        ?array $exception,
        int $userId
    ) {
        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }
        $credentialId = 1;

        if (null === $exception || $exception['code'] !== 2020) {
            $this->userServiceMock->expects($this->once())->method('getUserId')->willReturn($userId);
        }
        $this->userCredentialStorageMock->expects($this->once())->method('findOneById')
            ->with($credentialId)->willReturn($credentialEntity);

        $this->assertEquals($credentialEntity, $this->userCredentialService->getUserCredentialById($credentialId));
    }

    public function dataGetUserCredentialById()
    {
        $credentials01 = $this->getDefaultCredentials();
        $userId01 = $this->getDefaultUserId();

        $exception01 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2021
        ];
        $exception02 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2020
        ];
        return [
            'Common Case' => [
                $credentials01[0],
                null,
                $userId01
            ],
            'Access Denied Case' => [
                $credentials01[1],
                $exception01,
                $userId01
            ],
            'Not found Case' => [
                null,
                $exception02,
                $userId01
            ]
        ];
    }

    public function testAddUserCredential()
    {
        $userEntity = new UserEntity();
        $userEntity->setId(1);

        $credentialEntity = new UserCredentialEntity();
        $credentialEntity->setBrokerId('tinkoff');
        $credentialEntity->setApiKey('ENCRYPT1');
        $credentialEntity->setActive(0);
        $credentialEntity->setUserId(1);
        $credentialEntity->setUser($userEntity);

        $credentialModel = new UserCredentialModel();
        $credentialModel->setBroker('tinkoff');
        $credentialModel->setApiKey('AAAA-BBBB');
        $credentialModel->setIsActive(0);

        $this->userServiceMock->expects($this->once())->method('getUser')->willReturn($userEntity);
        $this->userServiceMock->expects($this->once())->method('getUserId')->willReturn(1);
        $this->encryptService->expects($this->once())->method('encrypt')->with('AAAA-BBBB')
            ->willReturn('ENCRYPT1');

        $this->userCredentialStorageMock->expects($this->once())->method('addEntity')
            ->with($credentialEntity);

        $this->assertEquals($credentialEntity, $this->userCredentialService->addUserCredential($credentialModel));
    }

    /** @dataProvider dataUpdateUserCredential */
    public function testUpdateUserCredential(
        UserCredentialEntity $expected,
        ?array $exception,
        UserCredentialModel $userCredentialModel,
        UserCredentialEntity $userCredentialEntity,
        int $userId,
        int $credentialId,
        ?array $key
    ) {
        $this->userServiceMock->expects($this->once())
            ->method('getUserId')->willReturn($userId);

        $this->userCredentialStorageMock->expects($this->once())
            ->method('findOneById')->with($credentialId)
            ->willReturn($userCredentialEntity);

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        } else {
            $this->encryptService->expects($this->once())->method('encrypt')
                ->with($key['encrypt'])
                ->willReturn($key['decrypt']);
            $this->userCredentialStorageMock->expects($this->once())
                ->method('addEntity')->with($expected);
        }

        $this->userCredentialService->updateUserCredential($userCredentialModel);
    }

    public function dataUpdateUserCredential(): array
    {
        $userId01 = 2;
        $userId02 = 3;
        $credentialId01 = 1;
        $userCredential01 = [
            'user_credential_id' => $credentialId01,
            'broker' => 'tinkoff',
            'api_key' => 'ENCRYPT1',
            'is_active' => 0
        ];
        $userCredentialModel01 = new UserCredentialModel($userCredential01);
        $encrypt01 = 'ENCRYPT1';
        $key01 = 'AAAA-BBBB-CCCC';

        $userCredential02 = [
            'user_credential_id' => $credentialId01,
            'broker' => 'tinkoff',
            'api_key' => 'ENCRYPT2',
            'is_active' => 0
        ];
        $encrypt02 = 'ENCRYPT2';
        $key02 = 'AAAA-BBBB-CCCC';

        $userCredentialModel02 = new UserCredentialModel($userCredential02);

        $userCredentialEntityResult01 = new UserCredentialEntity();
        $userCredentialEntityResult01->setActive(0);
        $userCredentialEntityResult01->setUserId($userId01);
        $userCredentialEntityResult01->setBrokerId('tinkoff');
        $userCredentialEntityResult01->setApiKey('AAAA-BBBB-CCCC');
        $userCredentialEntityResult01->setId(1);

        $userCredentialEntity01 = new UserCredentialEntity();
        $userCredentialEntity01->setActive(1);
        $userCredentialEntity01->setUserId($userId01);
        $userCredentialEntity01->setBrokerId('bcs');
        $userCredentialEntity01->setApiKey('DDDD-EEEE-FFFF');
        $userCredentialEntity01->setId(1);

        $exception01 = null;
        $exception02 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2022
        ];

        return [
            'Common Case' => [
                $userCredentialEntityResult01,
                $exception01,
                $userCredentialModel01,
                $userCredentialEntity01,
                $userId01,
                $credentialId01,
                ['encrypt' => $encrypt01, 'decrypt' => $key01]
            ],
            'Default is Active Case' => [
                $userCredentialEntityResult01,
                $exception01,
                $userCredentialModel02,
                $userCredentialEntity01,
                $userId01,
                $credentialId01,
                ['encrypt' => $encrypt02, 'decrypt' => $key02]
            ],
            'Credential Access Denied for User' => [
                $userCredentialEntityResult01,
                $exception02,
                $userCredentialModel02,
                $userCredentialEntity01,
                $userId02,
                $credentialId01,
                null
            ]
        ];
    }

    public function testGetUserCredentialDecryptedById()
    {
        $userCredentialId = 101;
        $userId01 = 1;
        $userCredentialEntity01 = new UserCredentialEntity();
        $userCredentialEntity01->setActive(1);
        $userCredentialEntity01->setUserId($userId01);
        $userCredentialEntity01->setBrokerId('bcs');
        $userCredentialEntity01->setApiKey('ENCRYPT01');
        $userCredentialEntity01->setId(101);

        $expected = new UserCredentialModel([
            'userCredentialId' => 101,
            'isActive' => 1,
            'userId' => 1,
            'broker' => 'bcs',
            'apiKey' => 'DDDD-EEEE-FFFF'
        ]);

        $this->userCredentialStorageMock->expects($this->once())->method('findOneById')
            ->with($userCredentialId)->willReturn($userCredentialEntity01);
        $this->encryptService->expects($this->once())->method('decrypt')
            ->with('ENCRYPT01')
            ->willReturn('DDDD-EEEE-FFFF');
        $this->userServiceMock->expects($this->once())->method('getUserId')->willReturn(1);

        $this->assertEquals($expected, $this->userCredentialService->getUserCredentialDecryptedById(101));
    }
}
