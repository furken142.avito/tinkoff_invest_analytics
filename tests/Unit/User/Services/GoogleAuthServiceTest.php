<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\Common\HttpClient;
use App\User\Models\UserAuthModel;
use App\User\Services\GoogleAuthService;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class GoogleAuthServiceTest extends TestCase
{
    private HttpClient $httpClient;
    private GoogleAuthService $googleAuthService;

    public function setUp(): void
    {
        parent::setUp();

        $this->httpClient = $this->createMock(HttpClient::class);
        $this->googleAuthService = new GoogleAuthService($this->httpClient);
    }

    /** @dataProvider dataGetUserInfo */
    public function testGetUserInfo(
        ?UserAuthModel $expected,
        string $accessToken,
        ResponseInterface $response
    ) {
        $this->httpClient->expects($this->once())->method('doRequest')
            ->with(
                'GET',
                'people/me',
                ['personFields' => 'emailAddresses,names,birthdays'],
                ['Authorization' => 'Bearer ' . 'AAAA-BBBB-CCCC']
            )->willReturn($response);

        $this->assertEquals($expected, $this->googleAuthService->getUserInfo($accessToken));
    }

    public function dataGetUserInfo()
    {
        $accessToken01 = 'AAAA-BBBB-CCCC';

        $content01 = json_encode([
            'names' => [
                ['displayName' => 'a.treschilov']
            ],
            'emailAddresses' => [
                ['value' => 'a.treschilov@email.com']
            ],
            'birthdays' => [
                [
                    'date' => [
                        'year' => 1985,
                        'month' => 4,
                        'day' => 6
                    ]
                ]
            ],
            'resourceName' => '1111-2222'
        ]);
        $expected01 = new UserAuthModel([
            'login' => 'a.treschilov',
            'access_token' => 'AAAA-BBBB-CCCC',
            'auth_type' => 'GOOGLE',
            'email' => 'a.treschilov@email.com',
            'birthday' => new \DateTime('1985-04-06'),
            'client_id' => '1111-2222'
        ]);
        $body01 = $this->createMock(StreamInterface::class);
        $body01->expects($this->once())->method('getContents')->willReturn($content01);
        $response01 = $this->createMock(ResponseInterface::class);
        $response01->expects($this->once())->method('getBody')->willReturn($body01);
        $response01->expects($this->once())->method('getStatusCode')->willReturn('200');


        $expected02 = null;
        $response02 = $this->createMock(ResponseInterface::class);
        $response02->expects($this->once())->method('getStatusCode')->willReturn('403');

        return [
            'Common Case' => [
                $expected01,
                $accessToken01,
                $response01
            ],
            'Unsuccessful response' => [
                $expected02,
                $accessToken01,
                $response02
            ]
        ];
    }
}
