<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\Services\AuthService;
use App\User\Collections\UserCollection;
use App\User\Entities\UserAuthEntity;
use App\User\Entities\UserEntity;
use App\User\Entities\UserSettingsEntity;
use App\User\Exceptions\AuthTypeException;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Models\UserSettingsModel;
use App\User\Services\UserService;
use App\User\Storages\UserAuthStorage;
use App\User\Storages\UserSettingsStorage;
use App\User\Storages\UserStorage;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    private UserService $userService;
    private UserStorage $userStorageMock;
    private UserSettingsStorage $userSettingsStorage;
    private UserAuthStorage $userAuthStorageMock;
    private AuthService $authService;
    private AuthServiceInterface $googleAuthService;
    private AuthServiceInterface $authServiceInterfaceMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->userStorageMock = $this->createMock(UserStorage::class);
        $this->userSettingsStorage = $this->createMock(UserSettingsStorage::class);
        $this->userAuthStorageMock = $this->createMock(UserAuthStorage::class);
        $this->authService = $this->createMock(AuthService::class);
        $this->authServiceInterfaceMock = $this->createMock(AuthServiceInterface::class);
        $this->googleAuthService = $this->createMock(AuthServiceInterface::class);
        $this->userService = new UserService(
            $this->userStorageMock,
            $this->userAuthStorageMock,
            $this->userSettingsStorage,
            $this->authServiceInterfaceMock,
            $this->googleAuthService,
            $this->authService
        );
    }

    /** @dataProvider dataGetUser */
    public function testGetUser(?UserEntity $expected, ?UserEntity $user)
    {
        if (null !== $user) {
            $this->userService->setUser($user);
        }

        $this->assertEquals($expected, $this->userService->getUser());
    }

    public function dataGetUser()
    {
        $userEntity = new UserEntity();
        $userEntity->setId(1);

        return [
            'Common Case' => [
                $userEntity,
                $userEntity
            ],
            'Without user' => [
                null,
                null
            ]
        ];
    }

    /** @dataProvider dataTestLogin */
    public function testLogin($expected, $token, $userAuth)
    {
        $this->userAuthStorageMock->expects($this->once())
            ->method('findActiveByToken')
            ->with($token)
            ->willReturn($userAuth);
        $userService = new UserService(
            $this->userStorageMock,
            $this->userAuthStorageMock,
            $this->userSettingsStorage,
            $this->authServiceInterfaceMock,
            $this->googleAuthService,
            $this->authService
        );

        $this->assertEquals($expected, $userService->login($token));
    }

    public function dataTestLogin()
    {
        $token01 = 'AAAA-BBBB';
        $user01 = new UserEntity();
        $user01->setId(1);
        $userAuth01 = new UserAuthEntity();
        $userAuth01->setId(2);
        $userAuth01->setUser($user01);
        $userAuth01->setAuthType('YANDEX');
        $userAuth01->setAccessToken('AAAA-BBBB');

        $token02 = 'CCCC-DDDD';
        $user02 = null;
        $userAuth02 = null;

        return [
            'Common Case' => [
                $user02,
                $token01,
                $userAuth01
            ],
            'User does not exists' => [
                $user02,
                $token02,
                $userAuth02
            ]
        ];
    }

    /** @dataProvider dataCreateAndLogin */
    public function testCreateAndLogin(
        string $token,
        UserAuthModel $user,
        ?UserEntity $dbUser,
        ?UserAuthEntity $dbAuthUser,
        string $authType,
        ?array $exception
    ) {
        $authService = $this->createMock(AuthService::class);
        $authServiceInterface = $this->getMockBuilder(AuthServiceInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userAuthStorage = $this->getMockBuilder(UserAuthStorage::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userStorage = $this->getMockBuilder(UserStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionMessage($exception['message']);
        } else {
            if ($dbAuthUser === null) {
                $exactlyCount = 1;
            } else {
                $exactlyCount = 2;
            }
            $authServiceInterface->expects($this->exactly($exactlyCount))
                ->method('getUserInfo')
                ->with($token)
                ->willReturn($user);

            $userStorage->expects($this->once())
                ->method('findByLogin')
                ->with($user->getLogin())
                ->willReturn($dbUser);

            $userAuthStorage->expects($this->exactly(2))
                ->method('findActiveByToken')
                ->with($token)
                ->willReturn($dbAuthUser);

            if (null === $dbUser) {
                $userStorage->expects($this->once())
                    ->method('addEntity');
            } else {
                $userStorage->expects($this->never())
                    ->method('addEntity');
            }

            if (null === $dbAuthUser) {
                $userAuthStorage->expects($this->once())
                    ->method('addEntity');
            } else {
                $userAuthStorage->expects($this->never())
                    ->method('addEntity');
            }
        }

        $userService = new UserService(
            $userStorage,
            $userAuthStorage,
            $this->userSettingsStorage,
            $authServiceInterface,
            $this->googleAuthService,
            $authService
        );

        $userService->createAndLogin($authType, $token);
    }

    public function dataCreateAndLogin()
    {
        $user01 = new UserAuthModel();
        $user01->setUserId(1);
        $user01->setAuthType('YANDEX');
        $user01->setClientId('1111-2222');
        $user01->setAccessToken('AAAA-BBBB');
        $user01->setLogin('a.treschilov');

        $dbUSer01 = new UserEntity();
        $dbUSer01->setLogin('a.treschilov');
        $dbUSer01->setEmail('mail@mail.com');

        $dbUser02 = null;

        $dbAuthUser01 = new UserAuthEntity();
        $dbAuthUser01->setId(1);
        $dbAuthUser01->setAuthType('YANDEX');
        $dbAuthUser01->setClientId('1111-2222');
        $dbAuthUser01->setAccessToken('AAAA-BBBB');
        $dbAuthUser01->setUser(new UserEntity());

        $dbAuthUser02 = new UserAuthEntity();
        $dbAuthUser02->setAuthType('YANDEX');
        $dbAuthUser02->setClientId('1111-2222');
        $dbAuthUser02->setAccessToken('AAAA-BBBB');

        $dbAuthUser03 = null;

        $token01 = 'AAAA-BBBB';

        $authType01 = 'YANDEX';
        $authType02 = 'Unknown type';

        $exception01 = [
            'class' => AuthTypeException::class,
            'message' => 'Auth type is not supported'
        ];

        return [
            'Common Case' => [
                $token01,
                $user01,
                $dbUSer01,
                $dbAuthUser01,
                $authType01,
                null
            ],
            'New User Case' => [
                $token01,
                $user01,
                $dbUser02,
                $dbAuthUser03,
                $authType01,
                null
            ],
            'New Auth Case' => [
                $token01,
                $user01,
                $dbUSer01,
                $dbAuthUser03,
                $authType01,
                null
            ],
            'Wrong auth type' => [
                $token01,
                $user01,
                $dbUser02,
                $dbAuthUser03,
                $authType02,
                $exception01
            ]
        ];
    }

    public function testGetUserById()
    {
        $userId = 1;
        $userEntity = new UserEntity();
        $userEntity->setId($userId);

        $this->userStorageMock->expects($this->once())->method('findById')
            ->with($userId)->willReturn($userEntity);

        $this->assertEquals($userEntity, $this->userService->getUserById($userId));
    }

    public function testAddUser()
    {
        $user = new UserEntity();
        $user->setId(1);

        $this->userStorageMock->expects($this->once())->method('addEntity')
            ->with($user);

        $this->assertEquals(1, $this->userService->addUser($user));
    }

    /** @dataProvider dataLogout */
    public function testLogout(string $token, ?UserAuthEntity $userAuth, ?UserAuthEntity $userAuthE = null)
    {
        $this->userAuthStorageMock->expects($this->once())
            ->method('findActiveByToken')
            ->with($token)
            ->willReturn($userAuth);
        if (null !== $userAuth) {
            $this->userAuthStorageMock->expects($this->once())
                ->method('addEntity')
                ->with($userAuthE);
        }

        $this->userService->logout($token);
    }

    public function testGetUserSettings()
    {
        $userSettings = new UserSettingsEntity();
        $userSettings->setId(1);
        $userSettings->setDesiredPension(100.2);
        $userSettings->setUserId(2);

        $this->userSettingsStorage->expects($this->once())
            ->method('findByUserId')
            ->with(1)
            ->willReturn($userSettings);

        $this->assertEquals($userSettings, $this->userService->getUserSettings(1));
    }

    public function dataLogout()
    {
        $date = new \DateTime();
        $token01 = 'XXXX-YYYY-ZZZZ';
        $userAuth01 = new UserAuthEntity();
        $userAuth01->setId(1);
        $userAuth01->setIsActive(1);
        $userAuth01->setAuthDate($date);

        $userAuth01e = new UserAuthEntity();
        $userAuth01e->setId(1);
        $userAuth01e->setIsActive(0);
        $userAuth01e->setAuthDate($date);

        $userAuth02 = null;

        return [
            'Common Case' => [
                $token01, $userAuth01, $userAuth01e
            ],
            'No Login User Case' => [
                $token01, $userAuth02
            ]
        ];
    }

    public function testGetUserId()
    {
        $userId = 1;
        $userEntity = new UserEntity();
        $userEntity->setId($userId);

        $this->userService->setUser($userEntity);

        $this->assertEquals($userId, $this->userService->getUserId());
    }

    public function testGenerateJWT()
    {
        $userId = 1;
        $userLogin = 'i.ivanov';
        $authType = 'YANDEX';
        $userEntity = new UserEntity();
        $userEntity->setId($userId);
        $userEntity->setLogin($userLogin);
        $jwt = '1111.222222.333';

        $payload = [
            'iis' => 'tinkoff_invest_analytics',
            'sub' => 'user data',
            'auth_type' => $authType,
            'userId' => $userId,
            'userLogin' => $userLogin
        ];

        $this->authService->expects($this->once())
            ->method('generateJWT')
            ->with($payload)
            ->willReturn($jwt);

        $this->assertEquals($jwt, $this->userService->generateJWT($userEntity, $authType));
    }

    /** @dataProvider dataSetUserSettings */
    public function testSetUserSettings(
        UserSettingsModel $userSettingsModel,
        UserSettingsEntity $userSettingsEntity,
        int $userId,
        ?UserSettingsEntity $existedUserSettingsEntity,
        ?array $exception
    ) {
        $user = new UserEntity();
        $user->setId($userId);
        $this->userService->setUser($user);

        if ($existedUserSettingsEntity !== null) {
            $this->userSettingsStorage->expects($this->once())->method('findByUserId')
                ->with($userId)->willReturn($existedUserSettingsEntity);
        }

        if ($exception === null) {
            $this->userSettingsStorage->expects($this->once())->method('addEntity')
                ->with($userSettingsEntity);
        } else {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }

        $this->userService->setUserSettings($userSettingsModel);
    }

    public function dataSetUserSettings()
    {
        $userId01 = 2;
        $userSettingsModel01 = new UserSettingsModel([
            'userSettingsId' => null,
            'desiredPension' => 100,
            'retirementAge' => 50
        ]);
        $userSettingsModel02 = new UserSettingsModel([
            'userSettingsId' => 1,
            'desiredPension' => 100,
            'retirementAge' => 60
        ]);

        $userSettingsEntity01 = new UserSettingsEntity();
        $userSettingsEntity01->setDesiredPension(100);
        $userSettingsEntity01->setRetirementAge(50);
        $userSettingsEntity01->setUserId(2);

        $userSettingsEntity02 = new UserSettingsEntity();
        $userSettingsEntity02->setId(1);
        $userSettingsEntity02->setDesiredPension(100);
        $userSettingsEntity02->setRetirementAge(60);
        $userSettingsEntity02->setUserId(2);

        $existedUserSettingsEntity01 = null;
        $existedUserSettingsEntity02 = new UserSettingsEntity();
        $existedUserSettingsEntity02->setId(1);
        $existedUserSettingsEntity02->setDesiredPension(200);
        $existedUserSettingsEntity02->setRetirementAge(80);
        $existedUserSettingsEntity02->setUserId(2);

        $existedUserSettingsEntity03 = new UserSettingsEntity();
        $existedUserSettingsEntity03->setId(3);
        $existedUserSettingsEntity03->setDesiredPension(200);
        $existedUserSettingsEntity03->setRetirementAge(80);
        $existedUserSettingsEntity03->setUserId(3);

        $exception01 = null;
        $exception02 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2024
        ];

        return [
            'New User' => [
                $userSettingsModel01,
                $userSettingsEntity01,
                $userId01,
                $existedUserSettingsEntity01,
                $exception01
            ],
            'Existed User' => [
                $userSettingsModel02,
                $userSettingsEntity02,
                $userId01,
                $existedUserSettingsEntity02,
                $exception01
            ],
            'Access denied Case' => [
                $userSettingsModel02,
                $userSettingsEntity02,
                $userId01,
                $existedUserSettingsEntity03,
                $exception02
            ]
        ];
    }

    public function testGetUserList()
    {
        $user01 = new UserEntity();
        $user01->setId(1);
        $user02 = new UserEntity();
        $user02->setId(2);
        $collection = new UserCollection([$user01, $user02]);

        $this->userStorageMock->expects($this->once())->method('findAll')
            ->willReturn([$user01, $user02]);

        $this->assertEquals($collection, $this->userService->getUserList());
    }
}
