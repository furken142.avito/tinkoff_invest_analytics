<?php

declare(strict_types=1);

namespace Tests\Unit\User\Entities;

use App\User\Entities\UserSettingsEntity;
use Tests\ModelTestCase;

class UserSettingsEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserSettingsEntity();
    }

    public function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setDesiredPension' => 100.9,
                    'setRetirementAge' => 45
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getDesiredPension' => 100.9,
                    'getRetirementAge' => 45
                ]
            ],
            'Empty fields Test' => [
                [
                    'setId' => 1,
                    'setUserId' => 2
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getDesiredPension' => null,
                    'getRetirementAge' => null
                ]
            ]
        ];
    }
}
