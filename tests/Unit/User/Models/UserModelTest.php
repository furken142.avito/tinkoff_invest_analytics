<?php

declare(strict_types=1);

namespace Tests\Unit\User\Models;

use App\User\Models\UserAuthModel;
use Tests\ModelTestCase;

class UserModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserAuthModel();
    }

    public function dataTestGetSet(): array
    {
        $birthday = new \DateTime('1985-04-23');
        return [
            'Common Case' => [
                [
                    'setUserId' => 1,
                    'setLogin' => 'i.ivanov',
                    'setEmail' => 'i.ivanov@yandex.ru',
                    'setAuthType' => 'YANDEX',
                    'setClientId' => '1234',
                    'setAccessToken' => 'AAAABBBBCCCC',
                    'setBirthday' => $birthday
                ],
                [
                    'getUserId' => 1,
                    'getLogin' => 'i.ivanov',
                    'getEmail' => 'i.ivanov@yandex.ru',
                    'getAuthType' => 'YANDEX',
                    'getClientId' => '1234',
                    'getAccessToken' => 'AAAABBBBCCCC',
                    'getBirthday' => $birthday
                ]
            ],
            'Empty fields Test' => [
                [
                    'setUserId' => 1,
                    'setAuthType' => 'YANDEX',
                    'setClientId' => '1234',
                ],
                [
                    'getUserId' => 1,
                    'getLogin' => null,
                    'getEmail' => null,
                    'getAuthType' => 'YANDEX',
                    'getClientId' => '1234',
                    'getAccessToken' => null,
                    'getBirthday' => null
                ]
            ]
        ];
    }
}
