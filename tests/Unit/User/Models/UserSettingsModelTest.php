<?php

namespace Tests\Unit\User\Models;

use App\User\Models\UserSettingsModel;
use Tests\ModelTestCase;

class UserSettingsModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserSettingsModel();
    }

    public function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setUserSettingsId' => 1,
                    'setUserId' => 2,
                    'setDesiredPension' => 1.5,
                    'setRetirementAge' => 50
                ],
                [
                    'getUserSettingsId' => 1,
                    'getUserId' => 2,
                    'getDesiredPension' => 1.5,
                    'getRetirementAge' => 50
                ]
            ],
            'Empty Field Case' => [
                [
                    'setUserId' => 2
                ],
                [
                    'getUserSettingsId' => null,
                    'getUserId' => 2,
                    'getDesiredPension' => null,
                    'getRetirementAge' => null
                ]
            ]
        ];
    }
}
