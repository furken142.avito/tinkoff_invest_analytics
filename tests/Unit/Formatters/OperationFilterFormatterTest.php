<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\OperationFilterFormatter;
use App\Item\Models\OperationFiltersModel;
use PHPUnit\Framework\TestCase;

class OperationFilterFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $dateFrom = new \DateTime('2021-12-11 01:20:58');
        $dateTo = new \DateTime('2022-12-11 01:20:58');
        $data = [
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'operationStatus' => ['Done'],
            'operationType' => ['Stock'],
            'brokerId' => ['money_friends']
        ];
        $expected = [
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'operationStatus' => ['Done'],
            'operationType' => ['Stock'],
            'brokerId' => ['money_friends']
        ];
        $model = new OperationFiltersModel($data);
        $formatter = new OperationFilterFormatter($model);
        $this->assertEquals($expected, $formatter->format());
    }
}
