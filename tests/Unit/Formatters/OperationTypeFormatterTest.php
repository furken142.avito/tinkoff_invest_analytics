<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\OperationTypesFormatter;
use App\Item\Collections\OperationTypeCollection;
use App\Item\Models\OperationTypeModel;
use PHPUnit\Framework\TestCase;

class OperationTypeFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /** @dataProvider  dataFormat */
    public function testFormat($expected, $operationTypeCollection)
    {
        $formatter = new OperationTypesFormatter($operationTypeCollection);
        $this->assertEquals($expected, $formatter->format());
    }

    public function dataFormat()
    {
        $operationType01 = new OperationTypeModel([
            'operationTypeId' => 1,
            'name' => 'Покупка',
            'externalId' => 'Buy'
        ]);
        $operationType02 = new OperationTypeModel([
            'operationTypeId' => 2,
            'name' => 'Продажа',
            'externalId' => 'Sell'
        ]);

        $operationTypeCollection01 = new OperationTypeCollection([$operationType01, $operationType02]);
        $operationTypeCollection02 = new OperationTypeCollection();

        $expected01 = [
            [
                'id' => 'Buy',
                'name' => 'Покупка'
            ],
            [
                'id' => 'Sell',
                'name' => 'Продажа',
            ]
        ];
        $expected02 = [];

        return [
            'Common Case' => [
                $expected01,
                $operationTypeCollection01
            ],
            'Empty Case' => [
                $expected02,
                $operationTypeCollection02
            ]
        ];
    }
}
