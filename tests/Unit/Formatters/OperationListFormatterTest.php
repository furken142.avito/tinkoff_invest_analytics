<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Item\Collections\OperationCollection;
use App\Entities\MarketInstrumentEntity;
use App\Formatters\OperationListFormatter;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel;
use PHPUnit\Framework\TestCase;

class OperationListFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /** @dataProvider dataFormat */
    public function testFormat(array $expected, OperationCollection $operationCollection)
    {
        $formatter = new OperationListFormatter($operationCollection);
        $this->assertEquals($expected, $formatter->format());
    }

    public function dataFormat()
    {
        $date01 = new \DateTime('2021-12-11 15:05:28');
        $date02 = new \DateTime('2021-12-11 15:05:29');
        $instrumentEntity01 = new MarketInstrumentEntity();
        $instrumentEntity01->setName('Лукойл');
        $instrumentEntity01->setType('Stock');

        $operation01 = new OperationModel([
            'itemOperationId' => 1,
            'brokerId' => 'tinkoff',
            'status' => 'Done',
            'currencyIso' => 'RUB',
            'amount' => 50,
            'quantity' => 5,
            'item' => new ItemModel([
                'itemId' => 1,
                'name' => 'Лукойл',
                'type' => 'market'
            ]),
            'date' => $date01,
            'operationType' => 'Buy'
        ]);
        $operation02 = new OperationModel([
            'itemOperationId' => 2,
            'brokerId' => 'tinkoff',
            'status' => 'Done',
            'currencyIso' => 'RUB',
            'amount' => 50,
            'quantity' => null,
            'date' => $date02,
            'operationType' => 'Fee',
            'item' => null,
            'itemId' => null,
        ]);

        $operationCollection01 = new OperationCollection([$operation01, $operation02]);

        $expected01 = [
            [
                'id' => 1,
                'type' => 'Buy',
                'itemName' => 'Лукойл',
                'broker' => 'tinkoff',
                'quantity' => 5,
                'amount' => 50,
                'currency' => 'RUB',
                'date' => $date01,
                'status' => 'Done'
            ],
            [
                'id' => 2,
                'type' => 'Fee',
                'itemName' => null,
                'broker' => 'tinkoff',
                'quantity' => null,
                'amount' => 50,
                'currency' => 'RUB',
                'date' => $date02,
                'status' => 'Done'
            ]
        ];

        return [
            'Common Case' => [
                $expected01, $operationCollection01
            ]
        ];
    }
}
