<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\CountryFormatter;
use App\Formatters\CurrencyFormatter;
use App\Formatters\MarketSectorFormatter;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Collections\MarketSectorCollection;
use Tests\TITestCase;

class MarketSectorFormatterTest extends TITestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $sector01 = $this->createMarketSectorEntity([
            'id' => 1,
            'name' => 'Utilities'
        ]);
        $sector02 = $this->createMarketSectorEntity([
            'id' => 2,
            'name' => 'Technology'
        ]);
        $sector03 = $this->createMarketSectorEntity([
            'id' => 3,
            'name' => 'Consumer Defensive'
        ]);
        $collection = new MarketSectorCollection([$sector01, $sector02, $sector03]);

        $expected = [
            [
                'id' => 1,
                'name' => 'Utilities'
            ],
            [
                'id' => 2,
                'name' => 'Technology'
            ],
            [
                'id' => 3,
                'name' => 'Consumer Defensive'
            ]
        ];

        $formatter = new MarketSectorFormatter($collection);
        $this->assertEquals($expected, $formatter->format());
    }
}
