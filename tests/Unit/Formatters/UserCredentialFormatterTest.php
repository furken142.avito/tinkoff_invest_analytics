<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\UserCredentialFormatter;
use App\User\Models\UserCredentialModel;
use Monolog\Test\TestCase;

class UserCredentialFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $credential01 = new UserCredentialModel();
        $credential01->setUserCredentialId(1);
        $credential01->setApiKey('AAAA-BBBB-CCCC');
        $credential01->setBroker('tinkoff');
        $credential01->setIsActive(1);

        $expected01 = [
                'userCredentialId' => 1,
                'apiKey' => 'AAAA-BBBB-CCCC',
                'isActive' => 1,
                'broker' => 'tinkoff'
            ];

        $formatter = new UserCredentialFormatter($credential01);
        $this->assertEquals($expected01, $formatter->format());
    }
}
