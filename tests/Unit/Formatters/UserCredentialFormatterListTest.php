<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\UserCredentialListFormatter;
use App\User\Collections\UserCredentialCollection;
use App\User\Models\UserCredentialModel;
use Monolog\Test\TestCase;

class UserCredentialFormatterListTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $credential01 = new UserCredentialModel();
        $credential01->setUserCredentialId(1);
        $credential01->setApiKey('AAAA-BBBB-CCCC');
        $credential01->setBroker('tinkoff');
        $credential01->setIsActive(1);

        $credential02 = new UserCredentialModel();
        $credential02->setUserCredentialId(2);
        $credential02->setApiKey('DDDD-EEEE-FFFF');
        $credential02->setBroker('bcs');
        $credential02->setIsActive(0);

        $expected01 = [
            [
                'userCredentialId' => 1,
                'apiKey' => 'AAAA-BBBB-CCCC',
                'isActive' => 1,
                'broker' => 'tinkoff'
            ],
            [
                'userCredentialId' => 2,
                'apiKey' => 'DDDD-EEEE-FFFF',
                'isActive' => 0,
                'broker' => 'bcs'
            ]
        ];

        $collection = new UserCredentialCollection([$credential01, $credential02]);
        $formatter = new UserCredentialListFormatter($collection);
        $this->assertEquals($expected01, $formatter->format());
    }
}
