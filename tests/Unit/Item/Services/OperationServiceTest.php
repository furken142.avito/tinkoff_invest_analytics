<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Services;

use App\Intl\Services\CurrencyService;
use App\Item\Collections\OperationCollection;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\OperationStatusStorage;
use App\Item\Storages\OperationTypeStorage;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use PHPUnit\Framework\TestCase;

class OperationServiceTest extends TestCase
{
    private ItemOperationStorage $itemOperationStorage;
    private ItemStorage $itemStorage;
    private BrokerStorage $brokerStorage;
    private CurrencyService $currencyService;
    private OperationTypeStorage $operationTypeStorage;
    private OperationStatusStorage $operationStatusStorage;
    private ExchangeCurrencyService $exchangeCurrencyService;
    private OperationService $operationService;

    public function setUp(): void
    {
        parent::setUp();

        $this->itemOperationStorage = $this->createMock(ItemOperationStorage::class);
        $this->itemStorage = $this->createMock(ItemStorage::class);
        $this->brokerStorage = $this->createMock(BrokerStorage::class);
        $this->currencyService = $this->createMock(CurrencyService::class);
        $this->operationTypeStorage = $this->createMock(OperationTypeStorage::class);
        $this->operationStatusStorage = $this->createMock(OperationStatusStorage::class);
        $this->exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);

        $this->operationService = new OperationService(
            $this->itemOperationStorage,
            $this->brokerStorage,
            $this->itemStorage,
            $this->currencyService,
            $this->operationTypeStorage,
            $this->operationStatusStorage,
            $this->exchangeCurrencyService
        );
    }

    /** @dataProvider dataCalculateOperationSummary */
    public function testCalculateOperationSummary(
        PriceModel $expected,
        string $currency,
        OperationCollection $operations,
        array $exchange
    ) {
        $this->exchangeCurrencyService->expects($this->exactly(count($exchange['with'])))
            ->method('getExchangeRateByDayCandies')
            ->withConsecutive(...$exchange['with'])
            ->willReturnOnConsecutiveCalls(...$exchange['return']);
        $this->assertEquals($expected, $this->operationService->calculateOperationSummary($operations, $currency));
    }

    public function dataCalculateOperationSummary()
    {
        $op01 = new OperationModel([
            'id' => '2',
            'amount' => 100,
            'currencyIso' => 'RUB',
            'date' => new \DateTime('2022-04-12 04:43:13')
        ]);
        $op02 = new OperationModel([
            'id' => '3',
            'amount' => 5,
            'currencyIso' => 'USD',
            'date' => new \DateTime('2022-05-04 06:41:23')
        ]);
        $op03 = new OperationModel([
            'id' => '4',
            'amount' => -2,
            'currencyIso' => 'EUR',
            'date' => new \DateTime('2022-02-11 14:11:04')
        ]);

        $currency01 = 'RUB';
        $expected01 = new PriceModel(['amount' => 290, 'currency' => 'RUB']);

        $expected02 = new PriceModel(['amount' => 0, 'currency' => 'RUB']);

        $operationCollection01 = new OperationCollection([$op01, $op02, $op03]);
        $operationCollection02 = new OperationCollection([]);

        $exchangeCalls01 = [
            'with' => [
                ['RUB', 'RUB', new \DateTime('2022-04-12 04:43:13')],
                ['USD', 'RUB', new \DateTime('2022-05-04 06:41:23')],
                ['EUR', 'RUB', new \DateTime('2022-02-11 14:11:04')]
            ],
            'return' => [1, 70, 80]
        ];
        $exchangeCalls02 = [
            'with' => [],
            'return' => []
        ];

        $exchangeCurrencyService02 = $this->createMock(ExchangeCurrencyService::class);
        $exchangeCurrencyService02->expects($this->never())
            ->method('getExchangeRateByDayCandies');

        return [
            'Common Case' => [
                $expected01,
                $currency01,
                $operationCollection01,
                $exchangeCalls01
            ],
            'Empty Operations Case' => [
                $expected02,
                $currency01,
                $operationCollection02,
                $exchangeCalls02
            ]
        ];
    }
}
