<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Entities;

use App\Assets\Entities\AssetsEntity;
use App\Entities\MarketInstrumentEntity;
use App\Item\Entities\ItemEntity;
use Tests\ModelTestCase;

class ItemEntityTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ItemEntity();
    }

    public function dataTestGetSet(): array
    {
        $marketInstrument = new MarketInstrumentEntity();
        $marketInstrument->setId(2);

        $asset = new AssetsEntity();
        $asset->setId(3);
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setType' => 'market',
                    'setSource' => 'potok',
                    'setExternalId' => 'External ID 01',
                    'setName' => 'Visa',
                    'setMarketInstrument' => $marketInstrument,
                    'setAsset' => $asset
                ],
                [
                    'getId' => 1,
                    'getType' => 'market',
                    'getSource' => 'potok',
                    'getExternalId' => 'External ID 01',
                    'getName' => 'Visa',
                    'getMarketInstrument' => $marketInstrument,
                    'getAsset' => $asset
                ]
            ],
            'Default Values Case' => [
                [
                    'setType' => 'market',
                    'setSource' => 'potok',
                    'setName' => 'Visa'
                ],
                [
                    'getId' => null,
                    'getType' => 'market',
                    'getSource' => 'potok',
                    'getExternalId' => null,
                    'getName' => 'Visa',
                    'getMarketInstrument' => null,
                    'getAsset' => null
                ]
            ]
        ];
    }
}
