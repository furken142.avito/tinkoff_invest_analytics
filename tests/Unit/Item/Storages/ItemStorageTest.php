<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Item\Entities\ItemEntity;
use App\Item\Storages\ItemStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class ItemStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private ObjectRepository $repository;
    private ItemStorage $itemStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(ObjectRepository::class);
        $this->itemStorage = new ItemStorage($this->entityManager);
    }

    public function testAddEntity()
    {
        $entity = new ItemEntity();

        $this->entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $this->entityManager->expects($this->once())->method('flush');

        $this->itemStorage->addEntity($entity);
    }

    public function testFindByExternalIdAndSource()
    {
        $externalId = 'visa';
        $source = 'market';
        $itemEntity = new ItemEntity();
        $itemEntity->setId(1);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(ItemEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['externalId' => $externalId, 'source' => $source])
            ->willReturn($itemEntity);

        $this->assertEquals($itemEntity, $this->itemStorage->findByExternalIdAndSource($externalId, $source));
    }
}
