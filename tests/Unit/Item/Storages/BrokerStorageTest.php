<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Item\Entities\BrokerEntity;
use App\Item\Storages\BrokerStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class BrokerStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private ObjectRepository $repository;
    private BrokerStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(ObjectRepository::class);
        $this->storage = new BrokerStorage($this->entityManager);
    }

    public function testFindById()
    {
        $id = 'tinkoff';
        $brokerEntity = new BrokerEntity();
        $brokerEntity->setId('tinkoff');

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(BrokerEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['id' => $id])
            ->willReturn($brokerEntity);

        $this->assertEquals($brokerEntity, $this->storage->findById($id));
    }
}
