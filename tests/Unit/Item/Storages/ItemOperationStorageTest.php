<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Item\Entities\ItemOperationEntity;
use App\Item\Storages\ItemOperationStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class ItemOperationStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private ObjectRepository $repository;
    private ItemOperationStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(ObjectRepository::class);
        $this->storage = new ItemOperationStorage($this->entityManager);
    }

    public function testAddEntity()
    {
        $entity = new ItemOperationEntity();
        $entity->setId(1);

        $this->entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $this->entityManager->expects($this->once())->method('flush');

        $this->assertEquals(1, $this->storage->addEntity($entity));
    }

    public function testFindByExternalIdAndPlatform()
    {
        $externalId = 'operation 12345';
        $brokerId = 'tinkoff';

        $itemOperation = new ItemOperationEntity();
        $itemOperation->setId(2);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(ItemOperationEntity::class)
            ->willReturn($this->repository);
        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['externalId' => $externalId, 'brokerId' => $brokerId])
            ->willReturn($itemOperation);

        $this->assertEquals($itemOperation, $this->storage->findByExternalIdAndBroker($externalId, $brokerId));
    }

    /** @dataProvider dataFindByUserItemAmountDateType */
    public function testFindByUserItemAmountDateType(
        ItemOperationEntity|null $expected,
        array $operations,
        float $amount
    ) {
        $userId01 = 1;
        $brokerId01 = 'tinkoff';
        $itemId01 = 3;
        $date01 = new \DateTime('2022-11-19');
        $typeId01 = 4;

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(ItemOperationEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())->method('findBy')
            ->with([
                'userId' => $userId01,
                'brokerId' => $brokerId01,
                'itemId' => $itemId01,
                'date' => $date01,
                'operationTypeId' => $typeId01
            ])
            ->willReturn($operations);

        $this->assertEquals(
            $expected,
            $this->storage->findByUserItemAmountDateType(
                $userId01,
                $brokerId01,
                $itemId01,
                $date01,
                $typeId01,
                $amount
            )
        );
    }

    public function dataFindByUserItemAmountDateType()
    {
        $amount01 = 99.94;
        $amount02 = 9.99;

        $operation01 = new ItemOperationEntity();
        $operation01->setId(1);
        $operation01->setAmount(15.54);

        $operation02 = new ItemOperationEntity();
        $operation02->setId(2);
        $operation02->setAmount(99.94);

        return [
            'Common Case' => [
                $operation02,
                [$operation01, $operation02],
                $amount01
            ],
            'Not found case' => [
                null,
                [$operation01, $operation02],
                $amount02
            ]
        ];
    }

    /** @dataProvider dataFindByUserItemAmountDateType */
    public function testFindByUserAndAmountAndDate(
        ItemOperationEntity|null $expected,
        array $operations,
        float $amount
    ) {
        $userId01 = 1;
        $brokerId01 = 'tinkoff';
        $date01 = new \DateTime('2022-11-19');

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(ItemOperationEntity::class)->willReturn($this->repository);
        $this->repository->expects($this->once())->method('findBy')
            ->with(['userId' => $userId01, 'date' => $date01, 'brokerId' => $brokerId01])
            ->willReturn($operations);

        $this->assertEquals(
            $expected,
            $this->storage->findByUserAndAmountAndDate($userId01, $amount, $date01, $brokerId01)
        );
    }
}
