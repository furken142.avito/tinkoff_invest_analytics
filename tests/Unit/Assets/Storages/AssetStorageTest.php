<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Storages;

use App\Assets\Entities\AssetsEntity;
use App\Assets\Storages\AssetsStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class AssetStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private ObjectRepository $repository;
    private AssetsStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(ObjectRepository::class);
        $this->storage = new AssetsStorage($this->entityManager);
    }

    public function testFindByExternalIdAndPlatform()
    {
        $externalId = '1665';
        $entity01 = new AssetsEntity();
        $entity01->setId(1);

        $platform = 'potok';

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['platform' => $platform, 'externalId' => $externalId])
            ->willReturn($entity01);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(AssetsEntity::class)
            ->willReturn($this->repository);

        $this->assertEquals($entity01, $this->storage->findByExternalIdAndPlatform($externalId, $platform));
    }

    public function testAddEntity()
    {
        $entity = new AssetsEntity();
        $entity->setId(10);

        $this->entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $this->entityManager->expects($this->once())->method('flush');

        $this->storage->addEntity($entity);
    }
}
