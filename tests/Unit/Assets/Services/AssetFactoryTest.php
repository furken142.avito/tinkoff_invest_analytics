<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Services;

use App\Assets\Interfaces\AssetCalculatorInterface;
use App\Assets\Interfaces\ThirdPartyIntegrationInterface;
use App\Assets\Services\AssetFactory;
use App\Assets\Services\Calculators\DifferentiatedScheduleCalculator;
use App\Assets\Services\ThirdPartyIntegration\Potok;
use App\Assets\Storages\PayoutTypeStorage;
use PHPUnit\Framework\TestCase;

class AssetFactoryTest extends TestCase
{
    private AssetFactory $assetFactory;
    private PayoutTypeStorage $payoutTypeStorage;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->payoutTypeStorage = $this->createMock(PayoutTypeStorage::class);
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->assetFactory = new AssetFactory($this->payoutTypeStorage);
    }

    /** @dataProvider dataGetCalculator */
    public function testGetCalculator(
        AssetCalculatorInterface $class,
        string $type
    ) {
        $this->assertEquals($class, $this->assetFactory->getCalculator($type));
    }

    public function dataGetCalculator(): array
    {
        return [
            'Crowdfunding Case' => [
                'expected' => new DifferentiatedScheduleCalculator(),
                'type' => 'differentiated'
            ]
        ];
    }

    /** @dataProvider dataThirdPartyService */
    public function testThirdPartyService(
        ThirdPartyIntegrationInterface $class,
        string $platform
    ) {
        $this->assertEquals($class, $this->assetFactory->getThirdPartyService($platform));
    }

    public function dataThirdPartyService(): array
    {
        return [
            'Crowdfunding Case' => [
                'expected' => new Potok($this->payoutTypeStorage),
                'type' => 'potok'
            ]
        ];
    }
}
