<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\BrokerInterface;
use App\Broker\Models\BrokerCurrencyModel;
use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Intl\Services\CountryService;
use App\Item\Entities\ItemEntity;
use App\Item\Storages\ItemStorage;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\MarketInstrumentService;
use App\Services\StockBrokerFactory;
use App\Services\Sources\YahooSourceService;
use App\Storages\MarketCurrencyStorage;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class FillMarketInstrumentServiceDecoratorTest extends TITestCase
{
    private MockObject $broker;
    private MockObject $brokerFactory;
    private MockObject $marketInstrumentService;
    private ItemStorage $itemStorage;
    private MockObject $marketCurrencyStorage;
    private MockObject $yahooService;
    private CountryService $countryService;


    private FillMarketInstrumentServiceDecorator $fillMarketInstrumentServiceDecorator;

    public function setUp(): void
    {
        parent::setUp();

        $this->broker = $this->createMock(BrokerInterface::class);
        $this->brokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->itemStorage = $this->createMock(ItemStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentService::class);
        $this->marketCurrencyStorage = $this->createMock(MarketCurrencyStorage::class);
        $this->yahooService = $this->createMock(YahooSourceService::class);
        $this->countryService = $this->createMock(CountryService::class);

        $this->fillMarketInstrumentServiceDecorator = new FillMarketInstrumentServiceDecorator(
            $this->marketInstrumentService,
            $this->itemStorage,
            $this->marketCurrencyStorage,
            $this->brokerFactory,
            $this->yahooService,
            $this->countryService
        );
    }

    /** @dataProvider dataGetMarketInstrumentByFigi */
    public function testGetMarketInstrumentByFigi(
        $expected,
        $figi,
        $instrument,
        $clientRequestCount,
        $stockRequestCount,
        $tiInstrument,
        $instrumentTicker,
        $marketStock,
        $currencyData
    ) {
        $this->brokerFactory->expects($this->once())->method('getBrokerAdapter')
            ->with(StockBrokerFactory::TINKOFF_V2)->willReturn($this->broker);

        $this->marketInstrumentService->expects($this->once())
            ->method('getInstrumentByFigi')
            ->with($figi)
            ->willReturn($instrument);

        $this->broker->expects($this->exactly($clientRequestCount))
            ->method('getInstrumentByFigi')
            ->with($figi)
            ->willReturn($tiInstrument);

        $this->yahooService->expects($this->exactly($stockRequestCount))
            ->method('getCompanyProfile')
            ->with($instrumentTicker)
            ->willReturn($marketStock);

        if ($currencyData !== null) {
            $this->marketCurrencyStorage->expects($this->exactly($currencyData['findCurrencyIdDbCount']))
                ->method('findOneByFigi')->with($figi)->willReturn($currencyData['marketCurrencyEntity']);

            $this->broker->expects($this->exactly($currencyData['findCurrencyByApiCount']))
                ->method('getCurrencyByFigi')->with($figi)
                ->willReturn($currencyData['brokerCurrencyModel']);
        }

        $this->assertEquals($expected, $this->fillMarketInstrumentServiceDecorator->getInstrumentByFigi($figi));
    }

    public function dataGetMarketInstrumentByFigi()
    {
        $figi01 = 'figi01';
        $figi03 = 'figi03';

        $ticker03 = 'ticker03';

        $marketStock03 = new MarketStockEntity();
        $marketStock03->setFigi('figi03');
        $marketStock03->setCountryIso('Germany');
        $marketStock03->setSector('Energy');
        $marketStock03->setIndustry('Oil & Gas Midstream');

        $marketCurrencyEntity04 = new MarketCurrencyEntity();
        $marketCurrencyEntity04->setFigi('figi03');
        $marketCurrencyEntity04->setIso('RUB');

        $item01 = new ItemEntity();
        $item01->setSource('market');
        $item01->setType('market');
        $item01->setName('Luckoil');
        $item01->setExternalId('figi01');
        $instrument01 = new MarketInstrumentEntity();
        $instrument01->setType('Bond');
        $instrument01->setTicker('1234');
        $instrument01->setFigi('figi01');
        $instrument01->setIsin('isin01');
        $instrument01->setMinPriceIncrement(0.5);
        $instrument01->setLot(1);
        $instrument01->setCurrency('USD');
        $instrument01->setName('Luckoil');
        $instrument01->setItem($item01);
        $item01->setMarketInstrument($instrument01);

        $instrument02 = null;

        $item03 = new ItemEntity();
        $item03->setSource('market');
        $item03->setType('market');
        $item03->setName('Luckoil');
        $item03->setExternalId('figi03');
        $instrument03 = new MarketInstrumentEntity();
        $instrument03->setType('Stock');
        $instrument03->setTicker('ticker03');
        $instrument03->setFigi('figi03');
        $instrument03->setIsin('isin03');
        $instrument03->setMinPriceIncrement(0.5);
        $instrument03->setLot(2);
        $instrument03->setCurrency('RUB');
        $instrument03->setName('Luckoil');
        $instrument03->setMarketStock($marketStock03);
        $instrument03->setItem($item03);
        $item03->setMarketInstrument($instrument03);

        $item04 = new ItemEntity();
        $item04->setSource('market');
        $item04->setType('market');
        $item04->setName('Luckoil');
        $item04->setExternalId('figi03');
        $instrument04 = new MarketInstrumentEntity();
        $instrument04->setType('Currency');
        $instrument04->setTicker('1234');
        $instrument04->setFigi('figi03');
        $instrument04->setIsin('isin01');
        $instrument04->setMinPriceIncrement(0.5);
        $instrument04->setLot(1);
        $instrument04->setCurrency('USD');
        $instrument04->setName('Luckoil');
        $instrument04->setMarketCurrency($marketCurrencyEntity04);
        $marketCurrencyEntity04->setMarketInstrument($instrument04);
        $instrument04->setItem($item04);
        $item04->setMarketInstrument($instrument04);

        $marketInstrumentData01 = [
            'type' => 'Bond',
            'ticker' => '1234',
            'figi' => 'figi01',
            'isin' => 'isin01',
            'minPriceIncrement' => 0.5,
            'lot' => 1,
            'currency' => 'USD',
            'name' => 'Luckoil'
        ];
        $tiMarketInstrument01 = $this->createBrokerInstrument($marketInstrumentData01);

        $marketInstrumentData03 = [
            'type' => 'Stock',
            'ticker' => 'ticker03',
            'figi' => 'figi03',
            'isin' => 'isin03',
            'minPriceIncrement' => 0.5,
            'lot' => 2,
            'currency' => 'RUB',
            'name' => 'Luckoil'
        ];
        $tiMarketInstrument03 = $this->createBrokerInstrument($marketInstrumentData03);

        $marketInstrumentData04 = [
            'type' => 'Currency',
            'ticker' => '1234',
            'figi' => 'figi01',
            'isin' => 'isin01',
            'minPriceIncrement' => 0.5,
            'lot' => 1,
            'currency' => 'USD',
            'name' => 'Luckoil'
        ];
        $tiMarketInstrument04 = $this->createBrokerInstrument($marketInstrumentData04);

        $fillCurrencyData04 = [
            'addEntityCounts' => 1,
            'findCurrencyIdDbCount' => 1,
            'findCurrencyByApiCount' => 1,
            'marketCurrencyEntity' => null,
            'brokerCurrencyModel' => new BrokerCurrencyModel(['iso' => 'RUB', 'figi' => 'figi03']),
            'newMarketCurrencyEntity' => $marketCurrencyEntity04
        ];

        return [
            'Common Case' => [
                $instrument01,
                $figi01,
                $instrument01,
                0,
                0,
                null,
                null,
                null,
                null
            ],
            'Fill Data Case' => [
                $instrument01,
                $figi01,
                $instrument02,
                1,
                0,
                $tiMarketInstrument01,
                null,
                null,
                null
            ],
            'Stock Fill Data Case' => [
                $instrument03,
                $figi03,
                $instrument02,
                1,
                1,
                $tiMarketInstrument03,
                $ticker03,
                $marketStock03,
                null
            ],
            'Currency Fill Data Case' => [
                $instrument04,
                $figi03,
                $instrument02,
                1,
                0,
                $tiMarketInstrument04,
                $ticker03,
                null,
                $fillCurrencyData04
            ]
        ];
    }

    public function testGetStocks()
    {
        $marketInstrument01 = $this->createBrokerShare(['figi' => 'figi01']);
        $marketInstrument02 = $this->createBrokerShare(['figi' => 'figi02']);

        $broker = $this->createMock(BrokerInterface::class);
        $expected = new ShareCollection([$marketInstrument01, $marketInstrument02]);
        $this->brokerFactory->expects($this->once())->method('getBrokerAdapter')
            ->with('tinkoff2')
            ->willReturn($broker);

        $broker->expects($this->once())->method('getShares')
            ->willReturn(new ShareCollection([$marketInstrument01, $marketInstrument02]));

        $this->assertEquals($expected, $this->fillMarketInstrumentServiceDecorator->getStocks());
    }

    /** @dataProvider dataGetCurrencyByIso */
    public function testGetCurrencyByIso(
        MarketCurrencyEntity $expected,
        string $iso,
        ?MarketCurrencyEntity $marketCurrencyEntity,
        ?BrokerCurrencyCollection $brokerCurrencyCollection,
        ?MarketCurrencyEntity $addedEntity
    ) {
        $this->marketInstrumentService->expects($this->once())->method('getCurrencyByIso')
            ->with($iso)->willReturn($marketCurrencyEntity);

        if (null === $marketCurrencyEntity) {
            $this->brokerFactory->expects($this->once())->method('getBrokerAdapter')
                ->with('tinkoff2')
                ->willReturn($this->broker);

            $this->broker->expects($this->once())->method('getCurrencies')
                ->willReturn($brokerCurrencyCollection);
        }

        if ($addedEntity) {
            $this->marketCurrencyStorage->expects($this->once())->method('addEntity')
                ->with($addedEntity);
        }

        $this->assertEquals($expected, $this->fillMarketInstrumentServiceDecorator->getCurrencyByIso($iso));
    }

    public function dataGetCurrencyByIso()
    {
        $iso01 = 'RUB';
        $marketCurrencyEntity01 = new MarketCurrencyEntity();
        $marketCurrencyEntity01->setId(1);
        $marketCurrencyEntity02 = null;

        $currency01 = new BrokerCurrencyModel(['iso' => 'RUB', 'figi' => 'AAAA-BBBB']);
        $currency02 = new BrokerCurrencyModel(['iso' => 'RUB', 'figi' => 'AAAA-BBBB']);
        $currencyCollection01 = null;
        $currencyCollection02 = new BrokerCurrencyCollection([$currency01, $currency02]);

        $addedEntity01 = null;
        $addedEntity02 = new MarketCurrencyEntity();
        $addedEntity02->setFigi('AAAA-BBBB');
        $addedEntity02->setIso('RUB');

        $expected01 = new MarketCurrencyEntity();
        $expected01->setId(1);

        $expected02 = new MarketCurrencyEntity();
        $expected02->setFigi('AAAA-BBBB');
        $expected02->setIso('RUB');

        return [
            'Common Case' => [
                $expected01,
                $iso01,
                $marketCurrencyEntity01,
                $currencyCollection01,
                $addedEntity01
            ],
            'New Currency Case' => [
                $expected02,
                $iso01,
                $marketCurrencyEntity02,
                $currencyCollection02,
                $addedEntity02
            ]
        ];
    }
}
