<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\OperationCollection as BrokerOperationCollection;
use App\Broker\Interfaces\BrokerInterface;
use App\Collections\OperationCollection;
use App\Entities\MarketInstrumentEntity;
use App\Entities\OperationTypeEntity;
use App\Interfaces\CandleServiceInterface;
use App\Intl\Services\CurrencyService;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Models\OperationFiltersModel;
use App\Services\AccountService;
use App\Services\MarketInstrumentService;
use App\Services\MarketOperationsService;
use App\Services\StockBrokerFactory;
use App\Storages\OperationTypeStorage;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Tests\TITestCase;

class MarketOperationsServiceTest extends TITestCase
{
    private StockBrokerFactory $stockBrokerFactory;
    private OperationTypeStorage $operationTypeStorage;
    private MarketInstrumentService $marketInstrumentService;
    private AccountService $accountService;
    private MarketOperationsService $operationsService;
    private UserService $userService;
    private BrokerStorage $brokerStorage;
    private ItemOperationStorage $itemOperationStorage;
    private CurrencyService $currencyService;
    private ItemStorage $itemStorage;
    private \DateTime $currentDateTime;
    private UserCredentialService $userCredentialService;
    private AMQPStreamConnection $amqpConnection;
    private ItemOperationImportStorage $itemOperationImportStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockBrokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->operationTypeStorage = $this->createMock(OperationTypeStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentService::class);
        $this->accountService = $this->createMock(AccountService::class);
        $this->userService = $this->createMock(UserService::class);
        $this->brokerStorage = $this->createMock(BrokerStorage::class);
        $this->itemOperationStorage = $this->createMock(ItemOperationStorage::class);
        $this->currencyService = $this->createMock(CurrencyService::class);
        $this->itemStorage = $this->createMock(ItemStorage::class);
        $this->currentDateTime = new \DateTime('2023-01-08 23:07:22');
        $this->userCredentialService = $this->createMock(UserCredentialService::class);
        $this->amqpConnection = $this->createMock(AMQPStreamConnection::class);
        $this->itemOperationImportStorage = $this->createMock(ItemOperationImportStorage::class);

        $this->operationsService = new MarketOperationsService(
            $this->stockBrokerFactory,
            $this->operationTypeStorage,
            $this->marketInstrumentService,
            $this->accountService,
            $this->userService,
            $this->brokerStorage,
            $this->itemOperationStorage,
            $this->currencyService,
            $this->itemStorage,
            $this->currentDateTime,
            $this->userCredentialService,
            $this->amqpConnection,
            $this->itemOperationImportStorage
        );
    }

    /** @dataProvider dataGetOperations */
    public function testGetOperations(
        $expected,
        UserBrokerAccountCollection $accounts,
        array $userCredentials,
        $countAccounts,
        array $ops1,
        $exceptionCode,
        OperationFiltersModel $filtres,
        MarketInstrumentService $marketInstrumentService
    ) {
        $clients = [];
        /**
         * @var  $key
         * @var UserBrokerAccountEntity  $account
         */
        foreach ($accounts as $key => $account) {
            $dateTo = $filtres->getDateTo();
            $dateTo = $dateTo->setTime(23, 59, 59);
            $clients[$key] = $this->createMock(BrokerInterface::class);
            $clients[$key]->expects($this->once())->method('getOperations')
                ->with($account->getExternalId(), $filtres->getDateFrom(), $dateTo)
                ->willReturn($ops1[$key]);
        }

        $this->accountService->expects($this->once())->method('getUserBrokerAccounts')->willReturn($accounts);

        if (null !== $exceptionCode) {
            $this->expectExceptionCode($exceptionCode);
        } else {
            $this->stockBrokerFactory->expects($this->exactly(count($accounts)))
                ->method('getBroker')
                ->withConsecutive(...$userCredentials)
                ->willReturn(...$clients);
        }

        $operationService = new MarketOperationsService(
            $this->stockBrokerFactory,
            $this->operationTypeStorage,
            $marketInstrumentService,
            $this->accountService,
            $this->userService,
            $this->brokerStorage,
            $this->itemOperationStorage,
            $this->currencyService,
            $this->itemStorage,
            $this->currentDateTime,
            $this->userCredentialService,
            $this->amqpConnection,
            $this->itemOperationImportStorage
        );

        $this->assertEquals($expected, $operationService->getOperations($filtres));
    }

    public function dataGetOperations()
    {
        $userCredential01 = new UserCredentialEntity();
        $userCredential01->setId(1);
        $account01 = new UserBrokerAccountEntity();
        $account01->setId(1);
        $account01->setExternalId('AAAA-BBBB-CCCC');
        $account01->setUserCredential($userCredential01);

        $userCredential02 = new UserCredentialEntity();
        $userCredential02->setId(2);
        $account02 = new UserBrokerAccountEntity();
        $account02->setId(2);
        $account02->setExternalId('DDDD-EEEE-FFFF');
        $account02->setUserCredential($userCredential02);

        $accounts01 = new UserBrokerAccountCollection([$account01, $account02]);
        $accounts02 = new UserBrokerAccountCollection();
        $countAccounts01 = 2;

        $date01 = new \DateTime('2021-01-01 00:00:00');
        $date02 = new \DateTime('2020-01-01 00:00:00');
        $date03 = new \DateTime('2022-01-01 00:00:00');

        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setFigi('01');

        $marketInstrument02 = new MarketInstrumentEntity();
        $marketInstrument02->setFigi('02');

        $marketInstrument03 = new MarketInstrumentEntity();
        $marketInstrument03->setFigi('03');

        $op01 = $this->createBrokerOperation([
            'id' => '2',
            'date' => $date01,
            'type' => 'Buy',
            'status' => 'Done',
            'figi' => '01',
            'broker' => 'tinkoff2'
        ]);
        $op02 = $this->createBrokerOperation([
            'id' => '3',
            'date' => $date02,
            'type' => 'Sell',
            'status' => 'Done',
            'figi' => '02',
            'broker' => 'tinkoff2'
        ]);
        $op03 = $this->createBrokerOperation([
            'id' => '4',
            'date' => $date03,
            'type' => 'Sell',
            'status' => 'Decline',
            'figi' => '03',
            'broker' => 'tinkoff2'
        ]);
        $op04 = $this->createBrokerOperation([
            'id' => '4',
            'date' => $date03,
            'type' => 'Buy',
            'status' => 'Decline',
            'figi' => null,
            'broker' => 'tinkoff2'
        ]);

        $expectedOp01 = $this->createOperation([
            'id' => '2',
            'date' => $date01,
            'type' => 'Buy',
            'status' => 'Done',
            'figi' => '01',
            'instrument' => $marketInstrument01,
            'broker' => 'tinkoff2'
        ]);
        $expectedOp02 = $this->createOperation([
            'id' => '3',
            'date' => $date02,
            'type' => 'Sell',
            'status' => 'Done',
            'figi' => '02',
            'instrument' => $marketInstrument02,
            'broker' => 'tinkoff2'
        ]);
        $expectedOp03 = $this->createOperation([
            'id' => '4',
            'date' => $date03,
            'type' => 'Sell',
            'status' => 'Decline',
            'figi' => '03',
            'instrument' => $marketInstrument03,
            'broker' => 'tinkoff2'
        ]);
        $expectedOp04 = $this->createOperation([
            'id' => '4',
            'date' => $date03,
            'type' => 'Buy',
            'status' => 'Decline',
            'figi' => null,
            'instrument' => null,
            'broker' => 'tinkoff2'
        ]);

        $operations01 = new BrokerOperationCollection([$op01, $op02]);
        $operations02 = new BrokerOperationCollection([$op03]);
        $operations03 = new BrokerOperationCollection([$op03, $op04]);
        $expected01 = new OperationCollection([$expectedOp03, $expectedOp01, $expectedOp02]);
        $expected02 = new OperationCollection([$expectedOp04, $expectedOp01]);
        $expected03 = new OperationCollection([$expectedOp01, $expectedOp02]);

        $exceptionCode01 = null;
        $exceptionCode02 = 1030;

        $marketInstrumentService01 = $this->getMockBuilder(MarketInstrumentService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $marketInstrumentService01
            ->expects($this->exactly(3))
            ->method('getInstrumentByFigi')
            ->withConsecutive(['01'], ['02'], ['03'])
            ->willReturnOnConsecutiveCalls($marketInstrument01, $marketInstrument02, $marketInstrument03);

        $marketInstrumentService02 = $this->getMockBuilder(MarketInstrumentService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $marketInstrumentService03 = $this->getMockBuilder(MarketInstrumentService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $marketInstrumentService03
            ->expects($this->exactly(3))
            ->method('getInstrumentByFigi')
            ->withConsecutive(['01'], ['02'], ['03'])
            ->willReturnOnConsecutiveCalls($marketInstrument01, $marketInstrument02, $marketInstrument03);

        $marketInstrumentService04 = $this->getMockBuilder(MarketInstrumentService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $marketInstrumentService04
            ->expects($this->exactly(3))
            ->method('getInstrumentByFigi')
            ->withConsecutive(['01'], ['02'], ['03'])
            ->willReturnOnConsecutiveCalls($marketInstrument01, $marketInstrument02, $marketInstrument03);

        $marketInstrumentService05 = $this->getMockBuilder(MarketInstrumentService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $marketInstrumentService05
            ->expects($this->exactly(3))
            ->method('getInstrumentByFigi')
            ->withConsecutive(['01'], ['02'], ['03'])
            ->willReturnOnConsecutiveCalls($marketInstrument01, $marketInstrument02, $marketInstrument03);

        $filters01 = new OperationFiltersModel([]);
        $filters02 = new OperationFiltersModel(['OperationType' => 'Buy']);
        $filters03 = new OperationFiltersModel(['OperationType' => '', 'OperationStatus' => '']);
        $filters04 = new OperationFiltersModel(['OperationStatus' => 'Done']);

        return [
            'Success Case' => [
                $expected01,
                $accounts01,
                [[$userCredential01], [$userCredential02]],
                $countAccounts01,
                [$operations01, $operations02],
                $exceptionCode01,
                $filters01,
                $marketInstrumentService01
            ],
            'Not Broker Accounts' => [
                $expected01,
                $accounts02,
                [],
                $countAccounts01,
                [$operations01, $operations02],
                $exceptionCode02,
                $filters01,
                $marketInstrumentService02
            ],
            'Filtered Case' => [
                $expected02,
                $accounts01,
                [[$userCredential01], [$userCredential02]],
                $countAccounts01,
                [$operations01, $operations03],
                $exceptionCode01,
                $filters02,
                $marketInstrumentService03
            ],
            'Filtered Case Empty String' => [
                $expected01,
                $accounts01,
                [[$userCredential01], [$userCredential02]],
                $countAccounts01,
                [$operations01, $operations02],
                $exceptionCode01,
                $filters03,
                $marketInstrumentService04
            ],
            'Filtered Operation Status' => [
                $expected03,
                $accounts01,
                [[$userCredential01], [$userCredential02]],
                $countAccounts01,
                [$operations01, $operations02],
                $exceptionCode01,
                $filters04,
                $marketInstrumentService05
            ],
        ];
    }

    public function testGetCredentialOperations()
    {
        $credential01 = new UserCredentialEntity();
        $credential01->setId(2);

        $credentialModel01 = new UserCredentialModel(['userCredentialId' => 2]);

        $filters01 = new OperationFiltersModel([
            'dateFrom' => new \DateTime('2021-01-01'),
            'dateTo' => new \DateTime('2022-11-02')
        ]);

        $userCredential01 = new UserCredentialEntity();
        $userCredential01->setId(1);
        $account01 = new UserBrokerAccountEntity();
        $account01->setId(1);
        $account01->setExternalId('AAAA-BBBB-CCCC');
        $account01->setUserCredential($userCredential01);
        $accounts01 = new UserBrokerAccountCollection([$account01]);

        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setFigi('01');

        $operation01 = $this->createBrokerOperation([
            'id' => '2',
            'date' => new \DateTime('2022-01-01'),
            'type' => 'Buy',
            'status' => 'Done',
            'figi' => '01',
            'broker' => 'tinkoff2'
        ]);
        $operations01 = new BrokerOperationCollection([$operation01]);

        $expectedOp01 = $this->createOperation([
            'id' => '2',
            'date' => new \DateTime('2022-01-01'),
            'type' => 'Buy',
            'status' => 'Done',
            'figi' => '01',
            'instrument' => $marketInstrument01,
            'broker' => 'tinkoff2'
        ]);
        $expected01 = new OperationCollection([$expectedOp01]);

        $this->marketInstrumentService
            ->expects($this->once())
            ->method('getInstrumentByFigi')
            ->with('01')
            ->willReturn($marketInstrument01);

        $this->accountService->expects($this->once())->method('getCredentialAccounts')
            ->with($credentialModel01)
            ->willReturn($accounts01);
        $broker01 = $this->createMock(BrokerInterface::class);
        $broker01->expects($this->once())->method('getOperations')
            ->with('AAAA-BBBB-CCCC', $filters01->getDateFrom(), $filters01->getDateTo())
            ->willReturn($operations01);
        $this->stockBrokerFactory->expects($this->once())->method('getBroker')
            ->with($userCredential01)
            ->willReturn($broker01);

        $this->assertEquals(
            $expected01,
            $this->operationsService->getCredentialOperations($filters01, $credentialModel01)
        );
    }

    public function testGetOperationTypeByExternalId()
    {
        $expected = new OperationTypeEntity();
        $expected->setId(1);

        $externalId = 'PayIn';

        $this->operationTypeStorage->expects($this->once())->method('findByExternalId')
            ->with($externalId)
            ->willReturn($expected);

        $this->assertEquals($expected, $this->operationsService->getOperationTypeByExternalId($externalId));
    }
}
