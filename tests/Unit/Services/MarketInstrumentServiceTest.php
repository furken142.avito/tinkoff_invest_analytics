<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Collections\MarketInstrumentCollection;
use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Services\MarketInstrumentService;
use App\Storages\MarketCurrencyStorage;
use App\Storages\MarkerInstrumentStorage;
use PHPUnit\Framework\TestCase;

class MarketInstrumentServiceTest extends TestCase
{
    private MarkerInstrumentStorage $marketInstrumentStorage;
    private MarketCurrencyStorage $marketCurrencyStorage;
    private MarketInstrumentService $marketInstrumentService;

    public function setUp(): void
    {
        parent::setUp();

        $this->marketInstrumentStorage = $this->createMock(MarkerInstrumentStorage::class);
        $this->marketCurrencyStorage = $this->createMock(MarketCurrencyStorage::class);
        $this->marketInstrumentService = new MarketInstrumentService(
            $this->marketInstrumentStorage,
            $this->marketCurrencyStorage
        );
    }

    public function testGetInstrumentByFigi()
    {
        $figi = 'figi';
        $marketInstrument = new MarketInstrumentEntity();

        $this->marketInstrumentStorage->expects($this->once())
            ->method('findOneByFigi')
            ->with('figi')
            ->willReturn($marketInstrument);

        $this->assertEquals($marketInstrument, $this->marketInstrumentService->getInstrumentByFigi($figi));
    }

    /** @dataProvider dataGetStocks */
    public function testGetStocks(
        MarketInstrumentCollection $expected,
        string|null $country,
        string $currency,
        string|null $sector,
        array $stocks
    ) {
        $this->marketInstrumentStorage->expects($this->once())->method('findStocks')
            ->with($currency)
            ->willReturn($stocks);

        $this->assertEquals($expected, $this->marketInstrumentService->getStocks($country, $currency, $sector));
    }

    public function dataGetStocks()
    {
        $currency01 = 'RUB';
        $country01 = 'RU';
        $country02 = null;
        $sector01 = 'Technology';
        $sector02 = null;

        $marketStockEntity01 = new MarketStockEntity();
        $marketStockEntity01->setCountryIso('RU');
        $marketStockEntity01->setSector('Technology');

        $marketStockEntity02 = new MarketStockEntity();
        $marketStockEntity02->setCountryIso('CN');
        $marketStockEntity02->setSector('Technology');

        $marketStockEntity03 = new MarketStockEntity();
        $marketStockEntity03->setCountryIso('RU');
        $marketStockEntity03->setSector('Utilities');

        $marketInstrumentEntity01 = new MarketInstrumentEntity();
        $marketInstrumentEntity01->setId(1);
        $marketInstrumentEntity01->setMarketStock($marketStockEntity01);

        $marketInstrumentEntity02 = new MarketInstrumentEntity();
        $marketInstrumentEntity02->setId(2);
        $marketInstrumentEntity02->setMarketStock($marketStockEntity02);

        $marketInstrumentEntity03 = new MarketInstrumentEntity();
        $marketInstrumentEntity03->setId(3);
        $marketInstrumentEntity03->setMarketStock($marketStockEntity03);

        $stocks01 = [$marketInstrumentEntity01, $marketInstrumentEntity02, $marketInstrumentEntity03];

        $expected01 = new MarketInstrumentCollection([$marketInstrumentEntity01]);
        $expected02 = new MarketInstrumentCollection([$marketInstrumentEntity01, $marketInstrumentEntity02]);
        $expected03 = new MarketInstrumentCollection([$marketInstrumentEntity01, $marketInstrumentEntity03]);

        return [
            'Common Case' => [
                $expected01,
                $country01,
                $currency01,
                $sector01,
                $stocks01
            ],
            'Null Country Case' => [
                $expected02,
                $country02,
                $currency01,
                $sector01,
                $stocks01
            ],
            'Null Sector Case' => [
                $expected03,
                $country01,
                $currency01,
                $sector02,
                $stocks01
            ]
        ];
    }

    public function testGetCurrencyByIso()
    {
        $iso = 'RUB';

        $currency = new MarketCurrencyEntity();
        $currency->setId(1);
        $currency->setIso('RUB');

        $expected = new MarketCurrencyEntity();
        $expected->setId(1);
        $expected->setIso('RUB');

        $this->marketCurrencyStorage->expects($this->once())->method('findOneByIso')
            ->with($iso)->willReturn($currency);

        $this->assertEquals($expected, $this->marketInstrumentService->getCurrencyByIso($iso));
    }
}
