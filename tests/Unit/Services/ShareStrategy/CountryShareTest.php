<?php

declare(strict_types=1);

namespace Tests\Unit\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\MarketCountryEntity;
use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Entities\StrategyCountryEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Intl\Entities\CountryEntity;
use App\Intl\Services\CountryService;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Services\MarketInstrumentService;
use App\Services\ShareStrategy\CountryShare;
use App\Storages\MarketCountryStorage;
use App\Storages\StrategyCountryStorage;
use ArrayIterator;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class CountryShareTest extends TITestCase
{
    private MockObject $marketInstrumentService;
    private \DateTime $currentDateTime;
    private MockObject $countryService;
    private MockObject $strategyCountryStorage;
    private MockObject $marketCountryStorage;
    private CountryShare $countryShare;

    public function setUp(): void
    {
        parent::setUp();

        $this->marketInstrumentService = $this->createMock(MarketInstrumentServiceInterface::class);
        $this->currentDateTime = new \DateTime('2022-06-06 00:06:23');
        $this->countryService = $this->createMock(CountryService::class);
        $this->strategyCountryStorage = $this->createMock(StrategyCountryStorage::class);
        $this->marketCountryStorage = $this->createMock(MarketCountryStorage::class);

        $this->countryShare = new CountryShare(
            $this->currentDateTime,
            $this->marketInstrumentService,
            $this->countryService,
            $this->strategyCountryStorage,
            $this->marketCountryStorage
        );
    }

    public function testCalculateShare()
    {
        $balance = new PortfolioBalanceCollection();
        $instrumentO1 = $this->createBrokerPortfolioPosition(['figi' => 'figi01']);
        $instrumentModel01 = new InstrumentModel();
        $instrumentModel01->setInstrument($instrumentO1);
        $instrumentModel01->setAmount(70);
        $instrumentO2 = $this->createBrokerPortfolioPosition(['figi' => 'figi02']);
        $instrumentModel02 = new InstrumentModel();
        $instrumentModel02->setInstrument($instrumentO2);
        $instrumentModel02->setAmount(20);
        $instrumentO3 = $this->createBrokerPortfolioPosition(['figi' => 'figi03']);
        $instrumentModel03 = new InstrumentModel();
        $instrumentModel03->setInstrument($instrumentO3);
        $instrumentModel03->setAmount(10);

        $instrumentArray = [$instrumentModel01, $instrumentModel02, $instrumentModel03];

        $country02 = new CountryEntity();
        $country02->setName('United States');
        $marketStock01 = new MarketStockEntity();
        $marketStock01->setCountryIso('United States');
        $marketStock01->setCountry($country02);
        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setMarketStock($marketStock01);

        $country02 = new CountryEntity();
        $country02->setName('Russian Federation');
        $marketStock02 = new MarketStockEntity();
        $marketStock02->setCountryIso('Russian Federation');
        $marketStock02->setCountry($country02);
        $marketInstrument02 = new MarketInstrumentEntity();
        $marketInstrument02->setMarketStock($marketStock02);

        $country03 = new CountryEntity();
        $country03->setName('United States');
        $marketStock03 = new MarketStockEntity();
        $marketStock03->setCountryIso('United States');
        $marketStock03->setCountry($country03);
        $marketInstrument03 = new MarketInstrumentEntity();
        $marketInstrument03->setMarketStock($marketStock03);

        $this->marketInstrumentService->expects($this->exactly(3))->method('getInstrumentByFigi')
            ->withConsecutive(['figi01'], ['figi02'], ['figi03'])
            ->willReturnOnConsecutiveCalls($marketInstrument01, $marketInstrument02, $marketInstrument03);

        $stockInstruments = $this->getMockBuilder(InstrumentCollection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockInstruments->expects($this->once())->method('calculateTotalAmount')
            ->with()->willReturn(100);
        $stockInstruments->expects($this->once())->method('getIterator')
            ->with()->willReturn(new ArrayIterator($instrumentArray));


        $instruments = $this->getMockBuilder(InstrumentCollection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $instruments->expects($this->once())->method('filteredByInstrumentType')
            ->with('Stock')->willReturn($stockInstruments);

        $expected01 = new ShareCollection([
            'United States' => 0.8,
            'Russian Federation' => 0.2
        ]);

        $this->assertEqualsWithDelta(
            $expected01,
            $this->countryShare->calculateShare($instruments, $balance),
            0.01
        );
    }

    public function testGetTargetShare()
    {
        $userId = 1;

        $marketCountry01 = new MarketCountryEntity();
        $marketCountry01->setId(1);
        $marketCountry01->setIso('RU');
        $marketCountry02 = new MarketCountryEntity();
        $marketCountry02->setId(2);
        $marketCountry02->setIso('US');
        $marketCountry03 = new MarketCountryEntity();
        $marketCountry03->setId(3);
        $marketCountry03->setIso('CN');

        $strategyCountry01 = new StrategyCountryEntity();
        $strategyCountry01->setMarketCountryId(1);
        $strategyCountry01->setShare(0.6);
        $strategyCountry02 = new StrategyCountryEntity();
        $strategyCountry02->setMarketCountryId(2);
        $strategyCountry02->setShare(0.3);

        $country01 = new CountryEntity();
        $country01->setName('Russian Federation');
        $country02 = new CountryEntity();
        $country02->setName('United States');
        $country03 = new CountryEntity();
        $country03->setName('China');

        $this->marketCountryStorage->expects($this->once())->method('findAll')
            ->willReturn([$marketCountry01, $marketCountry02, $marketCountry03]);

        $this->strategyCountryStorage->expects($this->once())->method('findActiveByUserId')
            ->with($userId)
            ->willReturn([$strategyCountry01, $strategyCountry02]);

        $this->countryService->expects($this->exactly(3))->method('getCountryByIso')
            ->withConsecutive(['RU'], ['US'], ['CN'])
            ->willReturnOnConsecutiveCalls($country01, $country02, $country03);

        $expected = new ShareCollection([
            new TargetShareModel(['externalId' => 'RU', 'name' => 'Russian Federation', 'value' => 0.6]),
            new TargetShareModel(['externalId' => 'US', 'name' => 'United States', 'value' => 0.3]),
            new TargetShareModel(['externalId' => 'CN', 'name' => 'China', 'value' => 0])
        ]);

        $this->assertEquals($expected, $this->countryShare->getTargetShare($userId));
    }

    public function testCalculateTotalAmount()
    {
        $balance = new PortfolioBalanceCollection();

        $stockInstruments = $this->getMockBuilder(InstrumentCollection::class)->disableOriginalConstructor()
            ->getMock();
        $stockInstruments->expects($this->once())->method('calculateTotalAmount')
            ->with()->willReturn(100);

        $instruments = $this->getMockBuilder(InstrumentCollection::class)->disableOriginalConstructor()
            ->getMock();
        $instruments->expects($this->once())->method('filteredByInstrumentType')
            ->with('Stock')->willReturn($stockInstruments);

        $this->assertEquals(100, $this->countryShare->calculateTotalAmount($instruments, $balance));
    }

    public function testUpdateTargetShares()
    {
        $dateTime01 = new \DateTime('2021-01-02 12:00:00');
        $userId01 = 1;

        $currencyEntity01 = new MarketCountryEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('RU');
        $currencyEntity02 = new MarketCountryEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('CN');
        $currencyEntity03 = new MarketCountryEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('US');

        $strategyEntity01 = new StrategyCountryEntity();
        $strategyEntity01->setId(1);
        $strategyEntity01->setIsActive(1);
        $strategyEntity01->setUpdatedDate($dateTime01);

        $targetShare01 = new TargetShareModel(['externalId' => 'RU', 'value' => 0.3]);
        $targetShare02 = new TargetShareModel(['externalId' => 'US', 'value' => 0.2]);
        $targetShare03 = new TargetShareModel(['externalId' => 'CN', 'value' => 0.15]);
        $shareCollection = new ShareCollection([$targetShare01, $targetShare02, $targetShare03]);

        $toUpdateStrategyEntity01 = new StrategyCountryEntity();
        $toUpdateStrategyEntity01->setId(1);
        $toUpdateStrategyEntity01->setIsActive(0);
        $toUpdateStrategyEntity01->setUpdatedDate($this->currentDateTime);

        $toUpdateStrategyEntity02 = new StrategyCountryEntity();
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setShare(0.3);
        $toUpdateStrategyEntity02->setUserId($userId01);
        $toUpdateStrategyEntity02->setMarketCountryId(1);
        $toUpdateStrategyEntity02->setMarketCountry($currencyEntity01);

        $toUpdateStrategyEntity03 = new StrategyCountryEntity();
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setShare(0.2);
        $toUpdateStrategyEntity03->setUserId($userId01);
        $toUpdateStrategyEntity03->setMarketCountryId(2);
        $toUpdateStrategyEntity03->setMarketCountry($currencyEntity02);

        $toUpdateStrategyEntity04 = new StrategyCountryEntity();
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setShare(0.15);
        $toUpdateStrategyEntity04->setUserId($userId01);
        $toUpdateStrategyEntity04->setMarketCountryId(3);
        $toUpdateStrategyEntity04->setMarketCountry($currencyEntity03);

        $toUpdated01 = [
            $toUpdateStrategyEntity01,
            $toUpdateStrategyEntity02,
            $toUpdateStrategyEntity03,
            $toUpdateStrategyEntity04
        ];

        $this->strategyCountryStorage->expects($this->once())
            ->method('findActiveByUserId')
            ->with($userId01)
            ->willReturn([$strategyEntity01]);

        $this->marketCountryStorage->expects($this->exactly($shareCollection->count()))
            ->method('findOneByIso')
            ->withConsecutive(
                [$targetShare01->getExternalId()],
                [$targetShare02->getExternalId()],
                [$targetShare03->getExternalId()]
            )->willReturnOnConsecutiveCalls($currencyEntity01, $currencyEntity02, $currencyEntity03);

        $this->strategyCountryStorage->expects($this->once())->method('updateArrayEntities')
            ->with($toUpdated01);

        $this->countryShare->updateTargetShare($userId01, $shareCollection);
    }
}
