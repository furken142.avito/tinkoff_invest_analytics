<?php

declare(strict_types=1);

namespace Tests\Unit\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketSectorEntity;
use App\Entities\MarketStockEntity;
use App\Entities\StrategySectorEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Services\MarketInstrumentService;
use App\Services\ShareStrategy\SectorShare;
use App\Storages\MarketSectorStorage;
use App\Storages\StrategySectorStorage;
use ArrayIterator;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class SectorShareTest extends TITestCase
{
    private StrategySectorStorage $strategySectorStorage;
    private MarketSectorStorage $marketSectorStorage;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private \DateTime $currentDateTime;
    private SectorShare $sectorShare;

    public function setUp(): void
    {
        parent::setUp();

        $this->strategySectorStorage = $this->createMock(StrategySectorStorage::class);
        $this->marketSectorStorage = $this->createMock(MarketSectorStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentServiceInterface::class);
        $this->currentDateTime = new \DateTime('2022-07-01 18:04:10');

        $this->sectorShare = new SectorShare(
            $this->currentDateTime,
            $this->marketInstrumentService,
            $this->strategySectorStorage,
            $this->marketSectorStorage
        );
    }

    public function testCalculateShare()
    {
        $balance = new PortfolioBalanceCollection();
        $instrumentO1 = $this->createBrokerPortfolioPosition(['figi' => 'figi01']);
        $instrumentModel01 = new InstrumentModel();
        $instrumentModel01->setInstrument($instrumentO1);
        $instrumentModel01->setAmount(70);
        $instrumentO2 = $this->createBrokerPortfolioPosition(['figi' => 'figi02']);
        $instrumentModel02 = new InstrumentModel();
        $instrumentModel02->setInstrument($instrumentO2);
        $instrumentModel02->setAmount(20);
        $instrumentO3 = $this->createBrokerPortfolioPosition(['figi' => 'figi03']);
        $instrumentModel03 = new InstrumentModel();
        $instrumentModel03->setInstrument($instrumentO3);
        $instrumentModel03->setAmount(10);

        $instrumentArray = [$instrumentModel01, $instrumentModel02, $instrumentModel03];

        $marketSock01 = new MarketStockEntity();
        $marketSock01->setSector('Basic Materials');
        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setMarketStock($marketSock01);

        $marketSock02 = new MarketStockEntity();
        $marketSock02->setSector('Basic Materials');
        $marketInstrument02 = new MarketInstrumentEntity();
        $marketInstrument02->setMarketStock($marketSock02);

        $marketSock03 = new MarketStockEntity();
        $marketSock03->setSector('Healthcare');
        $marketInstrument03 = new MarketInstrumentEntity();
        $marketInstrument03->setMarketStock($marketSock03);

        $this->marketInstrumentService->expects($this->exactly(3))->method('getInstrumentByFigi')
            ->withConsecutive(['figi01'], ['figi02'], ['figi03'])
            ->willReturnOnConsecutiveCalls($marketInstrument01, $marketInstrument02, $marketInstrument03);

        $stockInstruments = $this->getMockBuilder(InstrumentCollection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockInstruments->expects($this->once())->method('calculateTotalAmount')
            ->with()->willReturn(100);
        $stockInstruments->expects($this->once())->method('getIterator')
            ->with()->willReturn(new ArrayIterator($instrumentArray));

        $expected01 = new ShareCollection([
            'Basic Materials' => 0.9,
            'Healthcare' => 0.1
        ]);


        $this->assertEqualsWithDelta(
            $expected01,
            $this->sectorShare->calculateShare($stockInstruments, $balance),
            0.01
        );
    }

    public function testGetTargetShare()
    {
        $userId = 1;

        $userId = 1;

        $marketSector01 = new MarketSectorEntity();
        $marketSector01->setId(1);
        $marketSector01->setExternalId('technology');
        $marketSector01->setName('Technology');
        $marketSector02 = new MarketSectorEntity();
        $marketSector02->setId(2);
        $marketSector02->setExternalId('real_estate');
        $marketSector02->setName('Real Estate');
        $marketSector03 = new MarketSectorEntity();
        $marketSector03->setId(3);
        $marketSector03->setExternalId('consumer_defensive');
        $marketSector03->setName('Consumer Defensive');

        $strategySector01 = new StrategySectorEntity();
        $strategySector01->setMarketSectorId(1);
        $strategySector01->setShare(0.6);
        $strategySector02 = new StrategySectorEntity();
        $strategySector02->setMarketSectorId(2);
        $strategySector02->setShare(0.3);

        $this->marketSectorStorage->expects($this->once())->method('findAll')
            ->willReturn([$marketSector01, $marketSector02, $marketSector03]);

        $this->strategySectorStorage->expects($this->once())->method('findActiveByUserId')
            ->with($userId)
            ->willReturn([$strategySector01, $strategySector02]);

        $expected = new ShareCollection([
            new TargetShareModel(['externalId' => 'technology', 'name' => 'Technology', 'value' => 0.6]),
            new TargetShareModel(['externalId' => 'real_estate', 'name' => 'Real Estate', 'value' => 0.3]),
            new TargetShareModel(['externalId' => 'consumer_defensive', 'name' => 'Consumer Defensive', 'value' => 0])
        ]);

        $this->assertEquals($expected, $this->sectorShare->getTargetShare($userId));
    }

    public function testCalculateTotalAmount()
    {
        $balance = new PortfolioBalanceCollection();

        $stockInstruments = $this->getMockBuilder(InstrumentCollection::class)->disableOriginalConstructor()
            ->getMock();
        $stockInstruments->expects($this->once())->method('calculateTotalAmount')
            ->with()->willReturn(100);

        $instruments = $this->getMockBuilder(InstrumentCollection::class)->disableOriginalConstructor()
            ->getMock();
        $instruments->expects($this->once())->method('filteredByInstrumentType')
            ->with('Stock')->willReturn($stockInstruments);

        $this->assertEquals(100, $this->sectorShare->calculateTotalAmount($instruments, $balance));
    }

    public function testUpdateTargetShares()
    {
        $dateTime01 = new \DateTime('2021-06-08 12:10:22');
        $userId01 = 1;

        $currencyEntity01 = new MarketSectorEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setExternalId('technology');
        $currencyEntity02 = new MarketSectorEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setExternalId('utilities');
        $currencyEntity03 = new MarketSectorEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setExternalId('real_estate');

        $strategyEntity01 = new StrategySectorEntity();
        $strategyEntity01->setId(1);
        $strategyEntity01->setIsActive(1);
        $strategyEntity01->setUpdatedDate($dateTime01);

        $targetShare01 = new TargetShareModel(['externalId' => 'technology', 'value' => 0.3]);
        $targetShare02 = new TargetShareModel(['externalId' => 'utilities', 'value' => 0.2]);
        $targetShare03 = new TargetShareModel(['externalId' => 'real_estate', 'value' => 0.15]);
        $shareCollection = new ShareCollection([$targetShare01, $targetShare02, $targetShare03]);

        $toUpdateStrategyEntity01 = new StrategySectorEntity();
        $toUpdateStrategyEntity01->setId(1);
        $toUpdateStrategyEntity01->setIsActive(0);
        $toUpdateStrategyEntity01->setUpdatedDate($this->currentDateTime);

        $toUpdateStrategyEntity02 = new StrategySectorEntity();
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setShare(0.3);
        $toUpdateStrategyEntity02->setUserId($userId01);
        $toUpdateStrategyEntity02->setMarketSectorId(1);
        $toUpdateStrategyEntity02->setMarketSector($currencyEntity01);

        $toUpdateStrategyEntity03 = new StrategySectorEntity();
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setShare(0.2);
        $toUpdateStrategyEntity03->setUserId($userId01);
        $toUpdateStrategyEntity03->setMarketSectorId(2);
        $toUpdateStrategyEntity03->setMarketSector($currencyEntity02);

        $toUpdateStrategyEntity04 = new StrategySectorEntity();
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setShare(0.15);
        $toUpdateStrategyEntity04->setUserId($userId01);
        $toUpdateStrategyEntity04->setMarketSectorId(3);
        $toUpdateStrategyEntity04->setMarketSector($currencyEntity03);

        $toUpdated01 = [
            $toUpdateStrategyEntity01,
            $toUpdateStrategyEntity02,
            $toUpdateStrategyEntity03,
            $toUpdateStrategyEntity04
        ];

        $this->strategySectorStorage->expects($this->once())
            ->method('findActiveByUserId')
            ->with($userId01)
            ->willReturn([$strategyEntity01]);

        $this->marketSectorStorage->expects($this->exactly($shareCollection->count()))
            ->method('findOneByExternalId')
            ->withConsecutive(
                [$targetShare01->getExternalId()],
                [$targetShare02->getExternalId()],
                [$targetShare03->getExternalId()]
            )->willReturnOnConsecutiveCalls($currencyEntity01, $currencyEntity02, $currencyEntity03);

        $this->strategySectorStorage->expects($this->once())->method('updateArrayEntities')
            ->with($toUpdated01);

        $this->sectorShare->updateTargetShare($userId01, $shareCollection);
    }
}
