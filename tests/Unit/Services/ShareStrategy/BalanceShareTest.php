<?php

declare(strict_types=1);

namespace Tests\Unit\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\MarketCurrencyEntity;
use App\Entities\StrategyCurrencyBalanceEntity;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Services\ShareStrategy\BalanceShare;
use App\Storages\MarketCurrencyStorage;
use App\Storages\StrategyCurrencyBalanceStorage;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BalanceShareTest extends TestCase
{
    private BalanceShare $balanceShare;
    private MockObject $strategyCurrencyBalanceStorage;
    private MockObject $marketCurrencyStorage;
    private \DateTime $currentDateTime;

    public function setUp(): void
    {
        parent::setUp();

        $this->currentDateTime = new \DateTime('2022-06-02 20:04:43');
        $this->strategyCurrencyBalanceStorage = $this->createMock(StrategyCurrencyBalanceStorage::class);
        $this->marketCurrencyStorage = $this->createMock(MarketCurrencyStorage::class);
        $this->balanceShare = new BalanceShare(
            $this->currentDateTime,
            $this->strategyCurrencyBalanceStorage,
            $this->marketCurrencyStorage
        );
    }

    public function testGetTargetShare()
    {
        $userId01 = 1;
        $userStrategyEntity01 = new StrategyCurrencyBalanceEntity();
        $userStrategyEntity01->setId(10);
        $userStrategyEntity01->setMarketCurrencyId(1);
        $userStrategyEntity01->setShare(0.35);

        $userStrategyEntity02 = new StrategyCurrencyBalanceEntity();
        $userStrategyEntity02->setId(11);
        $userStrategyEntity02->setMarketCurrencyId(2);
        $userStrategyEntity02->setShare(0.1);

        $currencyEntity01 = new MarketCurrencyEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('USD');

        $currencyEntity02 = new MarketCurrencyEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('RUB');

        $currencyEntity03 = new MarketCurrencyEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('CNY');

        $userStrategy01 = [$userStrategyEntity01, $userStrategyEntity02];
        $currency01 = [$currencyEntity01, $currencyEntity02, $currencyEntity03];

        $this->strategyCurrencyBalanceStorage->expects($this->once())->method('findActiveByUserId')
            ->with(1)->willReturn($userStrategy01);
        $this->marketCurrencyStorage->expects($this->once())->method('findAll')
            ->willReturn($currency01);

        $expected = new ShareCollection(
            [
                new TargetShareModel(['externalId' => 'USD', 'name' => 'USD', 'value' => 0.35]),
                new TargetShareModel(['externalId' => 'RUB', 'name' => 'RUB', 'value' => 0.1]),
                new TargetShareModel(['externalId' => 'CNY', 'name' => 'CNY', 'value' => 0])
            ]
        );
        $this->assertEquals($expected, $this->balanceShare->getTargetShare($userId01));
    }

    public function testCalculateShare()
    {
        $balance01 = new BrokerPortfolioBalanceModel([
            'currency' => 'RUB',
            'amount' => 250,
            'blockedAmount' => 50
        ]);
        $balance02 = new BrokerPortfolioBalanceModel([
            'currency' => 'USD',
            'amount' => 10,
            'blockedAmount' => 0
        ]);
        $balanceCollection01 = new PortfolioBalanceCollection([$balance01, $balance02]);

        $instrument01 = new BrokerPortfolioPositionModel(['figi' => 'BBG0013HGFT4']); //USD
        $instrumentModel01 = new InstrumentModel(['instrument' => $instrument01, 'amount' => 100]);
        $currency01 = new MarketCurrencyEntity();
        $currency01->setIso('USD');
        $instrument02 = new BrokerPortfolioPositionModel(['figi' => 'BBG0013HJJ31']); //EUR
        $instrumentModel02 = new InstrumentModel(['instrument' => $instrument02, 'amount' => 200]);
        $currency02 = new MarketCurrencyEntity();
        $currency02->setIso('EUR');
        $instrument03 = new BrokerPortfolioPositionModel(['figi' => 'BBG0013HRTL0']); //CNY
        $instrumentModel03 = new InstrumentModel(['instrument' => $instrument03, 'amount' => 400]);
        $currency03 = new MarketCurrencyEntity();
        $currency03->setIso('CNY');
        $instrument04 = new BrokerPortfolioPositionModel(['figi' => 'BBG0013']); //Unkonown
        $instrumentModel04 = new InstrumentModel(['instrument' => $instrument04, 'amount' => 800]);
        $currency04 = null;
        $instrument05 = new BrokerPortfolioPositionModel(['figi' => 'BBG0013HGFT4']); //USD
        $instrumentModel05 = new InstrumentModel(['instrument' => $instrument05, 'amount' => 300]);
        $currency05 = new MarketCurrencyEntity();
        $currency05->setIso('USD');
        $instrument06 = new BrokerPortfolioPositionModel(['figi' => 'RUB000UTSTOM']); //RUB
        $instrumentModel06 = new InstrumentModel(['instrument' => $instrument06, 'amount' => 200]);
        $currency06 = new MarketCurrencyEntity();
        $currency06->setIso('RUB');

        $instrumentCollection01 = new InstrumentCollection([
            $instrumentModel01,
            $instrumentModel02,
            $instrumentModel03,
            $instrumentModel04,
            $instrumentModel05,
            $instrumentModel06
        ]);

        $expected01 = new ShareCollection([
            'RUB' => 0.1,
            'USD' => 0.2,
            'EUR' => 0.1,
            'CNY' => 0.2,
            'Other' => 0.4
        ]);

        $this->marketCurrencyStorage->expects($this->exactly(6))->method('findOneByFigi')
            ->withConsecutive(
                ['BBG0013HGFT4'],
                ['BBG0013HJJ31'],
                ['BBG0013HRTL0'],
                ['BBG0013'],
                ['BBG0013HGFT4'],
                ['RUB000UTSTOM']
            )
            ->willReturnOnConsecutiveCalls(
                $currency01,
                $currency02,
                $currency03,
                $currency04,
                $currency05,
                $currency06
            );
        $this->assertEquals(
            $expected01,
            $this->balanceShare->calculateShare($instrumentCollection01, $balanceCollection01)
        );
    }

    public function testUpdateTargetShares()
    {
        $dateTime01 = new \DateTime('2021-01-02 12:00:00');
        $userId01 = 1;

        $currencyEntity01 = new MarketCurrencyEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('RUB');
        $currencyEntity02 = new MarketCurrencyEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('EUR');
        $currencyEntity03 = new MarketCurrencyEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('USD');

        $strategyEntity01 = new StrategyCurrencyBalanceEntity();
        $strategyEntity01->setId(1);
        $strategyEntity01->setIsActive(1);
        $strategyEntity01->setUpdatedDate($dateTime01);

        $targetShare01 = new TargetShareModel(['externalId' => 'RUB', 'value' => 0.3]);
        $targetShare02 = new TargetShareModel(['externalId' => 'USD', 'value' => 0.2]);
        $targetShare03 = new TargetShareModel(['externalId' => 'EUR', 'value' => 0.15]);
        $shareCollection = new ShareCollection([$targetShare01, $targetShare02, $targetShare03]);

        $toUpdateStrategyEntity01 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity01->setId(1);
        $toUpdateStrategyEntity01->setIsActive(0);
        $toUpdateStrategyEntity01->setUpdatedDate($this->currentDateTime);

        $toUpdateStrategyEntity02 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setShare(0.3);
        $toUpdateStrategyEntity02->setUserId($userId01);
        $toUpdateStrategyEntity02->setMarketCurrencyId(1);
        $toUpdateStrategyEntity02->setMarketCurrency($currencyEntity01);

        $toUpdateStrategyEntity03 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setShare(0.2);
        $toUpdateStrategyEntity03->setUserId($userId01);
        $toUpdateStrategyEntity03->setMarketCurrencyId(2);
        $toUpdateStrategyEntity03->setMarketCurrency($currencyEntity02);

        $toUpdateStrategyEntity04 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setShare(0.15);
        $toUpdateStrategyEntity04->setUserId($userId01);
        $toUpdateStrategyEntity04->setMarketCurrencyId(3);
        $toUpdateStrategyEntity04->setMarketCurrency($currencyEntity03);

        $toUpdated01 = [
            $toUpdateStrategyEntity01,
            $toUpdateStrategyEntity02,
            $toUpdateStrategyEntity03,
            $toUpdateStrategyEntity04
        ];

        $this->strategyCurrencyBalanceStorage->expects($this->once())
            ->method('findActiveByUserId')
            ->with($userId01)
            ->willReturn([$strategyEntity01]);

        $this->marketCurrencyStorage->expects($this->exactly($shareCollection->count()))
            ->method('findOneByIso')
            ->withConsecutive(
                [$targetShare01->getExternalId()],
                [$targetShare02->getExternalId()],
                [$targetShare03->getExternalId()]
            )->willReturnOnConsecutiveCalls($currencyEntity01, $currencyEntity02, $currencyEntity03);

        $this->strategyCurrencyBalanceStorage->expects($this->once())->method('updateArrayEntities')
            ->with($toUpdated01);

        $this->balanceShare->updateTargetShare($userId01, $shareCollection);
    }
}
