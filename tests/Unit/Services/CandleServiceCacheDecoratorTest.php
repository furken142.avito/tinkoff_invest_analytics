<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Entities\CandleEntity;
use App\Entities\MarketInstrumentEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Services\CandleService;
use App\Services\CandleServiceCacheDecorator;
use App\Storages\CandleStorage;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class CandleServiceCacheDecoratorTest extends TiTestCase
{
    private MockObject $candleService;
    private MockObject $candleStorage;
    private MockObject $marketInstrumentService;
    private \DateTime $now;

    private CandleServiceCacheDecorator $candleServiceCacheDecorator;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->now = new \DateTime('2022-06-17 19:52:55');
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->candleService = $this->createMock(CandleService::class);
        $this->candleStorage = $this->createMock(CandleStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentServiceInterface::class);

        $this->candleServiceCacheDecorator = new CandleServiceCacheDecorator(
            $this->candleService,
            $this->candleStorage,
            $this->marketInstrumentService,
            $this->now
        );
    }

    /** @dataProvider dataGetTodayCandle */
    public function testGetTodayCandle(
        BrokerPortfolioPositionModel $portfolioInstrument,
        MarketInstrumentEntity $marketInstrument,
        BrokerCandleModel $tiCandle,
        ?CandleEntity $candleEntity,
        string $figi,
        int $marketInstrumentId,
        ?CandleEntity $candleAddedEntity
    ) {
        $today = clone $this->now;
        $cacheDate = clone $this->now;
        $cacheDate->sub((new \DateInterval('PT600S')));

        $this->marketInstrumentService->expects($this->once())->method('getInstrumentByFigi')
            ->with($figi)->willReturn($marketInstrument);

        $this->candleStorage->expects($this->once())->method('findOneByInstrumentIdAndIntervalAndAfterDate')
            ->with($marketInstrumentId, '1day', $today, $cacheDate)
            ->willReturn($candleEntity);

        if ($candleAddedEntity !== null) {
            $this->candleService->expects($this->once())->method('getTodayCandle')
                ->with($portfolioInstrument)
                ->willReturn($tiCandle);

            $candleAddedEntity->setCreatedDate($this->now);
            $this->candleStorage->expects($this->once())->method('addEntity')
                ->with($candleAddedEntity);
        }

        $this->assertEquals($tiCandle, $this->candleServiceCacheDecorator->getTodayCandle($portfolioInstrument));
    }

    public function dataGetTodayCandle()
    {
        $portfolioInstrument01 = $this->createBrokerPortfolioPosition(['figi' => 'figi-AAAA-BBBB']);

        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setId(1);

        $marketInstrumentId01 = 1;

        $figi01 = 'figi-AAAA-BBBB';

        $candleTime01 = new \DateTime('2022-01-01 00:00:00');
        $candleData01 = [
            'open' => 10,
            'close' => 15,
            'high' => 18,
            'low' => 8,
            'volume' => 1000,
            'time' => $candleTime01,
            'isComplete' => null
        ];
        $tiCandle01 = $this->createBrokerCandle($candleData01);

        $candleEntity01 = new CandleEntity();
        $candleEntity01->setOpen($candleData01['open']);
        $candleEntity01->setClose($candleData01['close']);
        $candleEntity01->setHigh($candleData01['high']);
        $candleEntity01->setLow($candleData01['low']);
        $candleEntity01->setVolume($candleData01['volume']);
        $candleEntity01->setTime($candleData01['time']);
        $candleEntity01->setCandleInterval('1day');

        $candleEntity02 = null;

        $candleEntityAdded01 = null;

        $candleEntityAdded02 = new CandleEntity();
        $candleEntityAdded02->setOpen($candleData01['open']);
        $candleEntityAdded02->setClose($candleData01['close']);
        $candleEntityAdded02->setHigh($candleData01['high']);
        $candleEntityAdded02->setLow($candleData01['low']);
        $candleEntityAdded02->setVolume($candleData01['volume']);
        $candleEntityAdded02->setTime($candleData01['time']);
        $candleEntityAdded02->setCandleInterval('1day');
        $candleEntityAdded02->setMarketInstrumentId($marketInstrumentId01);
        $candleEntityAdded02->setMarketInstrument($marketInstrument01);
        $candleEntityAdded02->setFinal(0);

        return [
            'Common Case' => [
                $portfolioInstrument01,
                $marketInstrument01,
                $tiCandle01,
                $candleEntity01,
                $figi01,
                $marketInstrumentId01,
                $candleEntityAdded01
            ],
            'Empty Cache Case' => [
                $portfolioInstrument01,
                $marketInstrument01,
                $tiCandle01,
                $candleEntity02,
                $figi01,
                $marketInstrumentId01,
                $candleEntityAdded02
            ]
        ];
    }

    /** @dataProvider dataGetDayCandleByMarketInstrument */
    public function testGetDayCandleByMarketInstrument(
        BrokerCandleModel $expected,
        int $marketInstrumentId,
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date,
        ?CandleEntity $candle,
        ?BrokerCandleModel $candleModel,
        ?CandleEntity $addedCandle
    ) {
        $this->candleStorage->expects($this->once())->method('findOneFinalByInstrumentIdAndDate')
            ->with($marketInstrumentId, '1day', $date)
            ->willReturn($candle);

        if (null === $candle) {
            $this->candleService->expects($this->once())->method('getDayCandleByMarketInstrument')
                ->with($marketInstrumentEntity, $date)
                ->willReturn($candleModel);

            $this->candleStorage->expects($this->once())->method('addEntity')
                ->with($addedCandle);
        }

        $this->assertEquals(
            $expected,
            $this->candleServiceCacheDecorator->getDayCandleByMarketInstrument($marketInstrumentEntity, $date)
        );
    }

    public function dataGetDayCandleByMarketInstrument()
    {
        $marketInstrumentId01 = 1;

        $marketInstrumentEntity01 = new MarketInstrumentEntity();
        $marketInstrumentEntity01->setId(1);

        $date01 = new \DateTime('2022-06-17 19:16:00');

        $candle01 = new CandleEntity();
        $candle01->setOpen(8);
        $candle01->setClose(9);
        $candle01->setHigh(10);
        $candle01->setLow(7);
        $candle01->setTime(new \DateTime('2022-06-17 17:00:00'));
        $candle01->setVolume(145);
        $candle02 = null;

        $candleModel01 = null;
        $candleModel02 = new BrokerCandleModel([
            'open' => 10,
            'close' => 14,
            'high' => 15,
            'low' => 9,
            'time' => new \DateTime('2022-06-17 20:00:00'),
            'volume' => 184,
            'isComplete' => true
        ]);
        $addedCandle01 = null;
        $addedCandle02 = new CandleEntity();
        $addedCandle02->setMarketInstrumentId(1);
        $addedCandle02->setMarketInstrument($marketInstrumentEntity01);
        $addedCandle02->setFinal(1);
        $addedCandle02->setOpen(10);
        $addedCandle02->setClose(14);
        $addedCandle02->setLow(9);
        $addedCandle02->setHigh(15);
        $addedCandle02->setVolume(184);
        $addedCandle02->setTime(new \DateTime('2022-06-17 20:00:00'));
        $addedCandle02->setCandleInterval('1day');
        $addedCandle02->setCreatedDate($this->now);

        $expected01 = new BrokerCandleModel([
            'open' => 8,
            'close' => 9,
            'high' => 10,
            'low' => 7,
            'time' => new \DateTime('2022-06-17 17:00:00'),
            'volume' => 145
        ]);
        $expected02 = new BrokerCandleModel([
            'open' => 10,
            'close' => 14,
            'high' => 15,
            'low' => 9,
            'time' => new \DateTime('2022-06-17 20:00:00'),
            'volume' => 184,
            'isComplete' => true
        ]);

        return [
            'Common Case' => [
                $expected01,
                $marketInstrumentId01,
                $marketInstrumentEntity01,
                $date01,
                $candle01,
                $candleModel01,
                $addedCandle01
            ],
            'New Candle Case' => [
                $expected02,
                $marketInstrumentId01,
                $marketInstrumentEntity01,
                $date01,
                $candle02,
                $candleModel02,
                $addedCandle02
            ]
        ];
    }
}
