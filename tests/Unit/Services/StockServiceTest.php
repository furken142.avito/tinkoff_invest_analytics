<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Collections\MarketInstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\MarketInstrumentEntity;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Collections\MarketSectorCollection;
use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\MarketSectorService;
use App\Models\ImportStatisticModel;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\MarketInstrumentService;
use App\Services\StockService;
use App\User\Services\UserService;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Tests\TITestCase;

class StockServiceTest extends TITestCase
{
    private MarketInstrumentService $marketInstrumentService;
    private UserService $userService;
    private AMQPStreamConnection $amqpConnection;
    private CountryService $countryService;
    private CurrencyService $currencyService;
    private MarketSectorService $marketSectorService;
    private FillMarketInstrumentServiceDecorator $fillMarketServiceInstrumentDecorator;
    private StockService $stockService;

    public function setUp(): void
    {
        parent::setUp();

        $this->marketInstrumentService = $this->createMock(MarketInstrumentService::class);
        $this->userService = $this->createMock(UserService::class);
        $this->amqpConnection = $this->createMock(AMQPStreamConnection::class);
        $this->countryService = $this->createMock(CountryService::class);
        $this->currencyService = $this->createMock(CurrencyService::class);
        $this->marketSectorService = $this->createMock(MarketSectorService::class);
        $this->fillMarketServiceInstrumentDecorator = $this->createMock(
            FillMarketInstrumentServiceDecorator::class
        );

        $this->stockService = new StockService(
            $this->marketInstrumentService,
            $this->userService,
            $this->amqpConnection,
            $this->countryService,
            $this->currencyService,
            $this->marketSectorService,
            $this->fillMarketServiceInstrumentDecorator
        );
    }

    public function testGetList()
    {
        $country01 = 'RU';
        $currency01 = 'RUB';
        $marketSectorId01 = 1;
        $marketSectorName01 = 'Technology';

        $marketInstrument01 = $this->createMarketInstrument(['figi' => 'figi01']);
        $marketInstrument02 = $this->createMarketInstrument(['figi' => 'figi02']);
        $stockList = new MarketInstrumentCollection([$marketInstrument01, $marketInstrument02]);
        $expected = new MarketInstrumentCollection([$marketInstrument01, $marketInstrument02]);

        $this->marketSectorService->expects($this->once())
            ->method('getNameById')
            ->with($marketSectorId01)
            ->willReturn($marketSectorName01);

        $this->marketInstrumentService->expects($this->once())
            ->method('getStocks')
            ->with($country01, $currency01, $marketSectorName01)
            ->willReturn($stockList);
        $this->assertEquals($expected, $this->stockService->getList($country01, $currency01, $marketSectorId01));
    }

    public function testImport()
    {
        $expected = new ImportStatisticModel([
            'countToImportItems' => 1,
            'countImportedItems' => 1
        ]);

        $ampqlChanel = $this->createMock(AMQPChannel::class);

        $marketInstrument01 = $this->createBrokerShare(['figi' => 'figi01']);
        $marketInstrument02 = $this->createBrokerShare(['figi' => 'figi02']);

        $marketInstrumentEntity01 = new MarketInstrumentEntity();
        $marketInstrumentEntity01->setId(1);
        $marketInstrumentEntity01->setFigi('figi01');
        $marketInstrumentEntity02 = null;

        $figiConsecutive = [['figi01'], ['figi02']];

        $stockList = new ShareCollection([$marketInstrument01, $marketInstrument02]);
        $stockEntityList = [$marketInstrumentEntity01, $marketInstrumentEntity02];

        $this->amqpConnection->expects($this->once())->method('channel')->willReturn($ampqlChanel);
        $this->amqpConnection->expects($this->once())->method('close');
        $ampqlChanel->expects($this->once())->method('queue_declare')
            ->with('main', false, false, false, false);
        $ampqlChanel->expects($this->once())->method('close');

        $this->userService->expects($this->once())->method('getUserId')->willReturn(1);

        $this->fillMarketServiceInstrumentDecorator->expects($this->once())
            ->method('getStocks')
            ->willReturn($stockList);
        $this->marketInstrumentService->expects($this->exactly(count($stockList)))
            ->method('getInstrumentByFigi')
            ->withConsecutive(...$figiConsecutive)
            ->willReturnOnConsecutiveCalls(...$stockEntityList);

        $message01 = [
            'type' => 'stockImport',
            'data' => [
                'figi' => 'figi02',
                'userId' => 1
            ]
        ];

        $ampqMessage = new AMQPMessage(json_encode($message01));

        $ampqlChanel->expects($this->once())->method('basic_publish')
            ->with($ampqMessage, '', 'main');

        $this->assertEquals($expected, $this->stockService->import());
    }

    public function testGetFiltres()
    {
        $countryEntity01 = $this->createCountryEntity(['iso' => 'RU']);
        $countryEntity02 = $this->createCountryEntity(['iso' => 'US']);
        $countryCollection01 = new CountryCollection([$countryEntity01, $countryEntity02]);

        $currency01 = $this->createCurrencyEntity(['iso' => 'RUB']);
        $currencyCollection01 = new CurrencyCollection([$currency01]);

        $sector01 = $this->createMarketSectorEntity(['id' => 1]);
        $sector02 = $this->createMarketSectorEntity(['id' => 2]);
        $sector03 = $this->createMarketSectorEntity(['id' => 3]);
        $sectorCollection01 = new MarketSectorCollection([$sector01, $sector02, $sector03]);

        $expected01 = [
            'country' => $countryCollection01,
            'currency' => $currencyCollection01,
            'sector' => $sectorCollection01
        ];

        $this->countryService->expects($this->once())
            ->method('getList')->willReturn($countryCollection01);
        $this->currencyService->expects($this->once())
            ->method('getList')->willReturn($currencyCollection01);
        $this->marketSectorService->expects($this->once())
            ->method('getList')->willReturn($sectorCollection01);

        $this->assertEquals($expected01, $this->stockService->getFilters());
    }
}
