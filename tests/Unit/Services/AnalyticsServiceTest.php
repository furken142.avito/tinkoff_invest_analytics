<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService as ItemPortfolioService;
use App\Models\PriceModel;
use App\RealEstate\Services\RealEstateService;
use App\Services\AccountService;
use App\Services\AnalyticsService;
use App\Services\CandleService;
use App\Services\ExchangeCurrencyService;
use App\Services\PortfolioService;
use App\User\Entities\UserEntity;
use App\User\Entities\UserSettingsEntity;
use App\User\Services\UserService;
use Tests\TITestCase;

class AnalyticsServiceTest extends TITestCase
{
    private AccountService $accountService;
    private CandleService $candleService;
    private ExchangeCurrencyService $exchangeCurrencyService;
    private AnalyticsService $analyticsService;
    private PortfolioService $portfolioService;
    private \DateTime $currentDateTime;
    private UserService $userService;
    private ItemPortfolioService $itemPortfolioService;
    private OperationService $operationService;
    private RealEstateService $realEstateService;

    public function setUp(): void
    {
        parent::setUp();

        $this->accountService = $this->createMock(AccountService::class);
        $this->candleService = $this->createMock(CandleService::class);
        $this->exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);
        $this->currentDateTime = new \DateTime('2022-06-14 00:45:54');
        $this->userService = $this->createMock(UserService::class);
        $this->portfolioService = $this->createMock(PortfolioService::class);
        $this->itemPortfolioService = $this->createMock(ItemPortfolioService::class);
        $this->operationService = $this->createMock(OperationService::class);
        $this->realEstateService = $this->createMock(RealEstateService::class);

        $this->analyticsService = new AnalyticsService(
            $this->candleService,
            $this->accountService,
            $this->portfolioService,
            $this->exchangeCurrencyService,
            $this->currentDateTime,
            $this->userService,
            $this->itemPortfolioService,
            $this->operationService,
            $this->realEstateService
        );
    }

    /**
     * @dataProvider dataCalculateProfitToday
     * @param array $expected
     * @param string|null $currency
     * @param array $instrumentsConsecutive
     * @param BrokerPortfolioModel $portfolio
     * @param BrokerCandleModel[] $candleConsecutive
     * @param array $currencyConsecutive
     * @param array $exchangeRateConsecutive
     * @param int $exchangeRateCallsCount
     */
    public function testCalculateProfitToday(
        array $expected,
        ?string $currency,
        array $instrumentsConsecutive,
        BrokerPortfolioModel $portfolio,
        array $candleConsecutive,
        array $currencyConsecutive,
        array $exchangeRateConsecutive,
        int $exchangeRateCallsCount
    ) {
        $this->accountService->expects($this->once())->method('getPortfolio')
            ->willReturn($portfolio);

        $this->candleService->expects($this->exactly(count($instrumentsConsecutive)))->method('getTodayCandle')
            ->withConsecutive(...$instrumentsConsecutive)
            ->willReturnOnConsecutiveCalls(...$candleConsecutive);

        $this->exchangeCurrencyService->expects($this->exactly($exchangeRateCallsCount))
            ->method('getExchangeRateByDayCandies')
            ->withConsecutive(...$currencyConsecutive)
            ->willReturnOnConsecutiveCalls(...$exchangeRateConsecutive);

        $this->assertEquals($expected, $this->analyticsService->calculateProfitToday($currency));
    }

    public function dataCalculateProfitToday(): array
    {
        $expectedYield01 = [
            'amount' => 1,
            'currency' => 'RUB'
        ];

        $currentDateTime = new \DateTime('2022-06-14 00:45:54');

        $instrument01 = $this->createBrokerPortfolioPosition([
            'figi' => 'BBG00V0WL952',
            'expectedYield' => $expectedYield01,
            'quantity' => 2
        ]);
        $expectedYield02 = [
            'amount' => 1,
            'currency' => 'USD'
        ];
        $instrument02 = $this->createBrokerPortfolioPosition([
            'figi' => 'BBG000C5HS04',
            'expectedYield' => $expectedYield02,
            'quantity' => 3
        ]);

        $expectedYield03 = [
            'amount' => 1,
            'currency' => 'EUR'
        ];
        $instrument03 = $this->createBrokerPortfolioPosition([
            'figi' => 'BBG00TQHNK64',
            'expectedYield' => $expectedYield03,
            'quantity' => 1
        ]);
        $instruments01 = new PortfolioPositionCollection([$instrument01, $instrument02, $instrument03]);

        $portfolio01 = new BrokerPortfolioModel([
            'positions' => $instruments01,
        ]);

        $currencyConsecutive01 = [['RUB', 'RUB', $currentDateTime], ['USD', 'RUB', $currentDateTime]];
        $currencyConsecutive02 = [['RUB', 'USD', $currentDateTime], ['USD', 'USD', $currentDateTime]];

        $exchangeRateConsecutive01 = [1, 75];
        $exchangeRateConsecutive02 = [0.15, 1];

        $instrumentsConsecutive01 = [[$instrument01], [$instrument02], [$instrument03]];

        $candle01 = $this->createBrokerCandle(['open' => 8, 'close' => 9]);
        $candle02 = $this->createBrokerCandle(['open' => 4, 'close' => 2]);
        $candle03 = null;

        $exchangeRateCallsCount01 = 2;

        $candleConsecutive01 = [$candle01, $candle02, $candle03];

        $expectedPrice01 = new PriceModel([
            'amount' => -448,
            'currency' => 'RUB'
        ]);
        $expectedPrice02 = new PriceModel([
            'amount' => -5.7,
            'currency' => 'USD'
        ]);

        $currency01 = 'RUB';
        $currency02 = null;

        $expected01 = [
            'revenueToday' => $expectedPrice01
        ];
        $expected02 = [
            'revenueToday' => $expectedPrice02
        ];

        return [
            'Common Case' => [
                $expected01,
                $currency01,
                $instrumentsConsecutive01,
                $portfolio01,
                $candleConsecutive01,
                $currencyConsecutive01,
                $exchangeRateConsecutive01,
                $exchangeRateCallsCount01
            ],
            'Default' => [
                $expected02,
                $currency02,
                $instrumentsConsecutive01,
                $portfolio01,
                $candleConsecutive01,
                $currencyConsecutive02,
                $exchangeRateConsecutive02,
                $exchangeRateCallsCount01
            ]
        ];
    }

    /**
     * @param float|null $targetCapital
     * @param UserSettingsEntity|null $userSettings
     * @dataProvider dataGetSummary
     * @return void
     */
    public function testGetSummary(
        ?float $targetCapital,
        UserEntity $user,
        ?UserSettingsEntity $userSettings
    ) {
        $userId01 = 1;
        $this->userService->expects($this->once())->method('getUserId')
            ->willReturn($userId01);
        $this->userService->expects($this->once())->method('getUserSettings')
            ->with($userId01)
            ->willReturn($userSettings);
        $this->userService->expects($this->once())->method('getUser')
            ->willReturn($user);

        $actives = 1000.43;
        $assetActives = 1500.5;
        $realEstateActives = 500.0;
        $yearLater = clone  $this->currentDateTime;
        $yearLater->add(new \DateInterval('P1Y'));
        $predictInterest = 34.0;
        $predictInterestRealEstate = 20.4;

        $yearBefore = clone  $this->currentDateTime;
        $yearBefore->sub(new \DateInterval('P1Y'));
        $lastYearInterest = 7.32;

        $portfolio = new BrokerPortfolioModel();
        $portfolioBalance = new PortfolioBalanceCollection();
        $this->accountService->expects($this->once())->method('getPortfolio')
            ->willReturn($portfolio);
        $this->accountService->expects($this->once())->method('getPortfolioBalance')
            ->willReturn($portfolioBalance);
        $this->portfolioService->expects($this->once())->method('calculateTotalAmount')
            ->with($portfolio, $portfolioBalance)
            ->willReturn($actives);
        $this->itemPortfolioService->expects($this->once())->method('calculateTotalAmount')
            ->with($userId01)
            ->willReturn($assetActives);
        $this->realEstateService->expects($this->once())->method('calculateUserRealEstatePrice')
            ->with($userId01)
            ->willReturn($realEstateActives);

        $this->operationService->expects($this->once())->method('calculateIncome')
            ->with($userId01, $yearBefore, $this->currentDateTime)
            ->willReturn($lastYearInterest);
        $this->itemPortfolioService->expects($this->once())->method('calculateForecastIncome')
            ->with($userId01, $this->currentDateTime, $yearLater)
            ->willReturn($predictInterest);
        $this->realEstateService->expects($this->once())->method('calculateForecastIncome')
            ->with($userId01, $this->currentDateTime, $yearLater)
            ->willReturn($predictInterestRealEstate);

        $passives = 0;
        $actives = $actives + $assetActives + $realEstateActives;
        $predictInterest = $predictInterest + $predictInterestRealEstate;
        $shouldIncrease = $targetCapital - $actives + $passives;
        $capital = $actives - $passives;
        $pension = ($actives - $passives) * 0.04 / 12;
        $futurePension = ($actives + $predictInterest - $passives) * 0.04 / 12;

        if (null === $userSettings || null === $userSettings->getRetirementAge()) {
            $saveSpeed = null;
        } else {
            $retirementDate = clone $user->getBirthday();
            $retirementDate->add(new \DateInterval('P' . $userSettings->getRetirementAge() . 'Y'));
            $saveSpeed = $shouldIncrease / $retirementDate->diff($this->currentDateTime)->days * 365 / 12;
        }

        $expected = [
            'targetCapital' => $targetCapital,
            'actives' => $actives,
            'passives' => $passives,
            'shouldIncrease' => $shouldIncrease,
            'saveSpeed' => $saveSpeed,
            'pension' => $pension,
            'futurePension' => $futurePension,
            'capital' => $capital,
            'predictInterest' => $predictInterest,
            'lastYearInterest' => $lastYearInterest
        ];

        $this->assertEquals($expected, $this->analyticsService->getSummary());
    }

    public function dataGetSummary()
    {
        $user01 = new UserEntity();
        $user01->setBirthday(new \DateTime('2000-11-06'));
        $user01->setId(1);

        $userSettings01 = new UserSettingsEntity();
        $userSettings01->setDesiredPension(100);
        $userSettings01->setRetirementAge(50);

        $userSettings02 = null;

        $targetCapital01 = 30000;
        $targetCapital02 = null;
        return [
            'Common Case' => [
                $targetCapital01,
                $user01,
                $userSettings01
            ],
            'Empty Settings' => [
                $targetCapital02,
                $user01,
                $userSettings02
            ]
        ];
    }
}
