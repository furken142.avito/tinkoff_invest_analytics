<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Interfaces\BrokerInterface;
use App\Broker\Models\BrokerCandleModel;
use App\Collections\CandleCollection;
use App\Entities\CandleEntity;
use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;
use App\Exceptions\ExchangeUnsupportedCurrencyException;
use App\Services\CandleService;
use App\Services\ExchangeCurrencyService;
use App\Services\StockBrokerFactory;
use App\Storages\MarketCurrencyStorage;
use JetBrains\PhpStorm\ArrayShape;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class ExchangeCurrencyServiceTest extends TITestCase
{
    private ExchangeCurrencyService $exchangeCurrency;
    private MarketCurrencyStorage $marketCurrencyStorage;
    private CandleService $candleService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->marketCurrencyStorage = $this->createMock(MarketCurrencyStorage::class);
        $this->candleService = $this->createMock(CandleService::class);

        $this->exchangeCurrency = new ExchangeCurrencyService(
            $this->marketCurrencyStorage,
            $this->candleService
        );
    }

    /**
     * @dataProvider dataExchangeRateByPortfolio
     * @param $fromCurrency
     * @param $toCurrency
     * @param $instruments
     * @param $expected
     * @param $exception
     * @param null $errorMessage
     * @throws ExchangeUnsupportedCurrencyException
     */
    public function testExchangeRateByPortfolio(
        $fromCurrency,
        $toCurrency,
        $instruments,
        $expected,
        $exception,
        $errorMessage = null
    ) {
        if (null !== $exception) {
            $this->expectException($exception);
            if (null !== $errorMessage) {
                $this->expectExceptionMessage($errorMessage);
            }
        }

        $fromCurrencyEntity = new MarketCurrencyEntity();
        $fromCurrencyEntity->setFigi($fromCurrency['figi']);
        $this->marketCurrencyStorage->expects($this->once())->method('findOneByIso')
            ->with($fromCurrency['iso'])
            ->willReturn($fromCurrencyEntity);

        $this->assertEquals(
            $expected,
            $this->exchangeCurrency->getExchangeRateByPortfolio($fromCurrency['iso'], $toCurrency, $instruments)
        );
    }

    public function dataExchangeRateByPortfolio()
    {
        $fromCurrency0 = ['iso' => 'USD', 'figi' => 'BBG0013HGFT4'];
        $fromCurrency1 = ['iso' => 'EUR', 'figi' => 'BBG0013HJJ31'];
        $toCurrency0 = 'RUB';

        $currentPrice01 = [
            'amount' => 69.8,
            'currency' => 'RUB'
        ];
        $currentPrice02 = [
            'amount' => 92,
            'currency' => 'RUB'
        ];
        $avPrice01 = [
            'currency' => 'RUB',
            'amount' => 70
        ];
        $avPrice02 = [
            'currency' => 'USD',
            'amount' => 2
        ];
        $avPrice03 = [
            'currency' => 'RUB',
            'amount' => 90
        ];

        $expectedYield01 = [
            'amount' => -2,
            'currency' => 'RUB'
        ];
        $expectedYield02 = [
            'amount' => 6,
            'currency' => 'RUB'
        ];

        $instr01 = $this->createBrokerPortfolioPosition([
            'figi' => 'BBG0013HGFT4', //USD
            'instrumentType' => 'Currency',
            'quantity' => 10,
            'expectedYield' => $expectedYield01,
            'averagePositionPrice' => $avPrice01,
            'currentPrice' => $currentPrice01
        ]);

        $instr02 = $this->createBrokerPortfolioPosition([
            'averagePositionPrice' => $avPrice02,
        ]);

        $instr03 = $this->createBrokerPortfolioPosition([
            'figi' => 'BBG0013HGFT4', //USD,
            'instrumentType' => 'Currency',
            'quantity' => 0,
            'expectedYield' => $expectedYield01,
            'averagePositionPrice' => $avPrice01
        ]);

        $instr04 = $this->createBrokerPortfolioPosition([
            'figi' => 'BBG0013HJJ31', //EUR
            'instrumentType' => 'Currency',
            'quantity' => 3,
            'expectedYield' => $expectedYield02,
            'averagePositionPrice' => $avPrice03,
            'currentPrice' => $currentPrice02
        ]);

        $instruments0 = new PortfolioPositionCollection([$instr01, $instr02, $instr04]);
        $instruments1 = new PortfolioPositionCollection([$instr02]);
        $instruments2 = new PortfolioPositionCollection([$instr02, $instr03]);

        return [
            'Success Case' => [$fromCurrency0, $toCurrency0, $instruments0, 69.8, null],
            'Convert to EUR' => [$fromCurrency1, $toCurrency0, $instruments0, 92, null],
            'Unsupported from currency' => [
                ['iso' => 'RUB', 'figi' => ''],
                $toCurrency0,
                $instruments0,
                69.8,
                ExchangeUnsupportedCurrencyException::class
            ],
            'Unsupported to currency' => [
                $fromCurrency0,
                'USD',
                $instruments0,
                69.8,
                ExchangeUnsupportedCurrencyException::class
            ],
            'Can\'t calculate exchange rate for this portfolio. From Currency is missing' => [
                $fromCurrency0,
                $toCurrency0,
                $instruments1,
                69.8,
                ExchangeUnsupportedCurrencyException::class,
                'Can\'t calculate exchange rate for this portfolio'
            ],
            'Can\'t calculate exchange rate for this portfolio for zero fromCurrency Balance' => [
                $fromCurrency0,
                $toCurrency0,
                $instruments2,
                69.8,
                ExchangeUnsupportedCurrencyException::class,
                'Can\'t calculate exchange rate for this portfolio'
            ]
        ];
    }

    /** @dataProvider dataGetExchangeRateByDayCandies */
    public function testGetExchangeRateByDayCandies(
        float $expected,
        array|null $exception,
        string $fromCurrency,
        string $toCurrency,
        MarketCurrencyEntity|null $currencyEntityFrom,
        MarketCurrencyEntity|null $currencyEntityTo,
        BrokerCandleModel|null $candleFrom,
        BrokerCandleModel|null $candleTo
    ) {
        $date = new \DateTime('2022-06-20 16:25:13');

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        } else {
            if ($fromCurrency !== 'RUB' && $toCurrency !== 'RUB') {
                $this->marketCurrencyStorage->expects($this->exactly(2))->method('findOneByIso')
                    ->withConsecutive([$fromCurrency], [$toCurrency])
                    ->willReturnOnConsecutiveCalls($currencyEntityFrom, $currencyEntityTo);

                $this->candleService->expects($this->exactly(2))->method('getDayCandleByMarketInstrument')
                    ->withConsecutive([
                        $currencyEntityFrom->getMarketInstrument(), $date
                    ], [
                        $currencyEntityTo->getMarketInstrument(), $date
                    ])
                    ->willReturnOnConsecutiveCalls($candleFrom, $candleTo);
            } elseif ($fromCurrency !== 'RUB') {
                $this->marketCurrencyStorage->expects($this->once())->method('findOneByIso')
                    ->with($fromCurrency)
                    ->willReturn($currencyEntityFrom);

                $this->candleService->expects($this->once())->method('getDayCandleByMarketInstrument')
                    ->with($currencyEntityFrom->getMarketInstrument(), $date)
                    ->willReturn($candleFrom);
            } elseif ($toCurrency !== 'RUB') {
                $this->marketCurrencyStorage->expects($this->once())->method('findOneByIso')
                    ->with($toCurrency)
                    ->willReturn($currencyEntityTo);

                $this->candleService->expects($this->once())->method('getDayCandleByMarketInstrument')
                    ->with($currencyEntityTo->getMarketInstrument(), $date)
                    ->willReturn($candleTo);
            }
        }

        $this->assertEquals(
            $expected,
            $this->exchangeCurrency->getExchangeRateByDayCandies($fromCurrency, $toCurrency, $date)
        );
    }

    public function dataGetExchangeRateByDayCandies(): array
    {
        $expected01 = 60;
        $expected02 = 0.75;

        $exception01 = null;
        $exception02 = [
            'class' => ExchangeUnsupportedCurrencyException::class,
            'code' => 1002
        ];

        $currencyUSD = 'USD';
        $currencyCNY = 'CNY';
        $currencyRUB = 'RUB';
        $currencyEUR = 'EUR';

        $candleEUR = $this->createBrokerCandle([
            'time' => new \DateTime('2022-01-01 04:00:00'),
            'close' => 80
        ]);
        $candleUSD = $this->createBrokerCandle([
            'time' => new \DateTime('2022-01-03 04:00:00'),
            'close' => 60
        ]);
        $candleCNY = null;

        $marketInstrumentUSD = new MarketInstrumentEntity();
        $marketInstrumentUSD->setId(1);
        $marketInstrumentCNY = new MarketInstrumentEntity();
        $marketInstrumentCNY->setId(2);
        $marketInstrumentEUR = new MarketInstrumentEntity();
        $marketInstrumentEUR->setId(3);
        $marketCurrencyEntityUSD = new MarketCurrencyEntity();
        $marketCurrencyEntityUSD->setFigi('BBG0013HGFT4');
        $marketCurrencyEntityUSD->setMarketInstrument($marketInstrumentUSD);
        $marketCurrencyEntityEUR = new MarketCurrencyEntity();
        $marketCurrencyEntityEUR->setFigi('BBG0013HJJ31');
        $marketCurrencyEntityEUR->setMarketInstrument($marketInstrumentEUR);
        $marketCurrencyEntityCNY = null;
        $marketCurrencyEntityRUB = new MarketCurrencyEntity();
        $marketCurrencyEntityRUB->setMarketInstrument(null);

        return [
            'Common Case' => [
                $expected01,
                $exception01,
                $currencyUSD,
                $currencyRUB,
                $marketCurrencyEntityUSD,
                $marketCurrencyEntityRUB,
                $candleUSD,
                null
            ],
            'Convert via RUB Case' => [
                $expected02,
                $exception01,
                $currencyUSD,
                $currencyEUR,
                $marketCurrencyEntityUSD,
                $marketCurrencyEntityEUR,
                $candleUSD,
                $candleEUR
            ],
            'Unsopported currency for convertion Case' => [
                $expected01,
                $exception02,
                $currencyCNY,
                $currencyEUR,
                $marketCurrencyEntityCNY,
                $marketCurrencyEntityUSD,
                $candleCNY,
                $candleUSD
            ]
        ];
    }
}
