<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Adapters\TIClientAdapter;
use App\Adapters\TIClientV2Adapter;
use App\Broker\Interfaces\BrokerInterface;
use App\Exceptions\AccountException;
use App\Services\StockBrokerFactory;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use ATreschilov\TinkoffInvestApiSdk\TIClient as TIClientV2;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TISiteEnum;
use Monolog\Test\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class StockBrokerFactoryTest extends TestCase
{
    private StockBrokerFactory $stockBrokerFactory;
    private UserService $userService;
    private UserCredentialService $userCredentialService;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->createMock(UserService::class);
        $this->userCredentialService = $this->createMock(UserCredentialService::class);
        $this->stockBrokerFactory = new StockBrokerFactory($this->userService, $this->userCredentialService);
    }

    /** @dataProvider dataGetBrokerAdapter */
    public function testGetBrokerAdapter(
        ?BrokerInterface $expected,
        ?array $exception,
        int $userId,
        string $brokerCode,
        ?UserCredentialEntity $userCredentialEntity,
        ?UserCredentialModel $userCredentialModel
    ) {
        $this->userService->expects($this->once())->method('getUserId')
            ->willReturn($userId);

        $this->userCredentialService->expects($this->once())
            ->method('getUserActiveCredential')
            ->with($userId, $brokerCode)
            ->willReturn($userCredentialEntity);

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        } else {
            $this->userCredentialService->expects($this->once())->method('convertEntityToModel')
                ->with($userCredentialEntity)->willReturn($userCredentialModel);
        }

        $this->assertEquals($expected, $this->stockBrokerFactory->getBrokerAdapter($brokerCode));
        $this->assertEquals($expected, $this->stockBrokerFactory->getBrokerAdapter($brokerCode));
    }

    public function dataGetBrokerAdapter()
    {
        $userCredentialEntity01 = new UserCredentialEntity();
        $userCredentialEntity01->setId(1);
        $userCredentialEntity01->setApiKey('AAAA-BBBB-CCCC');

        $userCredentialModel01 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'AAAA-BBBB-CCCC'
        ]);

        $expected01 = new TIClientAdapter(new TIClient('AAAA-BBBB-CCCC', TISiteEnum::EXCHANGE));
        $expected02 = new TIClientV2Adapter(new TIClientV2('AAAA-BBBB-CCCC'));
        $userId01 = 1;
        $brokerCode01 = 'tinkoff';
        $brokerCode02 = 'tinkoff2';
        $brokerCode03 = 'unknown_broker';

        $exception01 = [
            'class' => AccountException::class,
            'code' => 1031
        ];
        $exception02 = [
            'class' => AccountException::class,
            'code' => 1032
        ];
        return [
            'Common Case' => [
                $expected01,
                null,
                $userId01,
                $brokerCode01,
                $userCredentialEntity01,
                $userCredentialModel01
            ],
            'Empty Accounts' => [
                $expected01,
                $exception01,
                $userId01,
                $brokerCode01,
                null,
                null
            ],
            'Tinkoff V2 Case' => [
                $expected02,
                null,
                $userId01,
                $brokerCode02,
                $userCredentialEntity01,
                $userCredentialModel01
            ],
            'Unknows Broker Case' => [
                null,
                $exception02,
                $userId01,
                $brokerCode03,
                $userCredentialEntity01,
                $userCredentialModel01
            ]
        ];
    }

    /** @dataProvider dataGetBroker */
    public function testGetBroker(
        ?BrokerInterface $broker,
        ?array $exception,
        UserCredentialEntity $credentialEntity,
        UserCredentialModel $credentialModel
    ) {
        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }

        $this->userCredentialService->expects($this->once())->method('convertEntityToModel')
            ->with($credentialEntity)->willReturn($credentialModel);

        $this->assertEquals($broker, $this->stockBrokerFactory->getBroker($credentialEntity));
        $this->assertEquals($broker, $this->stockBrokerFactory->getBroker($credentialEntity));
    }

    public function dataGetBroker()
    {
        $apikey01 = 'AAAA-BBBB-CCCC';
        $apikey02 = 'CCCC-DDDD-EEEE';

        $credentialEntity01 = new UserCredentialEntity();
        $credentialEntity01->setId(1);
        $credentialEntity01->setApiKey('AAAA-BBBB-CCCC');
        $credentialEntity01->setBrokerId('tinkoff2');

        $credentialModel01 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'AAAA-BBBB-CCCC',
            'broker' => 'tinkoff2'
        ]);

        $credentialEntity02 = new UserCredentialEntity();
        $credentialEntity02->setId(1);
        $credentialEntity02->setApiKey('CCCC-DDDD-EEEE');
        $credentialEntity02->setBrokerId('tinkoff');

        $credentialModel02 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'CCCC-DDDD-EEEE',
            'broker' => 'tinkoff'
        ]);

        $credentialEntity03 = new UserCredentialEntity();
        $credentialEntity03->setId(1);
        $credentialEntity03->setApiKey('FFFF-GGGG-HHHH');
        $credentialEntity03->setBrokerId('unknown_broker');
        $credentialModel03 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'FFFF-GGGG-HHHH',
            'broker' => 'unknown_broker'
        ]);


        $broker01 = new TIClientV2Adapter(new TIClientV2($apikey01));
        $broker02 = new TIClientAdapter(new TIClient($apikey02, TISiteEnum::EXCHANGE));
        $broker03 = null;

        $exception01 = null;
        $exception02 = [
            'class' => AccountException::class,
            'code' => 1032
        ];

        return [
            'Common Case' => [
                $broker01,
                $exception01,
                $credentialEntity01,
                $credentialModel01
            ],
            'Tinkoff V1 Case' => [
                $broker02,
                $exception01,
                $credentialEntity02,
                $credentialModel02
            ],
            'Unknown Adapter' => [
                $broker03,
                $exception02,
                $credentialEntity03,
                $credentialModel03
            ]
        ];
    }
}
