<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Interfaces\BrokerInterface;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Collections\CandleCollection;
use App\Entities\MarketInstrumentEntity;
use App\Services\CandleService;
use App\Services\StockBrokerFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Stub\Exception;
use Tests\TITestCase;

class CandleServiceTest extends TITestCase
{
    private MockObject $stockBrokerFactory;
    private CandleService $candleService;
    private MockObject $broker;

    public function setUp(): void
    {
        parent::setUp();

        $this->broker = $this->createMock(BrokerInterface::class);
        $this->stockBrokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->candleService = new CandleService($this->stockBrokerFactory);
    }

    /** @dataProvider dataGetTodayCandle
     * @param BrokerCandleModel|null $expected
     * @param $exception
     * @param CandleCollection|Exception $candles
     * @param BrokerPortfolioPositionModel $portfolioPosition
     * @param string $figi
     * @throws \App\Exceptions\AccountException
     * @throws \App\Exceptions\ManyRequestException
     */
    public function testGetTodayCandle(
        ?BrokerCandleModel $expected,
        ?array $exception,
        CandleCollection|Exception $candles,
        BrokerPortfolioPositionModel $portfolioPosition,
        string $figi
    ) {
        $from = new \DateTime();
        $from->setTime(0, 0, 0);
        $to = new \DateTime();
        $to->setTime(23, 59, 59);

        $this->stockBrokerFactory->expects($this->once())->method('getBrokerAdapter')
            ->with(StockBrokerFactory::TINKOFF_V2)->willReturn($this->broker);
        $this->broker->expects($this->once())->method('getHistoryCandles')
            ->with($figi, $from, $to, BrokerCandleIntervalType::INTERVAL_HOUR)
            ->willReturn($candles);

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }
        $this->assertEquals($expected, $this->candleService->getTodayCandle($portfolioPosition));
    }

    public function dataGetTodayCandle()
    {
        $instrument01 = $this->createBrokerPortfolioPosition(['figi' => 'LKOH']);
        $figi01 = 'LKOH';
        $candle01 = $this->createBrokerCandle([
            'open' => 8,
            'close' => 9,
            'time' => new \DateTime('2022-01-01 04:00:00')
        ]);
        $candle02 = $this->createBrokerCandle([
            'open' => 9,
            'close' => 10,
            'time' => new \DateTime('2022-01-01 04:05:00')
        ]);
        return [
            'Common Case' => [
                $candle02,
                null,
                new CandleCollection([$candle01, $candle02]),
                $instrument01,
                $figi01
            ]
        ];
    }

    public function testGetDayCandleByMarketInstrument()
    {
        $date = new \DateTime('2022-06-14 19:27:14');

        $figi = 'LKOH';
        $instrument = new MarketInstrumentEntity();
        $instrument->setFigi($figi);

        $from = new \DateTime('2022-05-30 00:00:00');
        $to = new \DateTime('2022-06-14 23:59:59');

        $candle01 = $this->createBrokerCandle([
            'open' => 8,
            'close' => 9,
            'time' => new \DateTime('2022-01-01 04:00:00')
        ]);
        $candle02 = $this->createBrokerCandle([
            'open' => 9,
            'close' => 10,
            'time' => new \DateTime('2022-01-01 04:05:00')
        ]);
        $candles = new CandleCollection([$candle01, $candle02]);

        $this->stockBrokerFactory->expects($this->once())->method('getBrokerAdapter')
            ->with(StockBrokerFactory::TINKOFF_V2)->willReturn($this->broker);
        $this->broker->expects($this->once())->method('getHistoryCandles')
            ->with($figi, $from, $to, BrokerCandleIntervalType::INTERVAL_DAY)
            ->willReturn($candles);

        $this->assertEquals($candle02, $this->candleService->getDayCandleByMarketInstrument($instrument, $date));
    }
}
