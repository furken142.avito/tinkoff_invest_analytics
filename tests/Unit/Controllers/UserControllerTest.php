<?php

namespace Tests\Unit\Controllers;

use App\Controllers\UserController;
use App\Services\JsonValidatorService;
use App\User\Entities\UserEntity;
use App\User\Entities\UserSettingsEntity;
use App\User\Models\UserSettingsModel;
use App\User\Services\UserService;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Validator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class UserControllerTest extends TestCase
{
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;

    private UserService $userService;
    private JsonValidatorService $jsonValidatorService;

    private UserController $controller;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->createMock(UserService::class);

        $validator = new Validator();
        $errorFormatter = new ErrorFormatter();
        $this->jsonValidatorService = new JsonValidatorService(
            $validator,
            $errorFormatter
        );

        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);

        $this->controller = new UserController($this->jsonValidatorService, $this->userService);
    }

    /**
     * @dataProvider dataGetUserSettings
     */
    public function testGetUserSettings(
        ?UserSettingsEntity $userSettingsEntity,
        array $response
    ) {
        $userId = 1;
        $userEntity = new UserEntity();
        $userEntity->setId($userId);

        $responseJson = json_encode($response);

        $this->userService->expects($this->once())->method('getUser')
            ->willReturn($userEntity);
        $this->userService->expects($this->once())->method('getUserSettings')
            ->with($userId)
            ->willReturn($userSettingsEntity);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->controller->getSettings($this->request, $this->response);
    }

    public function dataGetUserSettings()
    {
        $userSettings01 = new UserSettingsEntity();
        $userSettings01->setDesiredPension(500);
        $userSettings01->setId(1);
        $userSettings01->setRetirementAge(50);

        $userSettings02 = null;

        $response01 = [
            'userSettingsId' => 1,
            'desiredPension' => 500,
            'retirementAge' => 50
        ];
        $response02 = [
            'userSettingsId' => null,
            'desiredPension' => null,
            'retirementAge' => null
        ];

        return [
            'Common Case' => [
                $userSettings01,
                $response01
            ],
            'Empty Settings Case' => [
                $userSettings02,
                $response02
            ]
        ];
    }

    /** @dataProvider dataSetUserSettings */
    public function testSetUserSettings(
        array $body,
        int $statusCode,
        string $responseJson,
        bool $isException
    ) {
        $user = new UserEntity();
        $user->setId(2);

        $userSettingsEntity = new UserSettingsEntity();
        $userSettingsEntity->setId(3);

        $this->request->expects($this->once())->method('getParsedBody')->willReturn($body);

        if ($isException === false) {
            $userSettings = new UserSettingsModel($body);
            $this->userService->expects($this->once())->method('getUser')->willReturn($user);
            $this->userService->expects($this->once())->method('setUserSettings')->with($userSettings);
            $this->userService->expects($this->once())->method('getUserSettings')->with(2)
                ->willReturn($userSettingsEntity);
        }

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with($statusCode);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->controller->setSettings($this->request, $this->response);
    }

    public function dataSetUserSettings()
    {
        $body01 = [
            'desiredPension' => 100,
            'retirementAge' => 50,
            'userSettingsId' => 1
        ];
        $statusCode01 = 200;
        $responseJson01 = json_encode([
            'userSettingsId' => 3,
        ]);
        $isException01 = false;

        $body02 = [
            'userSettingsId' => 1
        ];
        $statusCode02 = 422;
        $responseJson02 = json_encode([
            'error_code' => 1050,
            'error_message' => 'The required properties (desiredPension) are missing'
        ]);
        $isException02 = true;

        return [
            'Common Case' => [
                $body01,
                $statusCode01,
                $responseJson01,
                $isException01
            ],
            'Invalid Request Params' => [
                $body02,
                $statusCode02,
                $responseJson02,
                $isException02
            ]
        ];
    }

    public function testData()
    {
        $userId = 1;
        $login = 'IvanIvanov';
        $isNewUser = 0;

        $response = [
            'user_id' => $userId,
            'login' => $login,
            'isNewUser' => $isNewUser
        ];
        $responseJson = json_encode($response);

        $user = new UserEntity();
        $user->setId($userId);
        $user->setLogin($login);
        $user->setIsNewUser($isNewUser);

        $this->userService->expects($this->once())->method('getUser')
            ->willReturn($user);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->controller->data($this->request, $this->response);
    }
}
