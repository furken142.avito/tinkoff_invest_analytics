<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Collections\MarketInstrumentCollection;
use App\Controllers\StockController;
use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketSectorEntity;
use App\Entities\MarketStockEntity;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Collections\MarketSectorCollection;
use App\Intl\Entities\CountryEntity;
use App\Intl\Entities\CurrencyEntity;
use App\Models\ImportStatisticModel;
use App\Services\JsonValidatorService;
use App\Services\StockService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class StockControllerTest extends TestCase
{
    private MockObject $stockService;
    private MockObject $request;
    private MockObject $response;
    private MockObject $stream;
    private MockObject $jsonValidator;
    private StockController $stockController;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockService = $this->createMock(StockService::class);
        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->jsonValidator = $this->createMock(JsonValidatorService::class);

        $this->stockController = new StockController($this->stockService, $this->jsonValidator);
    }

    public function testImport()
    {
        $importStatistic = new ImportStatisticModel([
            'countToImportItems' => 3,
            'countImportedItems' => 5
        ]);
        $responseJson = json_encode([
            'existsStocks' => 5,
            'toImport' => 3
        ]);
        $this->stockService->expects($this->once())->method('import')->willReturn($importStatistic);
        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->stockController->import($this->request, $this->response);
    }

    /** @dataProvider dataGetList */
    public function testGetList(
        MarketInstrumentCollection $collection,
        array $queryParams,
        array $getListArguments,
        string $responseJson
    ) {
        $this->jsonValidator->expects($this->once())->method('validate')
            ->with('stock_list_get.json', $queryParams)
            ->willReturn(true);
        $this->request->expects($this->once())->method('getQueryParams')
            ->willReturn($queryParams);

        $this->stockService->expects($this->once())->method('getList')
            ->with(...$getListArguments)
            ->willReturn($collection);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->response);
        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->response->expects($this->once())->method('withStatus')->with(200);

        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->stockController->list($this->request, $this->response, []);
    }

    public function dataGetList()
    {
        $country = new CountryEntity();
        $country->setName('Russia');
        $country->setContinent('Europe');
        $country->setIso('RU');

        $marketStock01 = new MarketStockEntity();
        $marketStock01->setSector('Technology');
        $marketStock01->setIndustry('Industry01');
        $marketStock01->setCountryIso('RU');
        $marketStock01->setCountry($country);

        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setId(1);
        $marketInstrument01->setFigi('figi01');
        $marketInstrument01->setIsin('isin01');
        $marketInstrument01->setTicker('ticker01');
        $marketInstrument01->setName('Gazprom');
        $marketInstrument01->setCurrency('USD');
        $marketInstrument01->setMarketStock($marketStock01);

        $marketInstrument02 = new MarketInstrumentEntity();
        $marketInstrument02->setId(2);
        $marketInstrument02->setFigi('figi01');
        $marketInstrument02->setIsin('isin01');
        $marketInstrument02->setTicker('ticker01');
        $marketInstrument02->setName('Gazprom');
        $marketInstrument02->setCurrency('USD');
        $marketInstrument02->setMarketStock($marketStock01);

        $collection = new MarketInstrumentCollection([$marketInstrument01, $marketInstrument02]);

        $responseJson01 = json_encode([
            'stocks' => [
                [
                    'id' => 1,
                    'figi' => 'figi01',
                    'ticker' => 'ticker01',
                    'isin' => 'isin01',
                    'name' => 'Gazprom',
                    'currency' => 'USD',
                    'country' => [
                        'iso' => 'RU',
                        'name' => 'Russia',
                        'continent' => 'Europe'
                    ],
                    'sector' => 'Technology',
                    'industry' => 'Industry01'
                ],
                [
                    'id' => 2,
                    'figi' => 'figi01',
                    'ticker' => 'ticker01',
                    'isin' => 'isin01',
                    'name' => 'Gazprom',
                    'currency' => 'USD',
                    'country' => [
                        'iso' => 'RU',
                        'name' => 'Russia',
                        'continent' => 'Europe'
                    ],
                    'sector' => 'Technology',
                    'industry' => 'Industry01'
                ],
            ]
        ]);

        $queryParams01 = [
            'country' => 'RU',
            'currency' => 'USD',
            'sector' => '2'
        ];
        $queryParams02 = [];
        $queryParams03 = [
            'country' => 'RU',
            'sector' => 'Technology'
        ];

        $getListArguments01 = ['RU', 'USD', 2];
        $getListArguments02 = [null, null, null];
        $getListArguments03 = ['RU', null, 0];


        return [
            'Common Case' => [
                $collection,
                $queryParams01,
                $getListArguments01,
                $responseJson01
            ],
            'Empty Query Params Case' => [
                $collection,
                $queryParams02,
                $getListArguments02,
                $responseJson01
            ],
            'String Sector Case' => [
                $collection,
                $queryParams03,
                $getListArguments03,
                $responseJson01
            ]
        ];
    }

    public function testFilters()
    {
        $responseJson = json_encode([
            'country' => [
                [
                    'iso' => 'RU',
                    'name' => 'Russia',
                    'continent' => 'Europe'
                ],
                [
                    'iso' => 'CN',
                    'name' => 'China',
                    'continent' => 'Asia'
                ]
            ],
            'currency' => [
                [
                    'id' => 1,
                    'iso' => 'EUR',
                    'name' => 'Евро',
                    'symbol' => '€'
                ],
                [
                    'id' => 2,
                    'iso' => 'USD',
                    'name' => 'Доллар США',
                    'symbol' => '$'
                ]
            ],
            'marketSector' => [
                [
                    'id' => 1,
                    'name' => 'Basic Materials'
                ],
                [
                    'id' => 2,
                    'name' => 'Technology'
                ],
            ]
        ]);

        $country01 = new CountryEntity();
        $country01->setIso('RU');
        $country01->setName('Russia');
        $country01->setContinent('Europe');
        $country02 = new CountryEntity();
        $country02->setIso('CN');
        $country02->setName('China');
        $country02->setContinent('Asia');

        $sector01 = new MarketSectorEntity();
        $sector01->setName('Basic Materials');
        $sector01->setId(1);
        $sector02 = new MarketSectorEntity();
        $sector02->setName('Technology');
        $sector02->setId(2);

        $currency01 = new CurrencyEntity();
        $currency01->setId(1);
        $currency01->setName('Евро');
        $currency01->setSymbol('€');
        $currency01->setIso('EUR');

        $currency02 = new CurrencyEntity();
        $currency02->setId(2);
        $currency02->setName('Доллар США');
        $currency02->setSymbol('$');
        $currency02->setIso('USD');

        $countryCollection = new CountryCollection([$country01, $country02]);
        $currencyCollection = new CurrencyCollection([$currency01, $currency02]);
        $sectorCollection = new MarketSectorCollection([$sector01, $sector02]);

        $filters = [
            'country' => $countryCollection,
            'currency' => $currencyCollection,
            'sector' => $sectorCollection
        ];

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->response);
        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->stockService->expects($this->once())->method('getFilters')
            ->willReturn($filters);

        $this->stockController->filters($this->request, $this->response);
    }
}
