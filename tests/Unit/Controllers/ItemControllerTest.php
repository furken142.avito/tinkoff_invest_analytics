<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Controllers\ItemController;
use App\Models\ImportStatisticModel;
use App\Models\OperationFiltersModel;
use App\Services\MarketOperationsService;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class ItemControllerTest extends TestCase
{
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;
    private ItemController $controller;
    private MarketOperationsService $operationService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->operationService = $this->createMock(MarketOperationsService::class);

        $this->controller = new ItemController($this->operationService);
    }

    public function testImportOperations()
    {
        $dateFrom01 = '2022-01-11';
        $dateTo01 = '2022-04-15';
        $queryParams = ['dateFrom' => $dateFrom01, 'dateTo' => $dateTo01];
        $importStatistic01 = new ImportStatisticModel([
            'updated' => 10,
            'added' => 9,
            'skipped' => 8
        ]);
        $statusCode01 = 200;
        $response = [
            'added' => 9,
            'updated' => 10,
            'skipped' => 8
        ];
        $responseJson = json_encode($response);

        $this->request->expects($this->once())->method('getQueryParams')
            ->willReturn($queryParams);
        $this->operationService->expects($this->once())->method('importUserOperations')
            ->with(new OperationFiltersModel($queryParams))
            ->willReturn($importStatistic01);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with($statusCode01)
            ->willReturn($this->response);
        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')->with($responseJson);

        $this->controller->importOperations($this->request, $this->response, []);
    }
}
