<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\MarketInstrumentEntity;
use App\Storages\MarkerInstrumentStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MarketInstrumentStorageTest extends TestCase
{
    private MockObject $entityManager;
    private MockObject $repository;
    private MarkerInstrumentStorage $marketInstrumentStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(ObjectRepository::class);
        $this->marketInstrumentStorage = new MarkerInstrumentStorage($this->entityManager);
    }

    public function testFindOneByFigi()
    {
        $figi = 'BBG000C3J3C9';
        $instrument = new MarketInstrumentEntity();
        $instrument->setFigi('BBG000C3J3C9');

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['figi' => $figi])->willReturn($instrument);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketInstrumentEntity::class)
            ->willReturn($this->repository);

        $this->assertEquals($instrument, $this->marketInstrumentStorage->findOneByFigi($figi));
    }

    /** @dataProvider dataFindStocks */
    public function testFindStocks(
        array $expected,
        array $foundInstruments,
        string|null $currency,
        array $searchCriteria
    ) {
        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketInstrumentEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findBy')
            ->with($searchCriteria)
            ->willReturn($foundInstruments);

        $this->assertEquals($expected, $this->marketInstrumentStorage->findStocks($currency));
    }

    public function dataFindStocks()
    {
        $instrument01 = new MarketInstrumentEntity();
        $instrument01->setFigi('figi01');
        $instrument02 = new MarketInstrumentEntity();
        $instrument02->setFigi('figi02');

        $expected01 = [$instrument01, $instrument02];
        $foundInstruments01 = [$instrument01, $instrument02];

        $currency01 = 'RUB';
        $currency02 = null;

        $searchCriteria01 = ['type' => 'Stock', 'currency' => 'RUB'];
        $searchCriteria02 = ['type' => 'Stock'];

        return [
            'Common Case' => [
                $expected01,
                $foundInstruments01,
                $currency01,
                $searchCriteria01
            ],
            'Find without Currency Case' => [
                $expected01,
                $foundInstruments01,
                $currency02,
                $searchCriteria02
            ]
        ];
    }
}
