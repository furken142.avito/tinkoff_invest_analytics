<?php

declare(strict_types=1);

namespace Tests\Unit\Adapters;

use App\Adapters\TIClientV2Adapter;
use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Models\BrokerAccountModel;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Models\BrokerShareModel;
use App\Collections\CandleCollection;
use ATreschilov\TinkoffInvestApiSdk\Services\InstrumentsService;
use ATreschilov\TinkoffInvestApiSdk\Services\MarketDataService;
use ATreschilov\TinkoffInvestApiSdk\Services\OperationsService;
use ATreschilov\TinkoffInvestApiSdk\Services\UsersService;
use ATreschilov\TinkoffInvestApiSdk\TIClient;
use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Timestamp;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tinkoff\Invest\V1\Account;
use Tinkoff\Invest\V1\Bond;
use Tinkoff\Invest\V1\Currency;
use Tinkoff\Invest\V1\HistoricCandle;
use Tinkoff\Invest\V1\Instrument;
use Tinkoff\Invest\V1\MoneyValue;
use Tinkoff\Invest\V1\Operation;
use Tinkoff\Invest\V1\OperationsResponse;
use Tinkoff\Invest\V1\PortfolioPosition;
use Tinkoff\Invest\V1\PortfolioResponse;
use Tinkoff\Invest\V1\Quotation;
use Tinkoff\Invest\V1\Share;
use Tinkoff\Invest\V1\WithdrawLimitsResponse;

class TIClientV2AdapterTest extends TestCase
{
    private MockObject $client;
    private TIClientV2Adapter $adapter;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(TIClient::class);
        $this->adapter = new TIClientV2Adapter($this->client);
    }

    public function testGetAccounts()
    {
        $userClient = $this->createMock(UsersService::class);

        $openedDate01 = new Timestamp(['seconds' => 1643351932, 'nanos' => 0]);
        $closedDate01 = new Timestamp(['seconds' => 1643351941, 'nanos' => 100000000]);
        $tiData01 = [
            'id' => 'account01',
            'type' => 1,
            'name' => 'Primary Account',
            'status' => 1,
            'opened_date' => $openedDate01,
            'closed_date' => $closedDate01
        ];
        $openedDate02 = new Timestamp(['seconds' => 0, 'nanos' => 0]);
        $closedDate02 = new Timestamp(['seconds' => 0, 'nanos' => 100000000]);
        $tiData02 = [
            'id' => 'account02',
            'type' => 2,
            'name' => 'Secondary Account',
            'status' => 2,
            'opened_date' => $openedDate02,
            'closed_date' => $closedDate02
        ];
        $tiAccount01 = new Account($tiData01);
        $tiAccount02 = new Account($tiData02);

        $data01 = [
            'id' => 'account01',
            'type' => 1,
            'name' => 'Primary Account',
            'status' => 1,
            'openedDate' => new \DateTime('@1643351932'),
            'closedDate' => new \DateTime('@1643351941')
        ];
        $data02 = [
            'id' => 'account02',
            'type' => 2,
            'name' => 'Secondary Account',
            'status' => 2,
            'openedDate' => null,
            'closedDate' => null
        ];
        $account01 = new BrokerAccountModel($data01);
        $account02 = new BrokerAccountModel($data02);

        $this->client->expects($this->once())->method('getUser')->willReturn($userClient);
        $userClient->expects($this->once())
            ->method('getAccounts')
            ->willReturn([$tiAccount01, $tiAccount02]);

        $this->assertEquals(new AccountCollection([$account01, $account02]), $this->adapter->getAccounts());
    }

    public function testGetPortfolio()
    {
        $accountId = 'accountID01';
        $data01 = [
            'figi' => 'APPL',
            'instrument_type' => 'share',
            'quantity' => new Quotation(['units' => 10, 'nano' => 500000000]),
            'average_position_price' => new MoneyValue(['currency' => 'rub', 'units' => 6, 'nano' => 410000000]),
            'expected_yield' => new Quotation(['units' => 4, 'nano' => 300000000]),
            'current_nkd' => new MoneyValue(['currency' => 'rub', 'units' => 1, 'nano' => 410000000]),
            'average_position_price_pt' => new Quotation(['units' => 1, 'nano' => 50000000]),
            'current_price' => new MoneyValue(['currency' => 'rub', 'units' => 16, 'nano' => 410000000]),
        ];
        $data02 = [
            'figi' => 'USD000UTSTOM',
            'instrument_type' => 'currency',
            'quantity' => new Quotation(['units' => 10, 'nano' => 500000000]),
            'average_position_price' => new MoneyValue(['currency' => 'eur', 'units' => 6, 'nano' => 410000000]),
            'expected_yield' => new Quotation(['units' => 4, 'nano' => 300000000]),
            'current_nkd' => new MoneyValue(['currency' => 'eur', 'units' => 1, 'nano' => 410000000]),
            'average_position_price_pt' => new Quotation(['units' => 1, 'nano' => 50000000]),
            'current_price' => new MoneyValue(['currency' => 'eur', 'units' => 16, 'nano' => 410000000]),
        ];
        $data03 = [
            'figi' => 'LKOH',
            'instrument_type' => 'etf',
            'quantity' => new Quotation(['units' => 10, 'nano' => 500000000]),
            'average_position_price' => new MoneyValue(['currency' => 'RUB', 'units' => 6, 'nano' => 410000000]),
            'expected_yield' => new Quotation(['units' => 4, 'nano' => 300000000]),
            'current_nkd' => new MoneyValue(['currency' => 'RUB', 'units' => 0, 'nano' => 0]),
            'average_position_price_pt' => new Quotation(['units' => 1, 'nano' => 500000000]),
            'current_price' => new MoneyValue(['currency' => 'RUB', 'units' => 16, 'nano' => 410000000]),
        ];
        $data04 = [
            'figi' => 'RUA1234',
            'instrument_type' => 'bond',
            'quantity' => new Quotation(['units' => 10, 'nano' => 500000000]),
            'average_position_price' => new MoneyValue(['currency' => 'RUB', 'units' => 6, 'nano' => 410000000]),
            'expected_yield' => new Quotation(['units' => 4, 'nano' => 300000000]),
            'current_nkd' => new MoneyValue(['currency' => '', 'units' => 0, 'nano' => 0]),
            'average_position_price_pt' => new Quotation(['units' => 1, 'nano' => 500000000]),
            'current_price' => new MoneyValue(['currency' => 'RUB', 'units' => 16, 'nano' => 410000000]),
        ];
        $data05 = [
            'figi' => 'FUA1234',
            'instrument_type' => 'future',
            'quantity' => new Quotation(['units' => 10, 'nano' => 500000000]),
            'average_position_price' => new MoneyValue(['currency' => 'usd', 'units' => 6, 'nano' => 410000000]),
            'expected_yield' => new Quotation(['units' => 4, 'nano' => 300000000]),
            'current_nkd' => new MoneyValue(['currency' => '', 'units' => 0, 'nano' => 0]),
            'average_position_price_pt' => new Quotation(['units' => 1, 'nano' => 500000000]),
            'current_price' => new MoneyValue(['currency' => 'usd', 'units' => 16, 'nano' => 410000000]),
        ];
        $tiPosition01 = new PortfolioPosition($data01);
        $tiPosition02 = new PortfolioPosition($data02);
        $tiPosition03 = new PortfolioPosition($data03);
        $tiPosition04 = new PortfolioPosition($data04);
        $tiPosition05 = new PortfolioPosition($data05);
        $tiPositions = [$tiPosition01, $tiPosition02, $tiPosition03, $tiPosition04, $tiPosition05];

        $resultData01 = [
            'figi' => 'APPL',
            'instrumentType' => 'Stock',
            'quantity' => 10.5,
            'averagePositionPrice' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 6.41]),
            'expectedYield' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 4.3]),
            'currentNkd' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 1.41]),
            'currentPrice' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 16.41]),
        ];
        $resultData02 = [
            'figi' => 'BBG0013HGFT4',
            'instrumentType' => 'Currency',
            'quantity' => 10.5,
            'averagePositionPrice' => new BrokerMoneyModel(['currency' => 'EUR', 'amount' => 6.41]),
            'expectedYield' => new BrokerMoneyModel(['currency' => 'EUR', 'amount' => 4.3]),
            'currentNkd' => new BrokerMoneyModel(['currency' => 'EUR', 'amount' => 1.41]),
            'currentPrice' => new BrokerMoneyModel(['currency' => 'EUR', 'amount' => 16.41]),
        ];
        $resultData03 = [
            'figi' => 'LKOH',
            'instrumentType' => 'Etf',
            'quantity' => 10.5,
            'averagePositionPrice' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 6.41]),
            'expectedYield' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 4.3]),
            'currentNkd' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 0]),
            'currentPrice' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 16.41]),
        ];
        $resultData04 = [
            'figi' => 'RUA1234',
            'instrumentType' => 'Bond',
            'quantity' => 10.5,
            'averagePositionPrice' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 6.41]),
            'expectedYield' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 4.3]),
            'currentNkd' => null,
            'currentPrice' => new BrokerMoneyModel(['currency' => 'RUB', 'amount' => 16.41]),
        ];
        $resultData05 = [
            'figi' => 'FUA1234',
            'instrumentType' => 'future',
            'quantity' => 10.5,
            'averagePositionPrice' => new BrokerMoneyModel(['currency' => 'USD', 'amount' => 6.41]),
            'expectedYield' => new BrokerMoneyModel(['currency' => 'USD', 'amount' => 4.3]),
            'currentNkd' => null,
            'currentPrice' => new BrokerMoneyModel(['currency' => 'USD', 'amount' => 16.41]),
        ];
        $position01 = new BrokerPortfolioPositionModel($resultData01);
        $position02 = new BrokerPortfolioPositionModel($resultData02);
        $position03 = new BrokerPortfolioPositionModel($resultData03);
        $position04 = new BrokerPortfolioPositionModel($resultData04);
        $position05 = new BrokerPortfolioPositionModel($resultData05);
        $positionCollection01 = new PortfolioPositionCollection([
            $position01,
            $position02,
            $position03,
            $position04,
            $position05
        ]);
        $portfolio = new BrokerPortfolioModel(['positions' => $positionCollection01]);

        $operationsService = $this->createMock(OperationsService::class);
        $portfolioResponse = $this->createMock(PortfolioResponse::class);

        $this->client->expects($this->once())->method('getOperations')
            ->willReturn($operationsService);
        $operationsService->expects($this->once())->method('getPortfolio')
            ->with($accountId)
            ->willReturn($portfolioResponse);
        $portfolioResponse->expects($this->once())->method('getPositions')
            ->willReturn($tiPositions);

        $this->assertEquals($portfolio, $this->adapter->getPortfolio($accountId));
    }

    public function testGetInstrumentByFigi()
    {
        $figi01 = 'Figi';
        $instrumentResponse = new Instrument([
            'figi' => 'Figi',
            'ticker' => 'ticker',
            'class_code' => 'class_code',
            'isin' => 'isin',
            'lot' => 10,
            'currency' => 'rub',
            'klong' => new Quotation(['units' => 5, 'nano' => 410000000]),
            'kshort' => new Quotation(['units' => 7, 'nano' => 230000000]),
            'dlong' => new Quotation(['units' => 10, 'nano' => 490000000]),
            'dshort' => new Quotation(['units' => 1, 'nano' => 0]),
            'dlong_min' => new Quotation(['units' => 8, 'nano' => 410000000]),
            'dshort_min' => new Quotation(['units' => 6, 'nano' => 410000000]),
            'short_enabled_flag' => true,
            'name' => 'Apple',
            'exchange' => 'SPB',
            'country_of_risk' => 'RU',
            'country_of_risk_name' => 'Russia',
            'instrument_type' => 'etf',
            'trading_status' => 2,
            'buy_available_flag' => true,
            'otc_flag' => true,
            'sell_available_flag' => false,
            'min_price_increment' => new Quotation(['units' => 0, 'nano' => 100000000]),
            'api_trade_available_flag' => true
        ]);

        $instrument = new BrokerInstrumentModel([
            'figi' => 'Figi',
            'currency' => 'RUB',
            'ticker' => 'ticker',
            'type' => 'Etf',
            'name' => 'Apple',
            'isin' => 'isin',
            'minPriceIncrement' => 0.1,
            'lot' => 10
        ]);

        $instrumentService = $this->createMock(InstrumentsService::class);
        $this->client->expects($this->once())->method('getInstruments')->willReturn($instrumentService);
        $instrumentService->expects($this->once())->method('getInstrumentBy')
            ->with(1, null, $figi01)
            ->willReturn($instrumentResponse);

        $this->assertEquals($instrument, $this->adapter->getInstrumentByFigi($figi01));
    }

    public function testGetCurrencies()
    {
        $currency01 = new Currency([
            'figi' => 'AAAA-BBBB-CCCC',
            'iso_currency_name' => 'usd'
        ]);
        $currency02 = new Currency([
            'figi' => 'DDDD-EEEE-FFFF',
            'iso_currency_name' => 'cny'
        ]);
        $currencyList01 = new RepeatedField(GPBType::MESSAGE, Currency::class);
        $currencyList01->offsetSet(null, $currency01);
        $currencyList01->offsetSet(null, $currency02);

        $currencyModel01 = new BrokerCurrencyModel(['iso' => 'USD', 'figi' => 'AAAA-BBBB-CCCC']);
        $currencyModel02 = new BrokerCurrencyModel(['iso' => 'CNY', 'figi' => 'DDDD-EEEE-FFFF']);
        $expected01 = new BrokerCurrencyCollection([$currencyModel01, $currencyModel02]);

        $instrumentService = $this->createMock(InstrumentsService::class);
        $this->client->expects($this->once())->method('getInstruments')->willReturn($instrumentService);
        $instrumentService->expects($this->once())->method('getCurrencies')
            ->willReturn($currencyList01);

        $this->assertEquals($expected01, $this->adapter->getCurrencies());
    }

    public function testGetOperations()
    {
        $account01 = 'Primary Account';
        $from01 = new \DateTime('2022-02-10 18:18:14');
        $to01 = new \DateTime('2022-03-17 14:11:45');

        $date01 = new \DateTime('2022-02-13 01:32:08');
        $date02 = new \DateTime('2022-02-13 01:33:09');

        $operation1 = new Operation(
            [
                'id' => '1',
                'parent_operation_id' => '2',
                'currency' => 'rub',
                'payment' => new MoneyValue([
                    'units' => 10,
                    'nano' => 150000000,
                    'currency' => 'eur'
                ]),
                'price' => new MoneyValue([
                    'units' => 5,
                    'nano' => 50000000,
                    'currency' => 'usd'
                ]),
                'state' => 1,
                'quantity' => 2,
                'quantity_rest' => 0,
                'figi' => 'APPL',
                'date' => new Timestamp(['seconds' => 1644715928, 'nanos' => 0]),
                'operation_type' => 1,
                'instrument_type' => 'futures'
            ]
        );
        $operation2 = new Operation(
            [
                'id' => '2',
                'parent_operation_id' => '',
                'currency' => 'usd',
                'payment' => new MoneyValue([
                    'units' => 11,
                    'nano' => 0,
                    'currency' => 'eur'
                ]),
                'price' => new MoneyValue([
                    'units' => 0,
                    'nano' => 0,
                    'currency' => 'usd'
                ]),
                'state' => 2,
                'quantity' => 2,
                'quantity_rest' => 0,
                'figi' => '',
                'date' => new Timestamp(['seconds' => 1644715989, 'nanos' => 0]),
                'operation_type' => 2,
                'instrument_type' => ''
            ]
        );

        $expectedOperation01 = new BrokerOperationModel([
            'id' => '1',
            'parentOperationId' => '2',
            'currency' => 'RUB',
            'payment' => new BrokerMoneyModel([
                'amount' => 10.15,
                'currency' => 'EUR'
            ]),
            'price' => new BrokerMoneyModel([
                'amount' => 5.05,
                'currency' => 'USD'
            ]),
            'status' => 'Done',
            'quantity' => 2,
            'figi' => 'APPL',
            'date' => $date01,
            'type' => 'Input',
            'instrumentType' => 'Futures',
            'broker' => 'tinkoff2'
        ]);

        $expectedOperation02 = new BrokerOperationModel([
            'id' => '2',
            'parentOperationId' => null,
            'currency' => 'USD',
            'payment' => new BrokerMoneyModel([
                'amount' => 11,
                'currency' => 'EUR'
            ]),
            'price' => null,
            'status' => 'Decline',
            'quantity' => 2,
            'figi' => null,
            'date' => $date02,
            'type' => 'Tax',
            'instrumentType' => null,
            'broker' => 'tinkoff2'
        ]);
        $expected01 = new OperationCollection([$expectedOperation01, $expectedOperation02]);

        $clientOperations = new RepeatedField(GPBType::MESSAGE, Operation::class);
        $clientOperations->offsetSet(null, $operation1);
        $clientOperations->offsetSet(null, $operation2);

        $operationResponse = $this->createMock(OperationsResponse::class);
        $operationResponse->expects($this->once())->method('getOperations')
            ->with($account01, $from01, $to01)
            ->willReturn($clientOperations);
        $this->client->expects($this->once())->method('getOperations')
            ->willReturn($operationResponse);

        $this->assertEquals($expected01, $this->adapter->getOperations($account01, $from01, $to01));
    }

    /** @dataProvider dataGetHistoryCandles */
    public function testGetHistoryCandles(
        CandleCollection $candleCollection,
        string $figi,
        \DateTime $from,
        \DateTime $to,
        int $brokerInterval,
        string $interval,
        RepeatedField $candles,
        RepeatedField $bonds
    ) {
        $marketData = $this->createMock(MarketDataService::class);
        $marketData->expects($this->once())->method('getCandles')
            ->with($figi, $from, $to, $brokerInterval)
            ->willReturn($candles);

        $instrumentService = $this->createMock(InstrumentsService::class);
        $instrumentService->expects($this->once())->method('getBonds')->willReturn($bonds);
        $this->client->expects($this->once())->method('getInstruments')->willReturn($instrumentService);
        $this->client->expects($this->once())->method('getMarketData')->willReturn($marketData);

        $this->assertEquals($candleCollection, $this->adapter->getHistoryCandles($figi, $from, $to, $interval));
    }

    public function dataGetHistoryCandles()
    {
        $figi01 = 'bond01';
        $figi02 = 'stock01';
        $from01 = new \DateTime('2022-02-01 12:00:12');
        $to01 = new \DateTime('2022-02-02 09:12:47');
        $brokerInterval = 5;
        $interval = '1day';

        $bond01 = new Bond([
            'figi' => 'bond01',
            'nominal' => new MoneyValue([
                'currency' => 'rub',
                'units' => 1000,
                'nano' => 0
            ])
        ]);
        $bond02 = new Bond([
            'figi' => 'bond02',
            'nominal' => new MoneyValue([
                'currency' => 'rub',
                'units' => 97,
                'nano' => 190000000
            ])
        ]);
        $bonds01 = new RepeatedField(GPBType::MESSAGE, Bond::class);
        $bonds01->offsetSet(null, $bond01);
        $bonds01->offsetSet(null, $bond02);

        $candle01 = new HistoricCandle([
            'open' => new Quotation(['units' => 10, 'nano' => 0]),
            'close' => new Quotation(['units' => 11, 'nano' => 0]),
            'low' => new Quotation(['units' => 9, 'nano' => 0]),
            'high' => new Quotation(['units' => 12, 'nano' => 0]),
            'volume' => 100,
            'time' => new Timestamp([
                'seconds' => 1644562372,
                'nanos' => 0
            ]),
            'is_complete' => true
        ]);
        $candle02 = new HistoricCandle([
            'open' => new Quotation(['units' => 7, 'nano' => 0]),
            'close' => new Quotation(['units' => 8, 'nano' => 0]),
            'low' => new Quotation(['units' => 6, 'nano' => 0]),
            'high' => new Quotation(['units' => 11, 'nano' => 0]),
            'volume' => 200,
            'time' => new Timestamp([
                'seconds' => 1644562172,
                'nanos' => 0
            ]),
            'is_complete' => false
        ]);
        $candles01 = new RepeatedField(GPBType::MESSAGE, HistoricCandle::class);
        $candles01->offsetSet(null, $candle01);
        $candles01->offsetSet(null, $candle02);

        $brokerCandle01 = new BrokerCandleModel([
            'open' => 100,
            'close' => 110,
            'low' => 90,
            'high' => 120,
            'volume' => 100,
            'time' => new \DateTime('2022-02-11 06:52:52'),
            'isComplete' => true
        ]);
        $brokerCandle02 = new BrokerCandleModel([
            'open' => 70,
            'close' => 80,
            'low' => 60,
            'high' => 110,
            'volume' => 200,
            'time' => new \DateTime('2022-02-11 06:49:32'),
            'isComplete' => false
        ]);
        $brokerCandle03 = new BrokerCandleModel([
            'open' => 10,
            'close' => 11,
            'low' => 9,
            'high' => 12,
            'volume' => 100,
            'time' => new \DateTime('2022-02-11 06:52:52'),
            'isComplete' => true
        ]);
        $brokerCandle04 = new BrokerCandleModel([
            'open' => 7,
            'close' => 8,
            'low' => 6,
            'high' => 11,
            'volume' => 200,
            'time' => new \DateTime('2022-02-11 06:49:32'),
            'isComplete' => false
        ]);
        $candleCollection01 = new CandleCollection([$brokerCandle01, $brokerCandle02]);
        $candleCollection02 = new CandleCollection([$brokerCandle03, $brokerCandle04]);

        return [
            'Common Case' => [
                $candleCollection02,
                $figi02,
                $from01,
                $to01,
                $brokerInterval,
                $interval,
                $candles01,
                $bonds01
            ],
            'Band Case' => [
                $candleCollection01,
                $figi01,
                $from01,
                $to01,
                $brokerInterval,
                $interval,
                $candles01,
                $bonds01
            ]
        ];
    }

    public function testGetShares()
    {
        $tiShare01 = new Share([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'min_price_increment' => new Quotation(['units' => 0, 'nano' => 100000000]),
            'currency' => 'usd',
            'lot' => 1,
            'name' => 'name01',
            'sector' => 'sector',
            'country_of_risk' => 'US'
        ]);
        $tiShare02 = new Share([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'min_price_increment' => new Quotation(['units' => 1, 'nano' => 0]),
            'currency' => 'rub',
            'lot' => 10,
            'name' => 'name02',
            'sector' => 'it',
            'country_of_risk' => 'CN'
        ]);
        $shares = new RepeatedField(GPBType::MESSAGE, Share::class);
        $shares->offsetSet(null, $tiShare01);
        $shares->offsetSet(null, $tiShare02);

        $instrumentModel01 = new BrokerInstrumentModel([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'min_price_increment' => 0.1,
            'currency' => 'USD',
            'lot' => 1,
            'name' => 'name01',
            'type' => 'Stock'
        ]);
        $share01 = new BrokerShareModel([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'min_price_increment' => 0.1,
            'currency' => 'USD',
            'lot' => 1,
            'name' => 'name01',
            'sector' => 'Other',
            'country' => 'US',
            'industry' => null,
            'instrument' => $instrumentModel01
        ]);
        $instrumentModel02 = new BrokerInstrumentModel([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'min_price_increment' => 1,
            'currency' => 'RUB',
            'lot' => 10,
            'name' => 'name02',
            'type' => 'Stock'
        ]);
        $share02 = new BrokerShareModel([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'min_price_increment' => 1,
            'currency' => 'RUB',
            'lot' => 10,
            'name' => 'name02',
            'sector' => 'Technology',
            'country' => 'CN',
            'industry' => null,
            'instrument' => $instrumentModel02
        ]);
        $shareCollection = new ShareCollection([$share01, $share02]);

        $instrumentService = $this->createMock(InstrumentsService::class);
        $instrumentService->expects($this->once())->method('getShares')->willReturn($shares);

        $this->client->expects($this->once())->method('getInstruments')->willReturn($instrumentService);

        $this->assertEquals($shareCollection, $this->adapter->getShares());
    }

    /** @dataProvider dataGetShareByFigi */
    public function testGetShareByFigi(
        ?BrokerShareModel $expected,
        ?Share $share,
        string $figi
    ) {
        $instrumentService = $this->createMock(InstrumentsService::class);
        $instrumentService->expects($this->once())->method('getShareBy')
            ->with(1, null, $figi)
            ->willReturn($share);

        $this->client->expects($this->once())->method('getInstruments')->willReturn($instrumentService);

        $this->assertEquals($expected, $this->adapter->getShareByFigi($figi));
    }

    public function dataGetShareByFigi()
    {
        $tiShare01 = new Share([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'min_price_increment' => new Quotation(['units' => 0, 'nano' => 100000000]),
            'currency' => 'usd',
            'lot' => 1,
            'name' => 'name01',
            'sector' => 'financial_services',
            'country_of_risk' => 'US'
        ]);
        $tiShare02 = null;

        $instrument = new BrokerInstrumentModel([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'min_price_increment' => 0.1,
            'currency' => 'USD',
            'lot' => 1,
            'name' => 'name01',
            'type' => 'Stock'
        ]);
        $share01 = new BrokerShareModel([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'min_price_increment' => 0.1,
            'currency' => 'USD',
            'lot' => 1,
            'name' => 'name01',
            'sector' => 'Financial Services',
            'country' => 'US',
            'industry' => null,
            'instrument' => $instrument
        ]);
        $share02 = null;

        $figi01 = 'figi01';

        return [
            'Common Case' => [
                $share01,
                $tiShare01,
                $figi01
            ],
            'Not found case' => [
                $share02,
                $tiShare02,
                $figi01
            ]
        ];
    }

    public function testGetPortfolioBalance()
    {
        $accountId01 = 'AAAA-BBBB';
        $balance01 = new MoneyValue(['currency' => 'usd', 'units' => 100, 'nano' => 190000000]);
        $balance02 = new MoneyValue(['currency' => 'rub', 'units' => 10, 'nano' => 90000000]);

        $blockedBalance01 = new MoneyValue(['currency' => 'rub', 'units' => 2, 'nano' => 10000000]);
        $blockedBalance02 = new MoneyValue(['currency' => 'usd', 'units' => 2, 'nano' => 0]);
        $blockedBalance03 = new MoneyValue(['currency' => 'cny', 'units' => 1, 'nano' => 0]);

        $balanceResponse01 = new WithdrawLimitsResponse([
            'money' => [$balance01, $balance02],
            'blocked' => [$blockedBalance01, $blockedBalance02, $blockedBalance03]
        ]);

        $balanceModel01 = new BrokerPortfolioBalanceModel([
            'amount' => 100.19,
            'blockedAmount' => 2,
            'currency' => 'USD'
        ]);
        $balanceModel02 = new BrokerPortfolioBalanceModel([
            'amount' => 10.09,
            'blockedAmount' => 2.01,
            'currency' => 'RUB'
        ]);
        $expected01 = new PortfolioBalanceCollection([$balanceModel01, $balanceModel02]);

        $operationService = $this->createMock(OperationsService::class);
        $this->client->expects($this->once())->method('getOperations')
            ->willReturn($operationService);
        $operationService->expects($this->once())->method('getWithdrawLimits')
            ->with($accountId01)->willReturn($balanceResponse01);

        $this->assertEquals($expected01, $this->adapter->getPortfolioBalance($accountId01));
    }

    public function testGetCurrencyByFigi()
    {
        $figi = 'BBG0013HJJ31';
        $currency = new Currency();
        $currency->setFigi($figi);
        $currency->setIsoCurrencyName('eur');

        $expected = new BrokerCurrencyModel(['iso' => 'EUR', 'figi' => $figi]);

        $instrumentService = $this->createMock(InstrumentsService::class);
        $this->client->expects($this->once())->method('getInstruments')->willReturn($instrumentService);
        $instrumentService->expects($this->once())->method('getCurrencyBy')
            ->with(1, null, $figi)->willReturn($currency);

        $this->assertEquals($expected, $this->adapter->getCurrencyByFigi($figi));
    }
}
