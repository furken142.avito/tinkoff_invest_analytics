<?php

declare(strict_types=1);

namespace Tests\Unit\Adapters;

use App\Adapters\TIClientAdapter;
use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Models\BrokerShareModel;
use App\Collections\CandleCollection;
use jamesRUS52\TinkoffInvest\TIAccount;
use jamesRUS52\TinkoffInvest\TICandle;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TIInstrument;
use jamesRUS52\TinkoffInvest\TIOperation;
use jamesRUS52\TinkoffInvest\TIPortfolio;
use jamesRUS52\TinkoffInvest\TIPortfolioCurrency;
use jamesRUS52\TinkoffInvest\TIPortfolioInstrument;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class TIClientAdapterTest extends TITestCase
{
    private MockObject $client;
    private TIClientAdapter $adapter;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = $this->createMock(TIClient::class);
        $this->adapter = new TIClientAdapter($this->client);
    }

    public function testGetAccounts()
    {
        $account01 = new TIAccount('Tinkoff', '1');
        $account02 = new TIAccount('TinkoffIis', '2');
        $account03 = new TIAccount('Unknown', '3');

        $data01 = [
            'id' => '1',
            'type' => 1,
            'status' => 2,
            'name' => null,
            'openedDate' => null,
            'closedDate' => null
        ];
        $data02 = [
            'id' => '2',
            'type' => 2,
            'status' => 2,
            'name' => null,
            'openedDate' => null,
            'closedDate' => null
        ];
        $data03 = [
            'id' => '3',
            'type' => 1,
            'status' => 2,
            'name' => null,
            'openedDate' => null,
            'closedDate' => null
        ];
        $accountModel01 = $this->createBrokerAccount($data01);
        $accountModel02 = $this->createBrokerAccount($data02);
        $accountModel03 = $this->createBrokerAccount($data03);
        $expected = new AccountCollection([$accountModel01, $accountModel02, $accountModel03]);

        $this->client->expects($this->once())->method('getAccounts')
            ->willReturn([$account01, $account02, $account03]);

        $this->assertEquals($expected, $this->adapter->getAccounts());
    }

    public function testGetPortfolio()
    {
        $accountId = '1';

        $avPrice01 = new \stdClass();
        $avPrice01->currency = 'RUB';
        $avPrice01->value = 70;

        $instrument01 = new TIPortfolioInstrument(
            'figi',
            'ticker',
            'isin',
            'Stock',
            10,
            0,
            '10',
            10,
            'RUB',
            'Generated Instrument',
            $avPrice01,
            75
        );

        $tiPortfolio01 = new TIPortfolio([], [$instrument01]);

        $portfolioPositionData01 = [
            'figi' => 'figi',
            'instrumentType' => 'Stock',
            'quantity' => 10,
            'expectedYield' => new BrokerMoneyModel([
                'amount' => 10,
                'currency' => 'RUB'
            ]),
            'averagePositionPrice' => new BrokerMoneyModel([
                'amount' => 70,
                'currency' => 'RUB'
            ]),
            'currentNkd' => null,
            'currentPrice' => new BrokerMoneyModel([
                'currency' => 'RUB',
                'amount' => 71
            ])
        ];
        $position01 = new BrokerPortfolioPositionModel($portfolioPositionData01);
        $portfolio01 = new BrokerPortfolioModel(['positions' => new PortfolioPositionCollection([$position01])]);

        $this->client->expects($this->once())
            ->method('setBrokerAccount')->with($accountId);
        $this->client->expects($this->once())
            ->method('getPortfolio')
            ->willReturn($tiPortfolio01);

        $this->assertEquals($portfolio01, $this->adapter->getPortfolio($accountId));
    }

    public function testGetInstrumentByFigi()
    {
        $figi = 'BBG00N8HZBK8';

        $tiData01 = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond'
        ];
        $tiInstrument01 = $this->createMarketInstrument($tiData01);

        $data01 = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond'
        ];
        $instrument01 = new BrokerInstrumentModel($data01);

        $this->client->expects($this->once())->method('getInstrumentByFigi')
            ->with($figi)
            ->willReturn($tiInstrument01);

        $this->assertEquals($instrument01, $this->adapter->getInstrumentByFigi($figi));
    }

    public function testGetOperations()
    {
        $accountId01 = 'Primary Account';
        $from01 = new \DateTime('2022-02-10 18:18:14');
        $to01 = new \DateTime('2022-03-17 14:11:45');

        $date01 = new \DateTime('2022-02-13 10:05:12');
        $date02 = new \DateTime('2022-02-13 11:06:13');
        $operation01 = new TIOperation(
            '1',
            'Done',
            'Traded01',
            '3',
            'RUB',
            10,
            5,
            2,
            'APPL',
            'Stock',
            false,
            $date01,
            'Buy'
        );
        $operation02 = new TIOperation(
            '2',
            'Decline',
            'Traded02',
            '4',
            'EUR',
            7,
            null,
            null,
            null,
            'Etf',
            true,
            $date02,
            'Sell'
        );
        $expectedOperation01 = $this->createBrokerOperation([
            'id' => '1',
            'parentOperationId' => null,
            'currency' => 'RUB',
            'payment' => new BrokerMoneyModel([
                'amount' => 10,
                'currency' => 'RUB'
            ]),
            'price' => new BrokerMoneyModel([
                    'amount' => 5,
                    'currency' => 'RUB'
                ]),
            'status' => 'Done',
            'quantity' => 2,
            'figi' => 'APPL',
            'date' => $date01,
            'type' => 'Buy',
            'instrumentType' => 'Stock',
            'broker' => 'tinkoff'
        ]);
        $expectedOperation02 = $this->createBrokerOperation([
            'id' => '2',
            'parentOperationId' => null,
            'currency' => 'EUR',
            'payment' => new BrokerMoneyModel([
                'amount' => 7,
                'currency' => 'EUR'
            ]),
            'price' => null,
            'status' => 'Decline',
            'quantity' => null,
            'figi' => null,
            'date' => $date02,
            'type' => 'Sell',
            'instrumentType' => 'Etf',
            'broker' => 'tinkoff'
        ]);
        $expected01 = new OperationCollection([$expectedOperation01, $expectedOperation02]);

        $this->client->expects($this->once())
            ->method('setBrokerAccount')
            ->with($accountId01);
        $this->client->expects($this->once())
            ->method('getOperations')
            ->with($from01, $to01)
            ->willReturn([$operation01, $operation02]);

        $this->assertEquals($expected01, $this->adapter->getOperations($accountId01, $from01, $to01));
    }

    public function testGetCurrencies()
    {
        $currency01 = $this->createMarketInstrument(['currency' => 'RUB', 'figi' => 'AAAA-BBBB-CCCC']);
        $currency02 = $this->createMarketInstrument(['currency' => 'EUR', 'figi' => 'DDDD-EEEE-FFFF']);

        $currencyModel01 = new BrokerCurrencyModel(['iso' => 'RUB', 'figi' => 'AAAA-BBBB-CCCC']);
        $currencyModel02 = new BrokerCurrencyModel(['iso' => 'EUR', 'figi' => 'DDDD-EEEE-FFFF']);
        $expected01 = new BrokerCurrencyCollection([$currencyModel01, $currencyModel02]);

        $this->client->expects($this->once())->method('getCurrencies')
            ->willReturn([$currency01, $currency02]);

        $this->assertEquals($expected01, $this->adapter->getCurrencies());
    }

    public function testGetHistoryCandles()
    {
        $figi = 'APPL';
        $from = new \DateTime('2022-01-01 00:14:31');
        $to = new \DateTime('2022-02-01 14:22:12');
        $interval = '1day';
        $brokerInterval = 'day';
        $candle01 = new TICandle(
            8,
            9,
            7,
            10,
            100,
            new \DateTime('2022-01-12'),
            'day',
            'APPL'
        );
        $candle02 = new TICandle(
            1,
            2,
            2.5,
            0.5,
            10,
            new \DateTime('2022-01-14'),
            'day',
            'LKOH'
        );
        $brokerCandle01 = new BrokerCandleModel([
            'open' => 8,
            'close' => 9,
            'low' => 10,
            'high' => 7,
            'volume' => 100,
            'isComplete' => null,
            'time' => new \DateTime('2022-01-12')
        ]);
        $brokerCandle02 = new BrokerCandleModel([
            'open' => 1,
            'close' => 2,
            'low' => 0.5,
            'high' => 2.5,
            'volume' => 10,
            'isComplete' => null,
            'time' => new \DateTime('2022-01-14'),
        ]);
        $expected = new CandleCollection([$brokerCandle01, $brokerCandle02]);
        $this->client->expects($this->once())->method('getHistoryCandles')
            ->with($figi, $from, $to, $brokerInterval)
            ->willReturn([$candle01, $candle02]);

        $this->assertEquals($expected, $this->adapter->getHistoryCandles($figi, $from, $to, $interval));
    }

    public function testGetShares()
    {
        $instrumentModel01 = new BrokerInstrumentModel([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'USD',
            'name' => 'name01',
            'type' => 'Stock'
        ]);
        $share01 = new BrokerShareModel([
            'figi' => 'figi01',
            'ticker' => 'ticker01',
            'isin' => 'isin01',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'USD',
            'name' => 'name01',
            'sector' => null,
            'country' => null,
            'industry' => null,
            'instrument' => $instrumentModel01
        ]);
        $instrumentModel02 = new BrokerInstrumentModel([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'minPriceIncrement' => 1,
            'lot' => 10,
            'currency' => 'RUB',
            'name' => 'name02',
            'type' => 'Stock'
        ]);
        $share02 = new BrokerShareModel([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'minPriceIncrement' => 1,
            'lot' => 10,
            'currency' => 'RUB',
            'name' => 'name02',
            'sector' => null,
            'country' => null,
            'industry' => null,
            'instrument' => $instrumentModel02
        ]);
        $shareCollection01 = new ShareCollection([$share01, $share02]);
        $tiShare01 = new TIInstrument(
            'figi01',
            'ticker01',
            'isin01',
            0.1,
            1,
            'USD',
            'name01',
            'stock'
        );
        $tiShare02 = new TIInstrument(
            'figi02',
            'ticker02',
            'isin02',
            1,
            10,
            'RUB',
            'name02',
            'stock'
        );

        $this->client->expects($this->once())->method('getStocks')->willReturn([$tiShare01, $tiShare02]);
        $this->assertEquals($shareCollection01, $this->adapter->getShares());
    }

    /** @dataProvider dataGetShareByFigi */
    public function testGetShareByFigi(
        ?BrokerShareModel $expected,
        array $tiShares,
        string $figi
    ) {
        $this->client->expects($this->once())->method('getStocks')->willReturn($tiShares);
        $this->assertEquals($expected, $this->adapter->getShareByFigi($figi));
    }

    public function dataGetShareByFigi()
    {
        $tiShare01 = new TIInstrument(
            'figi01',
            'ticker01',
            'isin01',
            0.1,
            1,
            'USD',
            'name01',
            'stock'
        );
        $tiShare02 = new TIInstrument(
            'figi02',
            'ticker02',
            'isin02',
            1,
            10,
            'RUB',
            'name02',
            'stock'
        );
        $tiShares = [$tiShare01, $tiShare02];
        $figi01 = 'figi02';
        $figi02 = 'unknown_figi';

        $instrumentModel01 = new BrokerInstrumentModel([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'minPriceIncrement' => 1,
            'lot' => 10,
            'currency' => 'RUB',
            'name' => 'name02',
            'type' => 'Stock'
        ]);
        $share01 = new BrokerShareModel([
            'figi' => 'figi02',
            'ticker' => 'ticker02',
            'isin' => 'isin02',
            'minPriceIncrement' => 1,
            'lot' => 10,
            'currency' => 'RUB',
            'name' => 'name02',
            'sector' => null,
            'country' => null,
            'industry' => null,
            'instrument' => $instrumentModel01
        ]);
        $share02 = null;

        return [
            'Common Case' => [
                $share01,
                $tiShares,
                $figi01
            ],
            'Not found Case' => [
                $share02,
                $tiShares,
                $figi02
            ]
        ];
    }

    public function testGetPortfolioBalance()
    {
        $accountId01 = 'AAAA-BBBB-CCCC';

        $currency01 = new TIPortfolioCurrency(10, 'RUB', 5);
        $currency02 = new TIPortfolioCurrency(20, 'USD', 1);
        $portfolio01 = new TIPortfolio([$currency01, $currency02], []);

        $balance01 = new BrokerPortfolioBalanceModel(['amount' => 10, 'currency' => 'RUB', 'blockedAmount' => 5]);
        $balance02 = new BrokerPortfolioBalanceModel(['amount' => 20, 'currency' => 'USD', 'blockedAmount' => 1]);
        $expected01 = new PortfolioBalanceCollection([$balance01, $balance02]);

        $this->client->expects($this->once())->method('setBrokerAccount')->with($accountId01);
        $this->client->expects($this->once())->method('getPortfolio')->willReturn($portfolio01);

        $this->assertEquals($expected01, $this->adapter->getPortfolioBalance($accountId01));
    }

    /** @dataProvider  dateGetCurrencyByFigi */
    public function testGetCurrencyByFigi(?BrokerCurrencyModel $expected, string $figi)
    {
        $this->assertEquals($expected, $this->adapter->getCurrencyByFigi($figi));
    }

    public function dateGetCurrencyByFigi()
    {
        $figi01 = 'BBG0013HJJ31';
        $figi02 = 'Unknown_figi';

        $expected01 = new BrokerCurrencyModel(['iso' => 'EUR', 'figi' => 'BBG0013HJJ31']);
        $expected02 = null;

        return [
            'Common Case' => [$expected01, $figi01],
            'Unknown Figi Case' => [$expected02, $figi02]
        ];
    }
}
