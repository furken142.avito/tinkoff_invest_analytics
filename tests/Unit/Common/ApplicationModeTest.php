<?php

declare(strict_types=1);

namespace Tests\Unit\Common;

use App\Common\ApplicationMode;
use Monolog\Test\TestCase;

class ApplicationModeTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testIsProdMode()
    {
        putenv('mode=dev');
        $this->assertEquals(false, ApplicationMode::isProdMode());

        putenv('mode=prod');
        $this->assertEquals(true, ApplicationMode::isProdMode());
    }

    public function testIsDevMode()
    {
        putenv('mode=dev');
        $this->assertEquals(true, ApplicationMode::isDevMode());

        putenv('mode=prod');
        $this->assertEquals(false, ApplicationMode::isDevMode());
    }
}
