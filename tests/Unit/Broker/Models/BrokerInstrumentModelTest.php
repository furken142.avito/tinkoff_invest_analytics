<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerInstrumentModel;
use Tests\ModelTestCase;

class BrokerInstrumentModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerInstrumentModel();
    }

    public function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setFigi' => 'APPL',
                    'setTicker' => 'ticker',
                    'setIsin' => 'isin',
                    'setMinPriceIncrement' => 0.1,
                    'setLot' => 1,
                    'setCurrency' => 'USD',
                    'setName' => 'Apple',
                    'setType' => 'Stock'
                ],
                [
                    'getFigi' => 'APPL',
                    'getTicker' => 'ticker',
                    'getIsin' => 'isin',
                    'getMinPriceIncrement' => 0.1,
                    'getLot' => 1,
                    'getCurrency' => 'USD',
                    'getName' => 'Apple',
                    'getType' => 'Stock'
                ]
            ],
            'Default Values Case' => [
                [
                    'setFigi' => 'APPL',
                    'setTicker' => 'ticker',
                    'setIsin' => 'isin',
                    'setLot' => 1,
                    'setCurrency' => 'USD',
                    'setName' => 'Apple',
                    'setType' => 'Stock'
                ],
                [
                    'getFigi' => 'APPL',
                    'getTicker' => 'ticker',
                    'getIsin' => 'isin',
                    'getMinPriceIncrement' => null,
                    'getLot' => 1,
                    'getCurrency' => 'USD',
                    'getName' => 'Apple',
                    'getType' => 'Stock'
                ]
            ]
        ];
    }
}
