<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use Tests\ModelTestCase;

class BrokerPortfolioPositionModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerPortfolioPositionModel();
    }

    public function dataTestGetSet(): array
    {
        $averagePositionPrice = new BrokerMoneyModel(['amount' => 5, 'currency' => 'USD']);
        $expectedYield = new BrokerMoneyModel(['amount' => 3, 'currency' => 'RUB']);
        $currentNkd = new BrokerMoneyModel(['amount' => 1, 'currency' => 'EUR']);
        $currentPrice = new BrokerMoneyModel(['amount' => 9.3, 'currency' => 'EUR']);
        return [
            'Common Case' => [
                [
                    'setFigi' => 'figi01',
                    'setInstrumentType' => 'stock',
                    'setQuantity' => 5,
                    'setAveragePositionPrice' => $averagePositionPrice,
                    'setExpectedYield' => $expectedYield,
                    'setCurrentNkd' => $currentNkd,
                    'setCurrentPrice' => $currentPrice,
                ],
                [
                    'getFigi' => 'figi01',
                    'getInstrumentType' => 'stock',
                    'getQuantity' => 5,
                    'getAveragePositionPrice' => $averagePositionPrice,
                    'getExpectedYield' => $expectedYield,
                    'getCurrentNkd' => $currentNkd,
                    'getCurrentPrice' => $currentPrice,
                ]
            ],
            'Default Values Case' => [
                [
                    'setFigi' => 'figi01',
                    'setInstrumentType' => 'stock',
                    'setQuantity' => 5,
                    'setAveragePositionPrice' => $averagePositionPrice,
                    'setExpectedYield' => $expectedYield
                ],
                [
                    'getFigi' => 'figi01',
                    'getInstrumentType' => 'stock',
                    'getQuantity' => 5,
                    'getAveragePositionPrice' => $averagePositionPrice,
                    'getExpectedYield' => $expectedYield,
                    'getCurrentNkd' => null,
                    'getCurrentPrice' => null,
                ]
            ]
        ];
    }
}
