<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerAccountModel;
use Tests\ModelTestCase;

class BrokerAccountModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerAccountModel();
    }

    public function dataTestGetSet(): array
    {
        $openedDate = new \DateTime('2022-01-05 16:39:00');
        $closedDate = new \DateTime('2022-02-06 16:39:00');
        return [
            'Common Case' => [
                [
                    'setId' => '1',
                    'setType' => 1,
                    'setName' => 'Primary Account',
                    'setStatus' => 1,
                    'setOpenedDate' => $openedDate,
                    'setClosedDate' => $closedDate
                ],
                [
                    'getId' => '1',
                    'getType' => 1,
                    'getName' => 'Primary Account',
                    'getStatus' => 1,
                    'getOpenedDate' => $openedDate,
                    'getClosedDate' => $closedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => '1',
                    'setType' => 1,
                    'setStatus' => 1
                ],
                [
                    'getId' => '1',
                    'getType' => 1,
                    'getName' => null,
                    'getStatus' => 1,
                    'getOpenedDate' => null,
                    'getClosedDate' => null
                ]
            ]
        ];
    }
}
