<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Types;

use App\Broker\Types\BrokerAccountStatusType;
use PHPUnit\Framework\TestCase;

class BrokerAccountStatusTypeTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testTypes()
    {
        $this->assertEquals(0, BrokerAccountStatusType::ACCOUNT_STATUS_UNSPECIFIED);
        $this->assertEquals(1, BrokerAccountStatusType::ACCOUNT_STATUS_NEW);
        $this->assertEquals(2, BrokerAccountStatusType::ACCOUNT_STATUS_OPEN);
        $this->assertEquals(3, BrokerAccountStatusType::ACCOUNT_STATUS_CLOSED);
    }
}
