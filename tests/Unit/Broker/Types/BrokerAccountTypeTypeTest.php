<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Types;

use App\Broker\Types\BrokerAccountTypeType;
use PHPUnit\Framework\TestCase;

class BrokerAccountTypeTypeTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testTypes()
    {
        $this->assertEquals(0, BrokerAccountTypeType::ACCOUNT_TYPE_UNSPECIFIED);
        $this->assertEquals(1, BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL);
        $this->assertEquals(2, BrokerAccountTypeType::ACCOUNT_TYPE_IIS);
        $this->assertEquals(3, BrokerAccountTypeType::ACCOUNT_TYPE_INVEST_BOX);
    }
}
