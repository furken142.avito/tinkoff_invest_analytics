<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketCurrencyEntity;
use App\Entities\StrategyCurrencyBalanceEntity;
use Tests\ModelTestCase;

class StrategyCurrencyBalanceEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new StrategyCurrencyBalanceEntity();
    }

    public function dataTestGetSet(): array
    {
        $currency = new MarketCurrencyEntity();
        $currency->setId(1);
        $createdDate = new \DateTime('2022-05-21 12:49:12');
        $updatedDate = new \DateTime('2022-05-23 14:21:54');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setMarketCurrencyId' => 3,
                    'setIsActive' => 0,
                    'setMarketCurrency' => $currency,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getMarketCurrencyId' => 3,
                    'getIsActive' => 0,
                    'getMarketCurrency' => $currency,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setMarketCurrencyId' => 3,
                    'setMarketCurrency' => $currency,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getMarketCurrencyId' => 3,
                    'getIsActive' => 1,
                    'getMarketCurrency' => $currency,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
