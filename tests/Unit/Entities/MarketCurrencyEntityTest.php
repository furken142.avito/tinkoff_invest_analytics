<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;
use Tests\ModelTestCase;

class MarketCurrencyEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new MarketCurrencyEntity();
    }

    public function dataTestGetSet(): array
    {
        $marketInstrument = new MarketInstrumentEntity();
        $marketInstrument->setId(1);
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setIso' => 'EUR',
                    'setFigi' => 'BBG0013HJJ31',
                    'setMarketInstrumentId' => 1,
                    'setMarketInstrument' => $marketInstrument
                ],
                [
                    'getId' => 1,
                    'getIso' => 'EUR',
                    'getFigi' => 'BBG0013HJJ31',
                    'getMarketInstrumentId' => 1,
                    'getMarketInstrument' => $marketInstrument
                ]
            ],
            'Null Case' => [
                [
                    'setId' => 2,
                    'setIso' => 'RUB',
                    'setFigi' => null
                ],
                [
                    'getId' => 2,
                    'getIso' => 'RUB',
                    'getFigi' => null,
                    'getMarketInstrumentId' => null,
                    'getMarketInstrument' => null
                ]
            ]
        ];
    }
}
