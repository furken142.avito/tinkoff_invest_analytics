<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\CandleEntity;
use App\Entities\MarketInstrumentEntity;
use Tests\ModelTestCase;

class CandleEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new CandleEntity();
    }

    public function dataTestGetSet(): array
    {
        $marketInstrumentEntity = new MarketInstrumentEntity();
        $marketInstrumentEntity->setId(2);

        $createdDate = new \DateTime();
        $createdDate->setDate(2022, 1, 6);
        $createdDate->setTime(12, 30, 0);

        $time = new \DateTime();
        $time->setTime(2022, 1, 5);
        $time->setTime(10, 0, 0);

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setMarketInstrument' => $marketInstrumentEntity,
                    'setMarketInstrumentId' => 2,
                    'setFinal' => 1,
                    'setOpen' => 10,
                    'setClose' => 11,
                    'setLow' => 8,
                    'setHigh' => 12,
                    'setVolume' => 100,
                    'setCandleInterval' => 'day',
                    'setTime' => $time,
                    'setCreatedDate' => $createdDate
                ],
                [
                    'getId' => 1,
                    'getMarketInstrument' => $marketInstrumentEntity,
                    'getMarketInstrumentId' => 2,
                    'getIsFinal' => 1,
                    'getOpen' => 10,
                    'getClose' => 11,
                    'getLow' => 8,
                    'getHigh' => 12,
                    'getVolume' => 100,
                    'getCandleInterval' => 'day',
                    'getTime' => $time,
                    'getCreatedDate' => $createdDate
                ]
            ]
        ];
    }
}
