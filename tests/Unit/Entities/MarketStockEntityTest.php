<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Intl\Entities\CountryEntity;
use Tests\ModelTestCase;

class MarketStockEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new MarketStockEntity();
    }

    public function dataTestGetSet(): array
    {
        $marketInstrument = new MarketInstrumentEntity();
        $marketInstrument->setFigi('123');

        $country = new CountryEntity();
        $country->setName('Russia');
        $country->setIso('RU');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setMarketInstrumentId' => 10,
                    'setFigi' => 'figi',
                    'setCountryIso' => 'country',
                    'setSector' => 'sector',
                    'setindustry' => 'industry',
                    'setMarketInstrument' => $marketInstrument,
                    'setCountry' => $country
                ],
                [
                    'getId' => 1,
                    'getMarketInstrumentId' => 10,
                    'getFigi' => 'figi',
                    'getCountryIso' => 'country',
                    'getSector' => 'sector',
                    'getindustry' => 'industry',
                    'getMarketInstrument' => $marketInstrument,
                    'getCountry' => $country
                ]
            ],
            'Empty fields Test' => [
                [
                    'setId' => 1,
                    'setMarketInstrumentId' => 10,
                    'setFigi' => 'figi',
                    'setMarketInstrument' => $marketInstrument,
                ],
                [
                    'getId' => 1,
                    'getMarketInstrumentId' => 10,
                    'getFigi' => 'figi',
                    'getCountry' => null,
                    'getSector' => null,
                    'getindustry' => null,
                    'getMarketInstrument' => $marketInstrument,
                ]
            ]
        ];
    }
}
