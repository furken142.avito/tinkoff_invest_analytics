<?php

declare(strict_types=1);

namespace Tests\Unit\RealEstate\Services;

use App\Item\Entities\ItemEntity;
use App\RealEstate\Entities\RealEstateEntity;
use App\RealEstate\Entities\RealEstatePriceEntity;
use App\RealEstate\Services\RealEstateService;
use App\RealEstate\Storages\RealEstatePriceStorage;
use App\RealEstate\Storages\RealEstateStorage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\PersistentCollection;
use Monolog\Test\TestCase;

class RealEstateServiceTest extends TestCase
{
    private RealEstateStorage $realEstateStorage;
    private RealEstatePriceStorage $realEstatePriceStorage;
    private RealEstateService $realEstateService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->realEstateStorage = $this->createMock(RealEstateStorage::class);
        $this->realEstatePriceStorage = $this->createMock(RealEstatePriceStorage::class);

        $this->realEstateService = new RealEstateService(
            $this->realEstateStorage,
            $this->realEstatePriceStorage
        );
    }

    public function testCalculateUserRealEstatePrice()
    {
        $userId01 = 1;
        $em = $this->createMock(EntityManagerInterface::class);
        $metadata = $this->createMock(ClassMetadata::class);

        $priceEntity01 = new RealEstatePriceEntity();
        $priceEntity01->setId(1);
        $priceEntity01->setRealEstateId(1);
        $priceEntity01->setCurrency('RUB');
        $priceEntity01->setAmount(100);
        $priceEntity01->setDate(new \DateTime('2022-12-01 00:00:00'));
        $priceEntity02 = new RealEstatePriceEntity();
        $priceEntity02->setId(3);
        $priceEntity02->setRealEstateId(1);
        $priceEntity02->setCurrency('RUB');
        $priceEntity02->setAmount(200);
        $priceEntity02->setDate(new \DateTime('2022-12-31 00:00:00'));
        $priceEntity03 = new RealEstatePriceEntity();
        $priceEntity03->setId(3);
        $priceEntity03->setRealEstateId(1);
        $priceEntity03->setCurrency('RUB');
        $priceEntity03->setAmount(300);
        $priceEntity03->setDate(new \DateTime('2022-12-15 00:00:00'));

        $itemEntity01 = new ItemEntity();
        $itemEntity01->setName('Penthouse');
        $realEstate01 = new RealEstateEntity();
        $realEstate01->setId(1);
        $realEstate01->setItem($itemEntity01);
        $realEstate01->setUserId(1);
        $realEstate01->setItemId(2);
        $realEstate01->setIsActive(1);
        $realEstate01->setInterestAmount(10000);
        $realEstate01->setPayoutFrequency(1);
        $realEstate01->setPayoutFrequencyPeriod('month');
        $realEstate01->setCurrency('RUB');
        $realEstate01->setRealEstatePrices(new PersistentCollection(
            $em,
            $metadata,
            new ArrayCollection([$priceEntity01, $priceEntity02, $priceEntity03])
        ));

        $itemEntity02 = new ItemEntity();
        $itemEntity02->setName('Small House');
        $realEstate02 = new RealEstateEntity();
        $realEstate02->setId(2);
        $realEstate02->setItem($itemEntity02);
        $realEstate02->setUserId(1);
        $realEstate02->setItemId(3);
        $realEstate02->setIsActive(1);
        $realEstate02->setInterestAmount(100);
        $realEstate02->setPayoutFrequency(1);
        $realEstate02->setPayoutFrequencyPeriod('month');
        $realEstate02->setCurrency('RUB');
        $realEstate02->setRealEstatePrices(new PersistentCollection(
            $em,
            $metadata,
            new ArrayCollection()
        ));

        $this->realEstateStorage->expects($this->once())->method('findActiveByUser')
            ->with($userId01)
            ->willReturn([$realEstate01, $realEstate02]);

        $this->assertEquals(200, $this->realEstateService->calculateUserRealEstatePrice($userId01));
    }

    public function testCalculateForecastIncome()
    {
        $userId01 = 1;
        $dateStart = new \DateTime('2023-01-08 00:00:00');
        $dateEnd = new \DateTime('2024-01-08 00:00:00');
        $em = $this->createMock(EntityManagerInterface::class);
        $metadata = $this->createMock(ClassMetadata::class);

        $itemEntity01 = new ItemEntity();
        $itemEntity01->setName('Penthouse');
        $realEstate01 = new RealEstateEntity();
        $realEstate01->setId(1);
        $realEstate01->setItem($itemEntity01);
        $realEstate01->setUserId(1);
        $realEstate01->setItemId(2);
        $realEstate01->setIsActive(1);
        $realEstate01->setInterestAmount(10000);
        $realEstate01->setPayoutFrequency(2);
        $realEstate01->setPayoutFrequencyPeriod('month');
        $realEstate01->setCurrency('RUB');
        $realEstate01->setRealEstatePrices(new PersistentCollection(
            $em,
            $metadata,
            new ArrayCollection()
        ));

        $itemEntity02 = new ItemEntity();
        $itemEntity02->setName('Penthouse');
        $realEstate02 = new RealEstateEntity();
        $realEstate02->setId(1);
        $realEstate02->setItem($itemEntity02);
        $realEstate02->setUserId(1);
        $realEstate02->setItemId(2);
        $realEstate02->setIsActive(1);
        $realEstate02->setInterestAmount(10);
        $realEstate02->setPayoutFrequency(6);
        $realEstate02->setPayoutFrequencyPeriod('day');
        $realEstate02->setCurrency('RUB');
        $realEstate02->setRealEstatePrices(new PersistentCollection(
            $em,
            $metadata,
            new ArrayCollection()
        ));

        $itemEntity03 = new ItemEntity();
        $itemEntity03->setName('Penthouse');
        $realEstate03 = new RealEstateEntity();
        $realEstate03->setId(1);
        $realEstate03->setItem($itemEntity03);
        $realEstate03->setUserId(1);
        $realEstate03->setItemId(2);
        $realEstate03->setIsActive(1);
        $realEstate03->setInterestAmount(15000);
        $realEstate03->setPayoutFrequency(1);
        $realEstate03->setPayoutFrequencyPeriod('year');
        $realEstate03->setCurrency('RUB');
        $realEstate03->setRealEstatePrices(new PersistentCollection(
            $em,
            $metadata,
            new ArrayCollection()
        ));

        $itemEntity04 = new ItemEntity();
        $itemEntity04->setName('Penthouse');
        $realEstate04 = new RealEstateEntity();
        $realEstate04->setId(1);
        $realEstate04->setItem($itemEntity02);
        $realEstate04->setUserId(1);
        $realEstate04->setItemId(2);
        $realEstate04->setIsActive(1);
        $realEstate04->setInterestAmount(null);
        $realEstate04->setPayoutFrequency(1);
        $realEstate04->setPayoutFrequencyPeriod('year');
        $realEstate04->setCurrency('RUB');
        $realEstate04->setRealEstatePrices(new PersistentCollection(
            $em,
            $metadata,
            new ArrayCollection()
        ));

        $this->realEstateStorage->expects($this->once())->method('findActiveByUser')
            ->with($userId01)
            ->willReturn([$realEstate01, $realEstate02, $realEstate03, $realEstate04]);

        $this->assertEquals(
            600 + 60000 + 15000,
            $this->realEstateService->calculateForecastIncome($userId01, $dateStart, $dateEnd)
        );
    }
}
