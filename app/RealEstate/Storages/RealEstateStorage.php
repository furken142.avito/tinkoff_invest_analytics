<?php

declare(strict_types=1);

namespace App\RealEstate\Storages;

use App\RealEstate\Entities\RealEstateEntity;
use Doctrine\ORM\EntityManagerInterface;

class RealEstateStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(RealEstateEntity $entity): int
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param int $userId
     * @return RealEstateEntity[]
     */
    public function findActiveByUser(int $userId): array
    {
        return $this->entityManager->getRepository(RealEstateEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    public function findActiveByIdAndUser(int $realEstateId, int $userId): ?RealEstateEntity
    {
        return $this->entityManager->getRepository(RealEstateEntity::class)
            ->findOneBy(['id' => $realEstateId,'userId' => $userId, 'isActive' => 1]);
    }
}
