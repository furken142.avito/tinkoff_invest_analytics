<?php

declare(strict_types=1);

namespace App\RealEstate\Services;

use App\Item\Entities\ItemEntity;
use App\RealEstate\Collections\RealEstateCollection;
use App\RealEstate\Collections\RealEstatePriceCollection;
use App\RealEstate\Entities\RealEstateEntity;
use App\RealEstate\Entities\RealEstatePriceEntity;
use App\RealEstate\Exceptions\IncorrectDataException;
use App\RealEstate\Models\RealEstateModel;
use App\RealEstate\Models\RealEstatePriceModel;
use App\RealEstate\Storages\RealEstatePriceStorage;
use App\RealEstate\Storages\RealEstateStorage;
use App\User\Exceptions\UserAccessDeniedException;

class RealEstateService
{
    private const ITEM_TYPE = 'real_estate';
    private RealEstateStorage $realEstateStorage;
    private RealEstatePriceStorage $realEstatePriceStorage;

    public function __construct(
        RealEstateStorage $realEstateStorage,
        RealEstatePriceStorage $realEstatePriceStorage
    ) {
        $this->realEstateStorage = $realEstateStorage;
        $this->realEstatePriceStorage = $realEstatePriceStorage;
    }

    public function getUserRealEstate(int $userId): RealEstateCollection
    {
        $collection = new RealEstateCollection();
        $realEstates = $this->realEstateStorage->findActiveByUser($userId);
        foreach ($realEstates as $realEstate) {
            $collection->add($this->convertRealEstateEntityToModel($realEstate));
        }

        return $collection;
    }

    public function getUserRealEstateItem(int $userId, int $realEstateId): ?RealEstateModel
    {
        $realEstateEntity = $this->realEstateStorage->findActiveByIdAndUser($realEstateId, $userId);

        if ($realEstateEntity === null) {
            return null;
        }

        return $this->convertRealEstateEntityToModel($realEstateEntity);
    }

    public function updateUserRealEstateItem(int $userId, RealEstateModel $realEstate): void
    {
        $realEstateEntity = $this->realEstateStorage->findActiveByIdAndUser($realEstate->getRealEstateId(), $userId);

        if ($realEstateEntity === null) {
            throw new UserAccessDeniedException('Access denied update Real Estate to user');
        }

        $realEstateEntity->getItem()->setName($realEstate->getName());
        $realEstateEntity->setInterestAmount($realEstate->getInterestAmount());
        $realEstateEntity->setPayoutFrequency($realEstate->getPayoutFrequency());
        $realEstateEntity->setPayoutFrequencyPeriod($realEstate->getPayoutFrequencyPeriod());
        $realEstateEntity->setCurrency($realEstate->getCurrency());

        /** @var RealEstatePriceEntity $priceEntity */
        foreach ($realEstateEntity->getRealEstatePrices() as $priceEntity) {
            $price = $realEstate->getPrices()->findById($priceEntity->getRealEstateId());
            if ($price !== null) {
                $priceEntity->setCurrency($price->getCurrency());
                $priceEntity->setDate($price->getDate());
                $priceEntity->setAmount($price->getAmount());
            }
        }

        /** @var RealEstatePriceModel $price */
        foreach ($realEstate->getPrices()->getIterator() as $price) {
            if ($price->getRealEstatePriceId() === null) {
                if ($price->getAmount() === null) {
                    throw new IncorrectDataException('Amount can`t be empty', 1100);
                }
                $priceEntity = new RealEstatePriceEntity();
                $priceEntity->setAmount($price->getAmount());
                $priceEntity->setCurrency($price->getCurrency());
                $priceEntity->setDate($price->getDate());
                $priceEntity->setRealEstateId($realEstateEntity->getId());
                $priceEntity->setRealEstate($realEstateEntity);
                $realEstateEntity->getRealEstatePrices()->add($priceEntity);
            }
        }

        $this->realEstateStorage->addEntity($realEstateEntity);
    }

    public function addUserRealEstateItem(int $userId, RealEstateModel $realEstate): int
    {
        $item = new ItemEntity();
        $item->setName($realEstate->getName());
        $item->setType(self::ITEM_TYPE);
        $item->setSource('user');

        $realEstateEntity = new RealEstateEntity();
        $realEstateEntity->setUserId($userId);
        $realEstateEntity->setIsActive(1);
        $realEstateEntity->setInterestAmount($realEstate->getInterestAmount());
        $realEstateEntity->setPayoutFrequency($realEstate->getPayoutFrequency());
        $realEstateEntity->setPayoutFrequencyPeriod($realEstate->getPayoutFrequencyPeriod());
        $realEstateEntity->setCurrency($realEstate->getCurrency());
        $realEstateEntity->setItem($item);

        $item->setRealEstate($realEstateEntity);

        $realEstateId =  $this->realEstateStorage->addEntity($realEstateEntity);

        /** @var RealEstatePriceModel $price */
        foreach ($realEstate->getPrices() as $price) {
            $priceEntity = new RealEstatePriceEntity();
            if ($price->getAmount() !== null) {
                $priceEntity->setAmount($price->getAmount());
            }
            $priceEntity->setCurrency($price->getCurrency());
            $priceEntity->setDate($price->getDate());
            $priceEntity->setRealEstate($realEstateEntity);

            $this->realEstatePriceStorage->addEntity($priceEntity);
        }

        return $realEstateId;
    }

    public function calculateUserRealEstatePrice(int $userId): float
    {
        $realEstateEntityList = $this->realEstateStorage->findActiveByUser($userId);
        $realEstateCollection = new RealEstateCollection();
        foreach ($realEstateEntityList as $realEstateEntity) {
            $realEstate = $this->convertRealEstateEntityToModel($realEstateEntity);
            $realEstateCollection->add($realEstate);
        }

        $amount = 0;
        /** @var RealEstateModel $realEstate */
        foreach ($realEstateCollection->getIterator() as $realEstate) {
            $lastPrice = $realEstate->getPrices()?->getListPrice();
            if ($lastPrice !== null) {
                $amount += $lastPrice->getAmount();
            }
        }

        return $amount;
    }

    public function calculateForecastIncome(int $userId, \DateTime $startDate, \DateTime $endDate): float
    {
        $income = 0;
        $interval = $endDate->diff($startDate);

        $realEstateEntityList = $this->realEstateStorage->findActiveByUser($userId);
        $realEstateCollection = new RealEstateCollection();
        foreach ($realEstateEntityList as $realEstateEntity) {
            $realEstate = $this->convertRealEstateEntityToModel($realEstateEntity);
            $realEstateCollection->add($realEstate);
        }

        /** @var RealEstateModel $realEstate */
        foreach ($realEstateCollection->getIterator() as $realEstate) {
            if (
                $realEstate->getInterestAmount() !== null
                && $realEstate->getPayoutFrequency() !== null
                && $realEstate->getPayoutFrequencyPeriod() !== null
            ) {
                $countPeriods = match ($realEstate->getPayoutFrequencyPeriod()) {
                    'day' => floor($interval->days / $realEstate->getPayoutFrequency()),
                    'month' => floor(($interval->y * 12 + $interval->m) / $realEstate->getPayoutFrequency()),
                    'year' => floor($interval->y / $realEstate->getPayoutFrequency())
                };
                $income += $countPeriods * $realEstate->getInterestAmount();
            }
        }

        return $income;
    }

    private function convertRealEstateEntityToModel(RealEstateEntity $entity): RealEstateModel
    {
        $realEstatePrices = $entity->getRealEstatePrices();

        $prices = [];
        /** @var RealEstatePriceEntity $price */
        foreach ($realEstatePrices->getIterator() as $price) {
            $prices[] = $this->convertRealEstatePriceEntityToModel($price);
        }

        return new RealEstateModel([
            'realEstateId' => $entity->getId(),
            'itemId' => $entity->getItemId(),
            'userId' => $entity->getUserId(),
            'name' => $entity->getItem()->getName(),
            'isActive' => $entity->getIsActive(),
            'payoutFrequency' => $entity->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $entity->getPayoutFrequencyPeriod(),
            'interestAmount' => $entity->getInterestAmount(),
            'currency' => $entity->getCurrency(),
            'prices' => count($prices) ? new RealEstatePriceCollection($prices) : null
        ]);
    }

    private function convertRealEstatePriceEntityToModel(RealEstatePriceEntity $entity): RealEstatePriceModel
    {
        return new RealEstatePriceModel([
            'realEstatePriceId' => $entity->getId(),
            'realEstateId' => $entity->getRealEstateId(),
            'amount' => $entity->getAmount(),
            'currency' => $entity->getCurrency(),
            'date' => $entity->getDate()
        ]);
    }
}
