<?php

declare(strict_types=1);

namespace App\RealEstate\Models;

use App\Common\Models\BaseModel;
use App\RealEstate\Collections\RealEstatePriceCollection;

class RealEstateModel extends BaseModel
{
    private int $realEstateId;
    private int $userId;
    private int $itemId;
    private int $isActive;
    private ?int $payoutFrequency = null;
    private ?string $payoutFrequencyPeriod = null;
    private ?float $interestAmount = null;
    private ?string $currency = null;
    private string $name;
    private ?RealEstatePriceCollection $prices = null;

    /**
     * @return int
     */
    public function getRealEstateId(): int
    {
        return $this->realEstateId;
    }

    /**
     * @param int $realEstateId
     */
    public function setRealEstateId(int $realEstateId): void
    {
        $this->realEstateId = $realEstateId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return float|null
     */
    public function getInterestAmount(): ?float
    {
        return $this->interestAmount;
    }

    /**
     * @param float|null $interestAmount
     */
    public function setInterestAmount(?float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return RealEstatePriceCollection|null
     */
    public function getPrices(): ?RealEstatePriceCollection
    {
        return $this->prices;
    }

    /**
     * @param RealEstatePriceCollection|null $prices
     */
    public function setPrices(?RealEstatePriceCollection $prices): void
    {
        $this->prices = $prices;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getPayoutFrequency(): ?int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int|null $payoutFrequency
     */
    public function setPayoutFrequency(?int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string|null
     */
    public function getPayoutFrequencyPeriod(): ?string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string|null $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(?string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }
}
