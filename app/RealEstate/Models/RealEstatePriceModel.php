<?php

declare(strict_types=1);

namespace App\RealEstate\Models;

use App\Common\Models\BaseModel;

class RealEstatePriceModel extends BaseModel
{
    private ?int $realEstatePriceId = null;
    private ?int $realEstateId = null;
    private ?float $amount = null;
    private string $currency;
    private \DateTime $date;

    /**
     * @return int|null
     */
    public function getRealEstatePriceId(): ?int
    {
        return $this->realEstatePriceId;
    }

    /**
     * @param int|null $realEstatePriceId
     */
    public function setRealEstatePriceId(?int $realEstatePriceId): void
    {
        $this->realEstatePriceId = $realEstatePriceId;
    }

    /**
     * @return int|null
     */
    public function getRealEstateId(): ?int
    {
        return $this->realEstateId;
    }

    /**
     * @param int|null $realEstateId
     */
    public function setRealEstateId(?int $realEstateId): void
    {
        $this->realEstateId = $realEstateId;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     */
    public function setAmount(?float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }
}
