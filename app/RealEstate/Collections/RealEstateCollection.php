<?php

declare(strict_types=1);

namespace App\RealEstate\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class RealEstateCollection extends ArrayCollection
{
}
