<?php

declare(strict_types=1);

namespace App\RealEstate\Collections;

use App\RealEstate\Models\RealEstatePriceModel;
use Doctrine\Common\Collections\ArrayCollection;

class RealEstatePriceCollection extends ArrayCollection
{
    public function findById(int $id): ?RealEstatePriceModel
    {
        /** @var RealEstatePriceModel $price */
        foreach ($this->getIterator() as $price) {
            if ($price->getRealEstateId() === $id) {
                return $price;
            }
        }

        return null;
    }

    public function getListPrice(): ?RealEstatePriceModel
    {
        if ($this->count() === 0) {
            return null;
        }

        $iterator = $this->getIterator();
        $iterator->uasort(function (RealEstatePriceModel $el1, RealEstatePriceModel $el2) {
            if ($el1->getDate() === $el2->getDate()) {
                return 0;
            }

            return $el1->getDate() > $el2->getDate() ? -1 : 1;
        });
        $collection = new RealEstatePriceCollection(iterator_to_array($iterator));
        return $collection->first();
    }
}
