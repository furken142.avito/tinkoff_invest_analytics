<?php

declare(strict_types=1);

namespace App\Common;

class HttpResponeCode
{
    public const OK = 200;
    public const NO_CONTENT = 204;
    public const UNAUTHORIZED = 401;
    public const NOT_FOUND = 404;
    public const UNPROCCESSABLE_ENTITY = 422;
    public const MANY_REQUESTS = 429;
}
