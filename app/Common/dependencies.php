<?php

declare(strict_types=1);

use App\Common\ApplicationMode;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\PhpFilesAdapter;

require_once __DIR__ . '/../Dependencies/dependencies.php';
require_once __DIR__ . '/../User/Dependencies/dependencies.php';
require_once __DIR__ . '/../Intl/Dependencies/dependencies.php';
require_once __DIR__ . '/../Assets/Dependencies/dependency.php';
require_once __DIR__ . '/../Item/Dependencies/dependency.php';
require_once __DIR__ . '/../RealEstate/Dependencies/dependency.php';

/** @var ContainerInterface $container */
$container->set('doctrine.orm.entity_manager', function (ContainerInterface $c): EntityManager {
    $config = new Configuration();
    if (ApplicationMode::isProdMode()) {
        $directory = __DIR__ . '/../../cache/Doctrine/';
        $queryCache = new PhpFilesAdapter('doctrine_queries', 0, $directory);
        $metadataCache = new PhpFilesAdapter('doctrine_metadata', 0, $directory);
    } else {
        $queryCache = new ArrayAdapter();
        $metadataCache = new ArrayAdapter();
    }

    $config->setMetadataCache($metadataCache);
    $config->setQueryCache($queryCache);
    $driverImpl = $config->newDefaultAnnotationDriver(
        __DIR__ . '/../../app/*/Entities',
        false
    );
    $config->setMetadataDriverImpl($driverImpl);
    $config->setProxyDir(__DIR__ . '/../../DoctrineProxies');
    $config->setProxyNamespace('Doctrine\Proxies');

    if (ApplicationMode::isProdMode()) {
        $config->setAutoGenerateProxyClasses(true);
    } else {
        $config->setAutoGenerateProxyClasses(true);
    }

    $connectionOptions = array(
        'driver' => 'pdo_mysql',
        'host' => 'mysql',
        'dbname' => getenv('MYSQL_DATABASE'),
        'user' => getenv('MYSQL_USER'),
        'password' => getenv('MYSQL_PASSWORD'),
        'charset' => 'UTF8',
        'logging' => true
    );
    return EntityManager::create($connectionOptions, $config);
});

$container->set(Logger::class, function () {
    $logger = new Logger('app');
    $streamHandler = new StreamHandler(__DIR__ . '/../../' . getenv('LOGGER_PATH') .  '/' .
        date("Y-m-d") . '.log', getenv('LOGGER_LEVEL'));
    $logger->pushHandler($streamHandler);
    return $logger;
});

$container->set('Now' . DateTime::class, function () {
    return new DateTime();
});

$container->set(AMQPStreamConnection::class, function () {
    return new AMQPStreamConnection(
        'rabbitmq',
        5672,
        getenv('RABBITMQ_DEFAULT_USER'),
        getenv('RABBITMQ_DEFAULT_PASS')
    );
});
