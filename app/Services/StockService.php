<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Models\BrokerShareModel;
use App\Collections\MarketInstrumentCollection;
use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\MarketSectorService;
use App\Models\ImportStatisticModel;
use App\User\Entities\UserEntity;
use App\User\Services\UserService;
use JetBrains\PhpStorm\ArrayShape;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class StockService
{
    private MarketInstrumentService $marketInstrumentService;
    private UserService $userService;
    private AMQPStreamConnection $amqpConnection;
    private CountryService $countryService;
    private CurrencyService $currencyService;
    private MarketSectorService $marketSectorService;
    private FillMarketInstrumentServiceDecorator $marketInstrumentServiceDecorator;

    public function __construct(
        MarketInstrumentService $marketInstrumentService,
        UserService $userService,
        AMQPStreamConnection $connection,
        CountryService $countryService,
        CurrencyService $currencyService,
        MarketSectorService $marketSectorService,
        FillMarketInstrumentServiceDecorator $marketInstrumentServiceDecorator
    ) {
        $this->marketInstrumentService = $marketInstrumentService;
        $this->userService = $userService;
        $this->amqpConnection = $connection;
        $this->countryService = $countryService;
        $this->currencyService = $currencyService;
        $this->marketSectorService = $marketSectorService;
        $this->marketInstrumentServiceDecorator = $marketInstrumentServiceDecorator;
    }

    public function import(): ImportStatisticModel
    {
        $user = new UserEntity();
        $user->setId(1);
        $this->userService->setUser($user);
        $statistic = [
            'countImportedItems' => 0,
            'countToImportItems' => 0
        ];

        $stockList = $this->marketInstrumentServiceDecorator->getStocks();

        $channel = $this->amqpConnection->channel();
        $channel->queue_declare('main', false, false, false, false);

        /** @var BrokerShareModel $share */
        foreach ($stockList->getIterator() as $share) {
            $marketInstrumentEntity = $this->marketInstrumentService
                ->getInstrumentByFigi($share->getInstrument()->getFigi());
            if (null === $marketInstrumentEntity) {
                $message = [
                    'type' => 'stockImport',
                    'data' => [
                        'figi' => $share->getInstrument()->getFigi(),
                        'userId' => $this->userService->getUserId()
                    ]
                ];
                $ampqMessage = new AMQPMessage(json_encode($message));
                $channel->basic_publish($ampqMessage, '', 'main');
                $statistic['countToImportItems']++;
            } else {
                $statistic['countImportedItems']++;
            }
        }
        $channel->close();
        $this->amqpConnection->close();

        return new ImportStatisticModel($statistic);
    }

    public function getList(
        string $country = null,
        string $currency = null,
        int $marketSectorId = null
    ): MarketInstrumentCollection {
        $marketSectorName = $this->marketSectorService->getNameById($marketSectorId);
        return $this->marketInstrumentService->getStocks($country, $currency, $marketSectorName);
    }

    #[ArrayShape([
        'country' => "\App\Intl\Collections\CountryCollection",
        'currency' => "\App\Intl\Collections\CurrencyCollection",
        'sector' => "\App\Intl\Collections\MarketSectorCollection"
    ])]
    public function getFilters(): array
    {
        return [
            'country' => $this->countryService->getList(),
            'currency' => $this->currencyService->getList(),
            'sector' => $this->marketSectorService->getList()
        ];
    }
}
