<?php

declare(strict_types=1);

namespace App\Services;

use App\Assets\Services\AssetService;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Interfaces\CandleServiceInterface as CandleServiceInterface;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService as ItemPortfolioServiceAlias;
use App\Models\PriceModel;
use App\RealEstate\Services\RealEstateService;
use App\User\Services\UserService;

class AnalyticsService
{
    private CandleServiceInterface $candleService;
    private AccountService $accountService;
    private PortfolioService $portfolioService;
    private ExchangeCurrencyService $exchangeCurrencyService;
    private \DateTime $currentDateTime;
    private UserService $userService;
    private ItemPortfolioServiceAlias $itemPortfolioService;
    private OperationService $operationService;
    private RealEstateService $realEstateService;

    private const DEFAULT_CURRENCY = 'USD';

    public function __construct(
        CandleServiceInterface $candleService,
        AccountService $accountService,
        PortfolioService $portfolioService,
        ExchangeCurrencyService $exchangeCurrencyService,
        \DateTime $currentDateTime,
        UserService $userService,
        ItemPortfolioServiceAlias $itemPortfolioService,
        OperationService $operationService,
        RealEstateService $realEstateService
    ) {
        $this->candleService = $candleService;
        $this->accountService = $accountService;
        $this->portfolioService = $portfolioService;
        $this->exchangeCurrencyService = $exchangeCurrencyService;
        $this->currentDateTime = $currentDateTime;
        $this->userService = $userService;
        $this->itemPortfolioService = $itemPortfolioService;
        $this->operationService = $operationService;
        $this->realEstateService = $realEstateService;
    }

    public function calculateProfitToday(?string $currency = null): array
    {
        $currency = $currency ?? self::DEFAULT_CURRENCY;
        $portfolio = $this->accountService->getPortfolio();
        $instruments = $portfolio->getPositions();

        $profit = 0;
        /** @var BrokerPortfolioPositionModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $candle = $this->candleService->getTodayCandle($instrument);

            if ($candle === null) {
                continue;
            }

            $exchangeRate = $this->exchangeCurrencyService
                ->getExchangeRateByDayCandies(
                    $instrument->getExpectedYield()->getCurrency(),
                    $currency,
                    $this->currentDateTime
                );

            $profit += $instrument->getQuantity()
                * $exchangeRate
                * ($candle->getClose() - $candle->getOpen());
        }

        $revenueToday = new PriceModel([
            'amount' => $profit,
            'currency' => $currency
        ]);
        return ['revenueToday' => $revenueToday];
    }

    public function getSummary(): array
    {
        $userSettings = $this->userService->getUserSettings($this->userService->getUserId());
        $user = $this->userService->getUser();

        $portfolio = $this->accountService->getPortfolio();
        $balance = $this->accountService->getPortfolioBalance();

        $actives = $this->portfolioService->calculateTotalAmount($portfolio, $balance);
        $actives += $this->itemPortfolioService->calculateTotalAmount($user->getId());
        $actives += $this->realEstateService->calculateUserRealEstatePrice($user->getId());
        $targetCapital = $userSettings !== null ? $userSettings->getDesiredPension() / 0.04 * 12 : null;
        $passives = 0;
        $shouldIncrease = $targetCapital - $actives + $passives;

        $yearBefore = clone  $this->currentDateTime;
        $yearBefore->sub(new \DateInterval('P1Y'));
        $lastYearInterest = $this->operationService
            ->calculateIncome($user->getId(), $yearBefore, $this->currentDateTime);

        $yearLater = clone  $this->currentDateTime;
        $yearLater->add(new \DateInterval('P1Y'));
        $predictInterest = $this->itemPortfolioService
            ->calculateForecastIncome($user->getId(), $this->currentDateTime, $yearLater);
        $predictInterest += $this->realEstateService
            ->calculateForecastIncome($user->getId(), $this->currentDateTime, $yearLater);

        $pension = ($actives - $passives) * 0.04 / 12;
        $futurePension = ($actives + $predictInterest - $passives) * 0.04 / 12;
        $capital = ($actives - $passives);

        if (null === $userSettings || null === $userSettings->getRetirementAge() || null === $user->getBirthday()) {
            $saveSpeed = null;
        } else {
            $retirementDate = clone $user->getBirthday();
            $retirementDate->add(new \DateInterval('P' . $userSettings->getRetirementAge() . 'Y'));
            $saveSpeed = $shouldIncrease / $retirementDate->diff($this->currentDateTime)->days * 365 / 12;
        }

        return [
            'targetCapital' => $targetCapital,
            'actives' => $actives,
            'passives' => $passives,
            'shouldIncrease' => $shouldIncrease,
            'saveSpeed' => $saveSpeed,
            'pension' => $pension,
            'futurePension' => $futurePension,
            'capital' => $capital,
            'predictInterest' => $predictInterest,
            'lastYearInterest' => $lastYearInterest
        ];
    }
}
