<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Entities\CandleEntity;
use App\Entities\MarketInstrumentEntity;
use App\Interfaces\CandleServiceInterface;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Storages\CandleStorage;

class CandleServiceCacheDecorator implements CandleServiceInterface
{
    private CandleService $candleService;
    private CandleStorage $candleStorage;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private \DateTime $now;

    private const CACHE_CANDLE_SECONDS = 10 * 60;

    public function __construct(
        CandleService $candleService,
        CandleStorage $candleStorage,
        MarketInstrumentServiceInterface $marketInstrumentService,
        \DateTime $now
    ) {
        $this->candleService = $candleService;
        $this->candleStorage = $candleStorage;
        $this->marketInstrumentService = $marketInstrumentService;
        $this->now = $now;
    }

    public function getTodayCandle(BrokerPortfolioPositionModel $instrument): ?BrokerCandleModel
    {
        $marketInstrument = $this->marketInstrumentService->getInstrumentByFigi($instrument->getFigi());

        $today = clone $this->now;
        $cacheDate = clone $this->now;
        $cacheDate->sub((new \DateInterval('PT' . self::CACHE_CANDLE_SECONDS . 'S')));
        $candle = $this->candleStorage->findOneByInstrumentIdAndIntervalAndAfterDate(
            $marketInstrument->getId(),
            BrokerCandleIntervalType::INTERVAL_DAY,
            $today,
            $cacheDate
        );

        if (null !== $candle) {
            $tiCandle = new BrokerCandleModel([
                'open' => $candle->getOpen(),
                'close' => $candle->getClose(),
                'high' => $candle->getHigh(),
                'low' => $candle->getLow(),
                'time' => $candle->getTime(),
                'volume' => $candle->getVolume()
            ]);
        } else {
            $tiCandle = $this->candleService->getTodayCandle($instrument);

            if (null !== $tiCandle) {
                $candle = new CandleEntity();
                $candle->setFinal((int)$tiCandle->getIsComplete());
                $candle->setOpen($tiCandle->getOpen());
                $candle->setClose($tiCandle->getClose());
                $candle->setHigh($tiCandle->getHigh());
                $candle->setLow($tiCandle->getLow());
                $candle->setVolume($tiCandle->getVolume());
                $candle->setTime($tiCandle->getTime());
                $candle->setCandleInterval(BrokerCandleIntervalType::INTERVAL_DAY);
                $candle->setMarketInstrumentId($marketInstrument->getId());
                $candle->setMarketInstrument($marketInstrument);
                $candle->setCreatedDate($this->now);

                $this->candleStorage->addEntity($candle);
            }
        }
        return $tiCandle;
    }

    public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel {
        $candle = $this->candleStorage
            ->findOneFinalByInstrumentIdAndDate(
                $marketInstrumentEntity->getId(),
                BrokerCandleIntervalType::INTERVAL_DAY,
                $date
            );

        if (null !== $candle) {
            $candleModel = new BrokerCandleModel([
                'open' => $candle->getOpen(),
                'close' => $candle->getClose(),
                'high' => $candle->getHigh(),
                'low' => $candle->getLow(),
                'time' => $candle->getTime(),
                'volume' => $candle->getVolume()
            ]);
        } else {
            $candleModel = $this->candleService->getDayCandleByMarketInstrument($marketInstrumentEntity, $date);

            $candle = new CandleEntity();
            $candle->setMarketInstrumentId($marketInstrumentEntity->getId());
            $candle->setMarketInstrument($marketInstrumentEntity);
            $candle->setFinal((int)$candleModel->getIsComplete());
            $candle->setOpen($candleModel->getOpen());
            $candle->setClose($candleModel->getClose());
            $candle->setLow($candleModel->getLow());
            $candle->setHigh($candleModel->getHigh());
            $candle->setVolume($candleModel->getVolume());
            $candle->setTime($candleModel->getTime());
            $candle->setCandleInterval(BrokerCandleIntervalType::INTERVAL_DAY);
            $candle->setCreatedDate($this->now);

            $this->candleStorage->addEntity($candle);
        }
        return $candleModel;
    }
}
