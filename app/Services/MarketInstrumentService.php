<?php

declare(strict_types=1);

namespace App\Services;

use App\Collections\MarketInstrumentCollection;
use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Storages\MarketCurrencyStorage;
use App\Storages\MarkerInstrumentStorage;

class MarketInstrumentService implements MarketInstrumentServiceInterface
{
    private MarkerInstrumentStorage $marketInstrumentStorage;
    private MarketCurrencyStorage $marketCurrencyStorage;

    public function __construct(
        MarkerInstrumentStorage $marketInstrumentStorage,
        MarketCurrencyStorage $marketCurrencyStorage
    ) {
        $this->marketInstrumentStorage = $marketInstrumentStorage;
        $this->marketCurrencyStorage = $marketCurrencyStorage;
    }

    public function getInstrumentByFigi(string $figi): ?MarketInstrumentEntity
    {
        return $this->marketInstrumentStorage->findOneByFigi($figi);
    }

    public function getStocks(
        string $country = null,
        string $currency = null,
        string $marketSector = null
    ): MarketInstrumentCollection {
        $stockCollection = new MarketInstrumentCollection($this->marketInstrumentStorage->findStocks($currency));

        if (null !== $country) {
            $stockCollection = $stockCollection->filterByCountry($country);
        }

        if (null !== $marketSector) {
            $stockCollection = $stockCollection->filterBySector($marketSector);
        }

        return $stockCollection;
    }

    public function getCurrencyByIso(string $iso): ?MarketCurrencyEntity
    {
        return $this->marketCurrencyStorage->findOneByIso($iso);
    }
}
