<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Collections\CandleCollection;
use App\Entities\MarketInstrumentEntity;
use App\Exceptions\AccountException;
use App\Exceptions\BrokerAdapterException;
use App\Exceptions\ManyRequestException;
use App\Interfaces\CandleServiceInterface;

class CandleService implements CandleServiceInterface
{
    private StockBrokerFactory $stockBrokerFactory;

    public function __construct(StockBrokerFactory $stockBrokerFactory)
    {
        $this->stockBrokerFactory = $stockBrokerFactory;
    }

    /**
     * @throws AccountException
     * @throws ManyRequestException
     */
    public function getTodayCandle(BrokerPortfolioPositionModel $instrument): ?BrokerCandleModel
    {
        $broker = $this->stockBrokerFactory->getBrokerAdapter(StockBrokerFactory::TINKOFF_V2);

        $from = new \DateTime();
        $from->setTime(0, 0, 0);
        $to = new \DateTime();
        $to->setTime(23, 59, 59);

        try {
            $candleCollection = $broker->getHistoryCandles(
                $instrument->getFigi(),
                $from,
                $to,
                BrokerCandleIntervalType::INTERVAL_HOUR
            );
        } catch (BrokerAdapterException $exception) {
            $candleCollection = new CandleCollection();
        }

        return $candleCollection->getLastCandle();
    }

    public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel {
        $broker = $this->stockBrokerFactory->getBrokerAdapter(StockBrokerFactory::TINKOFF_V2);

        $from = clone $date;
        $from->sub(new \DateInterval("P15D"));
        $from->setTime(0, 0, 0);
        $to = clone $date;
        $to->setTime(23, 59, 59);

        try {
            $candleCollection = $broker->getHistoryCandles(
                $marketInstrumentEntity->getFigi(),
                $from,
                $to,
                BrokerCandleIntervalType::INTERVAL_DAY
            );
        } catch (BrokerAdapterException $exception) {
            $candleCollection = new CandleCollection();
        }

        return $candleCollection->getLastCandle();
    }
}
