<?php

declare(strict_types=1);

namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class AuthService
{
    private string $jwtSecret;

    public function __construct(string $jwrSecret)
    {
        $this->jwtSecret = $jwrSecret;
    }

    public function generateJWT(array $payload): string
    {
        return JWT::encode($payload, $this->jwtSecret, 'HS256');
    }

    public function validateJWT(string $jwt): bool
    {
        try {
            JWT::decode($jwt, new Key($this->jwtSecret, 'HS256'));
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function decodeJWT(string $jwt): array
    {
        $jwtDataObject = JWT::decode($jwt, new Key($this->jwtSecret, 'HS256'));
        return json_decode(json_encode($jwtDataObject), true);
    }
}
