<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Collections\OperationCollection as BrokerOperationCollection;
use App\Broker\Models\BrokerOperationModel;
use App\Collections\OperationCollection;
use App\Entities\OperationTypeEntity;
use App\Exceptions\AccountException;
use App\Interfaces\CandleServiceInterface;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\MarketOperationServiceInterface;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\ItemOperationImportCollection;
use App\Item\Entities\ItemOperationEntity;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Models\ImportStatisticModel;
use App\Models\OperationFiltersModel;
use App\Models\OperationModel;
use App\Storages\OperationTypeStorage;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Entities\UserEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MarketOperationsService implements MarketOperationServiceInterface
{
    private StockBrokerFactory $stockBrokerFactory;
    private OperationTypeStorage $operationTypeStorage;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private AccountService $accountService;
    private UserService $userService;
    private BrokerStorage $brokerStorage;
    private ItemOperationStorage $itemOperationStorage;
    private CurrencyService $currencyService;
    private ItemStorage $itemStorage;
    private \DateTime $currentDateTime;
    private UserCredentialService $userCredentialService;
    private AMQPStreamConnection $amqpConnection;
    private ItemOperationImportStorage $itemOperationImportStorage;

    public function __construct(
        StockBrokerFactory $stockBrokerFactory,
        OperationTypeStorage $operationTypeStorage,
        MarketInstrumentServiceInterface $marketInstrumentService,
        AccountService $accountService,
        UserService $userService,
        BrokerStorage $brokerStorage,
        ItemOperationStorage $itemOperationStorage,
        CurrencyService $currencyService,
        ItemStorage $itemStorage,
        \DateTime $currentDateTime,
        UserCredentialService $userCredentialService,
        AMQPStreamConnection $amqpConnection,
        ItemOperationImportStorage $itemOperationImportStorage
    ) {
        $this->stockBrokerFactory = $stockBrokerFactory;
        $this->operationTypeStorage = $operationTypeStorage;
        $this->marketInstrumentService = $marketInstrumentService;
        $this->accountService = $accountService;
        $this->userService = $userService;
        $this->brokerStorage = $brokerStorage;
        $this->itemOperationStorage = $itemOperationStorage;
        $this->currencyService = $currencyService;
        $this->itemStorage = $itemStorage;
        $this->currentDateTime = $currentDateTime;
        $this->userCredentialService = $userCredentialService;
        $this->amqpConnection = $amqpConnection;
        $this->itemOperationImportStorage = $itemOperationImportStorage;
    }

    /**
     * @param OperationFiltersModel $filters
     * @param UserCredentialEntity $credential
     * @return OperationCollection
     * @throws AccountException
     */
    public function getCredentialOperations(
        OperationFiltersModel $filters,
        UserCredentialModel $credential
    ): OperationCollection {
        $accounts = $this->accountService->getCredentialAccounts($credential);

        return $this->getAccountsOperations($accounts, $filters);
    }

    /**
     * @param OperationFiltersModel $filters
     * @return OperationCollection
     * @throws AccountException
     */
    public function getOperations(OperationFiltersModel $filters): OperationCollection
    {
        $accounts = $this->accountService->getUserBrokerAccounts();

        return $this->getAccountsOperations($accounts, $filters);
    }

    public function importBrokerOperations(
        OperationFiltersModel $operationFilter,
        string $brokerId
    ): ImportStatisticModel {
        $user = $this->userService->getUser();

        $credential = $this->userCredentialService->getUserActiveCredential($user->getId(), $brokerId);
        $credentialModel = $this->userCredentialService->convertEntityToModel($credential);

        $operations = $this->getCredentialOperations($operationFilter, $credentialModel);

        return $this->importOperations($operations);
    }

    public function importUserOperations(OperationFiltersModel $operationFilter): ImportStatisticModel
    {
        $operations = $this->getOperations($operationFilter);

        return $this->importOperations($operations);
    }

    public function getOperationsToImport(): array
    {
        $messages = [];
        $channel = $this->amqpConnection->channel();
        $channel->queue_declare('main', false, false, false, false);

        $credentials = $this->userCredentialService->getAllActiveCredentialList();
        /** @var UserCredentialEntity $credential */
        foreach ($credentials->getIterator() as $credential) {
            $imports = new ItemOperationImportCollection(
                $this->itemOperationImportStorage
                    ->findByUserAndBroker($credential->getUserId(), $credential->getBrokerId())
            );
            $lastImport = $imports->getImportWithLastImportDateEnd();

            if ($lastImport === null) {
                $dateFrom = new \DateTime(OperationFiltersModel::$DEFAULT_DATE_FROM);
            } else {
                $dateFrom = clone $lastImport->getImportDateEnd();
                $dateFrom->sub(new \DateInterval('P14D'));
            }

            do {
                $dateTo = clone $dateFrom;
                $dateTo->add(new \DateInterval('P3M'));
                $dateTo->sub(new \DateInterval('P1D'));
                $dateTo = min($dateTo, $this->currentDateTime);

                $message = [
                    'type' => 'operationImport',
                    'data' => [
                        'dateFrom' => $dateFrom->format('Y-m-d'),
                        'dateTo' => $dateTo->format('Y-m-d'),
                        'userId' => $credential->getUserId(),
                        'brokerId' => $credential->getBrokerId()
                    ]
                ];
                $ampqMessage = new AMQPMessage(json_encode($message));
                $channel->basic_publish($ampqMessage, '', 'main');

                $messages[] = $message;
                $dateFrom->add(new \DateInterval('P3M'));
            } while ($dateTo < $this->currentDateTime);
        }

        return $messages;
    }

    /**
     * @param UserBrokerAccountCollection $accounts
     * @param OperationFiltersModel $filters
     * @return OperationCollection
     * @throws AccountException
     */
    private function getAccountsOperations(
        UserBrokerAccountCollection $accounts,
        OperationFiltersModel $filters
    ): OperationCollection {
        if ($accounts->count() === 0) {
            throw new AccountException('You don`t have broker account. Please try Tinkoff Api Key', 1030);
        }

        $dateFrom = $filters->getDateFrom();
        $dateTo = $filters->getDateTo();
        $dateTo->setTime(23, 59, 59);

        $brokerOperations = new BrokerOperationCollection();
        /** @var UserBrokerAccountEntity $account */
        foreach ($accounts->getIterator() as $account) {
            $broker = $this->stockBrokerFactory->getBroker($account->getUserCredential());
            $brokerOperations = new BrokerOperationCollection(
                array_merge(
                    $brokerOperations->toArray(),
                    $broker->getOperations($account->getExternalId(), $dateFrom, $dateTo)->toArray()
                )
            );
        }

        $operations = new OperationCollection();
        /** @var BrokerOperationModel $operation */
        foreach ($brokerOperations->getIterator() as $brokerOperation) {
            $operation = $this->convertBrokerOperationToModel($brokerOperation);
            $operations->add($operation);
        }

        if (null !== $filters->getOperationType() && '' !== $filters->getOperationType()) {
            $operations = $operations->filterByOperationTypes([$filters->getOperationType()]);
        }
        if (null !== $filters->getOperationStatus() && '' !== $filters->getOperationStatus()) {
            $operations = $operations->filterByOperationStatus($filters->getOperationStatus());
        }

        return $operations->getSortedByDate();
    }

    public function getOperationTypeByExternalId(string $externalId): ?OperationTypeEntity
    {
        return $this->operationTypeStorage->findByExternalId($externalId);
    }

    private function convertBrokerOperationToModel(BrokerOperationModel $operation): OperationModel
    {
        $instrument = $operation->getFigi() !== null
            ? $this->marketInstrumentService->getInstrumentByFigi($operation->getFigi())
            : null;

        return new OperationModel([
            'id' => $operation->getId(),
            'parentOperationId' => $operation->getParentOperationId(),
            'type' => $operation->getType(),
            'date' => $operation->getDate(),
            'instrumentType' => $operation->getInstrumentType(),
            'figi' => $operation->getFigi(),
            'quantity' => $operation->getQuantity(),
            'status' => $operation->getStatus(),
            'currency' => $operation->getCurrency(),
            'payment' => $operation->getPayment(),
            'price' => $operation->getPrice(),
            'instrument' => $instrument,
            'broker' => $operation->getBroker()
        ]);
    }

    private function importOperations(OperationCollection $operations): ImportStatisticModel
    {
        $statistic = [
            'skipped' => 0,
            'updated' => 0,
            'added' => 0
        ];

        $user = $this->userService->getUser();

        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $broker = $this->brokerStorage->findById($operation->getBroker());

            $itemOperation = $this->itemOperationStorage
                ->findByExternalIdAndBroker($operation->getId(), $broker->getId());

            if (null === $itemOperation) {
                $itemOperation = $this->convertOperationModelToEntity($operation, $user);
                $this->itemOperationStorage->addEntity($itemOperation);
                $statistic['added'] += 1;
            } elseif ($itemOperation->getStatus() !== $operation->getStatus()) {
                $itemOperation->setStatus($operation->getStatus());
                $this->itemOperationStorage->addEntity($itemOperation);
                $statistic['updated'] += 1;
            } else {
                $statistic['skipped'] += 1;
            }
        }

        return new ImportStatisticModel($statistic);
    }


    private function convertOperationModelToEntity(
        OperationModel $operation,
        UserEntity $user
    ): ItemOperationEntity {
        $item = null;
        $itemId = null;
        $currency = $this->currencyService
            ->getCurrencyByIso($operation->getPayment()->getCurrency());

        $broker = $this->brokerStorage->findById($operation->getBroker());

        if ($operation->getFigi() !== null) {
            $item = $this->itemStorage
                ->findByExternalIdAndSource($operation->getFigi(), 'market');
            $itemId = $item->getId();
        }
        $externalId = $operation->getId() === '' ? null : $operation->getId();

        $operationType = $this->getOperationTypeByExternalId($operation->getType());

        $itemOperation = new ItemOperationEntity();
        $itemOperation->setUserId($user->getId());
        $itemOperation->setItemId($itemId);
        $itemOperation->setItem($item);
        $itemOperation->setExternalId($externalId);
        $itemOperation->setQuantity($operation->getQuantity());
        $itemOperation->setDate($operation->getDate());
        $itemOperation->setAmount($operation->getPayment()->getAmount());
        $itemOperation->setCurrencyId($currency->getId());
        $itemOperation->setCurrency($currency);
        $itemOperation->setCreatedDate($this->currentDateTime);
        $itemOperation->setOperationTypeId($operationType->getId());
        $itemOperation->setOperationType($operationType);
        $itemOperation->setStatus($operation->getStatus());
        $itemOperation->setBrokerId($broker->getId());
        $itemOperation->setBroker($broker);

        return $itemOperation;
    }
}
