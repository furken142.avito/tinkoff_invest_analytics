<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Exceptions\ExchangeUnsupportedCurrencyException;
use App\Interfaces\CandleServiceInterface;
use App\Storages\MarketCurrencyStorage;

/**
 * Class ExchangeCurrency
 * @package App\Services
 */
class ExchangeCurrencyService
{
    private MarketCurrencyStorage $marketCurrencyStorage;
    private CandleServiceInterface $candleService;

    public function __construct(
        MarketCurrencyStorage $marketCurrencyStorage,
        CandleServiceInterface $candleService
    ) {
        $this->marketCurrencyStorage = $marketCurrencyStorage;
        $this->candleService = $candleService;
    }

    /**
     * @param string $fromCurrency
     * @param string $toCurrency
     * @param PortfolioPositionCollection $instruments
     * @return float
     * @throws ExchangeUnsupportedCurrencyException
     */
    public function getExchangeRateByPortfolio(
        string $fromCurrency,
        string $toCurrency,
        PortfolioPositionCollection $instruments
    ): float {
        $fromCurrencyEntity = $this->marketCurrencyStorage->findOneByIso($fromCurrency);

        if (null === $fromCurrencyEntity || null === $fromCurrencyEntity->getFigi()) {
            throw new ExchangeUnsupportedCurrencyException('Convert from ' . $fromCurrency . ' does\'t supported yet');
        }

        if ($toCurrency !== 'RUB') {
            throw new ExchangeUnsupportedCurrencyException('Convert to ' . $toCurrency . ' does\'t supported yet');
        }

        $currency = $instruments->filterByFigi($fromCurrencyEntity->getFigi());
        if (empty($currency)) {
            throw new ExchangeUnsupportedCurrencyException('Can\'t calculate exchange rate for this portfolio');
        }

        if ($currency->getQuantity() === 0.0) {
            throw new ExchangeUnsupportedCurrencyException('Can\'t calculate exchange rate for this portfolio');
        }

        return $currency->getCurrentPrice()->getAmount();
    }

    public function getExchangeRateByDayCandies(string $fromCurrency, string $toCurrency, \DateTime $dateTime): float
    {
        $fromCurrencyExchangeRate = $this->getRubExchangeRateByDayCandle($fromCurrency, $dateTime);
        $toCurrencyExchangeRate = $this->getRubExchangeRateByDayCandle($toCurrency, $dateTime);

        return $fromCurrencyExchangeRate / $toCurrencyExchangeRate;
    }

    private function getRubExchangeRateByDayCandle(string $currency, \DateTime $date): float
    {
        if ($currency === 'RUB') {
            return 1;
        }

        $marketCurrencyEntity = $this->marketCurrencyStorage->findOneByIso($currency);

        if (null === $marketCurrencyEntity) {
            throw new ExchangeUnsupportedCurrencyException(
                'Convert ' . $currency . ' does\'t supported yet',
                1002
            );
        }

        $candle = $this->candleService->getDayCandleByMarketInstrument(
            $marketCurrencyEntity->getMarketInstrument(),
            $date
        );
        return $candle->getClose();
    }
}
