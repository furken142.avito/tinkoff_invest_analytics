<?php

declare(strict_types=1);

namespace App\Services;

use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Intl\Services\CountryService;
use App\Item\Entities\ItemEntity;
use App\Item\Storages\ItemStorage;
use App\Services\Sources\YahooSourceService;
use App\Storages\MarketCurrencyStorage;
use App\Storages\MarkerInstrumentStorage;

class FillMarketInstrumentServiceDecorator implements MarketInstrumentServiceInterface
{
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private ItemStorage $itemStorage;
    private MarketCurrencyStorage $marketCurrencyStorage;
    private StockBrokerFactory $stockBrokerFactory;
    private YahooSourceService $yahooService;
    private CountryService $countryService;

    public function __construct(
        MarketInstrumentServiceInterface $marketInstrumentService,
        ItemStorage $itemStorage,
        MarketCurrencyStorage $marketCurrencyStorage,
        StockBrokerFactory $stockBrokerFactory,
        YahooSourceService $yahooService,
        CountryService $countryService
    ) {
        $this->marketInstrumentService = $marketInstrumentService;
        $this->marketCurrencyStorage = $marketCurrencyStorage;
        $this->stockBrokerFactory = $stockBrokerFactory;
        $this->itemStorage = $itemStorage;
        $this->yahooService = $yahooService;
        $this->countryService = $countryService;
    }

    public function getInstrumentByFigi(string $figi): ?MarketInstrumentEntity
    {
        $instrument = $this->marketInstrumentService->getInstrumentByFigi($figi);

        $broker = $this->stockBrokerFactory->getBrokerAdapter(StockBrokerFactory::TINKOFF_V2);

        if (null === $instrument) {
            $marketInstrument = $broker->getInstrumentByFigi($figi);

            $item = new ItemEntity();
            $item->setName($marketInstrument->getName());
            $item->setType('market');
            $item->setSource('market');
            $item->setExternalId($figi);

            $instrument = new MarketInstrumentEntity();
            $instrument->setFigi($figi);
            $instrument->setCurrency($marketInstrument->getCurrency());
            $instrument->setIsin($marketInstrument->getIsin());
            $instrument->setLot($marketInstrument->getLot());
            $instrument->setMinPriceIncrement($marketInstrument->getMinPriceIncrement());
            $instrument->setName($marketInstrument->getName());
            $instrument->setTicker($marketInstrument->getTicker());
            $instrument->setType($marketInstrument->getType());

            if ($instrument->getType() === 'Stock') {
                $shareModel = $broker->getShareByFigi($marketInstrument->getFigi());
                if (null !== $shareModel) {
                    $marketStock = new MarketStockEntity();
                    $marketStock->setCountryIso($shareModel->getCountry());
                    $marketStock->setCountry($this->countryService->getCountryByIso($shareModel->getCountry()));
                    $marketStock->setIndustry($shareModel->getIndustry());
                    $marketStock->setSector($shareModel->getSector());
                }
                if (null === $shareModel) {
                    $marketStock = $this->yahooService->getCompanyProfile($marketInstrument->getTicker());
                }

                if (null !== $marketStock) {
                    $marketStock->setFigi($figi);
                    $marketStock->setMarketInstrument($instrument);
                    $instrument->setMarketStock($marketStock);
                }
            }

            if ($instrument->getType() === 'Currency') {
                if (null === $this->marketCurrencyStorage->findOneByFigi($figi)) {
                    $currency = $broker->getCurrencyByFigi($figi);
                    $marketCurrencyEntity = new MarketCurrencyEntity();
                    $marketCurrencyEntity->setIso($currency->getIso());
                    $marketCurrencyEntity->setFigi($currency->getFigi());
                    $marketCurrencyEntity->setMarketInstrument($instrument);
                    $instrument->setMarketCurrency($marketCurrencyEntity);
                }
            }

            $item->setMarketInstrument($instrument);
            $instrument->setItem($item);

            $this->itemStorage->addEntity($item);
        }

        return $instrument;
    }

    public function getStocks()
    {
        $broker = $this->stockBrokerFactory->getBrokerAdapter(StockBrokerFactory::TINKOFF_V2);
        return $broker->getShares();
    }

    public function getCurrencyByIso(string $iso): ?MarketCurrencyEntity
    {
        $currency = $this->marketInstrumentService->getCurrencyByIso($iso);

        if (null === $currency) {
            $broker = $this->stockBrokerFactory->getBrokerAdapter(StockBrokerFactory::TINKOFF_V2);
            $currencyModel = $broker->getCurrencies()->filterByIso($iso);

            if (null === $currencyModel) {
                return null;
            }

            $currency = new MarketCurrencyEntity();
            $currency->setIso($iso);
            $currency->setFigi($currencyModel->getFigi());

            $this->marketCurrencyStorage->addEntity($currency);
        }

        return $currency;
    }
}
