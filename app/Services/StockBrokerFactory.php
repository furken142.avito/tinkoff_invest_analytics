<?php

declare(strict_types=1);

namespace App\Services;

use App\Adapters\TIClientAdapter;
use App\Adapters\TIClientV2Adapter;
use App\Broker\Interfaces\BrokerInterface;
use App\Exceptions\AccountException;
use App\User\Entities\UserCredentialEntity;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use ATreschilov\TinkoffInvestApiSdk\TIClient as TIClientV2;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TISiteEnum;

class StockBrokerFactory
{
    public const TINKOFF = 'tinkoff';
    public const TINKOFF_V2 = 'tinkoff2';

    private array $brokerInstances = [];
    private array $stockBrokerAdapters = [];
    private UserService $userService;
    private UserCredentialService $userCredentialService;

    public function __construct(UserService $userService, UserCredentialService $userCredentialService)
    {
        $this->userService = $userService;
        $this->userCredentialService = $userCredentialService;
    }

    public function getBroker(UserCredentialEntity $credential): BrokerInterface
    {
        if (isset($this->brokerInstances[$credential->getId()])) {
            return $this->brokerInstances[$credential->getId()];
        }

        $credentialModel = $this->userCredentialService->convertEntityToModel($credential);

        $this->brokerInstances[$credential->getId()] = match ($credential->getBrokerId()) {
            'tinkoff' => new TIClientAdapter(new TIClient($credentialModel->getApiKey(), TISiteEnum::EXCHANGE)),
            'tinkoff2' => new TIClientV2Adapter(new TIClientV2($credentialModel->getApiKey())),
            default => throw new AccountException('Unknown broker', 1032)
        };

        return $this->brokerInstances[$credential->getId()];
    }

    public function getBrokerAdapter($brokerCode): BrokerInterface
    {
        if (isset($this->stockBrokerAdapters[$brokerCode])) {
            return $this->stockBrokerAdapters[$brokerCode];
        }

        $userCredential = $this->userCredentialService->getUserActiveCredential(
            $this->userService->getUserId(),
            $brokerCode
        );

        if (null === $userCredential) {
            throw new AccountException('Broker list is empty', 1031);
        }

        $credentialModel = $this->userCredentialService->convertEntityToModel($userCredential);
        $broker = match ($brokerCode) {
            'tinkoff' => new TIClientAdapter(new TIClient($credentialModel->getApiKey(), TISiteEnum::EXCHANGE)),
            'tinkoff2' => new TIClientV2Adapter(new TIClientV2($credentialModel->getApiKey())),
            default => throw new AccountException('Unknown broker', 1032)
        };
        $this->stockBrokerAdapters[$brokerCode] = $broker;

        return $this->stockBrokerAdapters[$brokerCode];
    }
}
