<?php

declare(strict_types=1);

namespace App\Adapters;

use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\BondCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\BrokerInterface;
use App\Broker\Models\BrokerAccountModel;
use App\Broker\Models\BrokerBondModel;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Models\BrokerShareModel;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Broker\Types\BrokerInstrumentIdType;
use App\Broker\Types\BrokerInstrumentType;
use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Collections\CandleCollection;
use App\Exceptions\BrokerAdapterException;
use App\User\Exceptions\UserAccessDeniedException;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;
use ATreschilov\TinkoffInvestApiSdk\TIClient;
use Google\Protobuf\Timestamp;
use JetBrains\PhpStorm\Pure;
use Tinkoff\Invest\V1\Bond;
use Tinkoff\Invest\V1\Currency;
use Tinkoff\Invest\V1\HistoricCandle;
use Tinkoff\Invest\V1\MoneyValue;
use Tinkoff\Invest\V1\Operation;
use Tinkoff\Invest\V1\PortfolioPosition;
use Tinkoff\Invest\V1\Quotation;
use Tinkoff\Invest\V1\Share;

class TIClientV2Adapter implements BrokerInterface
{
    private const BROKER = 'tinkoff2';

    private TIClient $client;
    private ?BondCollection $bonds;

    public function __construct(TIClient $client)
    {
        $this->client = $client;
        $this->bonds = null;
    }

    public function getAccounts(): AccountCollection
    {
        $result = [];
        try {
            $accounts = $this->client->getUser()->getAccounts();
        } catch (TIException $e) {
            throw new UserAccessDeniedException($e->getMessage(), 2023);
        }

        foreach ($accounts as $account) {
            $opendedDate = $account->getOpenedDate()->getSeconds() === 0
                ? null
                : $this->convertGoogleTimestampToDate($account->getOpenedDate());
            $closedDate = $account->getClosedDate()->getSeconds() === 0
                ? null
                : $this->convertGoogleTimestampToDate($account->getClosedDate());
            $data = [
                'id' => $account->getId(),
                'type' => $account->getType(),
                'name' => $account->getName(),
                'openedDate' => $opendedDate,
                'closedDate' => $closedDate,
                'status' => $account->getStatus()
            ];
            $result[] = new BrokerAccountModel($data);
        }

        return new AccountCollection($result);
    }

    public function getPortfolio(string $accountId): BrokerPortfolioModel
    {
        $positions = [];

        $tiPortfolio = $this->client->getOperations()->getPortfolio($accountId)->getPositions();

        /** @var PortfolioPosition $instrument */
        foreach ($tiPortfolio as $instrument) {
            $currency = $instrument->getAveragePositionPrice()->getCurrency() !== ''
                ? $instrument->getAveragePositionPrice()->getCurrency()
                : $instrument->getCurrentPrice()->getCurrency();

            $expectedYield = [
                'currency' => $this->getCurrencyISO($currency),
                'amount' => $this->convertQuotationToFloat($instrument->getExpectedYield())
            ];

            $averagePositionPrice = [
                'amount' => $this->convertMoneyValueToFloat($instrument->getAveragePositionPrice()),
                'currency' => $this->getCurrencyISO($currency)
            ];

            $currentPrice = [
                'amount' => $this->convertMoneyValueToFloat($instrument->getCurrentPrice()),
                'currency' => $this->getCurrencyISO($instrument->getCurrentPrice()->getCurrency())
            ];

            $figi = match ($instrument->getFigi()) {
                'USD000UTSTOM' => 'BBG0013HGFT4',
                default => $instrument->getFigi()
            };

            $currentNkd = null;
            if ($instrument->getCurrentNkd() !== null && $instrument->getCurrentNkd()->getCurrency() !== '') {
                $currentNkd = new BrokerMoneyModel([
                    'amount' => $this->convertMoneyValueToFloat($instrument->getCurrentNkd()),
                    'currency' => $this->getCurrencyISO($instrument->getCurrentNkd()->getCurrency())
                ]);
            }

            $data = [
                'instrumentType' => $this->getInstrumentType($instrument->getInstrumentType()),
                'figi' => $figi,
                'quantity' => $this->convertQuotationToFloat($instrument->getQuantity()),
                'averagePositionPrice' => new BrokerMoneyModel($averagePositionPrice),
                'expectedYield' => new BrokerMoneyModel($expectedYield),
                'currentNkd' => $currentNkd,
                'currentPrice' => new BrokerMoneyModel($currentPrice)
            ];
            $positions[] = new BrokerPortfolioPositionModel($data);
        }

        $portfolio = new BrokerPortfolioModel();
        $portfolio->setPositions(new PortfolioPositionCollection($positions));

        return $portfolio;
    }

    public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection
    {
        $currencies = $this->client->getOperations()->getWithdrawLimits($accountId);

        $currencyCollection = new PortfolioBalanceCollection();
        /** @var MoneyValue $currency */
        foreach ($currencies->getMoney() as $currency) {
            $data = [
                'amount' => $this->convertMoneyValueToFloat($currency),
                'currency' => $this->getCurrencyISO($currency->getCurrency()),
                'blockedAmount' => 0
            ];

            /** @var MoneyValue $blockedCurrency */
            foreach ($currencies->getBlocked() as $blockedCurrency) {
                if ($blockedCurrency->getCurrency() === $currency->getCurrency()) {
                    $data['blockedAmount'] = $this->convertMoneyValueToFloat($blockedCurrency);
                }
            }
            $currencyCollection->add(new BrokerPortfolioBalanceModel($data));
        }
        return $currencyCollection;
    }

    public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection
    {
        $operations = $this->client->getOperations()->getOperations($accountId, $from, $to);

        $collection = new OperationCollection();
        /** @var Operation $operation */
        foreach ($operations->getIterator() as $operation) {
            $data = [
                'id' => $operation->getId(),
                'parentOperationId' => $operation->getParentOperationId() === ''
                    ? null
                    : $operation->getParentOperationId(),
                'currency' => $this->getCurrencyISO($operation->getCurrency()),
                'payment' => new BrokerMoneyModel([
                    'amount' => $this->convertMoneyValueToFloat($operation->getPayment()),
                    'currency' => $this->getCurrencyISO($operation->getPayment()->getCurrency())
                ]),
                'price' => $operation->getPrice() === null
                    || $operation->getPrice()->getUnits() === 0 && $operation->getPrice()->getNano() === 0
                    ? null
                    : new BrokerMoneyModel([
                        'amount' => $this->convertMoneyValueToFloat($operation->getPrice()),
                        'currency' => $this->getCurrencyISO($operation->getPrice()->getCurrency())
                    ]),
                'status' => $this->getOperationStatus($operation->getState()),
                'quantity' => $operation->getQuantity() === 0 ? null : $operation->getQuantity(),
                'figi' => $operation->getFigi() === '' ? null : $operation->getFigi(),
                'date' => $this->convertGoogleTimestampToDate($operation->getDate()),
                'type' => $this->getOperationType($operation->getOperationType()),
                'instrumentType' => $operation->getInstrumentType() === ''
                    ? null
                    : $this->getInstrumentType($operation->getInstrumentType()),
                'broker' => self::BROKER
            ];
            $collection->add(new BrokerOperationModel($data));
        }
        return $collection;
    }

    public function getInstrumentByFigi(string $figi): ?BrokerInstrumentModel
    {
        $tiInstrument = $this->client->getInstruments()->getInstrumentBy(
            BrokerInstrumentIdType::FIGI,
            null,
            $figi
        );

        $data = [
            'figi' => $tiInstrument->getFigi(),
            'currency' => $this->getCurrencyISO($tiInstrument->getCurrency()),
            'ticker' => $tiInstrument->getTicker(),
            'type' => $this->getInstrumentType($tiInstrument->getInstrumentType()),
            'name' => $tiInstrument->getName(),
            'isin' => $tiInstrument->getIsin(),
            'minPriceIncrement' => $this->convertQuotationToFloat($tiInstrument->getMinPriceIncrement()),
            'lot' => $tiInstrument->getLot()
        ];

        return new BrokerInstrumentModel($data);
    }

    public function getCurrencyByFigi(string $figi): ?BrokerCurrencyModel
    {
        $currency = $this->client->getInstruments()->getCurrencyBy(BrokerInstrumentIdType::FIGI, null, $figi);

        if ($currency === null) {
            return null;
        }

        return new BrokerCurrencyModel([
            'iso' => $this->getCurrencyISO($currency->getIsoCurrencyName()),
            'figi' => $currency->getFigi()
        ]);
    }

    public function getCurrencies(): BrokerCurrencyCollection
    {
        $currencyList = $this->client->getInstruments()->getCurrencies();

        $collection = new BrokerCurrencyCollection();

        /** @var Currency $currency */
        foreach ($currencyList->getIterator() as $currency) {
            $currencyModel = new BrokerCurrencyModel([
                'iso' => $this->getCurrencyISO($currency->getIsoCurrencyName()),
                'figi' => $currency->getFigi()
            ]);
            $collection->add($currencyModel);
        }

        return $collection;
    }

    /**
     * @param string $figi
     * @param \DateTime $from
     * @param \DateTime $to
     * @param $interval
     * @return CandleCollection
     * @throws BrokerAdapterException
     * @throws \ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException
     */
    public function getHistoryCandles(string $figi, \DateTime $from, \DateTime $to, $interval): CandleCollection
    {
        $bonds = $this->getBonds();
        $bond = $bonds->findByFigi($figi);
        $rate = 1;
        if ($bond !== null) {
            $rate = $this->calculateBondRate($bond);
        };

        $tiInterval = $this->convertIntervalToBrokerInterval($interval);
        $candles = $this->client->getMarketData()->getCandles($figi, $from, $to, $tiInterval);

        $candleCollection = new CandleCollection();
        /** @var HistoricCandle $candle */
        foreach ($candles->getIterator() as $candle) {
            $candleModel = new BrokerCandleModel([
                'open' => $rate * $this->convertQuotationToFloat($candle->getOpen()),
                'close' => $rate * $this->convertQuotationToFloat($candle->getClose()),
                'high' => $rate * $this->convertQuotationToFloat($candle->getHigh()),
                'low' => $rate * $this->convertQuotationToFloat($candle->getLow()),
                'volume' => $candle->getVolume(),
                'time' => $this->convertGoogleTimestampToDate($candle->getTime()),
                'isComplete' => $candle->getIsComplete()
            ]);
            $candleCollection->add($candleModel);
        }

        return $candleCollection;
    }

    public function getShares(): ShareCollection
    {
        $brokerInstruments = $this->client->getInstruments()->getShares();

        $collection = new ShareCollection();
        /** @var Share $share */
        foreach ($brokerInstruments->getIterator() as $share) {
            $instrument = new BrokerInstrumentModel([
                'figi' => $share->getFigi(),
                'currency' => $this->getCurrencyISO($share->getCurrency()),
                'ticker' => $share->getTicker(),
                'name' => $share->getName(),
                'isin' => $share->getIsin(),
                'minPriceIncrement' => $this->convertQuotationToFloat($share->getMinPriceIncrement()),
                'lot' => $share->getLot(),
                'type' => 'Stock'
            ]);
            $data = [
                'figi' => $share->getFigi(),
                'currency' => $this->getCurrencyISO($share->getCurrency()),
                'ticker' => $share->getTicker(),
                'name' => $share->getName(),
                'isin' => $share->getIsin(),
                'minPriceIncrement' => $this->convertQuotationToFloat($share->getMinPriceIncrement()),
                'lot' => $share->getLot(),
                'sector' => $this->getSector($share->getSector()),
                'country' => $share->getCountryOfRisk(),
                'industry' => null,
                'instrument' => $instrument
            ];
            $share = new BrokerShareModel($data);
            $collection->add($share);
        }
        return $collection;
    }

    public function getShareByFigi(string $figi): ?BrokerShareModel
    {
        $share = $this->client->getInstruments()->getShareBy(BrokerInstrumentIdType::FIGI, null, $figi);
        if (null === $share) {
            return null;
        }

        $instrument = new BrokerInstrumentModel([
            'figi' => $share->getFigi(),
            'currency' => $this->getCurrencyISO($share->getCurrency()),
            'ticker' => $share->getTicker(),
            'name' => $share->getName(),
            'isin' => $share->getIsin(),
            'minPriceIncrement' => $this->convertQuotationToFloat($share->getMinPriceIncrement()),
            'lot' => $share->getLot(),
            'type' => 'Stock'
        ]);
        return new BrokerShareModel([
            'figi' => $share->getFigi(),
            'currency' => $this->getCurrencyISO($share->getCurrency()),
            'ticker' => $share->getTicker(),
            'name' => $share->getName(),
            'isin' => $share->getIsin(),
            'minPriceIncrement' => $this->convertQuotationToFloat($share->getMinPriceIncrement()),
            'lot' => $share->getLot(),
            'sector' => $this->getSector($share->getSector()),
            'country' => $share->getCountryOfRisk(),
            'industry' => null,
            'instrument' => $instrument
        ]);
    }

    private function getBonds(): BondCollection
    {
        if (null !== $this->bonds) {
            return $this->bonds;
        }

        $bonds = $this->client->getInstruments()->getBonds();
        $this->bonds = new BondCollection();
        /** @var Bond $tiBond */
        foreach ($bonds->getIterator() as $tiBond) {
            $bond = new BrokerBondModel([
                'figi' => $tiBond->getFigi(),
                'nominal' => new BrokerMoneyModel([
                    'amount' => $this->convertMoneyValueToFloat($tiBond->getNominal()),
                    'currency' => $this->getCurrencyISO($tiBond->getNominal()->getCurrency())
                ])
            ]);
            $this->bonds->add($bond);
        }

        return $this->bonds;
    }

    private function calculateBondRate(BrokerBondModel $bond): float
    {
        return $bond->getNominal()->getAmount() / 100;
    }

    private function getCurrencyISO(string $currency): string
    {
        return match ($currency) {
            'usd' => 'USD',
            'rub' => 'RUB',
            'eur' => 'EUR',
            default => strtoupper($currency)
        };
    }

    private function getSector(string $sector): string
    {
        return match ($sector) {
            'basic_materials' => 'Basic Materials',
            'consumer' => 'Consumer Cyclical',
            'financial_services' => 'Financial Services',
            'real_estate' => 'Real Estate',
            'consumer_defensive' => 'Consumer Defensive',
            'health_care' => 'Health Care',
            'utilities' => 'Utilities',
            'communication_services' => 'Communication Services',
            'energy' => 'Energy',
            'industrials' => 'Industrials',
            'it' => 'Technology',
            default => 'Other'
        };
    }

    private function getInstrumentType(string $type): string
    {
        return match ($type) {
            'share' => BrokerInstrumentType::STOCK,
            'currency' => BrokerInstrumentType::CURRENCY,
            'bond' => BrokerInstrumentType::BOND,
            'etf' => BrokerInstrumentType::ETF,
            'futures' => BrokerInstrumentType::FUTURES,
            default => $type
        };
    }

    /**
     * @param int $status
     * @return string
     * @throws BrokerAdapterException
     */
    private function getOperationStatus(int $status): string
    {
        return match ($status) {
            0 => BrokerOperationStatusType::OPERATION_STATE_UNSPECIFIED,
            1 => BrokerOperationStatusType::OPERATION_STATE_DONE,
            2 => BrokerOperationStatusType::OPERATION_STATE_DECLINED,
            3 => BrokerOperationStatusType::OPERATION_STATE_PROGRESS,
            default => throw new BrokerAdapterException('Unknown operation status', 1070)
        };
    }

    private function getOperationType(int $type): string
    {
        return match ($type) {
            0, 57, 58, 61, 64 => BrokerOperationTypeType::OPERATION_TYPE_UNSPECIFIED,
            1, 51, 54, 60 => BrokerOperationTypeType::OPERATION_TYPE_INPUT,
            2, 5, 8, 11, 13, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 44
                => BrokerOperationTypeType::OPERATION_TYPE_TAX,
            3 => BrokerOperationTypeType::OPERATION_TYPE_SECURITY_OUT,
            4, 21, 23, 26, 63 => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
            6, 10 => BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
            7 => BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
            9, 25, 27, 50, 53, 59 => BrokerOperationTypeType::OPERATION_TYPE_OUTPUT,
            12, 14, 19, 24, 30, 31, 45, 46, 47, 55, 56, 62 => BrokerOperationTypeType::OPERATION_TYPE_FEE,
            15, 20, 28 => BrokerOperationTypeType::OPERATION_TYPE_BUY,
            16 => BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
            17 => BrokerOperationTypeType::OPERATION_TYPE_SECURITY_IN,
            18, 22, 29 => BrokerOperationTypeType::OPERATION_TYPE_SELL,
            43 => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND_CARD,
            default => throw new BrokerAdapterException('Unknown operation type', 1071)
        };
    }

    #[Pure]
    private function convertQuotationToFloat(?Quotation $quotation): ?float
    {
        if (null === $quotation) {
            return null;
        }
        return $quotation->getUnits() + $quotation->getNano() / pow(10, 9);
    }

    #[Pure]
    private function convertMoneyValueToFloat(MoneyValue $moneyValue): float
    {
        return $moneyValue->getUnits() + $moneyValue->getNano() / pow(10, 9);
    }

    private function convertGoogleTimestampToDate(Timestamp $date): \DateTime
    {
        return new \DateTime('@' . $date->getSeconds());
    }

    private function convertIntervalToBrokerInterval(string $interval): int
    {
        return match ($interval) {
            BrokerCandleIntervalType::INTERVAL_1_MIN => 1,
            BrokerCandleIntervalType::INTERVAL_5_MIN => 2,
            BrokerCandleIntervalType::INTERVAL_15_MIN => 3,
            BrokerCandleIntervalType::INTERVAL_HOUR => 4,
            BrokerCandleIntervalType::INTERVAL_DAY => 5,
            default => throw new BrokerAdapterException('Unknown candle interval type', 1072)
        };
    }
}
