<?php

declare(strict_types=1);

namespace App\Adapters;

use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\BrokerInterface;
use App\Broker\Models\BrokerAccountModel;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Models\BrokerShareModel;
use App\Broker\Types\BrokerAccountStatusType;
use App\Broker\Types\BrokerAccountTypeType;
use App\Broker\Types\BrokerCandleIntervalType;
use App\Collections\CandleCollection;
use App\Exceptions\BrokerAdapterException;
use App\Exceptions\ManyRequestException;
use App\User\Exceptions\UserAccessDeniedException;
use jamesRUS52\TinkoffInvest\TICandleIntervalEnum;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TIException;

class TIClientAdapter implements BrokerInterface
{
    private const BROKER = 'tinkoff';

    private TIClient $client;

    public function __construct(TIClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return AccountCollection
     * @throws UserAccessDeniedException
     */
    public function getAccounts(): AccountCollection
    {
        $result = [];
        try {
            $accounts = $this->client->getAccounts();
        } catch (TIException $e) {
            throw new UserAccessDeniedException('Account credentials is wrong', 2023);
        }

        foreach ($accounts as $account) {
            $type = match ($account->getBrokerAccountType()) {
                'Tinkoff' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
                'TinkoffIis' => BrokerAccountTypeType::ACCOUNT_TYPE_IIS,
                default => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL
            };
            $data = [
                'id' => $account->getBrokerAccountId(),
                'type' => $type,
                'name' => null,
                'openedDate' => null,
                'closedDate' => null,
                'status' => BrokerAccountStatusType::ACCOUNT_STATUS_OPEN
            ];
            $result[] = new BrokerAccountModel($data);
        }

        return new AccountCollection($result);
    }

    public function getPortfolio(string $accountId): BrokerPortfolioModel
    {
        $portfolio = new BrokerPortfolioModel();
        $positions = [];

        $this->client->setBrokerAccount($accountId);
        $tiPortfolio = $this->client->getPortfolio();

        $tiInstruments = $tiPortfolio->getAllinstruments();
        foreach ($tiInstruments as $instrument) {
            $averagePositionPrice = [
                'amount' => $instrument->getAveragePositionPrice()->value,
                'currency' => $instrument->getAveragePositionPrice()->currency
            ];
            $expectedYield = [
                'currency' => $instrument->getExpectedYieldCurrency(),
                'amount' => $instrument->getExpectedYieldValue()
            ];
            $currentPrice = [
                'currency' => $instrument->getExpectedYieldCurrency(),
                'amount' => $instrument->getAveragePositionPrice()->value
                    + $instrument->getExpectedYieldValue() / $instrument->getBalance()
            ];
            $data = [
                'instrumentType' => $instrument->getInstrumentType(),
                'figi' => $instrument->getFigi(),
                'quantity' => $instrument->getBalance(),
                'averagePositionPrice' => new BrokerMoneyModel($averagePositionPrice),
                'expectedYield' => new BrokerMoneyModel($expectedYield),
                'currentNkd' => null,
                'currentPrice' => new BrokerMoneyModel($currentPrice)
            ];
            $positions[] = new BrokerPortfolioPositionModel($data);
        }

        $portfolio->setPositions(new PortfolioPositionCollection($positions));

        return $portfolio;
    }

    public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection
    {
        $currencyCollection = new PortfolioBalanceCollection();

        $this->client->setBrokerAccount($accountId);
        $currencies = $this->client->getPortfolio()->getAllCurrencies();
        foreach ($currencies as $currency) {
            $data = [
                'currency' => $currency->getCurrency(),
                'amount' => $currency->getBalance(),
                'blockedAmount' => $currency->getBlocked()
            ];
            $currencyCollection->add(new BrokerPortfolioBalanceModel($data));
        }

        return  $currencyCollection;
    }

    public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection
    {
        $collection = new OperationCollection([]);

        $this->client->setBrokerAccount($accountId);
        $operations = $this->client->getOperations($from, $to);

        foreach ($operations as $operation) {
            $data = [
                'id' => $operation->getId(),
                'parentOperationId' => null,
                'currency' => $operation->getCurrency(),
                'payment' => new BrokerMoneyModel([
                    'amount' => $operation->getPayment(),
                    'currency' => $operation->getCurrency()
                ]),
                'price' => $operation->getPrice() === null
                    ? null
                    : new BrokerMoneyModel([
                        'amount' => $operation->getPrice(),
                        'currency' => $operation->getCurrency()
                ]),
                'status' => $operation->getStatus(),
                'quantity' => $operation->getQuantity(),
                'figi' => $operation->getFigi(),
                'date' => $operation->getDate(),
                'type' => $operation->getOperationType(),
                'instrumentType' => $operation->getInstrumentType(),
                'broker' => self::BROKER
            ];
            $collection->add(new BrokerOperationModel($data));
        }

        return $collection;
    }

    public function getInstrumentByFigi(string $figi): ?BrokerInstrumentModel
    {
        $tiInstrument = $this->client->getInstrumentByFigi($figi);

        $data = [
            'figi' => $tiInstrument->getFigi(),
            'currency' => $tiInstrument->getCurrency(),
            'ticker' => $tiInstrument->getTicker(),
            'type' => $tiInstrument->getType(),
            'name' => $tiInstrument->getName(),
            'isin' => $tiInstrument->getIsin(),
            'minPriceIncrement' => $tiInstrument->getMinPriceIncrement(),
            'lot' => $tiInstrument->getLot()
        ];
        return new BrokerInstrumentModel($data);
    }

    public function getCurrencyByFigi(string $figi): ?BrokerCurrencyModel
    {
        switch ($figi) {
            case 'BBG0013HGFT4':
                return new BrokerCurrencyModel(['iso' => 'USD', 'figi' => 'BBG0013HGFT4']);
            case 'BBG0013HJJ31':
                return new BrokerCurrencyModel(['iso' => 'EUR', 'figi' => 'BBG0013HJJ31']);
            case 'BBG0013HRTL0':
                return new BrokerCurrencyModel(['iso' => 'CNY', 'figi' => 'BBG0013HRTL0']);
            default:
                return null;
        }
    }

    public function getCurrencies(): BrokerCurrencyCollection
    {
        $currencies = $this->client->getCurrencies();

        $currencyCollection = new BrokerCurrencyCollection();
        foreach ($currencies as $currency) {
            $currencyModel = new BrokerCurrencyModel([
                'figi' => $currency->getFigi(),
                'iso' => $currency->getCurrency()
            ]);
            $currencyCollection->add($currencyModel);
        }

        return $currencyCollection;
    }

    public function getHistoryCandles(string $figi, \DateTime $from, \DateTime $to, $interval): CandleCollection
    {
        try {
            $brokerInterval = $this->convertIntervalToBrokerInterval($interval);
            $brokerCandles = $this->client->getHistoryCandles($figi, $from, $to, $brokerInterval);
            $candleCollection = new CandleCollection();
            foreach ($brokerCandles as $candle) {
                $candle = new BrokerCandleModel([
                    'open' => $candle->getOpen(),
                    'close' => $candle->getClose(),
                    'high' => $candle->getHigh(),
                    'low' => $candle->getLow(),
                    'volume' => $candle->getVolume(),
                    'time' => $candle->getTime()
                ]);
                $candleCollection->add($candle);
            }
            return $candleCollection;
        } catch (TIException $e) {
            if ($e->getMessage() === 'Too Many Requests') {
                throw new ManyRequestException('To Many Requests', 1060);
            }

            throw new BrokerAdapterException($e->getMessage(), 1073);
        }
    }

    public function getShares(): ShareCollection
    {
        $instruments = $this->client->getStocks();

        $collection = new ShareCollection();
        foreach ($instruments as $instrument) {
            $instrumentModel = new BrokerInstrumentModel([
                'figi' => $instrument->getFigi(),
                'currency' => $instrument->getCurrency(),
                'ticker' => $instrument->getTicker(),
                'name' => $instrument->getName(),
                'isin' => $instrument->getIsin(),
                'minPriceIncrement' => $instrument->getMinPriceIncrement(),
                'lot' => $instrument->getLot(),
                'type' => 'Stock'
            ]);
            $data = [
                'figi' => $instrument->getFigi(),
                'currency' => $instrument->getCurrency(),
                'ticker' => $instrument->getTicker(),
                'name' => $instrument->getName(),
                'isin' => $instrument->getIsin(),
                'minPriceIncrement' => $instrument->getMinPriceIncrement(),
                'lot' => $instrument->getLot(),
                'sector' => null,
                'country' => null,
                'industry' => null,
                'instrument' => $instrumentModel
            ];
            $share = new BrokerShareModel($data);
            $collection->add($share);
        }

        return $collection;
    }

    public function getShareByFigi(string $figi): ?BrokerShareModel
    {
        $instruments = $this->client->getStocks();
        foreach ($instruments as $share) {
            if ($share->getFigi() === $figi) {
                $instrumentModel = new BrokerInstrumentModel([
                    'figi' => $share->getFigi(),
                    'currency' => $share->getCurrency(),
                    'ticker' => $share->getTicker(),
                    'name' => $share->getName(),
                    'isin' => $share->getIsin(),
                    'minPriceIncrement' => $share->getMinPriceIncrement(),
                    'lot' => $share->getLot(),
                    'type' => 'Stock',
                ]);

                $data = [
                    'figi' => $share->getFigi(),
                    'currency' => $share->getCurrency(),
                    'ticker' => $share->getTicker(),
                    'name' => $share->getName(),
                    'isin' => $share->getIsin(),
                    'minPriceIncrement' => $share->getMinPriceIncrement(),
                    'lot' => $share->getLot(),
                    'sector' => null,
                    'country' => null,
                    'industry' => null,
                    'instrument' => $instrumentModel
                ];
                return new BrokerShareModel($data);
            }
        }

        return null;
    }

    private function convertIntervalToBrokerInterval(string $interval): string
    {
        return match ($interval) {
            BrokerCandleIntervalType::INTERVAL_1_MIN => TICandleIntervalEnum::MIN1,
            BrokerCandleIntervalType::INTERVAL_5_MIN => TICandleIntervalEnum::MIN5,
            BrokerCandleIntervalType::INTERVAL_15_MIN => TICandleIntervalEnum::MIN15,
            BrokerCandleIntervalType::INTERVAL_HOUR => TICandleIntervalEnum::HOUR1,
            BrokerCandleIntervalType::INTERVAL_DAY => TICandleIntervalEnum::DAY,
            default => throw new BrokerAdapterException('Unknown candle interval type', 1072)
        };
    }
}
