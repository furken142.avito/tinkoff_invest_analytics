<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketInstrumentEntity;

interface MarketInstrumentServiceInterface
{
    public function getInstrumentByFigi(string $figi): ?MarketInstrumentEntity;
    public function getCurrencyByIso(string $iso): ?MarketCurrencyEntity;
}
