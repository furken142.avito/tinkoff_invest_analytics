<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Entities\MarketInstrumentEntity;

interface CandleServiceInterface
{
    public function getTodayCandle(BrokerPortfolioPositionModel $instrument): ?BrokerCandleModel;

    public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel;
}
