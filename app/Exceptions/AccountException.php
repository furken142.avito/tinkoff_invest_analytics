<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Common\BaseException;

class AccountException extends BaseException
{
    protected static $CODE = 1030;
}
