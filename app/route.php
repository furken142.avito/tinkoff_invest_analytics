<?php

declare(strict_types=1);

/** @var App $app */

use App\Controllers\AnalyticsController;
use App\Controllers\AssetController;
use App\Controllers\AuthController;
use App\Controllers\ItemController;
use App\Controllers\OperationController;
use App\Controllers\PortfolioController;
use App\Controllers\RealEstateController;
use App\Controllers\StockController;
use App\Controllers\UserController;
use App\Controllers\UserCredentialController;
use App\Controllers\UserStrategyShareController;
use App\Middlewares\JsonBodyParserMiddleware;
use App\Middlewares\JwtMiddleware;
use Slim\App;

$app->add(JsonBodyParserMiddleware::createFromContainer($app));

$app->options('/{routes:.+}', AuthController::class . ':optionRequests');

$app->post('/api/v1/auth/authorization', AuthController::class . ':authorization');
$app->get('/api/v1/portfolio', PortfolioController::class . ':apiPortfolio')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/operations/operation/{id}', OperationController::class . ':getOperation')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/operations/list', OperationController::class . ':apiOperations')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/operations/item/{id}', OperationController::class . ':getItemOperations')
    ->add(JwtMiddleware::class);
$app->put('/api/v1/operations/item/{id}', OperationController::class . ':editOperation')
    ->add(JwtMiddleware::class);
$app->post('/api/v1/operations/add', OperationController::class . ':addOperation')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/operations/payin', OperationController::class . ':getPayIn')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/operations/earning', OperationController::class . ':getEarning')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/operations/status/list', OperationController::class . ':getStatusList')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/user/data', UserController::class . ':data')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/user/credential', UserCredentialController::class . ':getUserCredentialList')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/user/credential/{id}', UserCredentialController::class . ':getUserCredential')
    ->add(JwtMiddleware::class);
$app->post('/api/v1/user/credential', UserCredentialController::class . ':addUserCredential')
    ->add(JwtMiddleware::class);
$app->put('/api/v1/user/credential/{id}', UserCredentialController::class . ':updateUserCredential')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/user/credential/{id}/account', UserCredentialController::class . ':getBrokerAccounts')
    ->add(JwtMiddleware::class);
$app->post(
    '/api/v1/user/credential/{id}/account',
    UserCredentialController::class . ':setBrokerAccounts'
)->add(JwtMiddleware::class);
$app->get('/api/v1/user/settings', UserController::class . ':getSettings')
    ->add(JwtMiddleware::class);
$app->post('/api/v1/user/settings', UserController::class . ':setSettings')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/user/strategy/{type}', UserStrategyShareController::class . ':getTargetShare')
    ->add(JwtMiddleware::class);
$app->post('/api/v1/user/strategy/{type}', UserStrategyShareController::class . ':postTargetShare')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/analytics/summary/today', AnalyticsController::class . ':summaryToday')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/analytics/dashboard', AnalyticsController::class . ':dashboard')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/stock/list', StockController::class . ':list')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/stock/import', StockController::class . ':import')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/stock/filters', StockController::class . ':filters')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/asset/list/{type}', AssetController::class . ':getAssetTypedList')
    ->add(JwtMiddleware::class);
$app->post('/api/v1/asset/crowdfunding/importOperations', AssetController::class . ':postCrowdfundingOperationImport')
    ->add(JwtMiddleware::class);

$app->post('/api/v1/item/operations/import', ItemController::class . ':importOperations')
    ->add(JwtMiddleware::class);

$app->get('/api/v1/real_estate/list', RealEstateController::class . ':getUserRealEstateList')
    ->add(JwtMiddleware::class);
$app->get('/api/v1/real_estate/item/{id}', RealEstateController::class . ':getUserRealEstateItem')
    ->add(JwtMiddleware::class);
$app->put('/api/v1/real_estate/item/{id}', RealEstateController::class . ':updateUserRealEstateItem')
    ->add(JwtMiddleware::class);
$app->post('/api/v1/real_estate/item', RealEstateController::class . ':addUserRealEstateItem')
    ->add(JwtMiddleware::class);
