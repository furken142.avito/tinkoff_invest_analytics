<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\RealEstate\Models\RealEstateModel;

class RealEstateFormatter implements FormatterInterface
{
    private RealEstateModel $realEstate;

    public function __construct(RealEstateModel $realEstate)
    {
        $this->realEstate = $realEstate;
    }

    public function format(): array
    {
        $prices = [];
        if ($this->realEstate->getPrices() !== null) {
            foreach ($this->realEstate->getPrices()->getIterator() as $price) {
                $prices[] = (new RealEstatePriceFormatter($price))->format();
            }
        }

        return [
            'realEstateId' => $this->realEstate->getRealEstateId(),
            'itemId' => $this->realEstate->getItemId(),
            'payoutFrequency' => $this->realEstate->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $this->realEstate->getPayoutFrequencyPeriod(),
            'interestAmount' => $this->realEstate->getInterestAmount(),
            'currency' => $this->realEstate->getCurrency(),
            'prices' => $prices,
            'name' => $this->realEstate->getName()
        ];
    }
}
