<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Entities\MarketInstrumentEntity;
use App\Interfaces\FormatterInterface;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class MarketStockFormatter implements FormatterInterface
{
    private MarketInstrumentEntity $instrument;

    public function __construct(MarketInstrumentEntity $instrument)
    {
        $this->instrument = $instrument;
    }

    #[ArrayShape([
        'id' => "int",
        'figi' => "string",
        'ticker' => "string",
        'isin' => "null|string",
        'name' => "string",
        'currency' => "string",
        'country' => "null|string",
        'sector' => "null|string",
        'industry' => "null|string"
    ])]
    #[Pure]
    public function format(): array
    {
        $country = null;

        if ($this->instrument->getMarketStock()?->getCountry() !== null) {
            $countryEntity = $this->instrument->getMarketStock()->getCountry();
            $country = [
                'iso' => $countryEntity->getIso(),
                'name' => $countryEntity->getName(),
                'continent' => $countryEntity->getContinent()
            ];
        }

        return [
            'id' => $this->instrument->getId(),
            'figi' => $this->instrument->getFigi(),
            'ticker' => $this->instrument->getTicker(),
            'isin' => $this->instrument->getIsin(),
            'name' => $this->instrument->getName(),
            'currency' => $this->instrument->getCurrency(),
            'country' => $country,
            'sector' => $this->instrument->getMarketStock()?->getSector(),
            'industry' => $this->instrument->getMarketStock()?->getIndustry()
        ];
    }
}
