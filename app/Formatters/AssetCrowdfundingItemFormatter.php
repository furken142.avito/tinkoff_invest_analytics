<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\ItemModel;

class AssetCrowdfundingItemFormatter implements FormatterInterface
{
    private ItemModel $item;

    public function __construct(ItemModel $item)
    {
        $this->item = $item;
    }

    public function format(): array
    {
        $asset = $this->item->getAsset();
        return [
            'assetId' => $asset->getAssetId(),
            'itemId' => $this->item->getItemId(),
            'name' => $asset->getName(),
            'amount' => $asset->getNominalAmount(),
            'duration' => $asset->getDuration(),
            'durationPeriod' => $asset->getDurationPeriod(),
            'interestPercent' => $asset->getInterestPercent(),
            'dealDate' => $asset->getDealDate(),
            'payoutFrequency' => $asset->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $asset->getPayoutFrequencyPeriod()
        ];
    }
}
