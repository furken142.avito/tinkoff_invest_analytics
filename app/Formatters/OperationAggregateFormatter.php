<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Collections\OperationAggregateCollection;
use App\Interfaces\FormatterInterface;
use App\Models\OperationAggregateModel;

class OperationAggregateFormatter implements FormatterInterface
{
    private OperationAggregateCollection $operationAggregateCollection;

    public function __construct(OperationAggregateCollection $operationAggregateCollection)
    {
        $this->operationAggregateCollection = $operationAggregateCollection;
    }

    public function format(): array
    {
        $result = [];
        /** @var OperationAggregateModel $operation */
        foreach ($this->operationAggregateCollection as $operation) {
            $result[] = [
                'date' => $operation->getDate(),
                'amount' => $operation->getAmount()
            ];
        }
        return $result;
    }
}
