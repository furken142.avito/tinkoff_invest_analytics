<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Models\UserCredentialModel;

class UserCredentialFormatter implements FormatterInterface
{
    private UserCredentialModel $userCredential;

    public function __construct(UserCredentialModel $userCredential)
    {
        $this->userCredential = $userCredential;
    }
    public function format(): array
    {
        return [
            'userCredentialId' => $this->userCredential->getUserCredentialId(),
            'broker' => $this->userCredential->getBroker(),
            'apiKey' => $this->userCredential->getApiKey(),
            'isActive' => $this->userCredential->getIsActive()
        ];
    }
}
