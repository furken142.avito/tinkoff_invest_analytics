<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\OperationModel;

class OperationFormatter implements FormatterInterface
{
    private OperationModel $operation;

    public function __construct(OperationModel $operation)
    {
        $this->operation = $operation;
    }

    public function format(): array
    {
        return [
            'id' => $this->operation->getItemOperationId(),
            'type' => $this->operation->getOperationType(),
            'itemName' => $this->operation->getItem()?->getName(),
            'broker' => $this->operation->getBrokerId(),
            'amount' => $this->operation->getAmount(),
            'quantity' => $this->operation->getQuantity(),
            'currency' => $this->operation->getCurrencyIso(),
            'date' => $this->operation->getDate(),
            'status' => $this->operation->getStatus()
        ];
    }
}
