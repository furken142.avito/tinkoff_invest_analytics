<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\RealEstate\Models\RealEstatePriceModel;

class RealEstatePriceFormatter implements FormatterInterface
{
    private RealEstatePriceModel $price;

    public function __construct(RealEstatePriceModel $price)
    {
        $this->price = $price;
    }

    public function format(): array
    {
        return [
            'realEstatePriceId' => $this->price->getRealEstatePriceId(),
            'amount' => $this->price->getAmount(),
            'currency' => $this->price->getCurrency(),
            'date' => $this->price->getDate()
        ];
    }
}
