<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\OperationFiltersModel;

class OperationFilterFormatter implements FormatterInterface
{
    private OperationFiltersModel $operationFilterModel;

    public function __construct(OperationFiltersModel $operationFilterModel)
    {
        $this->operationFilterModel = $operationFilterModel;
    }

    public function format(): array
    {
        return [
            'dateFrom' => $this->operationFilterModel->getDateFrom(),
            'dateTo' => $this->operationFilterModel->getDateTo(),
            'operationStatus' => $this->operationFilterModel->getOperationStatus(),
            'operationType' => $this->operationFilterModel->getOperationType(),
            'brokerId' => $this->operationFilterModel->getBrokerId()
        ];
    }
}
