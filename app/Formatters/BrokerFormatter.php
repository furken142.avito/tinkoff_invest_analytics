<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\BrokerCollection;
use App\Item\Models\BrokerModel;

class BrokerFormatter implements FormatterInterface
{
    private BrokerCollection $brokerCollection;

    public function __construct(BrokerCollection $brokerCollection)
    {
        $this->brokerCollection = $brokerCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var BrokerModel $broker */
        foreach ($this->brokerCollection->getIterator() as $broker) {
            $formattedData[] = [
                'id' => $broker->getBrokerId(),
                'name' => $broker->getName(),
            ];
        }
        return $formattedData;
    }
}
