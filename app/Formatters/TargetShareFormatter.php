<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Collections\ShareCollection;
use App\Interfaces\FormatterInterface;
use App\Models\TargetShareModel;

class TargetShareFormatter implements FormatterInterface
{
    private ShareCollection $shareCollection;

    public function __construct(ShareCollection $shareCollection)
    {
        $this->shareCollection = $shareCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var TargetShareModel $share */
        foreach ($this->shareCollection->getIterator() as $share) {
            $formattedData[] = [
                'name' => $share->getName(),
                'value' => $share->getValue(),
                'externalId' => $share->getExternalId()
            ];
        }

        return $formattedData;
    }
}
