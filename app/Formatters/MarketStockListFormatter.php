<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Collections\MarketInstrumentCollection;
use App\Entities\MarketInstrumentEntity;
use App\Interfaces\FormatterInterface;

class MarketStockListFormatter implements FormatterInterface
{
    private MarketInstrumentCollection $collection;

    public function __construct(MarketInstrumentCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var MarketInstrumentEntity $instrument */
        foreach ($this->collection->getIterator() as $instrument) {
            $formattedData[] = (new MarketStockFormatter($instrument))->format();
        }

        return $formattedData;
    }
}
