<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserCredentialEntity;

class UserCredentialListFormatter implements FormatterInterface
{
    private UserCredentialCollection $collection;

    public function __construct(UserCredentialCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var UserCredentialEntity $credentialItem */
        foreach ($this->collection->getIterator() as $credentialItem) {
            $formattedData[] = (new UserCredentialFormatter($credentialItem))->format();
        }

        return $formattedData;
    }
}
