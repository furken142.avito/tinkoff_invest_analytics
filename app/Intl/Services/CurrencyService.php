<?php

declare(strict_types=1);

namespace App\Intl\Services;

use App\Intl\Collections\CurrencyCollection;
use App\Intl\Entities\CurrencyEntity;
use App\Intl\Storages\CurrencyStorage;

class CurrencyService
{
    private CurrencyStorage $currencyStorage;

    public function __construct(CurrencyStorage $currencyStorage)
    {
        $this->currencyStorage = $currencyStorage;
    }

    public function getList(): CurrencyCollection
    {
        return new CurrencyCollection($this->currencyStorage->findAll());
    }

    public function getCurrencyByIso(string $iso): ?CurrencyEntity
    {
        return $this->currencyStorage->findOneByIso($iso);
    }
}
