<?php

declare(strict_types=1);

namespace App\Intl\Services;

use App\Intl\Collections\MarketSectorCollection;
use App\Storages\MarketSectorStorage;

class MarketSectorService
{
    private MarketSectorStorage $marketSectorStorage;

    public function __construct(MarketSectorStorage $marketSectorStorage)
    {
        $this->marketSectorStorage = $marketSectorStorage;
    }

    public function getList(): MarketSectorCollection
    {
        return new MarketSectorCollection($this->marketSectorStorage->findAll());
    }

    public function getNameById(int $marketSectorId): string|null
    {
        $marketSector = $this->marketSectorStorage->findById($marketSectorId);
        return $marketSector?->getName();
    }
}
