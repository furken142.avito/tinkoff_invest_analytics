<?php

declare(strict_types=1);

use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Storages\CountryStorage;
use App\Intl\Storages\CurrencyStorage;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(CountryStorage::class, function (ContainerInterface $c) {
    return new CountryStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CurrencyStorage::class, function (ContainerInterface $c) {
    return new CurrencyStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CountryService::class, function (ContainerInterface $c) {
    return new CountryService(
        $c->get(CountryStorage::class)
    );
});

$container->set(CurrencyService::class, function (ContainerInterface $c) {
    return new CurrencyService(
        $c->get(CurrencyStorage::class)
    );
});
