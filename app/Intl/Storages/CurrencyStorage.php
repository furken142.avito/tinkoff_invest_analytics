<?php

declare(strict_types=1);

namespace App\Intl\Storages;

use App\Intl\Entities\CurrencyEntity;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return CurrencyEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(CurrencyEntity::class)
            ->findAll();
    }

    public function findOneByIso(string $iso): ?CurrencyEntity
    {
        return $this->entityManager->getRepository(CurrencyEntity::class)
            ->findOneBy(['iso' => $iso]);
    }
}
