<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\CandleEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class CandleStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $marketInstrumentId
     * @param string $candleInterval
     * @param \DateTime $dateTime
     * @param \DateTime $createdDate
     * @return CandleEntity|null
     * @throws \Exception
     */
    public function findOneByInstrumentIdAndIntervalAndAfterDate(
        int $marketInstrumentId,
        string $candleInterval,
        \DateTime $dateTime,
        \DateTime $createdDate
    ): object|null {
        $startCandleDate = new \DateTime($dateTime->format('Y-m-d'));
        $startCandleDate->setTime(0, 0, 0);
        $endCandleDate = new \DateTime($dateTime->format('Y-m-d'));
        $endCandleDate->setTime(23, 59, 59);
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval))
            ->andWhere(Criteria::expr()->gte('createdDate', $createdDate))
            ->andWhere(Criteria::expr()->gte('time', $startCandleDate))
            ->andWhere(Criteria::expr()->lte('time', $endCandleDate))
            ->orderBy(['createdDate' => Criteria::DESC])
            ->setMaxResults(1);
        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->get(0);
    }

    public function addEntity(CandleEntity $candleEntity): void
    {
        $this->entityManager->persist($candleEntity);
        $this->entityManager->flush();
    }

    public function findOneFinalByInstrumentIdAndDate(
        int $marketInstrumentId,
        string $candleInterval,
        \DateTime $date
    ): ?CandleEntity {
        $startDate = clone $date;
        $startDate->setTime(0, 0, 0);
        $endDate = clone $date;
        $endDate->setTime(23, 59, 59);

        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('isFinal', 1))
            ->andWhere(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval))
            ->andWhere(Criteria::expr()->gte('time', $startDate))
            ->andWhere(Criteria::expr()->lte('time', $endDate))
            ->setMaxResults(1);

        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->get(0);
    }
}
