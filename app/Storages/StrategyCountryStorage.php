<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\StrategyCountryEntity;
use Doctrine\ORM\EntityManagerInterface;

class StrategyCountryStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return StrategyCountryEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(StrategyCountryEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param StrategyCountryEntity[] $strategyCountryEntityArray
     * @return void
     */
    public function updateArrayEntities(array $strategyCountryEntityArray)
    {
        foreach ($strategyCountryEntityArray as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }
}
