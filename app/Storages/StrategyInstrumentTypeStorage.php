<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\StrategyInstrumentTypeEntity;
use Doctrine\ORM\EntityManagerInterface;

class StrategyInstrumentTypeStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return StrategyInstrumentTypeEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(StrategyInstrumentTypeEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param StrategyInstrumentTypeEntity[] $strategyInstrumentEntityArray
     * @return void
     */
    public function updateArrayEntities(array $strategyInstrumentEntityArray)
    {
        foreach ($strategyInstrumentEntityArray as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }
}
