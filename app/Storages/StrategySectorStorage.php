<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\StrategySectorEntity;
use Doctrine\ORM\EntityManagerInterface;

class StrategySectorStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return StrategySectorEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(StrategySectorEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param StrategySectorEntity[] $strategySectorEntityArray
     * @return void
     */
    public function updateArrayEntities(array $strategySectorEntityArray)
    {
        foreach ($strategySectorEntityArray as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }
}
