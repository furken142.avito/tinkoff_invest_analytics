<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\MarketInstrumentEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarkerInstrumentStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $figi
     * @return MarketInstrumentEntity|null
     */
    public function findOneByFigi(string $figi): object|null
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findOneBy(['figi' => $figi]);
    }

    /**
     * @param string|null $currency
     * @return MarketInstrumentEntity[]
     */
    public function findStocks(string $currency = null): array
    {
        $criteria['type'] = 'Stock';
        if (null !== $currency) {
            $criteria['currency'] = $currency;
        }

        return $this->entityManager->getRepository(MarketInstrumentEntity::class)->findBy($criteria);
    }
}
