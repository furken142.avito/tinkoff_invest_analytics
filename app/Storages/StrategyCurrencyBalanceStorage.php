<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\StrategyCurrencyBalanceEntity;
use Doctrine\ORM\EntityManagerInterface;

class StrategyCurrencyBalanceStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return StrategyCurrencyBalanceEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(StrategyCurrencyBalanceEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param StrategyCurrencyBalanceEntity[] $strategyCurrencyBalanceEntityArray
     * @return void
     */
    public function updateArrayEntities(array $strategyCurrencyBalanceEntityArray)
    {
        foreach ($strategyCurrencyBalanceEntityArray as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }
}
