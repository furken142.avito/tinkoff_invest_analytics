<?php

declare(strict_types=1);

use App\Common\HttpClient;
use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\MarketSectorService;
use App\Intl\Storages\CurrencyStorage;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService as ItemPortfolioServiceAlias;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Middlewares\JwtMiddleware;
use App\RealEstate\Services\RealEstateService;
use App\Services\AccountService;
use App\Services\AnalyticsService;
use App\Services\AuthService;
use App\Services\CandleService;
use App\Services\CandleServiceCacheDecorator;
use App\Services\ExchangeCurrencyService;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\JsonValidatorService;
use App\Services\MarketInstrumentService;
use App\Services\MarketOperationServiceFillDecorator;
use App\Services\MarketOperationsService;
use App\Services\PortfolioService;
use App\Services\ShareStrategyFactory;
use App\Services\StockBrokerFactory;
use App\Services\StockService;
use App\Services\StrategyService;
use App\Services\Sources\YahooSourceService;
use App\Storages\CandleStorage;
use App\Storages\InstrumentTypeStorage;
use App\Storages\MarketCountryStorage;
use App\Storages\MarketCurrencyStorage;
use App\Storages\MarkerInstrumentStorage;
use App\Storages\MarketSectorStorage;
use App\Storages\OperationTypeStorage;
use App\Storages\StrategyCountryStorage;
use App\Storages\StrategyCurrencyBalanceStorage;
use App\Storages\StrategyCurrencyStorage;
use App\Storages\StrategyInstrumentTypeStorage;
use App\Storages\StrategySectorStorage;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use GuzzleHttp\Client;
use Opis\JsonSchema\Errors\ErrorFormatter as ErrorFormatter;
use Opis\JsonSchema\Validator;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Psr\Container\ContainerInterface;

require_once __DIR__ . '/controller_dependencies.php';
require_once __DIR__ . '/consumer_dependencies.php';
require_once __DIR__ . '/storage_dependencies.php';

/** @var ContainerInterface $container */
$container->set(AccountService::class, function (ContainerInterface $c) {
    return new AccountService(
        $c->get(StockBrokerFactory::class),
        $c->get(PortfolioService::class),
        $c->get(UserBrokerAccountService::class),
        $c->get(UserService::class)
    );
});

$container->set(JsonValidatorService::class, function (ContainerInterface $c) {
    $validator = new Validator();
    $errorFormatter = new ErrorFormatter();
    return new JsonValidatorService(
        $validator,
        $errorFormatter
    );
});
$container->set(PortfolioService::class, function (ContainerInterface $c) {
    return new PortfolioService(
        $c->get(StrategyService::class),
        $c->get(ExchangeCurrencyService::class)
    );
});

$container->set(StrategyService::class, function (ContainerInterface $c) {
    return new StrategyService(
        $c->get(ShareStrategyFactory::class),
        $c->get(UserService::class)
    );
});

$container->set(ShareStrategyFactory::class, function (ContainerInterface $c) {
    return new ShareStrategyFactory(
        $c->get('Now' . DateTime::class),
        $c->get(FillMarketInstrumentServiceDecorator::class),
        $c->get(CountryService::class),
        $c->get(StrategyInstrumentTypeStorage::class),
        $c->get(StrategyCurrencyStorage::class),
        $c->get(StrategyCurrencyBalanceStorage::class),
        $c->get(StrategyCountryStorage::class),
        $c->get(StrategySectorStorage::class),
        $c->get(CurrencyStorage::class),
        $c->get(InstrumentTypeStorage::class),
        $c->get(MarketCurrencyStorage::class),
        $c->get(MarketCountryStorage::class),
        $c->get(MarketSectorStorage::class)
    );
});

$container->set(MarketOperationsService::class, function (ContainerInterface $c) {
    return new MarketOperationsService(
        $c->get(StockBrokerFactory::class),
        $c->get(OperationTypeStorage::class),
        $c->get(FillMarketInstrumentServiceDecorator::class),
        $c->get(AccountService::class),
        $c->get(UserService::class),
        $c->get(BrokerStorage::class),
        $c->get(ItemOperationStorage::class),
        $c->get(CurrencyService::class),
        $c->get(ItemStorage::class),
        $c->get('Now' . DateTime::class),
        $c->get(UserCredentialService::class),
        $c->get(AMQPStreamConnection::class),
        $c->get(ItemOperationImportStorage::class)
    );
});

$container->set(MarketOperationServiceFillDecorator::class, function (ContainerInterface $c) {
    return new MarketOperationServiceFillDecorator(
        $c->get('Now' . DateTime::class),
        $c->get(UserService::class),
        $c->get(MarketOperationsService::class),
        $c->get(ItemOperationImportStorage::class)
    );
});

$container->set(MarketInstrumentService::class, function (ContainerInterface $c) {
    return new MarketInstrumentService(
        $c->get(MarkerInstrumentStorage::class),
        $c->get(MarketCurrencyStorage::class)
    );
});

$container->set(FillMarketInstrumentServiceDecorator::class, function (ContainerInterface $c) {
    return new FillMarketInstrumentServiceDecorator(
        $c->get(MarketInstrumentService::class),
        $c->get(ItemStorage::class),
        $c->get(MarketCurrencyStorage::class),
        $c->get(StockBrokerFactory::class),
        $c->get(YahooSourceService::class),
        $c->get(CountryService::class)
    );
});

$container->set('YAHOO_DATA' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://query2.finance.yahoo.com/'
    ]);
    return new HttpClient($client);
});

$container->set(YahooSourceService::class, function (ContainerInterface $c) {
    return new YahooSourceService(
        $c->get('YAHOO_DATA' . HttpClient::class),
        $c->get(CountryService::class)
    );
});

$container->set(StockBrokerFactory::class, function (ContainerInterface $c) {
    return new StockBrokerFactory(
        $c->get(UserService::class),
        $c->get(UserCredentialService::class)
    );
});

$container->set(AuthService::class, function () {
    $secret = getenv('JWT_SECRET');
    return new AuthService($secret) ;
});

$container->set(JwtMiddleware::class, function (ContainerInterface $c) {
    return new JwtMiddleware(
        $c->get(AuthService::class),
        $c->get(UserService::class)
    );
});

$container->set(ExchangeCurrencyService::class, function (ContainerInterface $c) {
    return new ExchangeCurrencyService(
        $c->get(MarketCurrencyStorage::class),
        $c->get(CandleServiceCacheDecorator::class)
    );
});

$container->set(CandleService::class, function (ContainerInterface $c) {
    return new CandleService(
        $c->get(StockBrokerFactory::class)
    );
});

$container->set(CandleServiceCacheDecorator::class, function (ContainerInterface $c) {
    return new CandleServiceCacheDecorator(
        $c->get(CandleService::class),
        $c->get(CandleStorage::class),
        $c->get(FillMarketInstrumentServiceDecorator::class),
        $c->get('Now' . DateTime::class)
    );
});

$container->set(AnalyticsService::class, function (ContainerInterface $c) {
    return new AnalyticsService(
        $c->get(CandleServiceCacheDecorator::class),
        $c->get(AccountService::class),
        $c->get(PortfolioService::class),
        $c->get(ExchangeCurrencyService::class),
        $c->get('Now' . DateTime::class),
        $c->get(UserService::class),
        $c->get(ItemPortfolioServiceAlias::class),
        $c->get(OperationService::class),
        $c->get(RealEstateService::class)
    );
});

$container->set(StockService::class, function (ContainerInterface $c) {
    return new StockService(
        $c->get(MarketInstrumentService::class),
        $c->get(UserService::class),
        $c->get(AMQPStreamConnection::class),
        $c->get(CountryService::class),
        $c->get(CurrencyService::class),
        $c->get(MarketSectorService::class),
        $c->get(FillMarketInstrumentServiceDecorator::class)
    );
});

$container->set(MarketSectorService::class, function (ContainerInterface $c) {
    return new MarketSectorService(
        $c->get(MarketSectorStorage::class)
    );
});
