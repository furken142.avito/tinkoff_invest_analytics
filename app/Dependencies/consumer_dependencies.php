<?php

declare(strict_types=1);

use App\Consumers\ItemOperationImportConsumer;
use App\Consumers\StockConsumer;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\MarketOperationServiceFillDecorator;
use App\User\Services\UserService;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(StockConsumer::class, function (ContainerInterface $c) {
    return new StockConsumer(
        $c->get(FillMarketInstrumentServiceDecorator::class),
        $c->get(UserService::class)
    );
});

$container->set(ItemOperationImportConsumer::class, function (ContainerInterface $c) {
    return new ItemOperationImportConsumer(
        $c->get(UserService::class),
        $c->get(MarketOperationServiceFillDecorator::class)
    );
});
