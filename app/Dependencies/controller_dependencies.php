<?php

declare(strict_types=1);

use App\Assets\Services\AssetService;
use App\Controllers\AssetController;
use App\Controllers\AuthController;
use App\Controllers\ItemController;
use App\Controllers\OperationController;
use App\Controllers\PortfolioController;
use App\Controllers\RealEstateController;
use App\Controllers\StockController;
use App\Controllers\UserController;
use App\Controllers\UserCredentialController;
use App\Controllers\UserStrategyShareController;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService as ItemPortfolioService;
use App\RealEstate\Services\RealEstateService;
use App\Services\AccountService;
use App\Services\AnalyticsService;
use App\Services\JsonValidatorService;
use App\Services\MarketOperationsService;
use App\Services\PortfolioService;
use App\Services\StockService;
use App\Services\StrategyService;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use Psr\Container\ContainerInterface;
use App\Controllers\AnalyticsController;

/** @var ContainerInterface $container */
$container->set(PortfolioController::class, function (ContainerInterface $c) {
    return new PortfolioController(
        $c->get(PortfolioService::class),
        $c->get(AccountService::class)
    );
});

$container->set(OperationController::class, function (ContainerInterface $c) {
    return new OperationController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(OperationService::class)
    );
});

$container->set(UserController::class, function (ContainerInterface $c) {
    return new UserController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class)
    );
});

$container->set(AuthController::class, function (ContainerInterface $c) {
    return new AuthController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class)
    );
});

$container->set(AnalyticsController::class, function (ContainerInterface $c) {
    return new AnalyticsController(
        $c->get(AnalyticsService::class)
    );
});

$container->set(StockController::class, function (ContainerInterface $c) {
    return new StockController(
        $c->get(StockService::class),
        $c->get(JsonValidatorService::class)
    );
});

$container->set(UserCredentialController::class, function (ContainerInterface $c) {
    return new UserCredentialController(
        $c->get(UserService::class),
        $c->get(UserCredentialService::class),
        $c->get(UserBrokerAccountService::class),
        $c->get(JsonValidatorService::class)
    );
});

$container->set(UserStrategyShareController::class, function (ContainerInterface $c) {
    return new UserStrategyShareController(
        $c->get(JsonValidatorService::class),
        $c->get(StrategyService::class)
    );
});

$container->set(AssetController::class, function (ContainerInterface $c) {
    return new AssetController(
        $c->get(JsonValidatorService::class),
        $c->get(AssetService::class),
        $c->get(UserService::class),
        $c->get(ItemPortfolioService::class),
        $c->get(ItemService::class)
    );
});

$container->set(ItemController::class, function (ContainerInterface $c) {
    return new ItemController(
        $c->get(MarketOperationsService::class)
    );
});

$container->set(RealEstateController::class, function (ContainerInterface $c) {
    return new RealEstateController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(RealEstateService::class)
    );
});
