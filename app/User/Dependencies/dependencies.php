<?php

declare(strict_types=1);

use App\Common\HttpClient;
use App\Services\AuthService;
use App\Services\StockBrokerFactory;
use App\User\Services\EncryptService;
use App\User\Services\GoogleAuthService;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use App\User\Services\YandexAuthService;
use App\User\Storages\UserAuthStorage;
use App\User\Storages\UserBrokerAccountStorage;
use App\User\Storages\UserCredentialStorage;
use App\User\Storages\UserSettingsStorage;
use App\User\Storages\UserStorage;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set('YANDEX_LOGIN' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://login.yandex.ru/'
    ]);
    return new HttpClient($client);
});

/** @var ContainerInterface $container */
$container->set('GOOGLE_LOGIN' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://people.googleapis.com/v1/'
    ]);
    return new HttpClient($client);
});

$container->set(YandexAuthService::class, function (ContainerInterface $c) {
    return new YandexAuthService(
        $c->get('YANDEX_LOGIN' . HttpClient::class)
    );
});

$container->set(GoogleAuthService::class, function (ContainerInterface $c) {
    return new GoogleAuthService(
        $c->get('GOOGLE_LOGIN' . HttpClient::class)
    );
});

$container->set(UserStorage::class, function (ContainerInterface $c) {
    return new UserStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserAuthStorage::class, function (ContainerInterface $c) {
    return new UserAuthStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserService::class, function (ContainerInterface $c) {
    return new UserService(
        $c->get(UserStorage::class),
        $c->get(UserAuthStorage::class),
        $c->get(UserSettingsStorage::class),
        $c->get(YandexAuthService::class),
        $c->get(GoogleAuthService::class),
        $c->get(AuthService::class)
    );
});

$container->set(UserCredentialStorage::class, function (ContainerInterface $c) {
    return new UserCredentialStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserBrokerAccountStorage::class, function (ContainerInterface $c) {
    return new UserBrokerAccountStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserSettingsStorage::class, function (ContainerInterface $c) {
    return new UserSettingsStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserCredentialService::class, function (ContainerInterface $c) {
    return new UserCredentialService(
        $c->get(EncryptService::class),
        $c->get(UserCredentialStorage::class),
        $c->get(UserService::class)
    );
});

$container->set(UserBrokerAccountService::class, function (ContainerInterface $c) {
    return new UserBrokerAccountService(
        $c->get(StockBrokerFactory::class),
        $c->get(UserCredentialService::class),
        $c->get(UserBrokerAccountStorage::class)
    );
});

$container->set(EncryptService::class, function (ContainerInterface $c) {
    $key = getenv('ENCRYPT_KEY');
    return new EncryptService(
        $key
    );
});
