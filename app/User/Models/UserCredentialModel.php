<?php

declare(strict_types=1);

namespace App\User\Models;

use App\Common\Models\BaseModel;

class UserCredentialModel extends BaseModel
{
    private ?int $userCredentialId;
    private int $userId;
    private string $broker;
    private string $apiKey;
    private ?int $isActive = null;

    /**
     * @return int|null
     */
    public function getUserCredentialId(): ?int
    {
        return $this->userCredentialId;
    }

    /**
     * @param int|null $userCredentialId
     */
    public function setUserCredentialId(?int $userCredentialId): void
    {
        $this->userCredentialId = $userCredentialId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getBroker(): string
    {
        return $this->broker;
    }

    /**
     * @param string $broker
     */
    public function setBroker(string $broker): void
    {
        $this->broker = $broker;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return int|null
     */
    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    /**
     * @param int|null $isActive
     */
    public function setIsActive(?int $isActive): void
    {
        $this->isActive = $isActive;
    }
}
