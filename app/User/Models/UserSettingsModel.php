<?php

namespace App\User\Models;

use App\Common\Models\BaseModel;

class UserSettingsModel extends BaseModel
{
    private ?int $userSettingsId = null;
    private int $userId;
    private ?float $desiredPension = null;
    private ?int $retirementAge = null;

    /**
     * @return int|null
     */
    public function getUserSettingsId(): ?int
    {
        return $this->userSettingsId;
    }

    /**
     * @param int|null $userSettingsId
     */
    public function setUserSettingsId(?int $userSettingsId): void
    {
        $this->userSettingsId = $userSettingsId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return float|null
     */
    public function getDesiredPension(): ?float
    {
        return $this->desiredPension;
    }

    /**
     * @param float|null $desiredPension
     */
    public function setDesiredPension(?float $desiredPension): void
    {
        $this->desiredPension = $desiredPension;
    }

    /**
     * @return int|null
     */
    public function getRetirementAge(): ?int
    {
        return $this->retirementAge;
    }

    /**
     * @param int|null $retirementAge
     */
    public function setRetirementAge(?int $retirementAge): void
    {
        $this->retirementAge = $retirementAge;
    }
}
