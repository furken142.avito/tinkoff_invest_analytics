<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Services\AuthService;
use App\User\Collections\UserCollection;
use App\User\Entities\UserAuthEntity;
use App\User\Entities\UserEntity;
use App\User\Entities\UserSettingsEntity;
use App\User\Exceptions\AuthTypeException;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Models\UserSettingsModel;
use App\User\Storages\UserAuthStorage;
use App\User\Storages\UserSettingsStorage;
use App\User\Storages\UserStorage;
use JetBrains\PhpStorm\Pure;

class UserService
{
    public const YANDEX = 'YANDEX';
    public const GOOGLE = 'GOOGLE';

    private UserStorage $userStorage;
    private UserAuthStorage $userAuthStorage;
    private UserSettingsStorage $userSettingsStorage;
    private AuthServiceInterface $yandexAuthService;
    private AuthServiceInterface $googleAuthService;
    private ?UserEntity $user = null;
    private AuthService $authService;

    public function __construct(
        UserStorage $userStorage,
        UserAuthStorage $userAuthStorage,
        UserSettingsStorage $userSettingsStorage,
        AuthServiceInterface $yandexAuthService,
        AuthServiceInterface $googleAuthService,
        AuthService $authService
    ) {
        $this->userStorage = $userStorage;
        $this->userAuthStorage = $userAuthStorage;
        $this->userSettingsStorage = $userSettingsStorage;
        $this->yandexAuthService = $yandexAuthService;
        $this->googleAuthService = $googleAuthService;
        $this->authService = $authService;
    }

    #[Pure]
    public function getUser(): ?UserEntity
    {
        return $this->user;
    }

    #[Pure]
    public function getUserId(): int
    {
        return $this->getUser()->getId();
    }

    public function setUser(?UserEntity $user): void
    {
        $this->user = $user;
    }

    /**
     * @throws AuthTypeException
     */
    public function createAndLogin(string $authType, string $accessToken): void
    {
        $authService = $this->getAuthService($authType);

        if (null === $authService) {
            throw new AuthTypeException('Auth type is not supported');
        }

        $user = $authService->getUserInfo($accessToken);
        if (null === $user) {
            throw new AuthTypeException('User is not authorized', 2011);
        }

        $this->getAndCreateUser($user);
    }

    public function login(string $token): ?UserEntity
    {
        $userAuth = $this->userAuthStorage->findActiveByToken($token);
        if ($userAuth === null) {
            return $this->user;
        }

        $authService = $this->getAuthService($userAuth->getAuthType());
        $userAuthModel = $authService->getUserInfo($userAuth->getAccessToken());
        if (null !== $userAuthModel && $userAuthModel->getClientId() === $userAuth->getClientId()) {
            $this->setUser($userAuth->getUser());
        }

        return $this->user;
    }

    public function logout(string $token): void
    {
        $userAuth = $this->userAuthStorage->findActiveByToken($token);
        if (null !== $userAuth) {
            $userAuth->setIsActive(0);
            $this->userAuthStorage->addEntity($userAuth);
        }
    }

    private function getAndCreateUser(UserAuthModel $user): void
    {
        $userEntity = $this->userStorage->findByLogin($user->getLogin());
        if (null === $userEntity) {
            $userEntity = new UserEntity();
            $userEntity->setLogin($user->getLogin());
            $userEntity->setEmail($user->getEmail());
            $userEntity->setBirthday($user->getBirthday());
            $this->userStorage->addEntity($userEntity);
        }

        $userAuth = $this->userAuthStorage->findActiveByToken(
            $user->getAccessToken()
        );
        if (null === $userAuth) {
            $userAuth = new UserAuthEntity();
            $userAuth->setAccessToken($user->getAccessToken());
            $userAuth->setAuthType($user->getAuthType());
            $userAuth->setClientId($user->getClientId());
            $userAuth->setIsActive(1);
            $userAuth->setUser($userEntity);
            $this->userAuthStorage->addEntity($userAuth);
        }

        $this->login($userAuth->getAccessToken());
    }

    public function getUserById(int $userId): UserEntity
    {
        return $this->userStorage->findById($userId);
    }

    public function getUserSettings(int $userId): ?UserSettingsEntity
    {
        return $this->userSettingsStorage->findByUserId($userId);
    }

    public function setUserSettings(UserSettingsModel $userSettings): void
    {
        $userSettingsEntity = $this->userSettingsStorage->findByUserId($this->getUserId());
        if ($userSettingsEntity?->getId() !== $userSettings?->getUserSettingsId()) {
            throw new UserAccessDeniedException('Access denied for user', 2024);
        }

        if ($userSettingsEntity === null) {
            $userSettingsEntity = new UserSettingsEntity();
            $userSettingsEntity->setUserId($this->getUserId());
        }

        $userSettingsEntity->setDesiredPension($userSettings->getDesiredPension());
        $userSettingsEntity->setRetirementAge($userSettings->getRetirementAge());
        $this->userSettingsStorage->addEntity($userSettingsEntity);
    }

    public function generateJWT(UserEntity $user, string $authType)
    {
        $payload = [
            'iis' => 'tinkoff_invest_analytics',
            'sub' => 'user data',
            'auth_type' => $authType,
            'userId' => $user->getId(),
            'userLogin' => $user->getLogin()
        ];

        return $this->authService->generateJWT($payload);
    }

    public function getUserList(): UserCollection
    {
        return new UserCollection($this->userStorage->findAll());
    }

    public function addUser(UserEntity $user): int
    {
        $this->userStorage->addEntity($user);
        return $user->getId();
    }

    private function getAuthService(string $authType): ?AuthServiceInterface
    {
        return match ($authType) {
            self::YANDEX => $this->yandexAuthService,
            self::GOOGLE => $this->googleAuthService,
            default => null
        };
    }
}
