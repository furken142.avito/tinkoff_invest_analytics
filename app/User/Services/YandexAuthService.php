<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Common\HttpClient;
use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;

class YandexAuthService implements AuthServiceInterface
{
    private HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getUserInfo(string $accessToken): ?UserAuthModel
    {
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            'info',
            ['format' => 'json'],
            ['Authorization' => 'OAuth ' . $accessToken]
        );

        if ($response->getStatusCode() === 401) {
            return null;
        }

        $responseBody = json_decode($response->getBody()->getContents(), true);
        $modelData = [
            'access_token' => $accessToken,
            'authType' => UserService::YANDEX,
            'login' => $responseBody['login'],
            'email' => $responseBody['default_email'],
            'clientId' => $responseBody['client_id'],
            'birthday' => new \DateTime($responseBody['birthday'])
        ];
        return new UserAuthModel($modelData);
    }
}
