<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Exceptions\DoctrineException;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserCredentialEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Models\UserCredentialModel;
use App\User\Storages\UserCredentialStorage;

class UserCredentialService
{
    private UserCredentialStorage $userCredentialStorage;
    private UserService $userService;
    private EncryptService $encryptService;

    public function __construct(
        EncryptService $encryptService,
        UserCredentialStorage $userCredentialStorage,
        UserService $userService
    ) {
        $this->encryptService = $encryptService;
        $this->userCredentialStorage = $userCredentialStorage;
        $this->userService = $userService;
    }

    public function getCredentialList(int $userId): UserCredentialCollection
    {
        $userCredentialCollection = new UserCredentialCollection();
        $credentialList = $this->userCredentialStorage->findByUserId($userId);
        foreach ($credentialList as $credential) {
            $userCredentialCollection->add($this->convertEntityToModel($credential));
        }
        return $userCredentialCollection;
    }

    public function getActiveCredentialList(int $userId): UserCredentialCollection
    {
        return new UserCredentialCollection($this->userCredentialStorage->findActiveByUserId($userId));
    }

    public function getAllActiveCredentialList(): UserCredentialCollection
    {
        return new UserCredentialCollection($this->userCredentialStorage->findActive());
    }

    public function getUserActiveCredential(int $userId, string $broker): ?UserCredentialEntity
    {
        return $this->userCredentialStorage->findActiveByUserIdAndBroker($userId, $broker);
    }

    public function getUserCredentialById(int $userCredentialId): ?UserCredentialEntity
    {
        $credential = $this->userCredentialStorage->findOneById($userCredentialId);

        if (null === $credential) {
            throw new UserAccessDeniedException('User Credential not found', 2020);
        }

        if ($credential->getUserId() !== $this->userService->getUserId()) {
            throw new UserAccessDeniedException('Access denied for user', 2021);
        }

        return  $credential;
    }

    public function getUserCredentialDecryptedById(int $userCredentialId): ?UserCredentialModel
    {
        $credentialEntity = $this->getUserCredentialById($userCredentialId);
        return $this->convertEntityToModel($credentialEntity);
    }

    public function addUserCredential(UserCredentialModel $userCredentialModel): ?UserCredentialEntity
    {
        $user = $this->userService->getUser();
        $userCredentialModel->setUserId($this->userService->getUserId());
        $userCredentialModel->setIsActive($userCredentialModel->getIsActive() ?? 1);

        $userCredentialEntity = $this->convertModelToEntity($userCredentialModel);
        $userCredentialEntity->setUser($user);

        try {
            $this->userCredentialStorage->addEntity($userCredentialEntity);
        } catch (\Exception $e) {
            throw new DoctrineException($e->getMessage());
        }

        return $userCredentialEntity;
    }

    public function updateUserCredential(UserCredentialModel $userCredentialModel): void
    {
        $userCredentialEntity = $this->userCredentialStorage->findOneById($userCredentialModel->getUserCredentialId());

        if ($this->userService->getUserId() !== $userCredentialEntity->getUserId()) {
            throw new UserAccessDeniedException('Access denied for user', 2022);
        }

        $key = $this->encryptService->encrypt($userCredentialModel->getApiKey());

        $userCredentialEntity->setBrokerId($userCredentialModel->getBroker());
        $userCredentialEntity->setApiKey($key);
        $userCredentialEntity->setActive($userCredentialModel->getIsActive() ?? 1);

        $this->userCredentialStorage->addEntity($userCredentialEntity);
    }

    public function convertModelToEntity(UserCredentialModel $credentialModel): UserCredentialEntity
    {
        $key = $this->encryptService->encrypt($credentialModel->getApiKey());

        $userCredentialEntity = new UserCredentialEntity();
        $userCredentialEntity->setUserId($credentialModel->getUserId());
        $userCredentialEntity->setBrokerId($credentialModel->getBroker());
        $userCredentialEntity->setApiKey($key);
        $userCredentialEntity->setActive($credentialModel->getIsActive());

        return $userCredentialEntity;
    }

    public function convertEntityToModel(UserCredentialEntity $credentialEntity): UserCredentialModel
    {
        $key = $this->encryptService->decrypt($credentialEntity->getApiKey());

        return new UserCredentialModel([
            'userCredentialId' => $credentialEntity->getId(),
            'userId' => $credentialEntity->getUserId(),
            'broker' => $credentialEntity->getBrokerId(),
            'apiKey' => $key,
            'isActive' => $credentialEntity->getIsActive()
        ]);
    }
}
