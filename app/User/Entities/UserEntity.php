<?php

declare(strict_types=1);

namespace App\User\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.users")
 */
class UserEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @var UserAuthEntity[]
     * @ORM\OneToMany (targetEntity="UserAuthEntity", mappedBy="user", fetch="EAGER")
     */
    protected $userAuth;

    /**
     * @var UserCredentialEntity[]
     * @ORM\OneToMany (targetEntity="UserCredentialEntity", mappedBy="user", fetch="EAGER")
     */
    protected $userCredential = [];

    /**
     * @ORM\Column(name="login", type="string")
     */
    protected ?string $login = null;

    /**
     * @ORM\Column(name="email", type="string")
     */
    protected ?string $email = null;

    /**
     * @ORM\Column(name="birthday", type="datetime")
     */
    protected \DateTime|null $birthday = null;

    /**
     * @ORM\Column(name="is_new_user", type="integer")
     */
    protected int $isNewUser = 1;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     */
    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getUserAuth()
    {
        return $this->userAuth;
    }

    /**
     * @param mixed $userAuth
     */
    public function setUserAuth($userAuth): void
    {
        $this->userAuth = $userAuth;
    }

    /**
     * @return UserCredentialEntity[]
     */
    public function getUserCredential(): array
    {
        return $this->userCredential;
    }

    /**
     * @param UserCredentialEntity[] $userCredential
     */
    public function setUserCredential(array $userCredential): void
    {
        $this->userCredential = $userCredential;
    }

    /**
     * @return \DateTime|null
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime|null $birthday
     */
    public function setBirthday(?\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return int
     */
    public function getIsNewUser(): int
    {
        return $this->isNewUser;
    }

    /**
     * @param int $isNewUser
     */
    public function setIsNewUser(int $isNewUser): void
    {
        $this->isNewUser = $isNewUser;
    }
}
