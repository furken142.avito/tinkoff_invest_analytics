<?php

declare(strict_types=1);

namespace App\User\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.user_credential")
 */
class UserCredentialEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="user_credential_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="UserEntity", inversedBy="userCredential", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn (name="user_id", referencedColumnName="user_id")
     */
    protected UserEntity $user;

    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected int $userId;

    /**
     * @ORM\Column(name="broker_id", type="string")
     */
    protected string $brokerId;

    /**
     * @ORM\Column(name="api_key", type="string")
     */
    protected string $apiKey;

    /**
     * @ORM\Column(name="is_active", type="integer")
     */
    protected int $isActive = 1;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getBrokerId(): string
    {
        return $this->brokerId;
    }

    /**
     * @param string $brokerId
     */
    public function setBrokerId(string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }
}
