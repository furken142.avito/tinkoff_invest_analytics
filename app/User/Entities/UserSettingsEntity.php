<?php

namespace App\User\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.user_settings")
 */
class UserSettingsEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="user_settings_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected int $userId;

    /**
     * @ORM\Column(name="desired_pension", type="float")
     */
    protected ?float $desiredPension = null;

    /**
     * @ORM\Column(name="retirement_age", type="float")
     */
    protected ?int $retirementAge = null;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return float|null
     */
    public function getDesiredPension(): ?float
    {
        return $this->desiredPension;
    }

    /**
     * @param float|null $desiredPension
     */
    public function setDesiredPension(?float $desiredPension): void
    {
        $this->desiredPension = $desiredPension;
    }

    /**
     * @return int|null
     */
    public function getRetirementAge(): ?int
    {
        return $this->retirementAge;
    }

    /**
     * @param int|null $retirementAge
     */
    public function setRetirementAge(?int $retirementAge): void
    {
        $this->retirementAge = $retirementAge;
    }
}
