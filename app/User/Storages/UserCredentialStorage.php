<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserCredentialEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserCredentialStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @param string $brokerId
     * @return UserCredentialEntity|null
     */
    public function findActiveByUserIdAndBroker(int $userId, string $brokerId): ?object
    {
        return $this->entityManager->getRepository(UserCredentialEntity::class)
            ->findOneBy(['userId' => $userId, 'brokerId' => $brokerId, 'isActive' => 1]);
    }

    /**
     * @param int $userId
     * @param string $brokerId
     * @return UserCredentialEntity|null
     */
    public function findByUserIdAndBroker(int $userId, string $brokerId): ?object
    {
        return $this->entityManager->getRepository(UserCredentialEntity::class)
            ->findOneBy(['userId' => $userId, 'brokerId' => $brokerId]);
    }

    /**
     * @param int $userId
     * @return UserCredentialEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(UserCredentialEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param int $userId
     * @return UserCredentialEntity[]
     */
    public function findByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(UserCredentialEntity::class)
            ->findBy(['userId' => $userId]);
    }

    public function findOneById(int $userCredentialId): ?UserCredentialEntity
    {
        return $this->entityManager->getRepository(UserCredentialEntity::class)
            ->find($userCredentialId);
    }


    /**
     * @return UserCredentialEntity[]
     */
    public function findActive(): array
    {
        return $this->entityManager->getRepository(UserCredentialEntity::class)
            ->findBy(['isActive' => 1]);
    }

    public function addEntity(UserCredentialEntity $userCredential): void
    {
        $this->entityManager->persist($userCredential);
        $this->entityManager->flush();
    }
}
