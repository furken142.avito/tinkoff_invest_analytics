<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserAuthEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserAuthStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $token
     * @return UserAuthEntity|null
     */
    public function findActiveByToken(string $token): ?object
    {
        return $this->entityManager->getRepository(UserAuthEntity::class)
            ->findOneBy(['accessToken' => $token, 'isActive' => 1]);
    }

    /**
     * @param string $authType
     * @param string $client_id
     * @return UserAuthEntity|null
     */
    public function findByAuthTypeAndClientId(string $authType, string $client_id): ?object
    {
        return $this->entityManager->getRepository(UserAuthEntity::class)
            ->findOneBy(['authType' => $authType, 'clientId' => $client_id]);
    }

    /**
     * @param UserAuthEntity $user
     */
    public function addEntity(UserAuthEntity $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
