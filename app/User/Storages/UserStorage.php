<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $login
     * @return UserEntity|null
     */
    public function findByLogin(string $login): ?object
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findOneBy(['login' => $login]);
    }


    /**
     * @return UserEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findAll();
    }

    /**
     * @param int $id
     * @return UserEntity|null
     */
    public function findById(int $id): ?object
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param UserEntity $user
     */
    public function addEntity(UserEntity $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
