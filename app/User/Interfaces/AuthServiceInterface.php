<?php

declare(strict_types=1);

namespace App\User\Interfaces;

use App\User\Models\UserAuthModel;

interface AuthServiceInterface
{
    public function getUserInfo(string $accessToken): ?UserAuthModel;
}
