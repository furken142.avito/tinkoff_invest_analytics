<?php

declare(strict_types=1);

namespace App\Collections;

use App\Broker\Models\BrokerCandleModel;
use Doctrine\Common\Collections\ArrayCollection;

class CandleCollection extends ArrayCollection
{
    public function getLastCandle(): ?BrokerCandleModel
    {
        if ($this->count() === 0) {
            return null;
        }
        $iterator = $this->getIterator();

        $iterator->uasort(function (BrokerCandleModel $a, BrokerCandleModel $b) {
            if ($a->getTime() == $b->getTime()) {
                return 0;
            }
            return $a->getTime() > $b->getTime() ? -1 : 1;
        });

        return $iterator->current();
    }
}
