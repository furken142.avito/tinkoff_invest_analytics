<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Interfaces\MarketOperationServiceInterface;
use App\Models\OperationFiltersModel;
use App\User\Services\UserService;

class ItemOperationImportConsumer implements ConsumerProcessInterface
{
    private UserService $userService;
    private MarketOperationServiceInterface $operationService;

    public function __construct(
        UserService $userService,
        MarketOperationServiceInterface $operationService
    ) {
        $this->userService = $userService;
        $this->operationService = $operationService;
    }

    public function process(array $data): array
    {
        $user = $this->userService->getUserById($data['userId']);
        $this->userService->setUser($user);

        $filter = new OperationFiltersModel([
            'dateFrom' => new \DateTime($data['dateFrom']),
            'dateTo' => new \DateTime($data['dateTo']),
        ]);
        $brokerId = $data['brokerId'];
        $statistic = $this->operationService->importBrokerOperations($filter, $brokerId);
        return [
            'added' => $statistic->getAdded(),
            'updated' => $statistic->getUpdated(),
            'skipped' => $statistic->getSkipped()
        ];
    }
}
