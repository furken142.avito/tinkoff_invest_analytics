#!/usr/bin/php
<?php

declare(strict_types=1);

use App\Consumers\MainConsumer;
use DI\Container;

require_once __DIR__ . '/../../vendor/autoload.php';

$container = new Container();
require __DIR__ . '/../Common/dependencies.php';

$consumer = new MainConsumer(
    $container
);
$consumer->listen();
