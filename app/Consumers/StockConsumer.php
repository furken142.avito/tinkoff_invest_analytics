<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Services\FillMarketInstrumentServiceDecorator;
use App\User\Services\UserService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Psr\Log\LoggerInterface;

class StockConsumer implements ConsumerProcessInterface
{
    private FillMarketInstrumentServiceDecorator $fillMarketInstrumentServiceDecorator;
    private UserService $userService;

    public function __construct(
        FillMarketInstrumentServiceDecorator $fillMarketInstrumentServiceDecorator,
        UserService $userService
    ) {
        $this->fillMarketInstrumentServiceDecorator = $fillMarketInstrumentServiceDecorator;
        $this->userService = $userService;
    }

    public function process(array $data): array
    {
        $user = $this->userService->getUserById($data['userId']);
        $this->userService->setUser($user);
        $marketInstrument = $this->fillMarketInstrumentServiceDecorator->getInstrumentByFigi($data['figi']);

        return [
            'instrument_id' => $marketInstrument?->getId(),
            'figi' => $marketInstrument?->getFigi()
        ];
    }
}
