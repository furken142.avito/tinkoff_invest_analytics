<?php

declare(strict_types=1);

namespace App\Consumers;

use DI\Container;
use Exception;
use Monolog\Logger;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Psr\Log\LoggerInterface;

class MainConsumer
{
    private Container $container;
    private LoggerInterface $logger;
    private AMQPStreamConnection $amqpConnection;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;
        $this->logger = $container->get(Logger::class);
        $this->amqpConnection = $container->get(AMQPStreamConnection::class);
    }

    public function listen(): void
    {
        $channel = $this->amqpConnection->channel();

        $channel->queue_declare('main', false, false, false, false);

        $channel->basic_consume(
            'main',
            '',
            false,
            true,
            false,
            false,
            array($this, 'process')
        );

        while (count($channel->callbacks)) {
            try {
                $channel->wait(null, false, 60 * 60 * 1);
                sleep(2);
            } catch (Exception $e) {
            }
        }
    }

    public function process($message): void
    {
        $data = json_decode($message->body, true);
        $logStart = [
            'date' => (new \DateTime())->format('Y-m-d H:i:s'),
            'message' => $data
        ];
        $this->logInfo(json_encode($logStart));

        switch ($data['type']) {
            case 'stockImport':
                $service = $this->container->get(StockConsumer::class);
                break;
            case 'operationImport':
                $service = $this->container->get(ItemOperationImportConsumer::class);
                break;
            default:
                return;
        }
        $result = $service->process($data['data']);

        $logMessage = 'Processed ' . json_encode($result);
        $this->logInfo($logMessage);
    }

    private function logInfo(string $logMessage): void
    {
        echo $logMessage . "\n";
        $this->logger->info(
            $logMessage,
            ['Program' => 'Main Consumer']
        );
    }
}
