<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Assets\Models\AssetModel;
use App\Common\Models\BaseModel;

class ItemModel extends BaseModel
{
    public ?int $itemId = null;
    public string $type;
    public string $source;
    public string $externalId;
    public string $name;
    public ?AssetModel $asset = null;

    /**
     * @return int|null
     */
    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    /**
     * @param int|null $itemId
     */
    public function setItemId(?int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return AssetModel|null
     */
    public function getAsset(): ?AssetModel
    {
        return $this->asset;
    }

    /**
     * @param AssetModel|null $asset
     */
    public function setAsset(?AssetModel $asset): void
    {
        $this->asset = $asset;
    }
}
