<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Collections\PortfolioItemCollection;

class PortfolioModel extends BaseModel
{
    private PortfolioBalanceCollection $balance;
    private PortfolioItemCollection $items;

    /**
     * @return PortfolioBalanceCollection
     */
    public function getBalance(): PortfolioBalanceCollection
    {
        return $this->balance;
    }

    /**
     * @param PortfolioBalanceCollection $balance
     */
    public function setBalance(PortfolioBalanceCollection $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return PortfolioItemCollection
     */
    public function getItems(): PortfolioItemCollection
    {
        return $this->items;
    }

    /**
     * @param PortfolioItemCollection $items
     */
    public function setItems(PortfolioItemCollection $items): void
    {
        $this->items = $items;
    }
}
