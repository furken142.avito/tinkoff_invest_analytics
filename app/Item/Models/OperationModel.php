<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;

class OperationModel extends BaseModel
{
    private ?int $itemOperationId = null;
    private int $userId;
    private ?string $brokerId = null;
    private ?int $itemId = null;
    private ?ItemModel $item = null;
    private string $operationType;
    private string $externalId;
    private \DateTime $createdDate;
    private ?float $quantity = null;
    private \DateTime $date;
    private float $amount;
    private string $currencyIso;
    private string $status;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if (!isset($data['createdDate'])) {
            $this->createdDate = new \DateTime();
        }
    }

    /**
     * @return int|null
     */
    public function getItemOperationId(): ?int
    {
        return $this->itemOperationId;
    }

    /**
     * @param int|null $itemOperationId
     */
    public function setItemOperationId(?int $itemOperationId): void
    {
        $this->itemOperationId = $itemOperationId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getBrokerId(): ?string
    {
        return $this->brokerId;
    }

    /**
     * @param string|null $brokerId
     */
    public function setBrokerId(?string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return int|null
     */
    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    /**
     * @param int|null $itemId
     */
    public function setItemId(?int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemModel|null
     */
    public function getItem(): ?ItemModel
    {
        return $this->item;
    }

    /**
     * @param ItemModel|null $item
     */
    public function setItem(?ItemModel $item): void
    {
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getOperationType(): string
    {
        return $this->operationType;
    }

    /**
     * @param string $operationType
     */
    public function setOperationType(string $operationType): void
    {
        $this->operationType = $operationType;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float|null $quantity
     */
    public function setQuantity(?float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
}
