<?php

declare(strict_types=1);

namespace App\Item\Entities;

use App\Assets\Entities\AssetsEntity;
use App\Entities\MarketInstrumentEntity;
use App\RealEstate\Entities\RealEstateEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.item")
 */
class ItemEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="item_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="type", type="string")
     */
    protected string $type;

    /**
     * @ORM\Column(name="source", type="string")
     */
    protected string $source;

    /**
     * @ORM\Column(name="external_id", type="string")
     */
    protected ?string $externalId = null;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected string $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entities\MarketInstrumentEntity", mappedBy="item", cascade={"persist"})
     */
    private ?MarketInstrumentEntity $marketInstrument = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Assets\Entities\AssetsEntity", mappedBy="item", cascade={"persist"})
     */
    private ?AssetsEntity $asset = null;

    /**
     * @ORM\OneToOne(targetEntity="App\RealEstate\Entities\RealEstateEntity", mappedBy="item", cascade={"persist"})
     */
    private ?RealEstateEntity $realEstate = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return MarketInstrumentEntity|null
     */
    public function getMarketInstrument(): ?MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    /**
     * @param MarketInstrumentEntity|null $marketInstrument
     */
    public function setMarketInstrument(?MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    /**
     * @return AssetsEntity|null
     */
    public function getAsset(): ?AssetsEntity
    {
        return $this->asset;
    }

    /**
     * @param AssetsEntity|null $asset
     */
    public function setAsset(?AssetsEntity $asset): void
    {
        $this->asset = $asset;
    }

    /**
     * @return RealEstateEntity|null
     */
    public function getRealEstate(): ?RealEstateEntity
    {
        return $this->realEstate;
    }

    /**
     * @param RealEstateEntity|null $realEstate
     */
    public function setRealEstate(?RealEstateEntity $realEstate): void
    {
        $this->realEstate = $realEstate;
    }
}
