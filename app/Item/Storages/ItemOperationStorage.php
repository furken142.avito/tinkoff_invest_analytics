<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemOperationEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class ItemOperationStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemOperationEntity $itemEntity): int
    {
        $this->entityManager->persist($itemEntity);
        $this->entityManager->flush();

        return $itemEntity->getId();
    }

    public function findByExternalIdAndBroker(string $externalId, string $brokerId): ?ItemOperationEntity
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findOneBy(['externalId' => $externalId, 'brokerId' => $brokerId]);
    }

    public function findByUserItemAmountDateType(
        int $userId,
        string $brokerId,
        int $itemId,
        \DateTime $date,
        int $typeId,
        float $amount
    ): ?ItemOperationEntity {
        $entities = $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy([
                'userId' => $userId,
                'brokerId' => $brokerId,
                'itemId' => $itemId,
                'date' => $date,
                'operationTypeId' => $typeId
            ]);

        /** @var ItemOperationEntity $operation */
        foreach ($entities as $operation) {
            if (sha1($operation->getAmount() . '') === sha1($amount . '')) {
                return $operation;
            }
        }

        return null;
    }

    public function findByUserAndAmountAndDate(
        int $userId,
        float $amount,
        \DateTime $date,
        string $brokerId
    ): ?ItemOperationEntity {
        $entities = $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy(['userId' => $userId, 'date' => $date, 'brokerId' => $brokerId]);

        /** @var ItemOperationEntity $operation */
        foreach ($entities as $operation) {
            if (sha1($operation->getAmount() . '') === sha1($amount . '')) {
                return $operation;
            }
        }

        return null;
    }

    /**
     * @param int $userId
     * @return ItemOperationEntity[]
     */
    public function findByUser(int $userId): array
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy(['userId' => $userId]);
    }

    /**
     * @param int $userId
     * @param \DateTime $start
     * @param \DateTime $end
     * @return ItemOperationEntity[]
     */
    public function findByUserAndDateInterval(int $userId, \DateTime $start, \DateTime $end): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId))
            ->andWhere(Criteria::expr()->gte('date', $start))
            ->andWhere(Criteria::expr()->lte('date', $end));
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->matching($criteria)->toArray();
    }


    /**
     * @param int $userId
     * @param array|null $itemId
     * @param \DateTime|null $start
     * @param \DateTime|null $end
     * @param array $operationTypes
     * @param array $operationStatuses
     * @param array $brokers
     * @return ItemOperationEntity[]
     */
    public function findByUserAndFilters(
        int $userId,
        array $itemId = null,
        \DateTime $start = null,
        \DateTime $end = null,
        array $operationTypes = [],
        array $operationStatuses = [],
        array $brokers = []
    ): array {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId));

        if ($start !== null) {
            $criteria->andWhere(Criteria::expr()->gte('date', $start));
        }
        if ($end !== null) {
            $criteria->andWhere(Criteria::expr()->lte('date', $end));
        }
        if (count($operationTypes) > 0) {
            $criteria->andWhere(Criteria::expr()->in('operationType', $operationTypes));
        }
        if (count($operationStatuses) > 0) {
            $criteria->andWhere(Criteria::expr()->in('status', $operationStatuses));
        }
        if (count($brokers) > 0) {
            $criteria->andWhere(Criteria::expr()->in('brokerId', $brokers));
        }
        if (count($itemId) > 0) {
            $criteria->andWhere(Criteria::expr()->in('itemId', $itemId));
        }

        $criteria->orderBy(['date' => Criteria::DESC]);

        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->matching($criteria)->toArray();
    }

    public function findById(int $id): ?ItemOperationEntity
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findOneBy(['id' => $id]);
    }
}
