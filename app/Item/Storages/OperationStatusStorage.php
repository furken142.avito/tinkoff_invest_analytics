<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Entities\OperationStatusEntity;
use Doctrine\ORM\EntityManagerInterface;

class OperationStatusStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return OperationStatusEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(OperationStatusEntity::class)
            ->findAll();
    }
}
