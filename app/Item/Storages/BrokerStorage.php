<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\BrokerEntity;
use Doctrine\ORM\EntityManagerInterface;

class BrokerStorage
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findById(string $id): ?BrokerEntity
    {
        return $this->entityManager->getRepository(BrokerEntity::class)
            ->findOneBy(['id' => $id]);
    }


    /**
     * @return BrokerEntity[]
     */
    public function findActive(): array
    {
        return $this->entityManager->getRepository(BrokerEntity::class)
            ->findBy(['isActive' => 1]);
    }
}
