<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemEntity;
use Doctrine\ORM\EntityManagerInterface;

class ItemStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemEntity $itemEntity): void
    {
        $this->entityManager->persist($itemEntity);
        $this->entityManager->flush();
    }

    public function findByExternalIdAndSource(string $externalId, string $source): ?ItemEntity
    {
        return $this->entityManager->getRepository(ItemEntity::class)
            ->findOneBy(['externalId' => $externalId, 'source' => $source]);
    }

    public function findById(int $id): ?ItemEntity
    {
        return $this->entityManager->getRepository(ItemEntity::class)
            ->findOneBy(['id' => $id]);
    }
}
