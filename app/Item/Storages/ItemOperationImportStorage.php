<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemOperationsImportEntity;
use Doctrine\ORM\EntityManagerInterface;

class ItemOperationImportStorage
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemOperationsImportEntity $entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function findByUserAndBroker(int $userId, string $brokerId): array
    {
        return $this->entityManager->getRepository(ItemOperationsImportEntity::class)
            ->findBy(['userId' => $userId, 'brokerId' => $brokerId]);
    }
}
