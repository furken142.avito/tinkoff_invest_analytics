<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Entities\ItemOperationsImportEntity;
use Doctrine\Common\Collections\ArrayCollection;

class ItemOperationImportCollection extends ArrayCollection
{
    public function getImportWithLastImportDateEnd(): ?ItemOperationsImportEntity
    {
        $lastImport = null;
        /** @var ItemOperationsImportEntity $import */
        foreach ($this->getIterator() as $import) {
            if ($lastImport === null) {
                $lastImport = $import;
            }
            if ($lastImport->getImportDateEnd() < $import->getImportDateEnd()) {
                $lastImport = $import;
            }
        }

        return $lastImport;
    }
}
