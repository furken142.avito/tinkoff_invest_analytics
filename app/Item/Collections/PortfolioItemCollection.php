<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\PortfolioItemModel;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioItemCollection extends ArrayCollection
{
    public function getByItemId(int $itemId): ?PortfolioItemModel
    {
        /** @var PortfolioItemModel $item */
        foreach ($this->getIterator() as $item) {
            if ($item->getItemId() === $itemId) {
                return $item;
            }
        }

        return null;
    }
}
