<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\Intl\Services\CurrencyService;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemPaymentScheduleStorage;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\OperationStatusStorage;
use App\Item\Storages\OperationTypeStorage;
use App\Services\ExchangeCurrencyService;
use Psr\Container\ContainerInterface;

$container->set(ItemService::class, function (ContainerInterface $c) {
    return new ItemService(
        $c->get(ItemStorage::class)
    );
});

$container->set(ItemStorage::class, function (ContainerInterface $c) {
    return new ItemStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(ItemOperationStorage::class, function (ContainerInterface $c) {
    return new ItemOperationStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(ItemOperationImportStorage::class, function (ContainerInterface $c) {
    return new ItemOperationImportStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(BrokerStorage::class, function (ContainerInterface $c) {
    return new BrokerStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationTypeStorage::class, function (ContainerInterface $c) {
    return new OperationTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationStatusStorage::class, function (ContainerInterface $c) {
    return new OperationStatusStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(ItemPaymentScheduleStorage::class, function (ContainerInterface $c) {
    return new ItemPaymentScheduleStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationService::class, function (ContainerInterface $c) {
    return new OperationService(
        $c->get(ItemOperationStorage::class),
        $c->get(BrokerStorage::class),
        $c->get(ItemStorage::class),
        $c->get(CurrencyService::class),
        $c->get(OperationTypeStorage::class),
        $c->get(OperationStatusStorage::class),
        $c->get(ExchangeCurrencyService::class)
    );
});

$container->set(PortfolioService::class, function (ContainerInterface $c) {
    return new PortfolioService(
        $c->get('Now' . DateTime::class),
        $c->get(OperationService::class),
        $c->get(ItemPaymentScheduleStorage::class),
        $c->get(ExchangeCurrencyService::class)
    );
});
