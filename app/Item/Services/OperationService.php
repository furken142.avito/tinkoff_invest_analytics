<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Models\ImportStatisticModel;
use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Collections\OperationAggregateCollection;
use App\Entities\OperationStatusEntity;
use App\Entities\OperationTypeEntity;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\BrokerCollection;
use App\Item\Collections\OperationCollection;
use App\Item\Collections\OperationStatusCollection;
use App\Item\Collections\OperationTypeCollection;
use App\Item\Entities\BrokerEntity;
use App\Item\Entities\ItemOperationEntity;
use App\Item\Models\BrokerModel;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Models\OperationStatusModel;
use App\Item\Models\OperationTypeModel;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\OperationStatusStorage;
use App\Item\Storages\OperationTypeStorage;
use App\Models\OperationAggregateModel;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use App\User\Exceptions\UserAccessDeniedException;

class OperationService
{
    private ItemOperationStorage $itemOperationStorage;
    private ItemStorage $itemStorage;
    private BrokerStorage $brokerStorage;
    private CurrencyService $currencyService;
    private OperationTypeStorage $operationTypeStorage;
    private OperationStatusStorage $operationStatusStorage;
    private ExchangeCurrencyService $exchangeCurrencyService;

    public function __construct(
        ItemOperationStorage $itemOperationStorage,
        BrokerStorage $brokerStorage,
        ItemStorage $itemStorage,
        CurrencyService $currencyService,
        OperationTypeStorage $operationTypeStorage,
        OperationStatusStorage $operationStatusStorage,
        ExchangeCurrencyService $exchangeCurrencyService
    ) {
        $this->itemOperationStorage = $itemOperationStorage;
        $this->brokerStorage = $brokerStorage;
        $this->itemStorage = $itemStorage;
        $this->currencyService = $currencyService;
        $this->operationTypeStorage = $operationTypeStorage;
        $this->operationStatusStorage = $operationStatusStorage;
        $this->exchangeCurrencyService = $exchangeCurrencyService;
    }

    public function calculateIncome(int $userId, \DateTime $startDate, \DateTime $endDate): float
    {
        $income = 0;
        $rates = [
            'RUB' => 1
        ];

        $operations = $this->itemOperationStorage->findByUserAndDateInterval($userId, $startDate, $endDate);

        foreach ($operations as $operation) {
            $operationType = $operation->getOperationType()->getExternalId();
            $operationStatus = $operation->getStatus();

            if ($operationStatus === BrokerOperationStatusType::OPERATION_STATE_DONE) {
                if (
                    in_array(
                        $operationType,
                        [
                            BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
                            BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND_CARD
                        ]
                    )
                ) {
                    $currencyIso = $operation->getCurrency()->getIso();
                    if (!isset($rates[$currencyIso])) {
                        $rates[$currencyIso] =
                            $this->exchangeCurrencyService->getExchangeRateByDayCandies(
                                $currencyIso,
                                'RUB',
                                $endDate
                            );
                    }

                    $income += $operation->getAmount() * $rates[$currencyIso];
                }
            }
        }

        return $income;
    }

    public function addOperations(OperationCollection $operations): ImportStatisticModel
    {
        $statistic = [
            'skipped' => 0,
            'updated' => 0,
            'added' => 0
        ];

        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $itemOperation = $this->itemOperationStorage->findByExternalIdAndBroker(
                $operation->getExternalId(),
                $operation->getBrokerId()
            );

            if (null === $itemOperation) {
                $statistic['added']++;
                $operationEntity = $this->convertOperationModelToEntity($operation);
                $this->itemOperationStorage->addEntity($operationEntity);
            } elseif ($itemOperation->getStatus() !== $operation->getStatus()) {
                $statistic['updated']++;
                $itemOperation->setStatus($operation->getStatus());
                $this->itemOperationStorage->addEntity($itemOperation);
            } else {
                $statistic['skipped']++;
            }
        }

        return new ImportStatisticModel($statistic);
    }

    public function getUserOperations(int $userId): OperationCollection
    {
        $operationCollection = new OperationCollection();
        $operations = $this->itemOperationStorage->findByUser($userId);

        foreach ($operations as $operation) {
            $operationCollection->add($this->convertOperationEntityToModel($operation));
        }

        return $operationCollection;
    }

    public function getOperationTypeList(): OperationTypeCollection
    {
        $operationTypeCollection = new OperationTypeCollection();

        $operationTypes = $this->operationTypeStorage->findAll();
        foreach ($operationTypes as $type) {
            $operationTypeCollection->add($this->convertOperationTypeEntityToModel($type));
        }

        return $operationTypeCollection;
    }

    public function getOperationStatusList(): OperationStatusCollection
    {
        $operationStatusList = new OperationStatusCollection();

        $operationStatues = $this->operationStatusStorage->findAll();
        foreach ($operationStatues as $status) {
            $operationStatusList->add($this->convertOperationStatusEntityToModel($status));
        }

        return $operationStatusList;
    }

    public function getActiveBrokerList(): BrokerCollection
    {
        $brokers = new BrokerCollection();

        $brokerEntities = $this->brokerStorage->findActive();
        foreach ($brokerEntities as $entity) {
            $brokers->add($this->convertBrokerEntityToModel($entity));
        }

        return $brokers;
    }

    public function getFilteredUserOperation(int $userId, OperationFiltersModel $filter): OperationCollection
    {
        $operations = new OperationCollection();
        $types = [];
        if ($filter->getOperationType() !== null) {
            foreach ($filter->getOperationType() as $type) {
                $types[] = $this->operationTypeStorage->findByExternalId($type)?->getId();
            }
        }
        $statuses = $filter->getOperationStatus() === null ? [] : $filter->getOperationStatus();
        $brokers = $filter->getBrokerId() === null ? [] : $filter->getBrokerId();
        $itemId = $filter->getItemId() === null ? [] : $filter->getItemId();
        $operationEntityList = $this->itemOperationStorage->findByUserAndFilters(
            $userId,
            $itemId,
            $filter->getDateFrom(),
            $filter->getDateTo(),
            $types,
            $statuses,
            $brokers
        );
        foreach ($operationEntityList as $operation) {
            $operations->add($this->convertOperationEntityToModel($operation));
        }
        return $operations;
    }

    public function calculateOperationSummary(OperationCollection $operations, string $currency): PriceModel
    {
        $amount = 0;
        $rates = [];
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operationCurrency = $operation->getCurrencyIso();
            if (!isset($rates[$operationCurrency])) {
                $rates[$operationCurrency] = $this->exchangeCurrencyService->getExchangeRateByDayCandies(
                    $operationCurrency,
                    $currency,
                    $operation->getDate()
                );
            }
            $amount += $operation->getAmount() * $rates[$operationCurrency];
        }
        return new PriceModel([
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    public function getPayIn(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        string $aggregatePeriod
    ): OperationAggregateCollection {
        $operationTypes = [
            BrokerOperationTypeType::OPERATION_TYPE_INPUT,
            BrokerOperationTypeType::OPERATION_TYPE_OUTPUT,
            BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD
        ];
        return $this->calculateSummaryByOperationTypes(
            $userId,
            $dateFrom,
            $dateTo,
            $operationTypes,
            $aggregatePeriod
        );
    }

    public function getEarning(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        string $aggregatePeriod
    ): OperationAggregateCollection {
        $operationTypes = [
            BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND_CARD,
            BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND
        ];
        return $this->calculateSummaryByOperationTypes(
            $userId,
            $dateFrom,
            $dateTo,
            $operationTypes,
            $aggregatePeriod
        );
    }

    public function getUserOperationById(int $userId, int $operationId): OperationModel
    {
        $operationEntity = $this->itemOperationStorage->findById($operationId);

        if ($operationEntity === null || $operationEntity->getUserId() !== $userId) {
            throw new UserAccessDeniedException('Access denied for user', 2025);
        }

        return $this->convertOperationEntityToModel($operationEntity);
    }

    public function addUserOperation(int $userId, OperationModel $operationModel): int
    {
        $operationModel->setUserId($userId);
        $operationModel->setExternalId(md5(
            $operationModel->getDate()->getTimestamp()
            . $operationModel->getUserId()
            . $operationModel->getAmount()
        ));

        $operationEntity = $this->convertOperationModelToEntity($operationModel);
        return $this->itemOperationStorage->addEntity($operationEntity);
    }

    public function editUserOperation($userId, OperationModel $operationModel): void
    {
        $operation = $this->itemOperationStorage->findById($operationModel->getItemOperationId());

        if (
            $operation === null
            || $userId !== $operation->getUserId()
            || $operation->getUserId() !== $operationModel->getUserId()
        ) {
            throw new UserAccessDeniedException('Access denied update operation for user', 2025);
        }

        $operation->setStatus($operationModel->getStatus());
        $operation->setDate($operationModel->getDate());
        $operation->setAmount($operationModel->getAmount());
        $this->itemOperationStorage->addEntity($operation);
    }

    private function convertOperationModelToEntity(
        OperationModel $operation
    ): ItemOperationEntity {
        $operationType = $this->operationTypeStorage->findByExternalId($operation->getOperationType());
        $broker = $operation->getBrokerId() === null
            ? null
            : $this->brokerStorage->findById($operation->getBrokerId());
        $item = $operation->getItemId() ? $this->itemStorage->findById($operation->getItemId()) : null;
        $currency = $this->currencyService->getCurrencyByIso($operation->getCurrencyIso());

        $operationEntity = new ItemOperationEntity();
        $operationEntity->setId($operation->getItemOperationId());
        $operationEntity->setItem($item);
        $operationEntity->setItemId($operation->getItemId());
        $operationEntity->setExternalId($operation->getExternalId());
        $operationEntity->setUserId($operation->getUserId());
        $operationEntity->setOperationType($operationType);
        $operationEntity->setBrokerId($operation->getBrokerId());
        $operationEntity->setBroker($broker);
        $operationEntity->setCreatedDate($operation->getCreatedDate());
        $operationEntity->setQuantity($operation->getQuantity());
        $operationEntity->setDate($operation->getDate());
        $operationEntity->setAmount($operation->getAmount());
        $operationEntity->setCurrencyId($currency->getId());
        $operationEntity->setCurrency($currency);
        $operationEntity->setStatus($operation->getStatus());

        return $operationEntity;
    }

    private function convertOperationEntityToModel(ItemOperationEntity $operation): OperationModel
    {
        $item = null;
        if ($operation->getItem() !== null) {
            $itemEntity = $operation->getItem();
            $item = new ItemModel([
                'itemId' => $itemEntity->getId(),
                'name' => $itemEntity->getName(),
                'type' => $itemEntity->getType(),
                'source' => $itemEntity->getSource(),
                'externalId' => $itemEntity->getExternalId(),
            ]);
        }
        return new OperationModel([
            'itemOperationId' => $operation->getId(),
            'userId' => $operation->getUserId(),
            'brokerId' => $operation->getBrokerId(),
            'itemId' => $operation->getItemId(),
            'item' => $item,
            'operationType' => $operation->getOperationType()->getExternalId(),
            'externalId' => $operation->getExternalId(),
            'createdDate' => $operation->getCreatedDate(),
            'quantity' => $operation->getQuantity(),
            'date' => $operation->getDate(),
            'amount' => $operation->getAmount(),
            'currencyIso' => $operation->getCurrency()->getIso(),
            'status' => $operation->getStatus()
        ]);
    }

    private function convertOperationTypeEntityToModel(OperationTypeEntity $operationType): OperationTypeModel
    {
        return new OperationTypeModel([
            'operationTypeId' => $operationType->getId(),
            'externalId' => $operationType->getExternalId(),
            'name' => $operationType->getName()
        ]);
    }

    private function convertOperationStatusEntityToModel(OperationStatusEntity $status): OperationStatusModel
    {
        return new OperationStatusModel([
            'operationStatusId' => $status->getId(),
            'externalId' => $status->getExternalId(),
            'name' => $status->getName()
        ]);
    }

    private function convertBrokerEntityToModel(BrokerEntity $broker): BrokerModel
    {
        return new BrokerModel([
            'brokerId' => $broker->getId(),
            'isActive' => $broker->getIsActive(),
            'name' => $broker->getName()
        ]);
    }

    private function calculateSummaryByOperationTypes(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        array $operationTypes,
        string $aggregatePeriod
    ): OperationAggregateCollection {
        $filterModel = new OperationFiltersModel([
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'operationStatus' => [BrokerOperationStatusType::OPERATION_STATE_DONE],
            'operationType' => $operationTypes
        ]);
        $operations = $this->getFilteredUserOperation($userId, $filterModel);

        return match ($aggregatePeriod) {
            'month' => $this->calculateMonthlySummaryByOperationTypes($dateFrom, $dateTo, $operations),
            'year' => $this->calculateYearSummaryByOperationTypes($dateFrom, $dateTo, $operations)
        };
    }

    private function calculateMonthlySummaryByOperationTypes(
        \DateTime $dateFrom,
        \DateTime $dateTo,
        OperationCollection $operations
    ): OperationAggregateCollection {
        $currentPeriod = clone $dateFrom;
        $currentPeriod->setDate(
            (int)$currentPeriod->format('Y'),
            (int)$currentPeriod->format('m'),
            1
        )->setTime(0, 0, 0);
        $aggregateCollection = new OperationAggregateCollection();

        while ($currentPeriod < $dateTo) {
            $monthOperations = $operations->filterByMonth($currentPeriod);

            $price = $this->calculateOperationSummary($monthOperations, 'RUB');

            $aggregateCollection->add(
                new OperationAggregateModel([
                    'date' => $currentPeriod->format('Y-m-d'),
                    'amount' => $price->getAmount()
                ])
            );

            $currentPeriod->add(new \DateInterval('P1M'));
        }

        return $aggregateCollection;
    }

    private function calculateYearSummaryByOperationTypes(
        \DateTime $dateFrom,
        \DateTime $dateTo,
        OperationCollection $operations
    ): OperationAggregateCollection {
        $currentPeriod = clone $dateFrom;
        $currentPeriod->setDate((int)$currentPeriod->format('Y'), 1, 1)
            ->setTime(0, 0, 0);

        $aggregateCollection = new OperationAggregateCollection();

        while ($currentPeriod < $dateTo) {
            $yearOperations = $operations->filterByYear($currentPeriod);

            $price = $this->calculateOperationSummary($yearOperations, 'RUB');
            $aggregateCollection->add(
                new OperationAggregateModel([
                    'date' => $currentPeriod->format('Y-m-d'),
                    'amount' => $price->getAmount()
                ])
            );

            $currentPeriod->add(new \DateInterval('P1Y'));
        }

        return $aggregateCollection;
    }
}
