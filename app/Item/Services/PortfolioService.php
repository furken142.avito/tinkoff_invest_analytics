<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Item\Collections\OperationCollection;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Collections\PortfolioItemCollection;
use App\Item\Models\OperationModel;
use App\Item\Models\PortfolioBalanceModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Models\PortfolioModel;
use App\Item\Storages\ItemPaymentScheduleStorage;
use App\Services\ExchangeCurrencyService;

class PortfolioService
{
    private \DateTime $currentDateTime;
    private OperationService $operationService;
    private ItemPaymentScheduleStorage $itemPaymentScheduleStorage;
    private ExchangeCurrencyService $exchangeCurrencyService;

    public function __construct(
        \DateTime $currentDateTime,
        OperationService $operationService,
        ItemPaymentScheduleStorage $itemPaymentScheduleStorage,
        ExchangeCurrencyService $exchangeCurrencyService
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->operationService = $operationService;
        $this->itemPaymentScheduleStorage = $itemPaymentScheduleStorage;
        $this->exchangeCurrencyService = $exchangeCurrencyService;
    }

    public function buildPortfolio(int $userId): PortfolioModel
    {
        $operations = $this->operationService->getUserOperations($userId)->sortByDateAsc();

        return $this->buildAssetPortfolio($operations);
    }

    public function calculateTotalAmount(int $userId): float
    {

        $portfolio = $this->buildPortfolio($userId);

        $rates = [
            'RUB' => 1
        ];

        $amount = 0;

        /** @var PortfolioBalanceModel $balance */
        foreach ($portfolio->getBalance()->getIterator() as $balance) {
            $currencyIso = $balance->getCurrencyIso();
            if (!isset($rates[$currencyIso])) {
                $rates[$currencyIso] =
                    $this->exchangeCurrencyService->getExchangeRateByDayCandies(
                        $currencyIso,
                        'RUB',
                        $this->currentDateTime
                    );
            }
            $amount += $rates[$currencyIso] * $balance->getAmount();
        }

        /** @var PortfolioItemModel $item */
        foreach ($portfolio->getItems()->getIterator() as $item) {
            $currencyIso = $item->getCurrencyIso();
            if (!isset($rates[$currencyIso])) {
                $rates[$currencyIso] =
                    $this->exchangeCurrencyService->getExchangeRateByDayCandies(
                        $currencyIso,
                        'RUB',
                        $this->currentDateTime
                    );
            }

            $amount += $rates[$currencyIso] * $item->getAmount();
        }

        return $amount;
    }

    private function buildAssetPortfolio(OperationCollection $operations): PortfolioModel
    {
        $balanceCollection = new PortfolioBalanceCollection();
        $itemCollection = new PortfolioItemCollection();
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            if ($operation->getStatus() === BrokerOperationStatusType::OPERATION_STATE_DONE) {
                if (in_array($operation->getBrokerId(), ['money_friends', 'potok'])) {
                    if (
                        in_array(
                            $operation->getOperationType(),
                            [
                                BrokerOperationTypeType::OPERATION_TYPE_INPUT,
                                BrokerOperationTypeType::OPERATION_TYPE_OUTPUT,
                                BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
                                BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
                                BrokerOperationTypeType::OPERATION_TYPE_BUY
                            ]
                        )
                    ) {
                        $balance = $balanceCollection->getByBrokerAndCurrency(
                            $operation->getBrokerId(),
                            $operation->getCurrencyIso()
                        );

                        if (null === $balance) {
                            $balance = new PortfolioBalanceModel([
                                'brokerId' => $operation->getBrokerId(),
                                'currencyIso' => $operation->getCurrencyIso(),
                                'amount' => $operation->getAmount()
                            ]);
                            $balanceCollection->add($balance);
                        } else {
                            $balance->setAmount($balance->getAmount() + $operation->getAmount());
                        }
                    }

                    if (
                        in_array(
                            $operation->getOperationType(),
                            [
                                BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
                                BrokerOperationTypeType::OPERATION_TYPE_BUY,
                                BrokerOperationTypeType::OPERATION_TYPE_DEFAULT
                            ]
                        )
                    ) {
                        $item = $itemCollection->getByItemId($operation->getItemId());
                        if ($item === null) {
                            $item = new PortfolioItemModel([
                                'itemId' => $operation->getItemId(),
                                'amount' => -1 * $operation->getAmount(),
                                'currencyIso' => $operation->getCurrencyIso(),
                                'quantity' => $operation->getQuantity()
                            ]);
                            $itemCollection->add($item);
                        } else {
                            $item->setAmount($item->getAmount() + (-1 * $operation->getAmount()));

                            if ($operation->getQuantity() !== null) {
                                $item->setQuantity($item->getQuantity() + $operation->getQuantity());
                            }
                        }
                    }
                }
            }
        }
        return new PortfolioModel([
            'balance' => $balanceCollection,
            'items' => $itemCollection
        ]);
    }

    public function calculateForecastIncome(int $userId, \DateTime $startDate, \DateTime $endDate): float
    {
        $income = 0;
        $rates = [
            'RUB' => 1
        ];

        $operations = $this->operationService->getUserOperations($userId)->sortByDateAsc();
        $portfolio = $this->buildAssetPortfolio($operations);

        /** @var PortfolioItemModel $item */
        foreach ($portfolio->getItems()->getIterator() as $item) {
            if ($item->getAmount() !== 0) {
                $payments = $this->itemPaymentScheduleStorage
                    ->findActiveByItemIdAndDateInterval($item->getItemId(), $startDate, $endDate);
                foreach ($payments as $payment) {
                    $currencyIso = $payment->getCurrency()->getIso();
                    if (!isset($rates[$currencyIso])) {
                        $rates[$currencyIso] =
                            $this->exchangeCurrencyService->getExchangeRateByDayCandies(
                                $currencyIso,
                                'RUB',
                                $endDate
                            );
                    }

                    $income += $payment->getInterestAmount() * $rates[$currencyIso] * $item->getQuantity();
                }
            }
        }

        return $income;
    }
}
