<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Models\AssetModel;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\PortfolioItemCollection;
use App\Item\Entities\ItemEntity;
use App\Item\Models\ItemModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Storages\ItemStorage;

class ItemService
{
    private ItemStorage $itemStorage;

    public function __construct(
        ItemStorage $itemStorage
    ) {
        $this->itemStorage = $itemStorage;
    }

    public function getPortfolioItemsByType(
        PortfolioItemCollection $portfolioItemCollection,
        string $type
    ): ItemCollection {
        $itemCollection = new ItemCollection();
        /** @var PortfolioItemModel $item */
        foreach ($portfolioItemCollection->getIterator() as $portfolioItem) {
            $itemEntity = $this->itemStorage->findById($portfolioItem->getItemId());
            $item = $this->covertItemEntityToModel($itemEntity);

            if ($item->getType() === $type) {
                $itemCollection->add($item);
            }
        }

        return $itemCollection;
    }

    private function covertItemEntityToModel(ItemEntity $itemEntity): ItemModel
    {
        $assetEntity = $itemEntity->getAsset();
        $asset = null;
        if ($assetEntity !== null) {
            $asset = new AssetModel([
                'assetId' => $assetEntity->getId(),
                'externalId' => $assetEntity->getExternalId(),
                'platform' => $assetEntity->getPlatform(),
                'name' => $assetEntity->getName(),
                'type' => $assetEntity->getType(),
                'payoutType' => $assetEntity->getPayoutType()?->getExternalId(),
                'dealDate' => $assetEntity->getDealDate(),
                'duration' => $assetEntity->getDuration(),
                'durationPeriod' => $assetEntity->getDurationPeriod(),
                'interestPercent' => $assetEntity->getInterestPercent(),
                'interestAmount' => $assetEntity->getInterestAmount(),
                'payoutFrequency' => $assetEntity->getPayoutFrequency(),
                'payoutFrequencyPeriod' => $assetEntity->getPayoutFrequencyPeriod(),
                'nominalAmount' => $assetEntity->getNominalAmount(),
                'currencyIso' => $assetEntity->getCurrency()?->getIso()
            ]);
        }

        return new ItemModel([
            'itemId' => $itemEntity->getId(),
            'type' => $itemEntity->getType(),
            'source' => $itemEntity->getSource(),
            'externalId' => $itemEntity->getExternalId(),
            'name' => $itemEntity->getName(),
            'asset' => $asset
        ]);
    }
}
