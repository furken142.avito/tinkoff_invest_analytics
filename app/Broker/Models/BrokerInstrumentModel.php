<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerInstrumentModel extends BaseModel
{
    private string $figi;
    private string $ticker;
    private string $isin;
    private ?float $minPriceIncrement = null;
    private int $lot;
    private string $currency;
    private string $name;
    private string $type;

    /**
     * @return string
     */
    public function getFigi(): string
    {
        return $this->figi;
    }

    /**
     * @param string $figi
     */
    public function setFigi(string $figi): void
    {
        $this->figi = $figi;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     */
    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }

    /**
     * @return float|null
     */
    public function getMinPriceIncrement(): ?float
    {
        return $this->minPriceIncrement;
    }

    /**
     * @param float|null $minPriceIncrement
     */
    public function setMinPriceIncrement(float $minPriceIncrement = null): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    /**
     * @return int
     */
    public function getLot(): int
    {
        return $this->lot;
    }

    /**
     * @param int $lot
     */
    public function setLot(int $lot): void
    {
        $this->lot = $lot;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
