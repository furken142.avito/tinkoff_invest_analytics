<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerBondModel extends BaseModel
{
    private string $figi;
    private BrokerMoneyModel $nominal;

    /**
     * @return string
     */
    public function getFigi(): string
    {
        return $this->figi;
    }

    /**
     * @param string $figi
     */
    public function setFigi(string $figi): void
    {
        $this->figi = $figi;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getNominal(): BrokerMoneyModel
    {
        return $this->nominal;
    }

    /**
     * @param BrokerMoneyModel $nominal
     */
    public function setNominal(BrokerMoneyModel $nominal): void
    {
        $this->nominal = $nominal;
    }
}
