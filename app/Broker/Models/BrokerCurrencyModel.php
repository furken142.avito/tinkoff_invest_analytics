<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerCurrencyModel extends BaseModel
{
    private string $iso;
    private string $figi;

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }

    /**
     * @return string
     */
    public function getFigi(): string
    {
        return $this->figi;
    }

    /**
     * @param string $figi
     */
    public function setFigi(string $figi): void
    {
        $this->figi = $figi;
    }
}
