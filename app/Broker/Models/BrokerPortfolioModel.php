<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Broker\Collections\PortfolioPositionCollection;
use App\Common\Models\BaseModel;

class BrokerPortfolioModel extends BaseModel
{
    private PortfolioPositionCollection $positions;

    /**
     * @return PortfolioPositionCollection
     */
    public function getPositions(): PortfolioPositionCollection
    {
        return $this->positions;
    }

    /**
     * @param PortfolioPositionCollection $positions
     */
    public function setPositions(PortfolioPositionCollection $positions): void
    {
        $this->positions = $positions;
    }
}
