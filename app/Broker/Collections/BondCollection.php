<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerBondModel;
use Doctrine\Common\Collections\ArrayCollection;

class BondCollection extends ArrayCollection
{
    public function findByFigi(string $figi): ?BrokerBondModel
    {
        /** @var BrokerBondModel $bond */
        foreach ($this->getIterator() as $bond) {
            if ($bond->getFigi() === $figi) {
                return $bond;
            }
        }

        return null;
    }
}
