<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerPortfolioBalanceModel;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioBalanceCollection extends ArrayCollection
{
    public function calculateAmountByCurrency(string $currency): float
    {
        $amount = 0;
        /** @var BrokerPortfolioBalanceModel $currencyBalance*/
        foreach ($this->getIterator() as $currencyBalance) {
            if ($currencyBalance->getCurrency() === $currency) {
                $amount += $currencyBalance->getAmount() - $currencyBalance->getBlockedAmount();
            }
        }

        return $amount;
    }
}
