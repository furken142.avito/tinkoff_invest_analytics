<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerCurrencyModel;
use Doctrine\Common\Collections\ArrayCollection;

class BrokerCurrencyCollection extends ArrayCollection
{
    public function filterByIso(string $iso): ?BrokerCurrencyModel
    {
        /** @var BrokerCurrencyModel $currency */
        foreach ($this->getIterator() as $currency) {
            if ($iso === $currency->getIso()) {
                return $currency;
            }
        }

        return null;
    }
}
