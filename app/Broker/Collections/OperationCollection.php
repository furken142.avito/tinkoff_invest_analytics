<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class OperationCollection extends ArrayCollection
{
}
