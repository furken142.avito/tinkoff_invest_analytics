<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerPortfolioPositionModel;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioPositionCollection extends ArrayCollection
{
    public function filterByFigi(string $figi): ?BrokerPortfolioPositionModel
    {
        /** @var BrokerPortfolioPositionModel $position */
        foreach ($this->getIterator() as $position) {
            if ($position->getFigi() === $figi) {
                return  $position;
            }
        }

        return null;
    }
}
