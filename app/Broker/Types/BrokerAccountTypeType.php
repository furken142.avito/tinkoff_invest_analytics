<?php

declare(strict_types=1);

namespace App\Broker\Types;

class BrokerAccountTypeType
{
    public const ACCOUNT_TYPE_UNSPECIFIED = 0; // Тип аккаунта не определён
    public const ACCOUNT_TYPE_GENERAL = 1; // Брокерский счёт
    public const ACCOUNT_TYPE_IIS = 2; // ИИС
    public const ACCOUNT_TYPE_INVEST_BOX = 3; // Инвесткопилка
}
