<?php

declare(strict_types=1);

namespace App\Broker\Types;

class BrokerCandleIntervalType
{
    public const INTERVAL_1_MIN = '1min';
    public const INTERVAL_5_MIN = '5min';
    public const INTERVAL_15_MIN = '15min';
    public const INTERVAL_HOUR = '1hour';
    public const INTERVAL_DAY = '1day';
}
