<?php

declare(strict_types=1);

namespace App\Broker\Types;

class BrokerInstrumentIdType
{
    public const UNSPECIFIED = 0;
    public const FIGI = 1;
    public const TICKER = 2;
}
