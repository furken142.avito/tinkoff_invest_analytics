<?php

declare(strict_types=1);

namespace App\Broker\Types;

class BrokerAccountStatusType
{
    public const ACCOUNT_STATUS_UNSPECIFIED = 0; // Статус не определён
    public const ACCOUNT_STATUS_NEW = 1; // Новый, в процессе открытия
    public const ACCOUNT_STATUS_OPEN = 2; // Открытый и активный счёт
    public const ACCOUNT_STATUS_CLOSED = 3; // Закрытый счет
}
