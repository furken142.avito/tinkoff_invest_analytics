<?php

declare(strict_types=1);

namespace App\Broker\Interfaces;

use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerShareModel;
use App\Collections\CandleCollection;

interface BrokerInterface
{
    public function getAccounts(): AccountCollection;
    public function getPortfolio(string $accountId): BrokerPortfolioModel;
    public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection;
    public function getInstrumentByFigi(string $figi): ?BrokerInstrumentModel;
    public function getCurrencyByFigi(string $figi): ?BrokerCurrencyModel;
    public function getCurrencies(): BrokerCurrencyCollection;
    public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection;
    public function getHistoryCandles(string $figi, \DateTime $from, \DateTime $to, $interval): CandleCollection;
    public function getShares(): ShareCollection;
    public function getShareByFigi(string $figi): ?BrokerShareModel;
}
