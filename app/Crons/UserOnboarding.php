#!/usr/bin/php
<?php

declare(strict_types=1);

use App\Item\Services\PortfolioService;
use App\User\Entities\UserEntity;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserService;
use DI\Container;

require_once __DIR__ . '/../../vendor/autoload.php';

$container = new Container();
require __DIR__ . '/../Common/dependencies.php';

$userService = $container->get(UserService::class);
$users = $userService->getUserList();

$updatedUsers = [];
/** @var UserEntity $user */
foreach ($users->getIterator() as $user) {
    if ($user->getIsNewUser() === 1) {
        $portfolioService = $container->get(PortfolioService::class);
        $portfolio = $portfolioService->buildPortfolio($user->getId());

        $userBrokerAccountService = $container->get(UserBrokerAccountService::class);
        $brokerAccounts = $userBrokerAccountService->getUserAccountByUserId($user->getId());

        if (
                $portfolio->getBalance()->count() > 0
                || $portfolio->getItems()->count() > 0
                || $brokerAccounts->count() > 0
        ) {
            $user->setIsNewUser(0);
            $userService->addUser($user);
            $updatedUsers[] = [
                'userId' => $user->getId(),
                'items' => $portfolio->getItems()->count(),
                'balance' => $portfolio->getBalance()->count(),
                'brokerAccounts' => $brokerAccounts->count()
            ];
        }
    }
}

echo json_encode($updatedUsers);
echo "\n";
