#!/usr/bin/php
<?php

declare(strict_types=1);

use App\Services\StockService;
use DI\Container;

require_once __DIR__ . '/../../vendor/autoload.php';

$container = new Container();
require __DIR__ . '/../Common/dependencies.php';

$stockService = $container->get(StockService::class);
$result = $stockService->import();
echo json_encode([
    'importedItems' => $result->getCountImportedItems(),
    'toImport' => $result->getCountToImportItems()
]);
echo "\n";
