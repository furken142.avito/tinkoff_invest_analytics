#!/usr/bin/php
<?php

declare(strict_types=1);

use App\Services\MarketOperationsService;
use DI\Container;

require_once __DIR__ . '/../../vendor/autoload.php';

$container = new Container();
require __DIR__ . '/../Common/dependencies.php';

$operationService = $container->get(MarketOperationsService::class);
echo json_encode($operationService->getOperationsToImport());
echo "\n";
