<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponeCode;
use App\Formatters\ItemOperationImportFormatter;
use App\Models\OperationFiltersModel;
use App\Services\MarketOperationsService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ItemController
{
    private MarketOperationsService $operationService;

    public function __construct(
        MarketOperationsService $operationService
    ) {
        $this->operationService = $operationService;
    }

    public function importOperations(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $filters = new OperationFiltersModel($request->getQueryParams());
        $importStatistic = $this->operationService->importUserOperations($filters);
        $responseBody = (new ItemOperationImportFormatter($importStatistic))->format();
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
