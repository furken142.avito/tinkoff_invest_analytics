<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\AccountException;
use App\Formatters\ShareFormatter;
use App\Services\AccountService;
use App\Services\PortfolioService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PortfolioController
{
    private PortfolioService $portfolioService;
    private AccountService $accountService;

    public function __construct(
        PortfolioService $portfolioService,
        AccountService $accountService
    ) {

        $this->portfolioService = $portfolioService;
        $this->accountService = $accountService;
    }

    public function apiPortfolio(RequestInterface $request, ResponseInterface $response, $args)
    {
        $data = $this->dataPortfolio();

        $formattedData = [
            'shares' => $data['shares'] ? (new ShareFormatter($data['shares']))->format() : null,
            'sharesCurrency' => $data['sharesCurrency']
                ? (new ShareFormatter($data['sharesCurrency']))->format()
                : null,
            'shareSector' => $data['shareSector'] ?  (new ShareFormatter($data['shareSector']))->format() : null,
            'shareCountry' => $data['shareCountry'] ? (new ShareFormatter($data['shareCountry']))->format() : null,
            'shareCurrencyBalance' => $data['shareCurrencyBalance']
                ? (new ShareFormatter($data['shareCurrencyBalance']))->format()
                : null,
            'totalAmount' => $data['totalAmount']
        ];

        $response->getBody()->write(json_encode($formattedData));

        return $response->withHeader('Content-Type', 'application/json');
    }

    private function dataPortfolio()
    {
        $portfolio = $this->accountService->getPortfolio();
        $balance = $this->accountService->getPortfolioBalance();
        if (count($portfolio->getPositions()) === 0) {
            throw new AccountException('Portfolio is empty', 1033);
        }

        $shares = $this->portfolioService->calculateInstrumentShare($portfolio, $balance);
        $sharesInstrumentCurrency = $this->portfolioService->calculateCurrencyShare($portfolio, $balance);
        $sharesSector = $this->portfolioService->calculateSectorShare($portfolio, $balance);
        $sharesCountry = $this->portfolioService->calculateCountryShare($portfolio, $balance);
        $sharesCurrencyBalance = $this->portfolioService->calculateCurrencyBalance($portfolio, $balance);
        $totalAmount = $this->portfolioService->calculateTotalAmount($portfolio, $balance);

        return [
            'shares' => $shares,
            'sharesCurrency' => $sharesInstrumentCurrency,
            'shareSector' => $sharesSector,
            'shareCountry' => $sharesCountry,
            'shareCurrencyBalance' => $sharesCurrencyBalance,
            'totalAmount' => $totalAmount
        ];
    }
}
