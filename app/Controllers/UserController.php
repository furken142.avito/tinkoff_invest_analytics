<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponeCode;
use App\Exceptions\JsonSchemeException;
use App\Services\JsonValidatorService;
use App\User\Models\UserSettingsModel;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class UserController
{
    private UserService $userService;
    private JsonValidatorService $jsonValidatorService;

    public function __construct(
        JsonValidatorService $jsonValidatorService,
        UserService $userService
    ) {
        $this->userService = $userService;
        $this->jsonValidatorService = $jsonValidatorService;
    }

    public function data(RequestInterface $request, ResponseInterface $response)
    {
        $user = $this->userService->getUser();
        $responseBody = [
            'user_id' => $user->getId(),
            'login' => $user->getLogin(),
            'isNewUser' => $user->getIsNewUser()
        ];
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getSettings(RequestInterface $request, ResponseInterface $response)
    {
        $user = $this->userService->getUser();

        $settings = $this->userService->getUserSettings($user->getId());

        $responseBody = [
            'userSettingsId' => $settings?->getId() ?? null,
            'desiredPension' => $settings?->getDesiredPension() ?? null,
            'retirementAge' => $settings?->getRetirementAge() ?? null
        ];
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function setSettings(RequestInterface $request, ResponseInterface $response)
    {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_settings_post.json', $body);

            $userSettings = new UserSettingsModel($body);

            $this->userService->setUserSettings($userSettings);

            $user = $this->userService->getUser();
            $userSettings = $this->userService->getUserSettings($user->getId());

            $responseBody = [
                'userSettingsId' => $userSettings->getId(),
            ];
            $statusCode = HttpResponeCode::OK;
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponeCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
