<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponeCode;
use App\Formatters\CountryFormatter;
use App\Formatters\CurrencyFormatter;
use App\Formatters\MarketSectorFormatter;
use App\Formatters\MarketStockListFormatter;
use App\Services\JsonValidatorService;
use App\Services\StockService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class StockController
{
    private StockService $stockService;
    private JsonValidatorService $jsonValidatorService;

    public function __construct(
        StockService $stockService,
        JsonValidatorService $jsonValidatorService
    ) {
        $this->stockService = $stockService;
        $this->jsonValidatorService = $jsonValidatorService;
    }

    public function list(RequestInterface $request, ResponseInterface $response, $args)
    {
        $queryParams = $request->getQueryParams();

        $this->jsonValidatorService->validate('stock_list_get.json', $queryParams);

        $country = $queryParams['country'] ?? null;
        $currency = $queryParams['currency'] ?? null;
        $marketSector = $queryParams['sector'] ?? null;

        $country = $country === '' ? null : $country;
        $currency = $currency === '' ? null : $currency;
        $marketSector = $marketSector === 0 ? null : $marketSector;

        $marketInstruments = $this->stockService->getList($country, $currency, (int)$marketSector);

        $responseBody = [
            'stocks' => (new MarketStockListFormatter($marketInstruments))->format()
        ];
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function import(RequestInterface $request, ResponseInterface $response)
    {
        $importStatistic = $this->stockService->import();

        $responseBody = [
            'existsStocks' => $importStatistic->getCountImportedItems(),
            'toImport' => $importStatistic->getCountToImportItems()
        ];
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function filters(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $filters = $this->stockService->getFilters();
        $responseBody = [
            'country' => (new CountryFormatter($filters['country']))->format(),
            'currency' => (new CurrencyFormatter($filters['currency']))->format(),
            'marketSector' => (new MarketSectorFormatter($filters['sector']))->format()
        ];

        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
