<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponeCode;
use App\Services\JsonValidatorService;
use App\User\Exceptions\AuthTypeException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AuthController
{
    private UserService $userService;
    private JsonValidatorService $jsonValidatorService;

    public function __construct(JsonValidatorService $jsonValidatorService, UserService $userService)
    {
        $this->jsonValidatorService = $jsonValidatorService;
        $this->userService = $userService;
    }

    public function optionRequests(RequestInterface $request, ResponseInterface $response, $args)
    {
        $response->getBody()->write('');
        return $response->withStatus(200);
    }

    public function authorization(RequestInterface $request, ResponseInterface $response, $args)
    {
        $body = $request->getParsedBody();

        $this->jsonValidatorService->validate('auth_authorization_get.json', $body);

        try {
            $this->userService->createAndLogin($body['auth_type'], $body['access_token']);
            $user = $this->userService->getUser();
            $responseBody = [
                'jwt' => $this->userService->generateJWT($user, $body['auth_type'])
            ];
            $statusCode = HttpResponeCode::OK;
        } catch (AuthTypeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponeCode::UNAUTHORIZED;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
