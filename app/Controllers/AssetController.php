<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Assets\Services\AssetService;
use App\Common\HttpResponeCode;
use App\Formatters\AssetListFormatter;
use App\Formatters\AssetImportFormatter;
use App\Formatters\PortfolioItemsFormatter;
use App\Item\Services\ItemService;
use App\Item\Services\PortfolioService;
use App\Services\JsonValidatorService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\UploadedFile;

class AssetController
{
    private AssetService $assetService;
    private UserService $userService;
    private JsonValidatorService $jsonValidatorService;
    private PortfolioService $portfolioService;
    private ItemService $itemService;

    public function __construct(
        JsonValidatorService $jsonValidatorService,
        AssetService $assetService,
        UserService $userService,
        PortfolioService $portfolioService,
        ItemService $itemService
    ) {
        $this->jsonValidatorService = $jsonValidatorService;
        $this->assetService = $assetService;
        $this->userService = $userService;
        $this->portfolioService = $portfolioService;
        $this->itemService = $itemService;
    }

    public function getAssetTypedList(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $type = $args['type'];
        $user = $this->userService->getUser();

        $portfolio = $this->portfolioService->buildPortfolio($user->getId());
        $items = $this->itemService->getPortfolioItemsByType($portfolio->getItems(), $type);

        $responseBody = [
            'items' => (new AssetListFormatter($items))->format(),
            'portfolio' => (new PortfolioItemsFormatter($portfolio->getItems()))->format()
        ];

        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function postCrowdfundingOperationImport(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('asset_crowdfunding_import_post.json', $body);

        $user = $this->userService->getUser();
        $platform = $body['platform'];

        $files = $request->getUploadedFiles();
        /** @var UploadedFile $file */
        $file = $files['transactions'];

        $import = $this->assetService->importOperations($user->getId(), $platform, $file);

        $responseBody = (new AssetImportFormatter($import))->format();
        $statusCode = HttpResponeCode::OK;

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
