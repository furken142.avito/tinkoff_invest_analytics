<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponeCode;
use App\Formatters\PriceFormatter;
use App\Services\AnalyticsService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AnalyticsController
{
    private AnalyticsService $analyticsService;

    public function __construct(
        AnalyticsService $analyticsService
    ) {
        $this->analyticsService = $analyticsService;
    }

    public function summaryToday(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $currency = $request->getQueryParams()['currency'] ?? null;
        $profit = $this->analyticsService->calculateProfitToday($currency);

        $formattedData['revenueToday'] = (new PriceFormatter($profit['revenueToday']))->format();

        $response->getBody()->write(json_encode($formattedData));

        return $response->withHeader('Content-Type', 'application/json');
    }

    public function dashboard(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $summary = $this->analyticsService->getSummary();
        $response->getBody()->write(json_encode($summary));

        $statusCode = HttpResponeCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
