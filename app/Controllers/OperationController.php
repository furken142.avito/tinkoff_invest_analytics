<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponeCode;
use App\Exceptions\InvalidRequestDataException;
use App\Exceptions\JsonSchemeException;
use App\Formatters\BrokerFormatter;
use App\Formatters\OperationAggregateFormatter;
use App\Formatters\OperationFilterFormatter;
use App\Formatters\OperationFormatter;
use App\Formatters\OperationListFormatter;
use App\Formatters\OperationStatusFormatter;
use App\Formatters\OperationTypesFormatter;
use App\Formatters\PriceFormatter;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Item\Models\OperationFiltersModel;
use App\Services\JsonValidatorService;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class OperationController
{
    private JsonValidatorService $jsonValidatorService;
    private OperationService $operationService;
    private UserService $userService;

    public function __construct(
        JsonValidatorService $jsonValidatorService,
        UserService $userService,
        OperationService $operationService
    ) {
        $this->jsonValidatorService = $jsonValidatorService;
        $this->userService = $userService;
        $this->operationService = $operationService;
    }

    public function apiOperations(RequestInterface $request, ResponseInterface $response, $args)
    {
        $queryParams = $request->getQueryParams();
        $filters = new OperationFiltersModel([
            'operationType' => !empty($queryParams['operationType']) ? [$queryParams['operationType']] : null,
            'operationStatus' => !empty($queryParams['operationStatus']) ? [$queryParams['operationStatus']] : null,
            'brokerId' => !empty($queryParams['brokerId']) ? [$queryParams['brokerId']] : null,
        ]);
        if (!empty($queryParams['dateFrom'])) {
            $filters->setDateFrom(new \DateTime($queryParams['dateFrom']));
        }
        if (!empty($queryParams['dateTo'])) {
            $filters->setDateTo(new \DateTime($queryParams['dateTo']));
        }

        $data = $this->dataOperations($filters);
        $operationData = [
            'operationType' => (new OperationTypesFormatter($data['operationTypes']))->format(),
            'operationStatus' => (new OperationStatusFormatter($data['operationStatuses']))->format(),
            'brokers' => (new BrokerFormatter($data['brokers']))->format(),
            'dateFrom' => $data['dateFrom'],
            'dateTo' => $data['dateTo'],
            'filters' => (new OperationFilterFormatter($filters))->format(),
            'operations' => (new OperationListFormatter($data['operations']))->format(),
            'summary' => (new PriceFormatter($data['summary']))->format()
        ];

        $response->getBody()->write(json_encode($operationData));
        $statusCode = HttpResponeCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    private function dataOperations(OperationFiltersModel $filters): array
    {
        $operations = $this->operationService->getFilteredUserOperation($this->userService->getUserId(), $filters);
        $summary = $this->operationService->calculateOperationSummary($operations, 'RUB');
        return [
            'operations' => $operations,
            'summary' => $summary,
            'brokers' => $this->operationService->getActiveBrokerList(),
            'operationTypes' => $this->operationService->getOperationTypeList(),
            'operationStatuses' => $this->operationService->getOperationStatusList(),
            'dateFrom' => $filters->getDateFrom()->format('Y-m-d'),
            'dateTo' => $filters->getDateTo()->format('Y-m-d'),
        ];
    }

    public function getPayIn(
        RequestInterface $request,
        ResponseInterface $response,
        $args
    ): ResponseInterface {
        $filters = $request->getQueryParams();
        if (!isset($filters['dateFrom'])) {
            throw new InvalidRequestDataException('Param "dateFrom" is required', 1080);
        };
        if (!isset($filters['dateTo'])) {
            throw new InvalidRequestDataException('Param "dateTo" is required', 1080);
        };
        if (!isset($filters['aggregatePeriod'])) {
            throw new InvalidRequestDataException('Param "aggregatePeriod" is required', 1080);
        };

        $filters['dateFrom'] = new \DateTime($filters['dateFrom']);
        $filters['dateTo'] = new \DateTime($filters['dateTo']);
        $operationData = $this->operationService->getPayIn(
            $this->userService->getUserId(),
            $filters['dateFrom'],
            $filters['dateTo'],
            $filters['aggregatePeriod']
        );

        $responseBody = [
            'summary' => $operationData->calculateSum(),
            'data' => (new OperationAggregateFormatter($operationData))->format()
        ];
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getEarning(
        RequestInterface $request,
        ResponseInterface $response,
        $args
    ): ResponseInterface {
        $filters = $request->getQueryParams();
        if (!isset($filters['dateFrom'])) {
            throw new InvalidRequestDataException('Param "dateFrom" is required', 1080);
        };
        if (!isset($filters['dateTo'])) {
            throw new InvalidRequestDataException('Param "dateTo" is required', 1080);
        };
        if (!isset($filters['aggregatePeriod'])) {
            throw new InvalidRequestDataException('Param "aggregatePeriod" is required', 1080);
        };

        $filters['dateFrom'] = new \DateTime($filters['dateFrom']);
        $filters['dateTo'] = new \DateTime($filters['dateTo']);
        $operationData = $this->operationService->getEarning(
            $this->userService->getUserId(),
            $filters['dateFrom'],
            $filters['dateTo'],
            $filters['aggregatePeriod']
        );

        $responseBody = [
            'summary' => $operationData->calculateSum(),
            'data' => (new OperationAggregateFormatter($operationData))->format()
        ];
        $statusCode = HttpResponeCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getItemOperations(
        RequestInterface $request,
        ResponseInterface $response,
        $args
    ): ResponseInterface {
        $filters = new OperationFiltersModel([
            'itemId' => [$args['id']],
            'dateTo' => new \DateTime('2499-12-12 23:59:59')
        ]);

        $operations = $this->operationService->getFilteredUserOperation($this->userService->getUserId(), $filters);
        $operationData = (new OperationListFormatter($operations))->format();
        $response->getBody()->write(json_encode($operationData));
        $statusCode = HttpResponeCode::OK;

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getStatusList(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $statusList = $this->operationService->getOperationStatusList();

        $statusData = (new OperationStatusFormatter($statusList))->format();
        $response->getBody()->write(json_encode($statusData));
        $statusCode = HttpResponeCode::OK;

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getOperation(
        RequestInterface $request,
        ResponseInterface $response,
        $args
    ): ResponseInterface {
        $operationId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        $statusCode = HttpResponeCode::OK;

        try {
            $operation = $this->operationService->getUserOperationById($userId, $operationId);
            $responseBody = (new OperationFormatter($operation))->format();
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponeCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function addOperation(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('operation_add_post.json', $body);
            $statusCode = HttpResponeCode::OK;

            $body['date'] = new \DateTime($body['date']['date']);
            $userId = $this->userService->getUserId();

            $operationModel = new OperationModel($body);
            $operationItemId = $this->operationService->addUserOperation($userId, $operationModel);

            $responseBody = [
                'operationItemId' => $operationItemId
            ];
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponeCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function editOperation(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('operation_edit_put.json', $body);

            $operationId = (int)$args['id'];
            $userId = $this->userService->getUserId();
            $body['date'] = new \DateTime($body['date']['date']);

            $operationModel = new OperationModel($body);
            $operationModel->setUserId($userId);
            $operationModel->setItemOperationId($operationId);

            try {
                $this->operationService->editUserOperation($userId, $operationModel);
                $statusCode = HttpResponeCode::NO_CONTENT;
                $responseBody = [];
            } catch (UserAccessDeniedException $e) {
                $responseBody = [
                    'error_code' => $e->getCode(),
                    'error_message' => $e->getMessage()
                ];
                $statusCode = HttpResponeCode::UNPROCCESSABLE_ENTITY;
            }
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponeCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
