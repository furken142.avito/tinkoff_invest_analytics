<?php

declare(strict_types=1);

namespace App\Assets\Services;

use App\Assets\Collections\PayoutCollection;
use App\Assets\Entities\AssetsEntity;
use App\Assets\Models\AssetModel;
use App\Assets\Models\ImportStatisticModel;
use App\Assets\Models\OperationModel;
use App\Assets\Models\PaymentScheduleCalculatorModel;
use App\Assets\Models\PayoutModel;
use App\Assets\Storages\AssetsStorage;
use App\Assets\Storages\PayoutTypeStorage;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\OperationCollection;
use App\Item\Entities\ItemEntity;
use App\Item\Entities\ItemPaymentScheduleEntity;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel as ItemOperationModel;
use App\Item\Services\OperationService;
use App\Item\Storages\ItemPaymentScheduleStorage;
use Slim\Psr7\UploadedFile;

class AssetService
{
    private \DateTime $currentDateTime;
    private AssetsStorage $assetsStorage;
    private AssetFactory $assetFactory;
    private PayoutTypeStorage $payoutTypeStorage;
    private CurrencyService $currencyService;
    private OperationService $operationService;
    private ItemPaymentScheduleStorage $itemPaymentScheduleStorage;

    public function __construct(
        \DateTime $currentDateTime,
        AssetFactory $assetFactory,
        AssetsStorage $assetsStorage,
        PayoutTypeStorage $payoutTypeStorage,
        CurrencyService $currencyService,
        OperationService $operationService,
        ItemPaymentScheduleStorage $itemPaymentScheduleStorage
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->assetFactory = $assetFactory;
        $this->assetsStorage = $assetsStorage;
        $this->payoutTypeStorage = $payoutTypeStorage;
        $this->currencyService = $currencyService;
        $this->operationService = $operationService;
        $this->itemPaymentScheduleStorage = $itemPaymentScheduleStorage;
    }

    public function importOperations(int $userId, string $platform, UploadedFile $file): ImportStatisticModel
    {
        $service = $this->assetFactory->getThirdPartyService($platform);
        $operations = $service->getOperationsFromFile($file);

        $operationCollection = new OperationCollection();
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $asset = $operation->getAsset();
            $assetEntity = null;
            if ($asset !== null) {
                $assetEntity = $this->assetsStorage->findByExternalIdAndPlatform(
                    $asset->getExternalId(),
                    $asset->getPlatform()
                );

                if ($assetEntity === null) {
                    $item = new ItemEntity();
                    $item->setName($asset->getName());
                    $item->setType($asset->getType());
                    $item->setSource($asset->getPlatform());
                    $item->setExternalId($asset->getExternalId());

                    $assetEntity = $this->convertAssetModelToEntity($asset);

                    $item->setAsset($assetEntity);
                    $assetEntity->setItem($item);

                    $this->assetsStorage->addEntity($assetEntity);
                    $assetEntity->setItemId($item->getId());
                } elseif ($asset->getInterestPercent() !== null) {
                    $needUpdate = false;
                    if ($assetEntity->getName() !== $asset->getName()) {
                        $assetEntity->getItem()->setName($asset->getName());
                        $assetEntity->setName($asset->getName());
                        $needUpdate = true;
                    }

                    if ($asset->getPayoutType() !== $assetEntity->getPayoutType()?->getExternalId()) {
                        $assetEntity->setPayoutTypeId(
                            $this->payoutTypeStorage->findByExternalId($asset->getPayoutType())->getId()
                        );
                        $needUpdate = true;
                    }

                    if ($asset->getDealDate() !== $assetEntity->getDealDate()) {
                        $assetEntity->setDealDate($asset->getDealDate());
                        $needUpdate = true;
                    }

                    if ($asset->getDuration() !== $assetEntity->getDuration()) {
                        $assetEntity->setDuration($asset->getDuration());
                        $needUpdate = true;
                    }

                    if ($asset->getDurationPeriod() !== $assetEntity->getDurationPeriod()) {
                        $assetEntity->setDurationPeriod($asset->getDurationPeriod());
                        $needUpdate = true;
                    }

                    if ($asset->getInterestPercent() !== $assetEntity->getInterestPercent()) {
                        $assetEntity->setInterestPercent($asset->getInterestPercent());
                        $needUpdate = true;
                    }

                    if ($asset->getInterestAmount() !== $assetEntity->getInterestAmount()) {
                        $assetEntity->setInterestAmount($asset->getInterestAmount());
                        $needUpdate = true;
                    }

                    if ($asset->getPayoutFrequency() !== $assetEntity->getPayoutFrequencyPeriod()) {
                        $assetEntity->setPayoutFrequency($asset->getPayoutFrequency());
                        $needUpdate = true;
                    }

                    if ($asset->getPayoutFrequencyPeriod() !== $assetEntity->getPayoutFrequencyPeriod()) {
                        $assetEntity->setPayoutFrequencyPeriod($asset->getPayoutFrequencyPeriod());
                        $needUpdate = true;
                    }

                    if ($asset->getNominalAmount() !== $assetEntity->getNominalAmount()) {
                        $assetEntity->setNominalAmount($asset->getNominalAmount());
                        $needUpdate = true;
                    }

                    if ($asset->getCurrencyIso() !== $assetEntity->getCurrency()?->getIso()) {
                        $currency = $this->currencyService->getCurrencyByIso($asset->getCurrencyIso());
                        $assetEntity->setCurrency($currency);
                        $assetEntity->setCurrencyId($currency->getId());
                        $needUpdate = true;
                    }

                    if ($needUpdate) {
                        $this->assetsStorage->addEntity($assetEntity);
                    }
                }

                if ($this->shouldCalculatePaymentSchedule($assetEntity)) {
                    $schedule = $this->calculatePaymentSchedule($assetEntity);

                    /** @var PayoutModel $payment */
                    foreach ($schedule->getIterator() as $payment) {
                        $paymentEntity = $this->convertPaymentModelToEntity($assetEntity, $payment);
                        $this->itemPaymentScheduleStorage->addEntity($paymentEntity);
                    }
                }
            }

            $operationModel = new ItemOperationModel([
                'externalId' => $operation->getExternalId(),
                'userId' => $userId,
                'brokerId' => $operation->getBrokerId(),
                'itemId' => $assetEntity?->getItem()->getId(),
                'item' => $assetEntity ? $this->convertAssetEntityToItemModel($assetEntity) : null,
                'operationType' => $operation->getOperationType(),
                'createdDate' => $this->currentDateTime,
                'quantity' => $operation->getQuantity(),
                'date' => $operation->getDate(),
                'amount' => $operation->getAmount(),
                'currencyIso' => $operation->getCurrencyIso(),
                'status' => $operation->getStatus()
            ]);
            $operationCollection->add($operationModel);
        }

        return $this->operationService->addOperations($operationCollection);
    }

    private function convertAssetModelToEntity(AssetModel $assetModel): AssetsEntity
    {
        $payoutType = $this->payoutTypeStorage->findByExternalId($assetModel->getPayoutType());
        $currency = $this->currencyService->getCurrencyByIso($assetModel->getCurrencyIso());

        $asset = new AssetsEntity();
        if (null !== $assetModel->getAssetId()) {
            $asset->setId($assetModel->getAssetId());
        }

        $asset->setExternalId($assetModel->getExternalId());
        $asset->setPlatform($assetModel->getPlatform());
        $asset->setName($assetModel->getName());
        $asset->setType($assetModel->getType());
        $asset->setPayoutTypeId($payoutType->getId());
        $asset->setPayoutType($payoutType);
        $asset->setDealDate($assetModel->getDealDate());
        $asset->setDuration($assetModel->getDuration());
        $asset->setDurationPeriod($assetModel->getDurationPeriod());
        $asset->setInterestPercent($assetModel->getInterestPercent());
        $asset->setInterestAmount($assetModel->getInterestAmount());
        $asset->setPayoutFrequency($assetModel->getPayoutFrequency());
        $asset->setPayoutFrequencyPeriod($assetModel->getPayoutFrequencyPeriod());
        $asset->setCurrencyId($currency->getId());
        $asset->setCurrency($currency);

        return $asset;
    }

    private function convertAssetEntityToItemModel(AssetsEntity $asset): ItemModel
    {
        return new ItemModel([
            'itemId' => $asset->getId(),
            'type' => $asset->getType(),
            'source' => $asset->getPlatform(),
            'externalId' => $asset->getExternalId(),
            'name' => $asset->getName()
        ]);
    }

    private function convertPaymentModelToEntity(AssetsEntity $asset, PayoutModel $payment): ItemPaymentScheduleEntity
    {
        $paymentEntity = new ItemPaymentScheduleEntity();
        $paymentEntity->setIsActive(1);
        $paymentEntity->setItemId($asset->getId());
        $paymentEntity->setItem($asset->getItem());
        $paymentEntity->setInterestAmount($payment->getInterest());
        $paymentEntity->setDebtAmount($payment->getDebtPayout());
        $paymentEntity->setCurrencyId($asset->getCurrencyId());
        $paymentEntity->setCurrency($asset->getCurrency());
        $paymentEntity->setDate($payment->getDate());

        return $paymentEntity;
    }

    private function shouldCalculatePaymentSchedule(AssetsEntity $asset): bool
    {
        return
            $asset->getInterestPercent() !== null
            && $asset->getPayoutTypeId() !== null
            && in_array(
                $asset->getPayoutType()->getExternalId(),
                [
                    'annuitant',
                    'differentiatedConstantBase'
                ]
            )
            && $asset->getPayoutFrequency() !== null
            && $asset->getPayoutFrequencyPeriod() !== null
            && $asset->getDuration() !== null
            && $asset->getDurationPeriod() !== null
            && $asset->getDealDate() !== null
            && $asset->getNominalAmount() !== null
            && $asset->getCurrency() !== null
            && !count($this->itemPaymentScheduleStorage->findActiveByItemId($asset->getItemId()));
    }

    private function calculatePaymentSchedule(AssetsEntity $asset): PayoutCollection
    {
        $calculator = $this->assetFactory->getCalculator(
            $asset->getPayoutType()->getExternalId()
        );

        $scheduleModel = new PaymentScheduleCalculatorModel([
            'date' => $asset->getDealDate(),
            'interestPercent' => $asset->getInterestPercent(),
            'interestAmount' => $asset->getInterestAmount(),
            'payoutFrequency' => $asset->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $asset->getPayoutFrequencyPeriod(),
            'duration' => $asset->getDuration(),
            'durationPeriod' => $asset->getDurationPeriod(),
            'nominalAmount' => $asset->getNominalAmount(),
            'currencyId' => $asset->getCurrencyId()
        ]);
        $calculator->setScheduleModel($scheduleModel);
        return $calculator->calculatePaymentSchedule();
    }
}
