<?php

declare(strict_types=1);

namespace App\Assets\Services\ThirdPartyIntegration;

use App\Assets\Collections\AssetCollection;
use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Assets\Interfaces\ThirdPartyIntegrationInterface;
use App\Assets\Models\AssetModel;
use App\Assets\Collections\OperationCollection;
use App\Assets\Models\OperationModel;
use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

class Potok implements ThirdPartyIntegrationInterface
{
    private const PAYOUT_TYPE = 'differentiatedConstantBase';
    private const ASSET_SHEET_NAME = 'Список займов (Портфель)';
    private const OPERATION_SHEET_NAME = 'История операций';
    private const PLATFORM = 'potok';
    private const ASSET_TYPE = 'crowdfunding';

    private AssetCollection $assets;
    private OperationCollection $operations;

    public function __construct()
    {
        $this->assets = new AssetCollection();
        $this->operations = new OperationCollection();
    }

    private function getAssetsFromSpreadSheet(Spreadsheet $spreadSheet): AssetCollection
    {
        $assetCollection = new AssetCollection();

        $sheet = $spreadSheet->getSheetByName(self::ASSET_SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }
        foreach ($sheet->getRowIterator(3) as $row) {
            $assetModel = new AssetModel([
                'platform' => self::PLATFORM,
                'type' => 'crowdfunding',
                'durationPeriod' => 'day',
                'payoutFrequencyPeriod' => 'day',
                'payoutType' => self::PAYOUT_TYPE,
                'interestAmount' => 0,
                'nominalAmount' => 1,
                'currencyIso' => 'RUB'
            ]);

            $name = '';
            foreach ($row->getCellIterator('A') as $cell) {
                switch ($cell->getColumn()) {
                    case 'B':
                        $name = $cell->getValue();
                        break;
                    case 'D':
                        $assetModel->setExternalId($cell->getValue());
                        break;
                    case 'G':
                        $assetModel->setInterestPercent($cell->getValue() / 100);
                        break;
                    case 'H':
                        $assetModel->setPayoutFrequency($cell->getValue());
                        break;
                    case 'I':
                        $assetModel->setDuration($cell->getValue());
                        break;
                    case 'J':
                        $assetModel->setDealDate(new \DateTime($cell->getFormattedValue()));
                        break;
                }
            }

            $assetModel->setName($assetModel->getExternalId() . ' - ' . $name);

            $assetCollection->add($assetModel);
        }
        return $assetCollection;
    }

    /**
     * @param UploadedFile $file
     * @return OperationCollection
     * @throws ThirdPartyUploadException
     */
    public function getOperationsFromFile(UploadedFile $file): OperationCollection
    {
        $spreadSheet = IOFactory::load($file->getFilePath());
        $this->assets = $this->getAssetsFromSpreadSheet($spreadSheet);
        $this->operations = $this->getOperationsFromSpreadSheet($spreadSheet);
        $this->mergeOperationsAndAssets();

        return $this->operations;
    }

    /**
     * @param Spreadsheet $spreadSheet
     * @return OperationCollection
     * @throws ThirdPartyUploadException
     */
    private function getOperationsFromSpreadSheet(Spreadsheet $spreadSheet): OperationCollection
    {
        $operations = new OperationCollection();
        $sheet = $spreadSheet->getSheetByName(self::OPERATION_SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3001);
        }

        $isEndOperations = false;
        foreach ($sheet->getRowIterator(7) as $row) {
            $cell = $row->getCellIterator('A')->current();
            if ($cell->getValue() === null) {
                $isEndOperations = true;
            }

            if (!$isEndOperations) {
                $operation = new OperationModel([
                    'brokerId' => self::PLATFORM,
                    'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
                    'currencyIso' => 'RUB'
                ]);
                foreach ($row->getCellIterator('A') as $cell) {
                    switch ($cell->getColumn()) {
                        case 'A':
                            $operation->setDate(new \DateTime($cell->getFormattedValue()));
                            break;
                        case 'D':
                            $operation->setOperationType($this->convertOperationType($cell->getValue()));
                            break;
                        case 'E':
                            $operation->setAmount((float)$cell->getValue());
                            break;
                        case 'F':
                            if ($cell->getValue() !== 0) {
                                $operation->setAmount(-1 * (float)$cell->getValue());
                            }
                            break;
                        case 'H':
                            $operation->setComment((string)$cell->getValue());
                    }
                }
                $operations->add($operation);
            }
        }
        return $operations;
    }

    private function convertOperationType(string $type): string
    {
        return match ($type) {
            'Пополнение л/с' => BrokerOperationTypeType::OPERATION_TYPE_INPUT,
            'Выдача займа' => BrokerOperationTypeType::OPERATION_TYPE_BUY,
            'Получение дохода (проценты, пени)' => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
            'Возврат основного долга' => BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
            default => BrokerOperationTypeType::OPERATION_TYPE_UNSPECIFIED
        };
    }

    private function mergeOperationsAndAssets()
    {
        /** @var OperationModel $operation */
        foreach ($this->operations->getIterator() as $operation) {
            if ($operation->getOperationType() === BrokerOperationTypeType::OPERATION_TYPE_INPUT) {
                $asset = null;

                $operation->setExternalId(
                    md5(
                        $operation->getDate()->getTimestamp() .
                        $operation->getAmount() .
                        $operation->getOperationType()
                    )
                );
            } else {
                $asset = $this->assets->findByExternalId($operation->getComment());

                if ($asset === null) {
                    $asset = new AssetModel([
                        'externalId' => $operation->getComment(),
                        'platform' => self::PLATFORM,
                        'name' => $operation->getComment(),
                        'type' => self::ASSET_TYPE,
                        'payoutType' => self::PAYOUT_TYPE,
                        'currencyIso' => 'RUB'
                    ]);
                }

                $operation->setExternalId(
                    md5(
                        $operation->getDate()->getTimestamp() .
                        $operation->getAmount() .
                        $operation->getOperationType() .
                        $operation->getComment()
                    )
                );

                if ($operation->getOperationType() === BrokerOperationTypeType::OPERATION_TYPE_BUY) {
                    $operation->setQuantity(-1 * $operation->getAmount());
                }
            }

            $operation->setAsset($asset);
        }
    }
}
