<?php

declare(strict_types=1);

namespace App\Assets\Services\ThirdPartyIntegration;

use App\Assets\Collections\AssetCollection;
use App\Assets\Collections\OperationCollection;
use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Assets\Interfaces\ThirdPartyIntegrationInterface;
use App\Assets\Models\AssetModel;
use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Assets\Models\OperationModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

class MoneyFriends implements ThirdPartyIntegrationInterface
{
    private const PAYOUT_TYPE = 'annuitant';
    private const SHEET_NAME = 'Отчет по сделкам';
    private const PLATFORM = 'money_friends';
    private const ASSET_TYPE = 'crowdfunding';

    private AssetCollection $assets;
    private OperationCollection $operations;

    public function __construct()
    {
        $this->assets = new AssetCollection();
        $this->operations = new OperationCollection();
    }

    public function getOperationsFromFile(UploadedFile $file): OperationCollection
    {
        $spreadSheet = IOFactory::load($file->getFilePath());

        $this->assets = $this->getAssetsFromPhpSpreadSheet($spreadSheet);
        $this->operations = $this->getOperationsFromSpreadSheet($spreadSheet);
        $this->mergeOperationsAndAssets();

        return $this->operations;
    }

    private function getAssetsFromPhpSpreadSheet(Spreadsheet $spreadSheet): assetCollection
    {
        $sheet = $spreadSheet->getSheetByName(self::SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $assetCollection = new AssetCollection();

        $isStartAssets = false;
        $isEndAssets = false;
        foreach ($sheet->getRowIterator() as $row) {
            if (!$isStartAssets) {
                foreach ($row->getCellIterator('C', 'C') as $cell) {
                    if ($cell->getValue() === "Сводные данные по активным займам\n") {
                        $isStartAssets = true;
                    }
                }
            }

            if ($isStartAssets && !$isEndAssets) {
                foreach ($row->getCellIterator('B', 'B') as $headerCell) {
                    if (is_int($headerCell->getValue())) {
                        $assetModel = new AssetModel([
                            'platform' => self::PLATFORM,
                            'type' => self::ASSET_TYPE,
                            'durationPeriod' => 'day',
                            'payoutFrequencyPeriod' => 'month',
                            'payoutFrequency' => 1,
                            'payoutType' => self::PAYOUT_TYPE,
                            'interestAmount' => 0,
                            'nominalAmount' => 1,
                            'currencyIso' => 'RUB'
                        ]);

                        $name = '';
                        foreach ($row->getCellIterator('E') as $cell) {
                            switch ($cell->getColumn()) {
                                case 'G':
                                    $assetModel->setExternalId((string)$cell->getValue());
                                    break;
                                case 'O':
                                    $assetModel->setDealDate(new \DateTime($cell->getFormattedValue()));
                                    break;
                                case 'V':
                                    $assetModel->setDuration($cell->getValue());
                                    break;
                                case 'AG':
                                    $name = $cell->getValue();
                                    break;
                            }
                        }
                        $assetModel->setName($assetModel->getExternalId() . ' - ' . $name);

                        $assetCollection->add($assetModel);
                    }
                }
            }

            if (!$isEndAssets) {
                foreach ($row->getCellIterator('B', 'B') as $cell) {
                    if ($cell->getValue() === "Комиссия платформе\n") {
                        $isEndAssets = true;
                    }
                }
            }
        }

        return $assetCollection;
    }

    public function getOperationsFromSpreadSheet(Spreadsheet $spreadSheet): OperationCollection
    {
        $sheet = $spreadSheet->getSheetByName(self::SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $operations = new OperationCollection();
        $isStartOperations = false;

        foreach ($sheet->getRowIterator() as $row) {
            if (!$isStartOperations) {
                foreach ($row->getCellIterator('C', 'C') as $cell) {
                    if ($cell->getValue() === "Сводные данные по движению денежных средств по счету инвестора\n") {
                        $isStartOperations = true;
                    }
                }
            }

            if ($isStartOperations) {
                foreach ($row->getCellIterator('D', 'D') as $headerCell) {
                    if (is_int($headerCell->getValue())) {
                        $operation = new OperationModel([
                            'brokerId' => self::PLATFORM,
                            'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
                            'currencyIso' => 'RUB'
                        ]);

                        foreach ($row->getCellIterator('J') as $cell) {
                            switch ($cell->getColumn()) {
                                case 'J':
                                    $operation->setDate(new \DateTime($cell->getValue()));
                                    break;
                                case 'Q':
                                    $operation->setAmount((float)$cell->getValue());
                                    break;
                                case 'U':
                                    $operation->setComment($cell->getValue());
                                    break;
                                case 'AT':
                                    $operation->setOperationType($this->convertOperationType($cell->getValue()));
                                    break;
                            }
                        }

                        $operations->add($operation);
                    }
                }
            }
        }
        return $operations;
    }

    private function convertOperationType(string $type): string
    {
        return match ($type) {
            'Пополнение средств' => BrokerOperationTypeType::OPERATION_TYPE_INPUT,
            'Перевод в резерв' => BrokerOperationTypeType::OPERATION_TYPE_HOLD,
            'Выдача займа' => BrokerOperationTypeType::OPERATION_TYPE_BUY,
            'Возврат долга' => BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
            'Возврат процентов по займу', 'Возврат пени' => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
            'Возврат средств' => BrokerOperationTypeType::OPERATION_TYPE_UN_HOLD,
            'Списание по цессии' => BrokerOperationTypeType::OPERATION_TYPE_DEFAULT,
            default => BrokerOperationTypeType::OPERATION_TYPE_UNSPECIFIED,
        };
    }

    private function mergeOperationsAndAssets()
    {
        /** @var OperationModel $operation */
        foreach ($this->operations->getIterator() as $operation) {
            if (
                in_array(
                    $operation->getOperationType(),
                    [
                        BrokerOperationTypeType::OPERATION_TYPE_BUY,
                        BrokerOperationTypeType::OPERATION_TYPE_HOLD,
                        BrokerOperationTypeType::OPERATION_TYPE_UN_HOLD
                    ]
                )
            ) {
                preg_match_all('/№([0-9]+)/', $operation->getComment(), $matches);
                $externalId = $matches[1][0];

                $asset = $this->assets->findByExternalId($externalId);
                if ($asset === null) {
                    $asset = new AssetModel(array_merge(
                        [
                            'name' => $externalId,
                            'externalId' => $externalId,
                        ],
                        $this->getEmptyAssetData()
                    ));
                }
                if (
                    in_array(
                        $operation->getOperationType(),
                        [
                            BrokerOperationTypeType::OPERATION_TYPE_BUY,
                            BrokerOperationTypeType::OPERATION_TYPE_HOLD
                        ]
                    )
                ) {
                    $operation->setAmount(-1 * $operation->getAmount());
                }

                if (
                    in_array(
                        $operation->getOperationType(),
                        [
                            BrokerOperationTypeType::OPERATION_TYPE_BUY,
                            BrokerOperationTypeType::OPERATION_TYPE_DEFAULT
                        ]
                    )
                ) {
                    $operation->setQuantity(-1 * $operation->getAmount());
                }

                $operation->setAsset($asset);
                $operation->setExternalId(md5(
                    $externalId .
                    $operation->getDate()->getTimestamp() .
                    $operation->getAmount() .
                    $operation->getOperationType()
                ));
            }
            if ($operation->getOperationType() === BrokerOperationTypeType::OPERATION_TYPE_INPUT) {
                $operation->setExternalId(md5($operation->getDate()->getTimestamp() . $operation->getAmount()));
            }
            if (
                in_array(
                    $operation->getOperationType(),
                    [
                        BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
                        BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
                        BrokerOperationTypeType::OPERATION_TYPE_DEFAULT
                    ]
                )
            ) {
                preg_match_all('/№([0-9]+)\//', $operation->getComment(), $matches);
                $externalId = $matches[1][0];

                $isFoundInterestPercent = preg_match_all(
                    '/Процентная ставка ([0-9]+)%/',
                    $operation->getComment(),
                    $matches
                );
                if ($isFoundInterestPercent) {
                    $interestPercent = $matches[1][0] / 100;
                }

                $asset = $this->assets->findByExternalId($externalId);
                if ($asset === null) {
                    $asset = new AssetModel(array_merge(
                        [
                            'name' => $externalId,
                            'externalId' => $externalId,
                        ],
                        $this->getEmptyAssetData()
                    ));
                }

                if ($isFoundInterestPercent) {
                    $asset->setInterestPercent($interestPercent);
                }

                $operation->setAsset($asset);
                $operation->setExternalId(md5(
                    $externalId .
                    $operation->getDate()->getTimestamp() .
                    $operation->getAmount() .
                    $operation->getOperationType()
                ));
            }
        }
    }

    private function getEmptyAssetData(): array
    {
        return [
            'platform' => self::PLATFORM,
            'type' => self::ASSET_TYPE,
            'payoutType' => self::PAYOUT_TYPE,
            'currencyIso' => 'RUB'
        ];
    }
}
