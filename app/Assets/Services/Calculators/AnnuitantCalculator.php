<?php

declare(strict_types=1);

namespace App\Assets\Services\Calculators;

use App\Assets\Collections\PayoutCollection;
use App\Assets\Interfaces\AssetCalculatorInterface;
use App\Assets\Models\PaymentScheduleCalculatorModel;
use App\Assets\Models\PayoutModel;

class AnnuitantCalculator implements AssetCalculatorInterface
{
    private PaymentScheduleCalculatorModel $paymentScheduleModel;

    public function setScheduleModel(PaymentScheduleCalculatorModel $paymentScheduleModel): void
    {
        $this->paymentScheduleModel = $paymentScheduleModel;
    }

    public function calculatePaymentSchedule(): PayoutCollection
    {
        $schedule = new PayoutCollection();

        $endDate = clone $this->paymentScheduleModel->getDate();
        $interval = $this->buildDateInterval(
            $this->paymentScheduleModel->getDuration(),
            $this->paymentScheduleModel->getDurationPeriod()
        );
        $endDate->add($interval);

        $numberPeriods = $this->calculateNumberPeriods();

        $startOfPeriod = clone $this->paymentScheduleModel->getDate();
        $payoutDate = clone $this->paymentScheduleModel->getDate();
        $payoutFrequency = $this->buildDateInterval(
            $this->paymentScheduleModel->getPayoutFrequency(),
            $this->paymentScheduleModel->getPayoutFrequencyPeriod()
        );
        $payoutDate->add($payoutFrequency);
        $debt = $this->paymentScheduleModel->getNominalAmount();

        $schedule->add(new PayoutModel([
            'date' => clone $startOfPeriod,
            'interest' => 0,
            'debtPayout' => 0
        ]));
        $periodPercent = $this->calculatePeriodPercent();

        for ($i = 1; $i <= $numberPeriods; $i++) {
            $interest = $debt * $periodPercent;

            $debtPayout = $periodPercent * pow(1 + $periodPercent, $numberPeriods)
                / (pow(1 + $periodPercent, $numberPeriods) - 1)
                * $this->paymentScheduleModel->getNominalAmount() - $interest;

            $debt -= $debtPayout;

            $schedule->add(new PayoutModel([
                'date' => clone $payoutDate,
                'interest' => $interest,
                'debtPayout' => $debtPayout
            ]));

            $payoutDate = min($endDate, $payoutDate->add($payoutFrequency));
        }

        return $schedule;
    }

    private function buildDateInterval(int $duration, string $durationPeriod): \DateInterval
    {
        $intervalPeriod = match ($durationPeriod) {
            'day' => 'D',
            'month' => 'M',
            'year' => 'Y'
        };
        return  new \DateInterval('P' . $duration . $intervalPeriod);
    }

    private function calculateNumberPeriods(): int
    {
        $numberPeriods = 0;
        $isLastPayout = false;

        $endDate = clone $this->paymentScheduleModel->getDate();
        $interval = $this->buildDateInterval(
            $this->paymentScheduleModel->getDuration(),
            $this->paymentScheduleModel->getDurationPeriod()
        );
        $endDate->add($interval);
        $payoutFrequency = $this->buildDateInterval(
            $this->paymentScheduleModel->getPayoutFrequency(),
            $this->paymentScheduleModel->getPayoutFrequencyPeriod()
        );
        $payoutDate = clone $this->paymentScheduleModel->getDate();
        $payoutDate = min($endDate, $payoutDate->add($payoutFrequency));
        while ($isLastPayout === false) {
            if ($payoutDate === $endDate) {
                $isLastPayout = true;
            }
            $payoutDate = min($endDate, $payoutDate->add($payoutFrequency));
            $numberPeriods++;
        }

        return $numberPeriods;
    }

    private function calculatePeriodPercent()
    {
        return $this->paymentScheduleModel->getPayoutFrequency() *
        match ($this->paymentScheduleModel->getPayoutFrequencyPeriod()) {
            'day' => $this->paymentScheduleModel->getInterestPercent() / (date('L') ? 366 : 365),
            'year' => $this->paymentScheduleModel->getInterestPercent(),
            default => $this->paymentScheduleModel->getInterestPercent() / 12 // 'month'
        };
    }
}
