<?php

declare(strict_types=1);

namespace App\Assets\Services;

use App\Assets\Interfaces\AssetCalculatorInterface;
use App\Assets\Interfaces\ThirdPartyIntegrationInterface;
use App\Assets\Services\Calculators\AnnuitantCalculator;
use App\Assets\Services\Calculators\DifferentiatedConstantBaseScheduleCalculator;
use App\Assets\Services\Calculators\DifferentiatedScheduleCalculator;
use App\Assets\Services\ThirdPartyIntegration\MoneyFriends;
use App\Assets\Services\ThirdPartyIntegration\Potok;

class AssetFactory
{
    public function getCalculator(string $type): AssetCalculatorInterface
    {
        return match ($type) {
            'differentiated' => new DifferentiatedScheduleCalculator(),
            'differentiatedConstantBase' => new DifferentiatedConstantBaseScheduleCalculator(),
            'annuitant' => new AnnuitantCalculator()
        };
    }

    public function getThirdPartyService($platform): ThirdPartyIntegrationInterface
    {
        return match ($platform) {
            'potok' => new Potok(),
            'money_friends' => new MoneyFriends()
        };
    }
}
