<?php

declare(strict_types=1);

use App\Assets\Services\AssetFactory;
use App\Assets\Services\AssetService;
use App\Assets\Storages\AssetsStorage;
use App\Assets\Storages\PayoutTypeStorage;
use App\Intl\Services\CurrencyService;
use App\Item\Services\OperationService;
use App\Item\Storages\ItemPaymentScheduleStorage;
use App\Item\Storages\ItemStorage;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(AssetsStorage::class, function (ContainerInterface $c) {
    return new AssetsStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(AssetService::class, function (ContainerInterface $c) {
    return new AssetService(
        $c->get('Now' . DateTime::class),
        $c->get(AssetFactory::class),
        $c->get(AssetsStorage::class),
        $c->get(PayoutTypeStorage::class),
        $c->get(CurrencyService::class),
        $c->get(OperationService::class),
        $c->get(ItemPaymentScheduleStorage::class)
    );
});

/** @var ContainerInterface $container */
$container->set(PayoutTypeStorage::class, function (ContainerInterface $c) {
    return new PayoutTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(AssetFactory::class, function (ContainerInterface $c) {
    return new AssetFactory();
});
