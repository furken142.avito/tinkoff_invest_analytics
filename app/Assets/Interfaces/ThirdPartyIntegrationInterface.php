<?php

declare(strict_types=1);

namespace App\Assets\Interfaces;

use App\Assets\Collections\AssetCollection;
use App\Assets\Collections\OperationCollection;
use Slim\Psr7\UploadedFile;

interface ThirdPartyIntegrationInterface
{
    public function getOperationsFromFile(UploadedFile $file): OperationCollection;
}
