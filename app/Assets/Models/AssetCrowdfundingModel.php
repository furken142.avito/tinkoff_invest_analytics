<?php

declare(strict_types=1);

namespace App\Assets\Models;

use App\Common\Models\BaseModel;

class AssetCrowdfundingModel extends BaseModel
{
    private int $assetCrowdfundingId;
    private int $assetId;
    private int $payoutTypeId;
    private \DateTime $dealDate;
    private int $duration;
    private string $durationPeriod;
    private float $amount;
    private float $interest;
    private int $payoutFrequency;
    private string $payoutFrequencyPeriod;
    private float $taxes;

    /**
     * @return int
     */
    public function getAssetCrowdfundingId(): int
    {
        return $this->assetCrowdfundingId;
    }

    /**
     * @param int $assetCrowdfundingId
     */
    public function setAssetCrowdfundingId(int $assetCrowdfundingId): void
    {
        $this->assetCrowdfundingId = $assetCrowdfundingId;
    }

    /**
     * @return int
     */
    public function getAssetId(): int
    {
        return $this->assetId;
    }

    /**
     * @param int $assetId
     */
    public function setAssetId(int $assetId): void
    {
        $this->assetId = $assetId;
    }

    /**
     * @return int
     */
    public function getPayoutTypeId(): int
    {
        return $this->payoutTypeId;
    }

    /**
     * @param int $payoutTypeId
     */
    public function setPayoutTypeId(int $payoutTypeId): void
    {
        $this->payoutTypeId = $payoutTypeId;
    }

    /**
     * @return \DateTime
     */
    public function getDealDate(): \DateTime
    {
        return $this->dealDate;
    }

    /**
     * @param \DateTime $dealDate
     */
    public function setDealDate(\DateTime $dealDate): void
    {
        $this->dealDate = $dealDate;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getDurationPeriod(): string
    {
        return $this->durationPeriod;
    }

    /**
     * @param string $durationPeriod
     */
    public function setDurationPeriod(string $durationPeriod): void
    {
        $this->durationPeriod = $durationPeriod;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getInterest(): float
    {
        return $this->interest;
    }

    /**
     * @param float $interest
     */
    public function setInterest(float $interest): void
    {
        $this->interest = $interest;
    }

    /**
     * @return int
     */
    public function getPayoutFrequency(): int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int $payoutFrequency
     */
    public function setPayoutFrequency(int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string
     */
    public function getPayoutFrequencyPeriod(): string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return float
     */
    public function getTaxes(): float
    {
        return $this->taxes;
    }

    /**
     * @param float $taxes
     */
    public function setTaxes(float $taxes): void
    {
        $this->taxes = $taxes;
    }
}
