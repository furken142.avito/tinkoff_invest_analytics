<?php

declare(strict_types=1);

namespace App\Assets\Collections;

use App\Assets\Models\PayoutModel;
use Doctrine\Common\Collections\ArrayCollection;

class PayoutCollection extends ArrayCollection
{
    public function getLastPayout(\DateTime $date): ?PayoutModel
    {
        $lastPayout = null;

        /** @var PayoutModel $payout */
        foreach ($this->getIterator() as $payout) {
            if ($payout->getDate() <= $date) {
                $lastPayout = $payout;
            }
        }

        return $lastPayout;
    }

    public function getInterest(\DateTime $startDate, \DateTime $endDate): float
    {
        $income = 0;

        /** @var PayoutModel $payout */
        foreach ($this->getIterator() as $payout) {
            if ($payout->getDate() >= $startDate && $payout->getDate() < $endDate) {
                $income += $payout->getInterest();
            }
        }

        return $income;
    }
}
