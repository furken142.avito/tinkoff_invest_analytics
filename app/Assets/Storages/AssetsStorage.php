<?php

declare(strict_types=1);

namespace App\Assets\Storages;

use App\Assets\Entities\AssetsEntity;
use Doctrine\ORM\EntityManagerInterface;

class AssetsStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function findByExternalIdAndPlatform(string $externalId, string $platform): ?AssetsEntity
    {
        return $this->entityManager->getRepository(AssetsEntity::class)
            ->findOneBy(['platform' => $platform, 'externalId' => $externalId]);
    }

    public function addEntity(AssetsEntity $assetsEntity): int
    {
        $this->entityManager->persist($assetsEntity);
        $this->entityManager->flush();

        return $assetsEntity->getId();
    }
}
