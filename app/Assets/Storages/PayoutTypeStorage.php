<?php

declare(strict_types=1);

namespace App\Assets\Storages;

use App\Assets\Entities\PayoutTypeEntity;
use Doctrine\ORM\EntityManagerInterface;

class PayoutTypeStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $externalId
     * @return PayoutTypeEntity|null
     */
    public function findByExternalId(string $externalId): ?PayoutTypeEntity
    {
        return $this->entityManager->getRepository(PayoutTypeEntity::class)
            ->findOneBy(['externalId' => $externalId]);
    }

    public function findById(int $payoutTypeId): ?PayoutTypeEntity
    {
        return $this->entityManager->getRepository(PayoutTypeEntity::class)
            ->findOneBy(['id' => $payoutTypeId]);
    }
}
