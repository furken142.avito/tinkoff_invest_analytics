<?php

declare(strict_types=1);

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.market_country")
 */
class MarketCountryEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="market_country_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="iso", type="string")
     * @var string
     */
    protected $iso;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }
}
