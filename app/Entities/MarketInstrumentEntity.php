<?php

declare(strict_types=1);

namespace App\Entities;

use App\Item\Entities\ItemEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.market_instrument")
 */
class MarketInstrumentEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="market_instrument_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @ORM\Column(name="item_id", type="integer")
     */
    protected int $itemId;

    /**
     * @ORM\OneToOne(targetEntity="App\Item\Entities\ItemEntity", inversedBy="marketInstrument")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
     */
    private ItemEntity $item;

    /**
     * @ORM\OneToOne(targetEntity="MarketStockEntity", mappedBy="marketInstrument", cascade={"persist", "remove"})
     */
    protected ?MarketStockEntity $marketStock = null;

    /**
     * @ORM\OneToOne(targetEntity="MarketCurrencyEntity", mappedBy="marketInstrument", cascade={"persist", "remove"})
     */
    protected ?MarketCurrencyEntity $marketCurrency = null;

    /**
     * @ORM\Column(name="figi", type="string")
     */
    protected string $figi;

    /**
     * @ORM\Column(name="ticker", type="string")
     */
    protected string $ticker;

    /**
     * @ORM\Column(name="isin", type="string")
     */
    protected string $isin;

    /**
     * @ORM\Column(name="min_price_increment", type="float")
     */
    protected ?float $minPriceIncrement = null;

    /**
     * @ORM\Column(name="lot", type="integer")
     */
    protected int $lot;

    /**
     * @ORM\Column(name="currency", type="string")
     */
    protected string $currency;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected string $name;

    /**
     * @ORM\Column(name="type", type="string")
     */
    protected string $type;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFigi(): string
    {
        return $this->figi;
    }

    /**
     * @param string $figi
     */
    public function setFigi(string $figi): void
    {
        $this->figi = $figi;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     */
    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    /**
     * @return string|null
     */
    public function getIsin(): string|null
    {
        return $this->isin;
    }

    /**
     * @param string|null $isin
     */
    public function setIsin(?string $isin): void
    {
        $this->isin = $isin;
    }

    /**
     * @return float|null
     */
    public function getMinPriceIncrement(): ?float
    {
        return $this->minPriceIncrement;
    }

    /**
     * @param float|null $minPriceIncrement
     */
    public function setMinPriceIncrement(float $minPriceIncrement = null): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    /**
     * @return int
     */
    public function getLot(): int
    {
        return $this->lot;
    }

    /**
     * @param int $lot
     */
    public function setLot(int $lot): void
    {
        $this->lot = $lot;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return MarketStockEntity|null
     */
    public function getMarketStock(): ?MarketStockEntity
    {
        return $this->marketStock;
    }

    /**
     * @param MarketStockEntity|null $marketStock
     */
    public function setMarketStock(?MarketStockEntity $marketStock): void
    {
        $this->marketStock = $marketStock;
    }

    /**
     * @return MarketCurrencyEntity|null
     */
    public function getMarketCurrency(): ?MarketCurrencyEntity
    {
        return $this->marketCurrency;
    }

    /**
     * @param MarketCurrencyEntity|null $marketCurrency
     */
    public function setMarketCurrency(?MarketCurrencyEntity $marketCurrency): void
    {
        $this->marketCurrency = $marketCurrency;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemEntity
     */
    public function getItem(): ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item): void
    {
        $this->item = $item;
    }
}
