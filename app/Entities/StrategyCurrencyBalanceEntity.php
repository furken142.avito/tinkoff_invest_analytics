<?php

declare(strict_types=1);

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.strategy_currency_balance")
 */
class StrategyCurrencyBalanceEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="strategy_currency_balance_id", type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected int $userId;

    /**
     * @ORM\Column(name="market_currency_id", type="integer")
     */
    protected int $marketCurrencyId;

    /**
     * @ORM\ManyToOne (targetEntity="App\Entities\MarketCurrencyEntity")
     * @ORM\JoinColumn (name="market_currency_id", referencedColumnName="market_currency_id")
     * @var MarketCurrencyEntity
     */
    protected MarketCurrencyEntity $marketCurrency;

    /**
     * @ORM\Column(name="is_active", type="integer")
     */
    protected int $isActive = 1;

    /**
     * @ORM\Column(name="share", type="float")
     */
    protected float $share;

    /**
     * @ORM\Column(name="created_date", type="datetimetz")
     */
    protected \DateTime $createdDate;

    /**
     * @ORM\Column(name="updated_date", type="datetimetz")
     */
    protected \DateTime $updatedDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getMarketCurrencyId(): int
    {
        return $this->marketCurrencyId;
    }

    /**
     * @param int $marketCurrencyId
     */
    public function setMarketCurrencyId(int $marketCurrencyId): void
    {
        $this->marketCurrencyId = $marketCurrencyId;
    }

    /**
     * @return MarketCurrencyEntity
     */
    public function getMarketCurrency(): MarketCurrencyEntity
    {
        return $this->marketCurrency;
    }

    /**
     * @param MarketCurrencyEntity $marketCurrency
     */
    public function setMarketCurrency(MarketCurrencyEntity $marketCurrency): void
    {
        $this->marketCurrency = $marketCurrency;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return float
     */
    public function getShare(): float
    {
        return $this->share;
    }

    /**
     * @param float $share
     */
    public function setShare(float $share): void
    {
        $this->share = $share;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }
}
