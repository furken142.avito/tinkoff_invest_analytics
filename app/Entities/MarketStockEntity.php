<?php

declare(strict_types=1);

namespace App\Entities;

use App\Intl\Entities\CountryEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tinkoff_invest.market_stock")
 */
class MarketStockEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="market_stock_id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="market_instrument_id", type="integer")
     * @var int
     */
    protected int $marketInstrumentId;

    /**
     * @ORM\OneToOne(targetEntity="MarketInstrumentEntity", inversedBy="marketStock")
     * @ORM\JoinColumn(name="market_instrument_id", referencedColumnName="market_instrument_id")
     * @var MarketInstrumentEntity
     */
    protected MarketInstrumentEntity $marketInstrument;

    /**
     * @ORM\Column(name="figi", type="string", nullable=false)
     * @var string
     */
    protected string $figi;

    /**
     * @ORM\Column(name="country", type="string")
     * @ORM\JoinColumn (name="country", referencedColumnName="iso")
     * @var string|null
     */
    protected $countryIso;

    /**
     * @ORM\ManyToOne (targetEntity="App\Intl\Entities\CountryEntity")
     * @ORM\JoinColumn (name="country", referencedColumnName="iso")
     * @var CountryEntity|null
     */
    protected $country;

    /**
     * @ORM\Column(name="sector", type="string")
     * @var string|null
     */
    protected $sector;

    /**
     * @ORM\Column(name="industry", type="string")
     * @var string|null
     */
    protected $industry;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int $marketInstrumentId
     */
    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return MarketInstrumentEntity
     */
    public function getMarketInstrument(): MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    /**
     * @param MarketInstrumentEntity $marketInstrument
     */
    public function setMarketInstrument(MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    /**
     * @return string
     */
    public function getFigi(): string
    {
        return $this->figi;
    }

    /**
     * @param string $figi
     */
    public function setFigi(string $figi): void
    {
        $this->figi = $figi;
    }

    /**
     * @return string|null
     */
    public function getCountryIso(): ?string
    {
        return $this->countryIso;
    }

    /**
     * @param string|null $countryIso
     */
    public function setCountryIso(?string $countryIso): void
    {
        $this->countryIso = $countryIso;
    }

    /**
     * @return string|null
     */
    public function getSector(): ?string
    {
        return $this->sector;
    }

    /**
     * @param string|null $sector
     */
    public function setSector(?string $sector): void
    {
        $this->sector = $sector;
    }

    /**
     * @return string|null
     */
    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    /**
     * @param string|null $industry
     */
    public function setIndustry(?string $industry): void
    {
        $this->industry = $industry;
    }

    /**
     * @return CountryEntity|null
     */
    public function getCountry(): ?CountryEntity
    {
        return $this->country;
    }

    /**
     * @param CountryEntity|null $country
     */
    public function setCountry(?CountryEntity $country): void
    {
        $this->country = $country;
    }
}
