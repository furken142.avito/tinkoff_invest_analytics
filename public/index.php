<?php

declare(strict_types=1);

use App\Handlers\AppErrorHandler;
use App\Handlers\ShutdownHandler;
use DI\Container;
use Monolog\Logger;
use Slim\Factory\AppFactory;
use Slim\Factory\ServerRequestCreatorFactory;

require __DIR__ . '/../vendor/autoload.php';

// Create Container using PHP-DI
$container = new Container();

require __DIR__ . '/../app/Common/dependencies.php';

// Configure the application via container
$app = AppFactory::createFromContainer($container);

require_once __DIR__. '/../app/route.php';

$displayErrorDetails = true;

$logger = $container->get(Logger::class);
$callableResolver = $app->getCallableResolver();
$responseFactory = $app->getResponseFactory();

$serverRequestCreator = ServerRequestCreatorFactory::create();
$request = $serverRequestCreator->createServerRequestFromGlobals();

$errorHandler = new AppErrorHandler($callableResolver, $responseFactory, $logger);
$shutdownHandler = new ShutdownHandler($request, $errorHandler, $displayErrorDetails);
register_shutdown_function($shutdownHandler);

$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, true, true, $logger);
$errorMiddleware->setDefaultErrorHandler($errorHandler);

$app->run();
