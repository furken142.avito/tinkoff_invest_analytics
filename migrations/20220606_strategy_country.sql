SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_country
(
    market_country_id INT AUTO_INCREMENT,
    iso VARCHAR(7) NOT NULL,
    PRIMARY KEY (market_country_id),
    UNIQUE KEY unq_market_country_iso(iso)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Dictionary market countries';

INSERT IGNORE INTO tinkoff_invest.market_country (iso)
VALUES ('US'), ('CN'), ('JP'), ('GB'), ('CA'), ('IN'), ('SA'), ('FR'), ('DE'), ('KR'), ('CH'),
       ('AU'), ('IR'), ('NL'), ('ZA'), ('BR'), ('ES'), ('RU'), ('SG'), ('IT');

CREATE TABLE IF NOT EXISTS tinkoff_invest.strategy_country
(
    strategy_country_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    market_country_id INT NOT NULL,
    is_active INT NOT NULL DEFAULT 1,
    share FLOAT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (strategy_country_id),
    CONSTRAINT strategy_country_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT strategy_market_country_market_country_id_fk
        FOREIGN KEY (market_country_id) REFERENCES tinkoff_invest.market_country (market_country_id),
    INDEX inx_strategy_country_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User strategy of countries';