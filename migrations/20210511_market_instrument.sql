SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_instrument
(
    market_instrument_id INT AUTO_INCREMENT,
    figi VARCHAR(255) NOT NULL,
    ticker VARCHAR(255) NOT NULL,
    isin VARCHAR(255) NULL,
    min_price_increment FLOAT,
    lot INT NOT NULL,
    currency VARCHAR(255) NOT NULL,
    name VARCHAR (255) NOT NULL,
    type VARCHAR (255) NOT NULL,
    PRIMARY KEY (market_instrument_id),
    UNIQUE KEY unq_market_instrument_figi(figi)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Справочник инструментов рынка';