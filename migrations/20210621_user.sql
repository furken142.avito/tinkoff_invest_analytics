SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.users
(
    user_id INT AUTO_INCREMENT,
    login VARCHAR(255),
    email VARCHAR(255),
    birthday DATETIME NULL,
    is_new_user TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (user_id),
    INDEX inx_users_login(login),
    UNIQUE KEY unq_users_login(login),
    INDEX inx_users_email(email),
    UNIQUE KEY unq_users_email(email)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Пользователи';

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_auth
(
    user_auth_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    auth_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    auth_type ENUM('YANDEX', 'GOOGLE') NOT NULL,
    access_token VARCHAR(255) NOT NULL,
    client_id VARCHAR(255) NOT NULL,
    PRIMARY KEY (user_auth_id),
    CONSTRAINT user_auth_user_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Авторизации пользователей';
