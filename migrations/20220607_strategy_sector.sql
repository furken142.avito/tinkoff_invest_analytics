SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.strategy_sector
(
    strategy_sector_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    market_sector_id INT NOT NULL,
    is_active INT NOT NULL DEFAULT 1,
    share FLOAT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (strategy_sector_id),
    CONSTRAINT strategy_sector_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT strategy_market_sector_market_sector_id_fk
        FOREIGN KEY (market_sector_id) REFERENCES tinkoff_invest.market_sector (market_sector_id),
    INDEX inx_strategy_sector_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User strategy of sectors';