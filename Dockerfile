FROM atreschilov/php-grpc:v0.1.2

ADD ./php.ini /usr/local/etc/php
ADD . /var/www
ADD ./crons/* /etc/cron.d

# cron start
RUN apt-get update && apt install cron -y
RUN chmod 0644 -R /etc/cron.d
CMD cron && tail -f /var/log/cron.log
COPY entrypoint.sh /etc/entrypoint.sh
RUN chmod +x /etc/entrypoint.sh
ENTRYPOINT ["/etc/entrypoint.sh"]
# cron end

WORKDIR /var/www

CMD ["php-fpm"]